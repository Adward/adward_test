/*
 * EDFA.c
 *
 * Created: 2015/12/8 18:05:10
 *  Author: Administrator
 */ 

#include <asf.h>
#include <string.h>
#include <math.h>
#include "conf_example.h"

/* 2016-05-09 */
// #include "console.h"


/* 2016-05-12, will use #include "console.h" replace the lines below. */
/************************************************************************/
/* Operation Mode Macro                                                 */
/************************************************************************/
//U17
#define activeOE pio_set_pin_high(SW_Latch_Qout);
#define disactOE pio_set_pin_low(SW_Latch_Qout);
//U19
#define activeLE pio_set_pin_high(SW_Latch_Din);		//74373 Qout no changes.
#define disactLE pio_set_pin_low(SW_Latch_Din);			//74373 Qout=Din.
//U5B
#define activeACC pio_set_pin_low(SW_FEEDBACK);
#define activeALC pio_set_pin_high(SW_FEEDBACK);
//U6A
#define activeDIS pio_set_pin_low(SW_Output);
#define activeLASER pio_set_pin_high(SW_Output);
//U6B
#define activeMUTE pio_set_pin_high(SW_PID);
#define activeSET pio_set_pin_low(SW_PID);
//U5A
#define activeAPC pio_set_pin_low(SW_APC);
#define activeAOPC pio_set_pin_high(SW_APC);

#define swDIS {\
	activeDIS;\
	disactLE;\
	activeOE;\
	activeLE;\
}

#define swMUTE {\
	activeACC;\
	activeMUTE;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}

#define swACC {\
	activeACC;\
	activeSET;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}

#define swALC {\
	activeALC;\
	activeSET;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}


/* 2016-06-13, add activeLASER, activeSET */
/* 2016-05-12, add "activeALC;\". for NP-2000. */
#define swAPC {\
	activeSET;\
	activeAPC;\
	activeALC;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}
/* 2016-06-13, add activeALC, activeLASER, activeSET */
#define swAOPC {\
	activeSET;\
	activeAOPC;\
	activeALC;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}


/* 2016-09-19, solve inrush-current, "EDFA ON" after TEC Control IC ADN8830 pin5 TEMPLOCK high,
even "PWRUP ON".
*/
extern int tec_lock;

/* 2016-09-19, execute "EDFA ON" when "PWRUP ON". */
extern int edfa_init_edfa_on;



volatile AppCfgMap_t		AppCfgMap;
volatile Hysteresis_t		Hys;
volatile AppCfgMap_t		factory_AppCfgMap;
volatile APP_RT_DATA_t		AppRtData;
volatile APP_SPEC_PARAM_t	sysvars_AppSpecParam;

/* ============   ALRM Occured Flags: Occured(1), Released(0)   =========== */
volatile uint8_t	InputOpt_hysteresis = 0;
volatile uint8_t	OutputOpt_hysteresis = 0;
volatile uint8_t	PumpBias_hysteresis = 0;
//volatile uint8_t	PumpITEC_hysteresis = 0;
volatile uint8_t	PumpTemp_hysteresis = 0;
volatile uint8_t	CaseTemp_hysteresis = 0;
volatile alarmLatch_t alarmLatch = {0,0,0,0,0,0};
	
/* ============ NuPhoton Command Variable ========================= */
volatile npMODE_t	NP_iniMODE;	//Saving in external EEPROM on address 0X3000
volatile npMODE_t	NP_rtMODE;	//Run-time status updating

/************************************************************************/
/*                                                                      */
/************************************************************************/
void EDFA_RestoreFactoryConfig(void) {
	//Read Default Setting Data to RAM
	OnEEPROM_Read_Page((unsigned char*)&AppCfgMap,sizeof(AppCfgMap), Addr_AppCfgMap_Ini);
	OnEEPROM_Read_Page((unsigned char*)&Hys,sizeof(Hys),Addr_Hys_Ini);
	
	//Save RAM to external EEPROM
	OnEEPROM_Write_Page((unsigned char*)&AppCfgMap,sizeof(AppCfgMap),Addr_AppCfgMap);
	OnEEPROM_Write_Page((unsigned char*)&Hys,sizeof(Hys),Addr_Hys);
}


static acc_soft_start(void)
{
	char ModeM[] = "MODE MNS\0";

	Console_ParseCmd(ModeM);
}

static aopc_soft_start(void)
{
	char ModeP[] = "MODE PNS\0";

	Console_ParseCmd(ModeP);
}

static apc_soft_start(void)
{
	char ModeP[] = "MODE PNS\0";

	Console_ParseCmd(ModeP);
}


/************************************************************************/
/*                                                                      */
/************************************************************************/	
void EDFA_Init(void)
{	
	float SetPower_APC_mA;
	
	/* 2016-09-21, EDFA power-on "PWRUP ON" soft start. */
	float ACC_curr_soft_start;
	float APC_curr_soft_start;
	float AOPC_dBm_soft_start;
	float AGC_dBm_soft_start;
	
	/* 2016-08-09 */
	float SetPower_AOPC_AGC;
	
	/* Read back this sector and compare them with the expected values */
	x1 = OnEEPROM_Read_Page((unsigned char*)&AppCfgMap,sizeof(AppCfgMap),Addr_AppCfgMap);
	if ( x1 != 0 ) {
		printf("EEROM READ Error: %d\n\r", x1);
	}
	
	if ( AppCfgMap.v.ResFactorySet == activeRST ) {
		sys_RestoreAppSpecParam();	
		EDFA_RestoreFactoryConfig();
		AppCfgMap.v.ResFactorySet = 0;
		save(rec_AppCfg);
	}
	else {
		x1 = OnEEPROM_Read_Page((unsigned char*)&Hys,sizeof(Hys),Addr_Hys);
		if ( x1 != 0 ) {
			printf("EEROM READ Error: %d\n\r", x1);
		}
		
		x1 = OnEEPROM_Read_Page((unsigned char*)&sysvars_AppSpecParam,sizeof(sysvars_AppSpecParam),Addr_sysvars_AppSpecParam);
		if ( x1 != 0 ) {
			printf("EEROM READ Error: %d\n\r", x1);
		}
	}
	
	x1 = OnEEPROM_Read_Page((unsigned char*)&NP_iniMODE,sizeof(NP_iniMODE),Addr_npMODE);
	if ( x1 != 0 ) {
		printf("EEROM READ Error: %d\n\r", x1);
	}
	
	x1 = OnEEPROM_Read_Page((unsigned char*)&NP_rtMODE,sizeof(NP_rtMODE),Addr_npMODE);
	if ( x1 != 0 ) {
		printf("EEROM READ Error: %d\n\r", x1);
	}
	
	/* 2016-07-07, due to hardware U8 +1.2V output problem has solved */
	/* 2016-05-09, from Vincent, only for the developing board.
	// DAC1_Output(AppCfgMap.v.AD5322Resister2, CH_A);
	DAC1_Output(65535, CH_A);	//Max. limit (65535) for test only.
	*/
	// DAC1_Output(19000, CH_A);  //1.2V for PID bias voltage setting.  Max. value is 3.3/2=1.65Volt.
	DAC1_Output(65535, CH_A);	//Max. limit (65535) for test only.



	char  ModeP[16] = "MODE PNS\0";
	// char  ModeP[16] = "MODE P 13\0";	
	char  ModeM[16] = "MODE MNS\0";
	char ModeD[] = "MODE D\0";


	/* 2016-05-17, initialize according cmd_PLDC, cmd_PLDP, cmd_PAOPC. */
	AppCfgMap.v.ACCcurrent = NP_iniMODE.SetCurrent_ACC;
	
	/*2016-06-17, a bug, fix it, about cmd_PAOPC(). */
	// NP_rtMODE.SetPower_APC = NP_iniMODE.SetPower_AOPC;
	NP_rtMODE.SetPower_AOPC = NP_iniMODE.SetPower_AOPC;
	
	NP_rtMODE.SetPower_APC = NP_iniMODE.SetPower_APC;
	
	/* 2016-06-17, implement latch function. */
	NP_rtMODE.SetPower_AOPC_cmd = NP_iniMODE.SetPower_AOPC;
	NP_rtMODE.SetPower_APC_cmd = NP_iniMODE.SetPower_APC;
	
	/* 2016-08-09, add AGC mode. */	
	NP_rtMODE.SetPower_AGC_cmd = NP_iniMODE.SetPower_AGC;
	NP_rtMODE.SetPower_AGC = NP_iniMODE.SetPower_AGC;
	
	/* 2016-05-13 */
	AppCfgMap.v.Private = 1;
	
	/* 2016-08-10 */
	/* 2016-06-17, move here. */
	NP_rtMODE.MODE = NP_iniMODE.MODE;
	NP_rtMODE.MODE_cmd = NP_iniMODE.MODE;
	

	volatile unsigned int tec_lock_timer_now, tec_lock_timer_prev;
	tec_lock_timer_prev = rtt_read_timer_value(RTT);
	tec_lock_timer_now = tec_lock_timer_prev;
	int teck_lock_counter = 0;
	
	
	volatile unsigned int soft_start_now, soft_start_prev;
	
	while (1) {
		tec_lock_timer_now = rtt_read_timer_value(RTT);
		
		// if templock(PA22) is high?
		if ( 1 == pio_get(PIO_PA22_IDX, PIO_TYPE_PIO_INPUT, PIO_PA22_IDX) ) {
			teck_lock_counter++;
		}
		else {
			teck_lock_counter = 0;
		}
		
		/* measured from the scope, about continuous 120 times high measured, is locked. */
		if ( 120 <= teck_lock_counter ) {
			tec_lock = TRUE;
			break;
		}
		
		/* 2016-09-19, normally inside 5 seconds, TEC temperature will lock. wait 5 seconds. */
		if ( tec_lock_timer_now > tec_lock_timer_prev + 1024*5 ) {
			break;
		}
		else if ( tec_lock_timer_now < tec_lock_timer_prev ) {
			if ( 0xFFFFFFFF - tec_lock_timer_prev + tec_lock_timer_now + 1 > 1024*5 ) {
				break;
			}
		}
	}
	
	

	/* 2016-05-09, execute "PWRUP ON" and re-poweron the EDFA, reference cmd_PWRUPUP(). */
	if ( ON == NP_iniMODE.iniStatusLD ) {
		NP_rtMODE.statePump = ON;
		
		if ( MAPC == NP_iniMODE.MODE ) {
			AppCfgMap.v.OperationMode = MALC;
			AppCfgMap.v.typeALC = MAPC;
			
			/* 2016-05-17, 移到前面, don't care if in APC MODE */
			// NP_rtMODE.SetPower_APC = NP_iniMODE.SetPower_APC;
			
			/* 2016-08-16, 20dBm EDFA, pump diode current 450mA, pump laser output power 300mW. */	
			/* 2016-08-05, APC mode, laser pump current mW -> mA. */
			/* 23dBm edfa, 940mA / 600mW */
			// NP_rtMODE.powerAD_APC = (-1) * NP_rtMODE.SetPower_APC*AppCfgMap.v.L + AppCfgMap.v.M;
			// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
			SetPower_APC_mA = NP_rtMODE.SetPower_APC * 940.0 / 600.0;
			
			
			/* 2016-09-21, APC soft-stat. */
			APC_curr_soft_start = 100;
			while (1) {
				if ( APC_curr_soft_start < SetPower_APC_mA ) {
					NP_rtMODE.powerAD_APC = APC_curr_soft_start * AppCfgMap.v.L + AppCfgMap.v.M;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
					swAPC
					
					// Console_ParseCmd(ModeP);
					apc_soft_start();
					
					APC_curr_soft_start += 100;
					
					soft_start_prev = rtt_read_timer_value(RTT);
					/* wait 200ms */
					while (1) {
						soft_start_now = rtt_read_timer_value(RTT);
						
						// delay 0.2 second, 1024*0.2 = 205
						if ( soft_start_now >= soft_start_prev + 2000) {
							break;
						}
						else if ( soft_start_now < soft_start_prev ){
							if ( 0xFFFFFFFF - soft_start_prev + soft_start_now + 1 > 2000 ) {
								break;
							}
						}
					}
				}
				else {
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
					NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
					swAPC
					
					// Console_ParseCmd(ModeP);
					apc_soft_start();
										
					break;
				}
			}
			
		
			
			/* 2016-09-21, APC soft-start, implemented above. */
			/*
			// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
			NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
			
			AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
			
			// 2016-07-12, may be a bug.
			// 2016-05-13, private mode no method to switch ACC/APC-AOPC, switch here!
			// 2016-05-13, activeAPC before Console_ParseCmd().
			// activeAPC
			swAPC
			
			// AppRtData.RtFlag.Bits.FinalRequest = TRUE;
			Console_ParseCmd(ModeP);
			*/
			
			
			/* 2016-06-17, move to the front of EDFA_Init(). */
			// NP_rtMODE.MODE = MAPC;
			/* 2016-05-17 */
			// NP_rtMODE.MODE_cmd = MAPC;
			
			/* 2016-06-19, move to the front. */
			// NP_rtMODE.statePump = ON;
			
			/* 2016-06-17 */
			/* 2016-05-13, will check. */
			// NP_rtMODE.statePump_edfa_on_off = ON;
		}
		else if ( MAOPC == NP_iniMODE.MODE ) {
						
			AppCfgMap.v.OperationMode = MALC;
			AppCfgMap.v.typeALC = MAOPC;
			
			
			
			/* 2016-09-21, AOPC soft-stat. */
			AOPC_dBm_soft_start = 15;
			while (1) {
				if ( AOPC_dBm_soft_start < NP_rtMODE.SetPower_AOPC ) {
					NP_rtMODE.powerAD_AOPC = AOPC_dBm_soft_start * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					swAOPC
					
					// Console_ParseCmd(ModeP);
					aopc_soft_start();
					
					AOPC_dBm_soft_start += 2;
					
					soft_start_prev = rtt_read_timer_value(RTT);
					while (1) {
						soft_start_now = rtt_read_timer_value(RTT);
						
						if ( soft_start_now >= soft_start_prev + 2000) {
							break;
						}
						else if ( soft_start_now < soft_start_prev ) {
							if ( 0xFFFFFFFF - soft_start_prev + soft_start_now + 1 > 2000 ) {
								break;
							}
						}
					
					}
				}
				else {
					NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					swAOPC
					
					// Console_ParseCmd(ModeP);
					aopc_soft_start();
								
					break;
				}
			}
			
			
			
			/* 2016-06-17, move to the front of EDFA_Init(). */
			// NP_rtMODE.MODE = MAOPC;
			/* 2016-05-17 */
			// NP_rtMODE.MODE_cmd = MAOPC;
			
			/* 2016-05-17, 移到前面, don't care if in APC MODE */
			// NP_rtMODE.SetPower_APC = NP_iniMODE.SetPower_AOPC;
			
			/* 2016-07-07, 1A Laser Pump */
			// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1749.4;
			/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
			// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1751.4;
			/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
			/* 2016-08-18, modify, auto-calibration */
			// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			/* 2016-09-21, AOPC soft-stat, implemnted two-line code above. */
			/*
			NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;							
			AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
			*/
			
			/* 2016-07-12, may be a bug. */
			/* 2016-05-13, private mode no method to switch ACC/APC-AOPC, switch here! */
			/* 2016-05-13, activeAOPC before Console_ParseCmd(). */
			// activeAOPC
			/* 2016-09-21, AOPC soft-stat, implemnted two-line code above. */
			/*
			swAOPC			
			Console_ParseCmd(ModeP);
			*/

			/* 2016-06-19, move to the front. */		
			// NP_rtMODE.statePump = ON;
			
			/* 2016-06-17 */
			/* 2016-05-13, will check. */
			// NP_rtMODE.statePump_edfa_on_off = ON;
		}
		/* 2016-08-09, add AGC mode. */
		else if ( MAGC == NP_iniMODE.MODE ) {
			AppCfgMap.v.OperationMode = MALC;
			AppCfgMap.v.typeALC = MAGC;
			
						
			/* 2016-08-09, add AGC mode. */
			/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
			SetPower_AOPC_AGC = AppRtData.vInputOptdBm + NP_rtMODE.SetPower_AGC;
			if ( SetPower_AOPC_AGC > 20.0 ) {
				SetPower_AOPC_AGC = 20.0;
			}
			else if ( SetPower_AOPC_AGC < 15.0 ) {
				SetPower_AOPC_AGC = 15.0;
			}
			
			
			/* 2016-09-22, AGC soft-stat. */
			AGC_dBm_soft_start = 12;
			while (1) {
				if ( AGC_dBm_soft_start < SetPower_AOPC_AGC ) {
					NP_rtMODE.powerAD_AOPC = AGC_dBm_soft_start * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					swAOPC
					
					// Console_ParseCmd(ModeP);
					aopc_soft_start();
					
					AGC_dBm_soft_start += 2;
					
					soft_start_prev = rtt_read_timer_value(RTT);
					while (1) {
						soft_start_now = rtt_read_timer_value(RTT);
						
						if ( soft_start_now >= soft_start_prev + 2000) {
							break;
						}
						else if ( soft_start_now < soft_start_prev ) {
							if ( 0xFFFFFFFF - soft_start_prev + soft_start_now + 1 > 2000 ) {
								break;
							}
						}
						
					}
				}
				else {
					NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					swAOPC
					
					// Console_ParseCmd(ModeP);
					aopc_soft_start();
					
					break;
				}
			}			
			
			
			
			/* 2016-09-22, AGC soft-stat, implement above. */
			/* 2016-08-18, modify, auto-calibration */
			// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			// NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			
			/* 2016-09-22, AGC soft-stat, implement above. */
			// AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
			
			/* 2016-09-22, AGC soft-stat, implement above. */
			/* 2016-07-12, may be a bug. */
			/* 2016-05-13, private mode no method to switch ACC/APC-AOPC, switch here! */
			/* 2016-05-13, activeAOPC before Console_ParseCmd(). */
			// activeAOPC
			// swAOPC
			
			/* 2016-09-22, AGC soft-stat, implement above. */
			// Console_ParseCmd(ModeP);
		}
		else if ( MACC == NP_iniMODE.MODE ) {
			/*
			NP_iniMODE.SetCurrent_ACC;
			NP_iniMODE.SetPower_APC;
			NP_iniMODE.SetPower_AOPC;
			NP_iniMODE.powerAD_APC;
			NP_iniMODE.powerAD_AOPC;
			NP_iniMODE.powerAD_ACC;
			*/
			
			/* 2016-05-17, always initialize, don't care if (MACC == NP_iniMODE.MODE). */
			/* 移到外面 */
			// AppCfgMap.v.ACCcurrent = NP_iniMODE.SetCurrent_ACC;
			
			/* 2016-09-21, ACC soft-stat, move code here. */
			AppCfgMap.v.OperationMode = MACC;
			// NP_rtMODE.SetCurrent_ACC = NP_iniMODE.SetCurrent_ACC;
			
			
			/* 2016-09-21, ACC soft-stat. */
			ACC_curr_soft_start = 200;
			while (1) {
				if ( ACC_curr_soft_start < NP_iniMODE.SetCurrent_ACC ) {
					NP_rtMODE.SetCurrent_ACC = ACC_curr_soft_start;
					
					// Console_ParseCmd(&ModeM[0]);
					acc_soft_start();
								
					ACC_curr_soft_start += 100;
					
					
					soft_start_prev = rtt_read_timer_value(RTT);
					/* wait 200ms */
					while (1) {
						soft_start_now = rtt_read_timer_value(RTT);
											
						if ( soft_start_now >= soft_start_prev + 2000) {
							break;
						}
						else if ( soft_start_now < soft_start_prev ) {
							if ( 0xFFFFFFFF - soft_start_prev + soft_start_now + 1 > 2000 ) {
								break;
							}
						}
					}
								
					/* wait 200ms */
				}
				else {
					NP_rtMODE.SetCurrent_ACC = NP_iniMODE.SetCurrent_ACC;
					
					// Console_ParseCmd(&ModeM[0]);
					acc_soft_start();
					
					break;
				}
			}
			
			
						
			/* 2016-09-21, ACC soft-stat, implemented above code. */
			/*
			AppCfgMap.v.OperationMode = MACC;
			NP_rtMODE.SetCurrent_ACC = NP_iniMODE.SetCurrent_ACC;
			*/
			
			/* 2016-08-22, a bug? */
			// AppCfgMap.v.ACCcurrent = NP_iniMODE.SetCurrent_ACC;
			
			/* 2016-06-17, move to the front of EDFA_Init(). */
			// NP_rtMODE.MODE = MACC;
			/* 2016-05-17 */
			// NP_rtMODE.MODE_cmd = MACC;			
			
			
			/* 2016-09-21, ACC soft-stat, implemented above code. */
			/* 2016-05-13, referenced cmd_FLASH(). */
			// Console_ParseCmd(&ModeM[0]);
			
			
			// AppRtData.RtFlag.Bits.FinalRequest = FALSE;
			
			/* 2016-06-19, move to the front. */
			// NP_rtMODE.statePump = ON;

			/* 2016-06-17 */						
			/* 2016-05-13, will check. */
			// NP_rtMODE.statePump_edfa_on_off = ON;
		}
		
	}
	else if ( OFF == NP_iniMODE.iniStatusLD ) {

		/* 2016-06-17, move to the front of EDFA_Init(). */
		/* 2016-05-17 */
		// NP_rtMODE.MODE_cmd = NP_rtMODE.MODE;
		
		/* 2016-06-17, a bug, although the EDFA OFF, still set AppCfgMap.v.PowerAD. */
		/* 2016-06-17, when execute "EDFA ON" first(not execute "FLASH"), it will be ok. */
		/* 2016-06-17, set NP_rtMODE.powerAD_APC to correct value according "PLDP x.xx" and "PAOPC x.x", 
		/* 2016-06-17, AppCfgMap.v.ACCcurrent already set in the front of EDFA_Init(). */
		if ( MAPC == NP_iniMODE.MODE ) {
			AppCfgMap.v.OperationMode = MALC;
			AppCfgMap.v.typeALC = MAPC;
			
			/* 2016-08-16, 20dBm EDFA, pump diode current 450mA, pump laser output power 300mW. */	
			/* 2016-08-05, APC mode, laser pump current mW -> mA. */
			/* 23dBm edfa, 940mA / 600mW */
			// NP_rtMODE.powerAD_APC = (-1) * NP_rtMODE.SetPower_APC*AppCfgMap.v.L + AppCfgMap.v.M;
			SetPower_APC_mA = NP_rtMODE.SetPower_APC * 940 / 600;
			// SetPower_APC_mA = NP_rtMODE.SetPower_APC * 450 / 300;
			// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
			NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
						
			AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
			
			/* 2016-06-17, move to the front of EDFA_Init(). */
			// NP_rtMODE.MODE = MAPC;
			// NP_rtMODE.MODE_cmd = MAPC;
		}
		/* 2016-06-17 */
		else if ( MAOPC == NP_iniMODE.MODE ) {
			AppCfgMap.v.OperationMode = MALC;
			AppCfgMap.v.typeALC = MAOPC;
			NP_rtMODE.MODE = MAOPC;
			NP_rtMODE.MODE_cmd = MAOPC;
			
			/* 2016-07-07, 1A Laser Pump */
			// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1749.4;
			/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
			// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1751.4;
			/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
			/* 2016-08-22 */
			// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			
			AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
		}
		else if ( MAGC == NP_iniMODE.MODE ) {
			AppCfgMap.v.OperationMode = MALC;
			AppCfgMap.v.typeALC = MAGC;
			
			/* 2016-08-09, add AGC mode. */
			/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
			SetPower_AOPC_AGC = AppRtData.vInputOptdBm + NP_rtMODE.SetPower_AGC;
			if ( SetPower_AOPC_AGC > 20.0 ) {
				SetPower_AOPC_AGC = 20.0;
			}
			else if ( SetPower_AOPC_AGC < 15.0 ) {
				SetPower_AOPC_AGC = 15.0;
			}
			
			/* 2016-08-22 */
			// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
			
			AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
		}
							
		Console_ParseCmd(ModeD);
		
		NP_rtMODE.statePump = OFF;
	}
	
	
	/* 2016-08-23, implement "LIPS ON" and "LIPS OFF" when the EDFA power-on. */
	//=========================================================================
	char cmdLOSN[] = "LOS N";
	char cmdLOSA[] = "LOS A";
		
	if ( LOS_A == AppCfgMap.v.bPumpAutoClose ) {
		/* 2016-09-22 */
		// Console_ParseCmd(cmdLOSN);
		Console_ParseCmd(cmdLOSA);
	}
	else if ( LOS_N == AppCfgMap.v.bPumpAutoClose ) {
		/* 2016-09-22 */
		// Console_ParseCmd(cmdLONA);
		Console_ParseCmd(cmdLOSN);
	}
	//=========================================================================
	
	/* 2016-05-13 */
	AppCfgMap.v.Private = 0;
	
}


/************************************************************************/
/* Check for System Alarm Status                                        */
/************************************************************************/
void Check_Alarm_Status(void) {
	uint8_t tmp_PrivateMode;

	//For PLIM only
	float TempPLIM;
	char  ModeD[16] = "MODE DNS\0";
	char  ModeP[16] = "MODE PNS\0";
	char  ModeM[16] = "MODE MNS\0";
	
	Get_EDFA_Monitor();
	
	tmp_PrivateMode = AppCfgMap.v.Private;
	AppCfgMap.v.Private = 1;
	
	if(AppRtData.RtFlag.Bits.CLRstatus == 0) {
		//=============================================================================================================//
		//1.Check Input Power Alarm
		//=============================================================================================================//
		if( (AppRtData.vInputOptdBm < AppCfgMap.v.vInputOptLowdBm) ) {
			AppRtData.RtFlag.Bits.InputOpt = 1;//low
			alarmLatch.LOS = 1;
			
			
			/* 2016-07-12, turn on Input Power Alarm LED. */
			ioport_set_pin_level(SW_LOI, SW_ALARM_Act);
			disactLE;
			activeOE;
			activeLE;
			
			
			if (AppCfgMap.v.bPumpAutoClose == LOS_A) { // ACC/ALC disable output
				//------------------------------------------------------------------------
				//LOS:A,I,P
				//------------------------------------------------------------------------
				AppRtData.RtFlag.Bits.FinalRequest = TRUE;
				
				/* 2016-09-21 */
				// Console_ParseCmd(&ModeD[0]);
				Console_ParseCmd(ModeD);
			}
			else if ((AppRtData.RtFlag.Bits.FinalRequest==TRUE) && (AppCfgMap.v.bPumpAutoClose==LOS_N)) { // ACC/ALC disable output
				//------------------------------------------------------------------------
				//LOS:N
				//------------------------------------------------------------------------
				AppRtData.RtFlag.Bits.FinalRequest = FALSE;
				
				/* 2016-05-18, check if "EDFA OFF", there should no output power. */
				if ( ON == NP_rtMODE.statePump ) {
					//------------------------------------------------------------------------
					//Restore: P mode if LOS in N
					//------------------------------------------------------------------------
					if (AppCfgMap.v.OperationMode==MALC) Console_ParseCmd(&ModeP[0]);
				
					//------------------------------------------------------------------------
					//Restore: M mode if LOS in N
					//------------------------------------------------------------------------
					if (AppCfgMap.v.OperationMode==MACC) Console_ParseCmd(&ModeM[0]);
					
				}
				
			}
		}
		
		if( AppRtData.RtFlag.Bits.InputOpt == 1  ) {	//Input power low
			InputOpt_hysteresis = 1;					//Guo: ALARM occured
		}
		
		if(AppRtData.vInputOptdBm > (AppCfgMap.v.vInputOptLowdBm + Hys.vInputOptLowdBm))
		AppRtData.RtFlag.Bits.FinalRequest = FALSE;

		if( (InputOpt_hysteresis) &&
		(AppRtData.vInputOptdBm > (AppCfgMap.v.vInputOptLowdBm + Hys.vInputOptLowdBm)) ){
			if ((AppCfgMap.v.bPumpAutoClose>0) && (AppRtData.RtFlag.Bits.OperationMode==MDIS)) {
								
				/* 2016-05-18, check if "EDFA OFF", there should no output power. */
				if ( ON == NP_rtMODE.statePump ) {
					//------------------------------------------------------------------------
					//Restore: P mode if LOS in A/I/P
					//------------------------------------------------------------------------
					if (AppCfgMap.v.OperationMode==MALC) {
						Console_ParseCmd(&ModeP[0]);
					}
				
					//------------------------------------------------------------------------
					//Restore: M mode if LOS in A/I/P
					//------------------------------------------------------------------------
					if (AppCfgMap.v.OperationMode==MACC) {
						Console_ParseCmd(&ModeM[0]);
					}
					
				}
				
			}
			AppRtData.RtFlag.Bits.InputOpt = 0;		//normal
			InputOpt_hysteresis = 0;				//Guo: ALARM released
						
			/* 2016-07-12, turn off Input Power Alarm LED. */
			ioport_set_pin_level(SW_LOI, SW_ALARM_DeAct);
			disactLE;
			activeOE;
			activeLE;
		}

		//=============================================================================================================//
		//2.Check Output Power Alarm
		//=============================================================================================================//
		if( (AppRtData.vOutputOptdBm < AppCfgMap.v.vOutputOptLowdBm) ) {
			AppRtData.RtFlag.Bits.OutputOpt = 1;	//low
			alarmLatch.LOP = 1;
			
			/* 2016-07-12, turn on Output Power Alarm LED. */
			ioport_set_pin_level(SW_LOP, SW_ALARM_Act);
			disactLE;
			activeOE;
			activeLE;		
		}
		
		if( AppRtData.RtFlag.Bits.OutputOpt==1  ) { //Lost of Power only
			OutputOpt_hysteresis = 1;				//Guo: ALARM occured
		}

		if( (OutputOpt_hysteresis) &&
		(AppRtData.vOutputOptdBm > AppCfgMap.v.vOutputOptLowdBm + Hys.vOutputOptLowdBm) ){
			AppRtData.RtFlag.Bits.OutputOpt = 0;	//normal
			OutputOpt_hysteresis = 0;				//Guo: ALARM released
			
			/* 2016-07-12, turn off Output Power Alarm LED. */
			ioport_set_pin_level(SW_LOP, SW_ALARM_DeAct);
			disactLE;
			activeOE;
			activeLE;
		}

		//=============================================================================================================//
		//3.Check Pump1 Temp Alarm
		//=============================================================================================================//
		
		
		if( (AppRtData.vPump1Temp > AppCfgMap.v.vPumpTempHigh) ) {
			alarmLatch.TMP = 1;
			AppRtData.RtFlag.Bits.Pump1Temp = 2;	//high
			
			
			/* 2016-07-12, turn on Pump1 Temp Alarm LED. */
			ioport_set_pin_level(SW_PumpTemp, SW_ALARM_Act);
			disactLE;
			activeOE;
			activeLE;			
		}
		if( AppRtData.RtFlag.Bits.Pump1Temp==2  ) {
			PumpTemp_hysteresis = 1;	 			//Guo: ALARM occured
		}

		if( (PumpTemp_hysteresis) &&
		(AppRtData.vPump1Temp > AppCfgMap.v.vPumpTempLow + Hys.vPumpTempLow) &&
		(AppRtData.vPump1Temp < AppCfgMap.v.vPumpTempHigh- Hys.vPumpTempHigh) ) {
			AppRtData.RtFlag.Bits.Pump1Temp = 0;	//normal
			PumpTemp_hysteresis = 0;				//Guo: ALARM released
			
			
			/* 2016-07-12, turn off Pump1 Temp Alarm LED. */
			ioport_set_pin_level(SW_PumpTemp, SW_ALARM_DeAct);
			disactLE;
			activeOE;
			activeLE;			
		}
		//=============================================================================================================//
		//4.Check Bias1 Current Alarm, ILD
		//=============================================================================================================//
		
		if(AppRtData.vPump1Bias > AppCfgMap.v.vPumpBiasHigh) {
			alarmLatch.ILD = 1;
			AppRtData.RtFlag.Bits.Pump1Bias = 2;
						
			/* 2016-07-12, turn on Bias1 Current Alarm LED. */
			ioport_set_pin_level(SW_PumpBias, SW_ALARM_Act);
			disactLE;
			activeOE;
			activeLE;
		}
		
		if(AppRtData.RtFlag.Bits.Pump1Bias==2) {	//check over current only
			PumpBias_hysteresis = 1;	 			//Guo: ALARM occured
		}
		if( (PumpBias_hysteresis) &&
	
		(AppRtData.vPump1Bias < AppCfgMap.v.vPumpBiasHigh- Hys.vPumpBiasHigh) ) {
			AppRtData.RtFlag.Bits.Pump1Bias = 0;	 //normal
			PumpBias_hysteresis = 0;				//Guo: ALARM released
						
			/* 2016-07-12, turn off Bias1 Current Alarm LED. */
			ioport_set_pin_level(SW_PumpBias, SW_ALARM_DeAct);
			disactLE;
			activeOE;
			activeLE;
		}
		//=============================================================================================================//
		//5.Check Case Temp Alarm, MTH & MTL
		//=============================================================================================================//
		if(AppRtData.vCaseTemp < AppCfgMap.v.vCaseTempLow) {
			alarmLatch.MTL = 1;
			AppRtData.RtFlag.Bits.Case2Temp = 1;	//low only, for MTL
		}
		if(AppRtData.vCaseTemp > AppCfgMap.v.vCaseTempHigh) {
			alarmLatch.MTH = 1;
			AppRtData.RtFlag.Bits.CaseTemp = 2;		//high only, for MTH
		}
		if(AppRtData.RtFlag.Bits.CaseTemp || AppRtData.RtFlag.Bits.Case2Temp) {
			CaseTemp_hysteresis = 1;	 			//Guo: ALARM occured
						
			/* 2016-07-12, turn on Case Temp Alarm LED. */
			ioport_set_pin_level(SW_CaseTemp, SW_ALARM_Act);
			disactLE;
			activeOE;
			activeLE;
		}
		if( (CaseTemp_hysteresis) && (AppRtData.vCaseTemp > AppCfgMap.v.vCaseTempLow + Hys.vCaseTempLow) ) {
			AppRtData.RtFlag.Bits.Case2Temp = 0;	//normal
		}
		if( (CaseTemp_hysteresis) && (AppRtData.vCaseTemp < AppCfgMap.v.vCaseTempHigh- Hys.vCaseTempHigh) ) {
			AppRtData.RtFlag.Bits.CaseTemp = 0;		//normal
		}
		if((CaseTemp_hysteresis) && !AppRtData.RtFlag.Bits.CaseTemp && !AppRtData.RtFlag.Bits.Case2Temp) {
			CaseTemp_hysteresis = 0;				//Guo: ALARM released
						
			/* 2016-07-12, turn off Case Temp Alarm LED. */
			ioport_set_pin_level(SW_CaseTemp, SW_ALARM_DeAct);
			disactLE;
			activeOE;
			activeLE;			
		}
	}
	
	AppCfgMap.v.Private = tmp_PrivateMode;
}

//----------------------------------------------------------------------------------
//	Glim auto control
//	
//----------------------------------------------------------------------------------
void Glimcontrol(void)
{
	float tmpGainLimitPower, tmpPower;
	
	Get_EDFA_Monitor();
	
	tmpGainLimitPower = (float)floor((AppRtData.vInputOptdBm+0.05)*10)/10 + AppCfgMap.v.OutputGOptLimit;			//power = input power + Glim power

	//if ((tmpGainLimitPower<AppCfgMap.v.setPower) && !AppRtDatas.GlimitOccured) {
	if ((tmpGainLimitPower<AppCfgMap.v.setPower) && AppRtData.RtFlag.Bits.GLIMstatus) {
		AppRtData.GlimitOccured = TRUE;
		tmpPower = tmpGainLimitPower*tmpGainLimitPower*AppCfgMap.v.K - tmpGainLimitPower*AppCfgMap.v.L + AppCfgMap.v.M;
		setLD_Value((int32_t)tmpPower);
	}
	//if (((tmpGainLimitPower+0.1)>=AppCfgMap.v.setPower) && AppRtDatas.GlimitOccured ){	//0.1 dB as hystersis
	if ((tmpGainLimitPower+0.1)>=AppCfgMap.v.setPower ){	//0.1 dB as hystersis
		AppRtData.GlimitOccured = FALSE;
		setLD_Value((int32_t)AppCfgMap.v.PowerAD);
	}

}