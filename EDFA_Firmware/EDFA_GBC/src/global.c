/*
 * global.c
 *
 * Created: 2015/12/3 11:32:50
 *  Author: Administrator
 */ 

#include "conf_example.h"
#include "math.h"

//* Debug variables 
volatile eeprom_status_t x1;

//-----------------------------------------------------------------------------
// logo                                              
//-----------------------------------------------------------------------------
/* 2016-07-11 */
/*
void ShowApplicationLogo(void) {
	puts("\n\r");
    puts("******************************************\r");
    puts("        MSA EDFA Console Interface        \r");
    puts("******************************************\r");
    puts("------------------------------------------\r");
    printf("FW Version:  %s\n\r", (char *)sysvars_AppSpecParam.AppPrivate.FWVersion);
	printf("Build Date:  %s\n\r", (char *)sysvars_AppSpecParam.AppPrivate.FWDate);
    puts("------------------------------------------\n\r");
}
*/
void ShowApplicationLogo(void) {
	puts("\n\r");
	puts("******************************************\n\r");
	puts("        MSA EDFA Console Interface        \n\r");
	puts("******************************************\n\r");
	puts("------------------------------------------\n\r");
	printf("FW Version:  %s\n\r", (char *)sysvars_AppSpecParam.AppPrivate.FWVersion);
	printf("Build Date:  %s\n\r", (char *)sysvars_AppSpecParam.AppPrivate.FWDate);
	puts("------------------------------------------\n\r");
}

//-----------------------------------------------------------------------------
//System Default Parameters                                                   //
//-----------------------------------------------------------------------------
char		deftSysDescr[] = "MSA EDFA Device";
char		deftSysContact[] = "999-9-9999999x9999, Service@optople.com";
char		deftSysName[] = "23dBm 1550nm EDFA";
char		deftSysLocation[] = "Fremont, CA";			
char		deftAmpLaserType[] = "980nm cooled DFB";

void  sys_RestoreAppSpecParam(void)
{
    //Read Default Setting Data to RAM
	OnEEPROM_Read_Page((unsigned char*)&sysvars_AppSpecParam,sizeof(sysvars_AppSpecParam),Addr_sysvars_AppSpecParam_Ini);
	
	//Save RAM to external EEPROM
	OnEEPROM_Write_Page((unsigned char*)&sysvars_AppSpecParam,sizeof(sysvars_AppSpecParam),Addr_sysvars_AppSpecParam);
}


/************************************************************************/
/* Save calibration/spec/hysteresis parameters to User section in		*/
/* external SPI memory													*/
/************************************************************************/
void save(rec_type_t rec_type) {
	switch (rec_type) {
		case rec_AppSpec: {
			OnEEPROM_Write_Page((unsigned char*)&sysvars_AppSpecParam,sizeof(sysvars_AppSpecParam),Addr_sysvars_AppSpecParam);
			break;
		}
		case rec_AppCfg: {
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap,sizeof(AppCfgMap),Addr_AppCfgMap);
			break;
		}
		case rec_Hys: {
			OnEEPROM_Write_Page((unsigned char*)&Hys,sizeof(Hys),Addr_Hys);
			break;
		}
		case rec_NUPhoton: {
			OnEEPROM_Write_Page((unsigned char*)&NP_iniMODE,sizeof(NP_iniMODE),Addr_npMODE);
			break;
		}
		default: { //rec_ALL
			OnEEPROM_Write_Page((unsigned char*)&sysvars_AppSpecParam,sizeof(sysvars_AppSpecParam),Addr_sysvars_AppSpecParam);
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap,sizeof(AppCfgMap),Addr_AppCfgMap);
			OnEEPROM_Write_Page((unsigned char*)&Hys,sizeof(Hys),Addr_Hys);
			OnEEPROM_Write_Page((unsigned char*)&NP_iniMODE,sizeof(NP_iniMODE),Addr_npMODE);
			break;
		}
	}
}

/************************************************************************/
/* Print Console                                                        */
/************************************************************************/
void Console_Prompt_Private(void) {
	if(AppCfgMap.v.Private == 1) printf("EDFA>");
}

/* 2016-09-05 */
volatile float pwr_voltage = 0;

/************************************************************************/
/* Get EDFA Monitor Information                                         */
/************************************************************************/
void Get_EDFA_Monitor(void)
{
	/* 2016-06-16 */
	// uint16_t result;
	volatile float result;
	                  
	volatile uint32_t g_afec1_sample_data0_tmp;
	volatile uint32_t g_afec0_sample_data1_tmp;
	volatile uint32_t g_afec0_sample_data2_tmp;
	volatile uint32_t g_afec0_sample_data3_tmp;
	volatile uint32_t g_afec0_sample_data4_tmp;
	volatile uint32_t g_afec0_sample_data5_tmp;
	volatile uint32_t g_afec0_sample_data15_tmp;
	volatile uint32_t g_afec0_sample_data_tmp;

	/* 2016-09-08 */
	/*
	float v;
	float LOG_Rth, LD_TEM;
	*/
	volatile double v;
	volatile double LOG_Rth, LD_TEM;
	volatile double Rth;
	
	/* 2016-05-26 */
	// uint8_t i, test;
	uint8_t i;
	volatile uint32_t test;
/*	==================================    AFEC1 for PD IN & OUT   ============================== */

	/* 2016-05-26 */
	/*
	//afec_start_software_conversion(AFEC1);
	AFEC1->AFEC_CR = AFEC_CR_START;
	
	do {
		test = (AFEC1->AFEC_ISR) & AFEC_ISR_EOC1;
	}
	while(test > 0);
	*/

	
	// PIN
	/* 2016-05-16 */
	g_afec1_sample_data0_tmp = g_afec1_sample_data0;
	// result = g_afec1_sample_data0;
	result = g_afec1_sample_data0_tmp * VOLT_REF / g_max_digital;
	
	// AppRtData.vInputOptdBm = AppCfgMap.v.A* (double)result*(double)result + AppCfgMap.v.B*(double)result - AppCfgMap.v.C;
	/* 2016-08-17 */
	// result = AppCfgMap.v.B * result - AppCfgMap.v.C;
	result = AppCfgMap.v.B * result + AppCfgMap.v.C;
	
	// printf("PIN: %4.2f dBm (%u)\n\r", result, (uint32_t)g_afec1_sample_data0_tmp);
	AppRtData.vInputOptdBm = result;

	//POUT
	result = g_afec1_sample_data1 * VOLT_REF / g_max_digital;
	/* 2016-08-05 */
	// AppRtData.vOutputOptdBm = AppCfgMap.v.E*(float)result -AppCfgMap.v.F;
	AppRtData.vOutputOptdBm = AppCfgMap.v.POUT_b_AOPC*(float)result + AppCfgMap.v.POUT_c_AOPC;


/*	==================================    AFEC0 for PD PUMP/TEC   ============================== */
	/* 2016-05-26 */
	/*
	//afec_start_software_conversion(AFEC0);
	AFEC0->AFEC_CR = AFEC_CR_START;
	
	do {
		test = (AFEC0->AFEC_ISR) & AFEC_ISR_EOC15;
	}
	while(test > 0);
	*/
	
	//do {
	//	
	//} while (is_temp_conversion_done==FALSE);
	//is_temp_conversion_done=FALSE;



	//ILD
	/* 2016-05-27 */
	g_afec0_sample_data_tmp = g_afec0_sample_data;
	// result = (g_afec0_sample_data * VOLT_REF / g_max_digital) - 113;	//mV
	/* 2016-07-07, unit mV. */
	result = (double)(g_afec0_sample_data_tmp * VOLT_REF / g_max_digital);
	/* 2016-07-07, edfa off, ILD_dec voltage 1151mV. */
	// AppRtData.vPump1Bias =(float)((result*AppCfgMap.v.I) + AppCfgMap.v.J);
	// AppRtData.vPump1Bias = (result -  1151.68) * 2 * 0.9525 + 1.6366;
	/* 2016-07-11 */
	AppRtData.vPump1Bias = AppCfgMap.v.I * result + AppCfgMap.v.J;

	
	
	
	
	
	// LD temperature
	/* 2016-05-16 */
	// result = g_afec0_sample_data1;
	g_afec0_sample_data1_tmp = g_afec0_sample_data1;
	v = (double)g_afec0_sample_data1_tmp * VOLT_REF / g_max_digital;
	
	/* 2016-09-08 */
	// v = (10*v)/(2.4-v)*1000;	//Rsense, kOHM
	// Rth = (7680 * v) / (2470 - v);
	Rth = (10000 * v) / (2400 - v);
	

	//============= The Steinhart-Hart Equation (3 parameters) =============
	/* 2016-09-08 */
	// LOG_Rth = log10f((float)v);
	LOG_Rth = log10f((float)Rth);
	
	v = LOG_Rth * LOG_Rth * LOG_Rth;
	LD_TEM = (float)AppCfgMap.v.N*v/10000000;	//N=2.248117721
	v = (float)AppCfgMap.v.O*LOG_Rth/10000;		//O=2.061314203
	LD_TEM  = (float)AppCfgMap.v.P/1000 + v + LD_TEM;	//P=1.297362049
	
	/* 2016-09-08 */
	LD_TEM = 1 / LD_TEM;
	//============= The Steinhart-Hart Equation (2 parameters) =============

	
	/* 2016-09-08 */
	// volatile double v2 = (double)v;
	/*
	v = log((float)v);
	v = (float)2.522829474*v/10000;
	LD_TEM  = (float)1.047056347/1000 + v;
	*/
	/*
	v2 = log((double)v2);
	v2 = (double)2.522829474 * v2/10000;
	LD_TEM  = (double)1.047056347/1000 + v2;
	LD_TEM = (double)1/LD_TEM;
	*/
	
	AppRtData.vPump1Temp = (float)(LD_TEM - (double)273.15);
	AppRtData.vPump1Temp = 25.0;
	
	
	
	// LD_PD (back facet monitor PD)
	/* 2016-05-16 */
	// result = g_afec0_sample_data2;
	g_afec0_sample_data2_tmp = g_afec0_sample_data2;
	// v = (double)result* VOLT_REF / g_max_digital;	// AppCfgMap.v.G should be equal to g_max_digital
	
	/* 2016-09-05, double -> float */
	// v = (double)g_afec0_sample_data2_tmp* VOLT_REF / g_max_digital;	// AppCfgMap.v.G should be equal to g_max_digital
	pwr_voltage = (float)(g_afec0_sample_data2_tmp * VOLT_REF / g_max_digital);	// AppCfgMap.v.G should be equal to g_max_digital
	v = pwr_voltage;
	
	/* 2016-08-16, 20dBm Laser Pump.
	For 20dBm EDFA, pump diode current=450mA, pump laser output power=300mw.
	For 23dBm EDFA, pump diode current=940mA, pump laser output power=600mw.
	*/
	/* 2016-08-09, LD_PD mV -> mA, 23dBm edfa, 940mA / 600mW */
	/* 2016-07-11, APC Pout Calibration. */
	// AppRtData.vOutputOptmW = v * AppCfgMap.v.APC_b + AppCfgMap.v.APC_c;
	// AppRtData.vOutputOptmW = (v * AppCfgMap.v.APC_b + AppCfgMap.v.APC_c) * 600 / 940;
	// AppRtData.vOutputOptmW = (v * AppCfgMap.v.APC_b + AppCfgMap.v.APC_c) * 300 / 450;
	/* 2016-09-07 */
	/* 2016-09-02 */
	// AppRtData.vOutputOptmW = (float)((v * AppCfgMap.v.APC_b + AppCfgMap.v.APC_c) * APC_REPORT_mW_mA);
	AppRtData.vOutputOptmW = (float)((v * AppCfgMap.v.APC_b + AppCfgMap.v.APC_c) * 600 / 940);
	
	
	
	// VTEC
	/* 2016-05-16 */
	// result = g_afec0_sample_data4;
	g_afec0_sample_data4_tmp = g_afec0_sample_data4;
	// v = (double)result* VOLT_REF / g_max_digital;
	v = (double)g_afec0_sample_data4_tmp * VOLT_REF / g_max_digital;
	
	AppRtData.vPump1Power = v;
	
	
	// ITEC in volt
	/* 2016-05-16 */
	// result = g_afec0_sample_data5;
	g_afec0_sample_data5_tmp = g_afec0_sample_data5;
	// v = (double)result* VOLT_REF / g_max_digital;
	v = (double)g_afec0_sample_data5_tmp * VOLT_REF / g_max_digital;
	
	// TCase
	/* 2016-05-16 */
	// result = g_afec0_sample_data3;
	g_afec0_sample_data3_tmp = g_afec0_sample_data3;
	// v = (float)result* VOLT_REF / g_max_digital;
	v = (float)g_afec0_sample_data3_tmp * VOLT_REF / g_max_digital;
	AppRtData.vCaseTemp = (float)(v/2-424) / 6.25;	//Needs to divided by 2 due to U43.B amplifies the voltage by 2.	
	
	
	// SAM4 Internal Temperature Sensor
	/* 2016-05-16 */
	// result = g_afec0_sample_data15;
	g_afec0_sample_data15_tmp = g_afec0_sample_data15;
	// v = (double)result* VOLT_REF / g_max_digital;
	v = (double)g_afec0_sample_data15_tmp * VOLT_REF / g_max_digital;
	
	/*
	* According to datasheet, The output voltage VT = 1.44V at 27C
	* and the temperature slope dVT/dT = 4.7 mV/C
	*/
	
	
}
