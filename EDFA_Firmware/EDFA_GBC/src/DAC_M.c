/*
 * DAC_M.c
 *
 * Created: 2015/12/21 11:42:21
 *  Author: Administrator
 */ 

#include <asf.h>
#include "conf_example.h"

/** Default frequency */
#define DEFAULT_FREQUENCY 1000

/** Analog control value */

#define DACC_ANALOG_CONTROL (DACC_ACR_IBCTLCH0(0x02) \
| DACC_ACR_IBCTLCH1(0x02) \
| DACC_ACR_IBCTLDACCORE(0x01))


void DAC_Init(void) {
	/* Enable clock for DACC */
	sysclk_enable_peripheral_clock(DACC_ID);

	/* Reset DACC registers */
	dacc_reset(DACC_BASE);

	/* Half word transfer mode */
	dacc_set_transfer_mode(DACC_BASE, 0);
	
	/* Initialize timing, amplitude and frequency */
	dacc_set_timing(DACC_BASE, 0x08, 0, 0x10);

	/* Set up analog current */
	dacc_set_analog_control(DACC_BASE, DACC_ANALOG_CONTROL);
	
	/* Enable output channel DACC_CHANNEL */
	dacc_enable_channel(DACC_BASE, DACC_CHANNEL0);
	dacc_enable_channel(DACC_BASE, DACC_CHANNEL1);
	
	/* Disable TAG and select output channel DACC_CHANNEL */
	dacc_set_channel_selection(DACC_BASE, DACC_CHANNEL1);
	dacc_write_conversion_data(DACC_BASE, 4095);
	
	/* Disable TAG and select output channel DACC_CHANNEL */
	dacc_set_channel_selection(DACC_BASE, DACC_CHANNEL0);
	dacc_write_conversion_data(DACC_BASE, 4095);
}

void DAC_test(void) {
	/* Disable TAG and select output channel DACC_CHANNEL */
	dacc_set_channel_selection(DACC_BASE, DACC_CHANNEL0);
	
	/* Enable output channel DACC_CHANNEL */
	dacc_enable_channel(DACC_BASE, DACC_CHANNEL0);
	
	dacc_write_conversion_data(DACC_BASE, dac_val);
	if (dac_val == DACC_MAX_DATA) {
		dac_val = 0;
		} else {
		dac_val = DACC_MAX_DATA;
	}
	
	/* Disable TAG and select output channel DACC_CHANNEL */
	dacc_set_channel_selection(DACC_BASE, DACC_CHANNEL1);
	
	/* Enable output channel DACC_CHANNEL */
	dacc_enable_channel(DACC_BASE, DACC_CHANNEL1);
	
	dacc_write_conversion_data(DACC_BASE, dac_val);
}

void setLD_Value(uint32_t dac_val) {
	/* Disable TAG and select output channel DACC_CHANNEL */
	dacc_set_channel_selection(DACC_BASE, DACC_CHANNEL1);	
	dacc_write_conversion_data(DACC_BASE, dac_val);
	
	/* Disable TAG and select output channel DACC_CHANNEL */
	dacc_set_channel_selection(DACC_BASE, DACC_CHANNEL0);	
	dacc_write_conversion_data(DACC_BASE, dac_val);
}