/*
 * Signature.h
 *
 * Created: 2016/2/3 13:20:50
 *  Author: Administrator
 */ 


#ifndef SIGNATURE_H_
#define SIGNATURE_H_

/************************************************************************/
/* Signature Data		                                                */
/************************************************************************/
#define PROG_A_ADDRESS 0x00410000u	//App#1 address
#define PROG_B_ADDRESS 0x00450000u	//App#2 address
#define TEMP_C_ADDRESS 0x00430000u	//for firmware upgrade receiving buffer
#define VerA	0xAA
#define VerB	0xBB
#define VerDefault 0xFF

typedef struct {
	char alphabet;
	uint8_t	number;
	uint8_t	YYH;
	uint8_t	YYL;
	uint8_t	MM;
	uint8_t	DD;
	uint8_t	s1;
	uint8_t	s2;
	uint8_t	s3;
	uint8_t	s4;
}versionInfo_t;

typedef struct {
	uint8_t			Loaded_Ver;
	uint8_t			Requested_Ver;
	versionInfo_t	VerA_Info;
	versionInfo_t	VerB_Info;
}signatureInfo_t;
	





#endif /* SIGNATURE_H_ */