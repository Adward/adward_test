/**
 * \file
 *
 * \brief Spi Master configuration.
 *
 * Copyright (c) 2011-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_SPI_MASTER_H_INCLUDED
#define CONF_SPI_MASTER_H_INCLUDED

/* Possibility to change low-level configurations here */

//! Default Config Spi Master Delay BCS
#define CONFIG_SPI_MASTER_DELAY_BCS            0

//! Default Config Spi Master Bits per Transfer Definition
#define CONFIG_SPI_MASTER_BITS_PER_TRANSFER    8

//! Default Config Spi Master Delay BCT
//#define CONFIG_SPI_MASTER_DELAY_BCT            0

//! Default Config Spi Master Delay BS
//#define CONFIG_SPI_MASTER_DELAY_BS             0

//! Default Config Spi Master Dummy Field
#define CONFIG_SPI_MASTER_DUMMY                0xFF


///////////////////////////////////////////////////////////////////////
// SPI related
//
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
#define SPI_CLK_CH0			 2000000UL		// for SPI ch0
#define SPI_CLK_CH1			24000000UL		// for SPI ch1
#define SPI_CLK_CH2			24000000UL		// for SPI ch2
#define SPI_CLK_CH3			24000000UL		// for SPI ch3

///////////////////////////////////////////////////////////////////////////////
#define EDFA_SPI					SPI
#define SPI_ID						ID_SPI
#define SPI_CH_DAC1					0x01	// 12-bit DAC LTC2602 for LD_limit & ASE
#define SPI_CH_DAC2					0x03	// 12-bit DAC LTC2602 for 3rd PUMP LASER ON/OFF Control
#define SPI_CH_DAC3					0x02	// 12-bit DAC LTC2602 for Pin & Pout Monitor
#define SPI_CH_EEPROM				0x00


///////////////////////////////////////////////////////////////////////////////
#define SPI_CH0					0x00	
#define SPI_CH1					0x01
#define SPI_CH2					0x02
#define SPI_CH3					0x03
///////////////////////////////////////////////////////////////////////////////
#define SPI_CH0_CLK_POL				0
#define SPI_CH1_CLK_POL				0
#define SPI_CH2_CLK_POL				0
#define SPI_CH3_CLK_POL				0
#define SPI_CH0_CLK_PHASE			1
#define SPI_CH1_CLK_PHASE			1
#define SPI_CH2_CLK_PHASE			1
#define SPI_CH3_CLK_PHASE			1
///////////////////////////////////////////////////////////////////////////////
#define NO_CH_SELECTED				0xF
#define SPI_DLYBSP					0x8
#define SPI_DLYBCT					0x3

//////////////////////////////////////////////////////////////
// SPI interface functions
void Onedfa_SPI_Initial(void);
void OnEDFA_SPI_CS(Spi *pSPI, uint32_t Channel);
char OnSPI_Read(Spi *pSPI, uint8_t *Data, uint16_t Length);
char OnSPI_Write(Spi *pSPI, uint8_t *Data, uint16_t Length);
char OnSPI_TxEmpty(Spi *pSPI);
//////////////////////////////////////////////////////////////

#endif /* CONF_SPI_MASTER_H_INCLUDED */
