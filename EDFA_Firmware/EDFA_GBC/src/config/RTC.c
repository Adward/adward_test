/*
 * RTC.c
 *
 * Created: 2016/1/21 11:49:35
 *  Author: Administrator
 */ 
#include <asf.h>
#include "conf_example.h"

volatile uint32_t Sys_Elapsed_time = 0;
/************************************************************************/
/* Interrupt handler for the RTC every second.                          */
/************************************************************************/
void RTC_Handler(void) {
	uint32_t ul_status = rtc_get_status(RTC);

	/* Second increment interrupt */
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		/* Disable RTC interrupt */
		rtc_disable_interrupt(RTC, RTC_IDR_SECDIS);
		Sys_Elapsed_time++;
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		rtc_enable_interrupt(RTC, RTC_IER_SECEN);
	} 
}

/**
 * \brief Set the RTC calibration.
 *
 * \param p_rtc Pointer to an RTC instance.
 * \param ul_direction_ppm Positive/negative correction.
 * \param ul_correction Correction value.
 * \param ul_range_ppm Low/high range correction.
 */
void rtc_set_calibration(Rtc *p_rtc, uint32_t ul_direction_ppm,
		uint32_t ul_correction, uint32_t ul_range_ppm)
{
	uint32_t ul_temp;

	ul_temp = p_rtc->RTC_MR;

	if (ul_direction_ppm) {
		ul_temp |= RTC_MR_NEGPPM;
	} else {
		ul_temp &= (~RTC_MR_NEGPPM);
	}

	ul_temp |= RTC_MR_CORRECTION(ul_correction);

	if (ul_range_ppm) {
		ul_temp |= RTC_MR_HIGHPPM;
	} else {
		ul_temp &= (~RTC_MR_HIGHPPM);
	}

	p_rtc->RTC_MR = ul_temp;
}

void rtc_setup(void) {
	pmc_switch_sclk_to_32kxtal(PMC_OSC_XTAL);
	
	while (!pmc_osc_is_ready_32kxtal());
	
	rtc_set_hour_mode(RTC, 0);
	
	rtc_set_calibration(RTC, 0, 23, 0);	//negative PPM correction; ;RTC accuracy is less than 1 ppm
}

void RTC_Init(void) {
	rtc_setup();
	
	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);
	rtc_enable_interrupt(RTC, RTC_IER_SECEN);
}