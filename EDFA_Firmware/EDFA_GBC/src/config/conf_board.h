/**
 * \file
 *
 * \brief User board configuration template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H

/** Enable Com Port. */
#define CONF_BOARD_UART_CONSOLE

/* USART1 module is used in serial mode. */
//#define CONF_BOARD_USART_RXD
//#define CONF_BOARD_USART_TXD

/* Configuration for console uart IRQ */
#define CONSOLE_UART_IRQn           UART0_IRQn

/* Configuration for console uart IRQ handler */
#define console_uart_irq_handler    UART0_Handler


/** Enable the SPI PINs */
#define CONF_BOARD_SPI
#define CONF_BOARD_SPI_NPCS0
#define CONF_BOARD_SPI_NPCS1
#define CONF_BOARD_SPI_NPCS2
#define CONF_BOARD_SPI_NPCS3

/* Debug */
//#define DG_EEPROM

#endif // CONF_BOARD_H
