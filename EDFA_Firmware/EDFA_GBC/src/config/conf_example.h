/**
 * \file
 *
 * \Optoplex EDFA Control Board header file.
 *
 */
/*
 *
 */
#ifndef CONF_EXAMPLE_H_INCLUDED
#define CONF_EXAMPLE_H_INCLUDED

#include "stdio_serial.h"
#include "conf_dacc.h"
#include "ctype.h"
#include "conf_spi_master.h"
#include "math.h"

/* 2016-07-12 */
#define QUEUE_SIZE (8)
#define BUFFER_SIZE (128)

/* 2016-07-19 */
#define PROMPT ("Optoplex:>")

/* 2016-08-24, EDFA Laser Pump 500mA / 1A setting values. */
// #define LASER_PUMP_TYPE_500mA
#undef LASER_PUMP_TYPE_500mA
#define LASER_PUMP_TYPE_1A

#ifdef LASER_PUMP_TYPE_500mA
/*
#define LDC_MAX_500mA (500)
#define LDC_MIN_500mA (0)
#define AOPC_MAX_500mA (20)
#define AOPC_MIN_500mA (0)
#define AGC_MAX_500mA (20)
#define AGC_MIN_500mA (10)
#define LDP_MAX_500mA (300)
#define LDP_MIN_500mA (0)
#define ACC_DAC_LIMIT_500mA (1000)
#define AOPC_DAC_LIMIT_500mA (1000)
#define APC_DAC_LIMIT_500mA (1000
#define AGC_MAX_dBm_500mA (20)
#define APC_SLOPE_mA_mW (450/300)
#define APC_REPORT_mW_mA (300/450)
*/
#else	// setting of 1A Laser Pump.

#ifdef LASER_PUMP_TYPE_1A

#define LDC_MAX (1000)
#define LDC_MIN (0)
#define AOPC_MAX (23)
#define AOPC_MIN (0)
#define AGC_MAX (20)
#define AGC_MIN (10)
#define LDP_MAX (600)
#define LDP_MIN (0)
#define ACC_DAC_LIMIT (1000)
#define AOPC_DAC_LIMIT (1000)
#define APC_DAC_LIMIT (1000)
#define AGC_MAX_dBm (23)
#define APC_SLOPE_mA_mW (940/600)
#define APC_REPORT_mW_mA (600/940)


#endif

#endif




/**
 * Memory type (must be used)
 *  -> Flash (USE_FLASH)
 */
#define USE_FLASH
#define IFLASH0_ADDR IFLASH_ADDR
// Errors (unsigned ints)
#define OK          0x00000000
#define ERROR       0xFFFFFFFF


/************************************************************************/
/*                                                                      */
/************************************************************************/
/* Using button BP2 on SAM4E-EK (PA19) */
#define EXAMPLE_BUTTON GPIO_PUSH_BUTTON_1

/** Programmable Clock ID for the example by default */
//#ifndef GCLK_ID
//#define GCLK_ID         PMC_PCK_0
//#endif


/************************************************************************/
/* For Application                                                      */
/************************************************************************/
#define  AGENT_LOCATION_LEN			96  //Location string Max Length
#define  ENTERPRISE_LOGO_LEN		64  //Enterprise LOGO Max Length
#define  SERIALNUMBER_SIZE			20  //Serial Number Max Lentth
#define  MODELTYPE_SIZE				20  //Model Type Max Length
#define  HWVERSION_SIZE				15  //HW Version Max Length
#define  FWVERSION_SIZE				20   //FW Version Max Length
#define  FWDATE_SIZE				20

/************************************************************************/
/*                                                                      */
/************************************************************************/
#define	Addr_Hys_Ini					0x1800		//Factory default sys_AppSpecParam Address
#define	Addr_sysvars_AppSpecParam_Ini	0x1400		//Factory default sys_AppSpecParam Address
#define	Addr_AppCfgMap_Ini				0x1000		//Factory default Addr_AppCfgMap Address
#define	Addr_Hys						0x0C00		//sys_AppSpecParam Address
#define	Addr_sysvars_AppSpecParam		0x0800		//sys_AppSpecParam Address
#define	Addr_AppCfgMap					0x0400		//Addr_AppCfgMap Address
#define Addr_npMODE						0x0000		//for NUPHOTON command set

/************************************************************************/
/*  LOS mode                                                            */
/************************************************************************/
#define LOS_N 0
#define LOS_A 1
#define LOS_P 2
#define LOS_I 3

/************************************************************************/
/* Operation Mode                                                       */
/************************************************************************/
typedef enum {
	MDIS,
	MACC,
	MALC,
	MAGC,
	MAPC,
	MAOPC,
	MUTE
} op_mode_t;

/************************************************************************/
/* EDFA ON/OFF State                                                    */
/************************************************************************/
typedef enum {
	OFF,
	ON
} OnOff_state_t;

/************************************************************************/
/* External DAC CH definition                                           */
/************************************************************************/
typedef enum {
	CH_A = 0x30,
	CH_B = 0x31,
	CH_ALL = 0x3F,
	Off_ALL = 0x4F
} DAC_CH_t;

/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef struct
{
	struct
	{
		char	sysDescr[ENTERPRISE_LOGO_LEN];					//64, 0x800
		char	sysContact[ENTERPRISE_LOGO_LEN];				//64, 0x840	//聯絡人
		char	sysName[ENTERPRISE_LOGO_LEN];					//64, 0x880
		char	sysLocation[AGENT_LOCATION_LEN]; 				//96, 0x8C0	//供應商
		char	EntpLogo[ENTERPRISE_LOGO_LEN];					//64, 0x920
		char	SerialNumber[SERIALNUMBER_SIZE];				//20, 0x960
		char	ModelType[MODELTYPE_SIZE];						//20, 0x974
		uint8_t	BaudRate;										// 1, 0x988, //BaudRate = 1:9600, 2:19200, 3:38400, 4:57600 ,5:115200
		char	HWVersion[HWVERSION_SIZE];						//15, 0x989
		char	FWVersion[FWVERSION_SIZE];						//20, 0x998 ==>38x64+24
		char	FWDate[FWDATE_SIZE];							//20, 0x9AC ==>38x64+44
		uint8_t	I2CAddress;										// 1, 0x9C0
	}AppPrivate;
	struct                                        				// 6, 0x9C1
	{
		uint16_t	Year;
		uint16_t	Month;
		uint16_t	Date;
	}ManufactureDate;
}APP_SPEC_PARAM_t;

////////////////////////////////////////////////////////////////////////////////
//EDFA Monitor Parameters, Alarm, Operation Status
////////////////////////////////////////////////////////////////////////////////
#define		SETVALUE_LEN     7		//Setting Value Tmp Max Length
#define		uint8	uint8_t
#define		uint16	uint16_t
#define		uint32	uint32_t
#define		TRUE	1
#define		FALSE	0
typedef struct
{
	uint8		TmpSetValue[SETVALUE_LEN];
	float		vInputOptmW;   	// XXXX
	float		vInputOptdBm;	// XX.XX
	float		vOutputOptmW; 	// XXXX
	float		vOutputOptdBm;	// XX.XX
	float		vPump1Bias;     // XXXX
	float		vPump1Temp;     // XX.X
	uint16		vPump1Power;	// XXXX
	float		vCaseTemp;      // XX.X
	
	uint8_t		GlimitOccured;
	
	struct
	{
		struct
		{
			uint8	InputOpt;		// 0 = Normal, 1 = Low, 2 = High 					==> Guo: LOS  (or Low) only
			uint8	OutputOpt;		// 0 = Normal, 1 = Low, 2 = High 				==> Guo: LOP  (or Low) only
			uint8	Pump1Temp;		// 0 = Normal, 1 = Low, 2 = High 				==> Guo: High only
			uint8	Pump1Bias;		// 0 = Normal, 1 = Low, 2 = High 				==> Guo: Overcurrent (or High) only
			uint8	CaseTemp;		// 0 = Normal, 2 = High
			uint8	Case2Temp;		// 0 = Normal, 1 = Low
			

			uint8	FinalRequest;	// 2 = pump on, 1 = pump off, 0 = Mute
			uint8	OperationMode;	// 2 = AGC, 1 = ALC, 0 = ACC, 3 = Disable
			uint8	typeALC;		// 0 = APC, 1 = AOPC
			uint8	PowerLimit;		// 1 = Power Limit, 0 = Normal
			uint8	RFLstatus;		// 0 = Normal, 1 = Latching
			uint8	PLIMstatus;		// 0 = Disable, 1 = Enable
			uint8	GLIMstatus;		// 0 = Disable, 1 = Enable
			uint8	CLRstatus;		// 0 = Disable, 1 = Enable
			uint8	RFLused;		// 0 = Disable, 1 = Enable

		}Bits;
		uint8     All;
	}RtFlag;
	//    uint8   vPumpNum;   // PumpNum
	uint32	sysUpTime;
}APP_RT_DATA_t;
//-----------------------------------------------------------------------------
//Coefficient Value in Flash
//-----------------------------------------------------------------------------
typedef struct
{
	struct
	{
		uint8	Private;
		double	A;			//get Pin_a
		double	B;			//get Pin_b
		double	C;			//get Pin_c
		
		/* 2016-08-05, replace D with POUT_a_AOPC. */
		/*
		double	D;			//get Pout_a
		double	E;			//get Pout_b
		double	F;			//get Pout_c
		*/
		double	POUT_a_AOPC;		//get Pout_a
		double	POUT_b_AOPC;		//get Pout_b
		double	POUT_c_AOPC;		//get Pout_c
		
		double	G;			//?
		double	I;			//set LD current_a
		double	J;			//set LD current_b
		double	K;			//set AOPC Pout_a, now used for APC.
		double	L;			//set AOPC Pout_b, now used for APC.
		double	M;			//set AOPC Pout_c, now used for APC.
		double	N;			//get LD temp
		double	O;			//get LD temp
		double	P;			//get LD temp
		double	Q;			//set LD current_a
		double	R;			//set LD current_b
		double	S;			//set LD current_c
		double	vTEC_a;		//get voltage of TEC
		double	vTEC_b;		//get voltage of TEC
		double	vTEC_c;		//get voltage of TEC
		double	iTEC_a;		//get current of TEC
		double	iTEC_b;		//get current of TEC
		double	iTEC_c;		//get current of TEC
		double	APC_a;		//set APC Pout_a
		double	APC_b;		//set APC Pout_b
		double	APC_c;		//set APC Pout_c
		
		/* 2016-08-05, add AOPC_a .. 3 coefficients, should erase EEPROM first!!! */
		double	AOPC_a_dac;			//set AOPC Pout_a, dBm -> DAC.
		double	AOPC_b_dac;			//set AOPC Pout_b
		double	AOPC_c_dac;			//set AOPC Pout_c
		
		float	ACCcurrent;    		
		uint8	bPumpAutoClose;    		// = 1 enable = 0 disable,
		float	vInputOptLowdBm;
		float	vInputOptHighdBm;
		float	vOutputOptLowdBm;
		float	vOutputOptHighdBm;
		float	vPumpBiasLow;
		float	vPumpBiasHigh;
		uint16	vPumpITECLow;
		uint16	vPumpITECHigh;
		float	vPumpTempLow;
		float	vPumpTempHigh;
		float	vCaseTempLow;
		float	vCaseTempHigh;
		uint16   AutoISP;	//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx     No needed
		uint16	ASECompensation;		//Compensate ASE Value
		op_mode_t	OperationMode;		//Operation Mode Status		// 2 = AGC, 1 = ALC, 0 = ACC, 3 = Disable
		uint8	typeALC;				// 0 = APC, 1 = AOPC
		int		AD5162Resister;			//Chip Max Resister
		uint16	DACASE;					//DAC ASE Compensate
		uint16	DACACCTHR;				//DAC THR ACC
		uint16	DACALCTHR;				//DAC THR ALC
		uint16  DACResister;	//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx     No needed
		uint16	AD5322Resister;		//for current control setting
		uint16	AD5322Resister2;	//for current upper limit
		float	OFG;
		uint8	ECHO;					//1 = ON, 0 = OFF
		uint8	Latch;					//1 = S (Latching), 0 = N (Normal)
		uint16	PumpBiasEOL;
		float	OutputOptLimit;			//Max Output Power
		float OutputGOptLimit;		//Max Output Gain
		uint8	AmpType;				//1 = Booster, 0 = PreAmp
		float	setPower;
		float	PowerAD;
		uint16	halfAD;
		float	RFLLimit;
		

		uint16	LOSILD;			//for LOS I val, set-point for current in-case of loss (mA)
		
		uint8	RFLstatus;		// 0 = Normal, 1 = Latching
		uint8	PLIMstatus;		// 0 = Disable, 1 = Enable
		uint8	GLIMstatus;		// 0 = Disable, 1 = Enable
		uint8	CLRstatus;		// 0 = Disable, 1 = Enable
		uint8	RFLused;		// 0 = Disable, 1 = Enable
		uint8_t ResFactorySet;		// 0xF2=active restore function
	}v;
}AppCfgMap_t;

typedef struct
{
	float	vInputOptLowdBm;
	float	vInputOptHighdBm;
	float	vOutputOptLowdBm;
	float	vOutputOptHighdBm;
	float	vPumpBiasLow;
	float	vPumpBiasHigh;
	uint16	vPumpITECLow;
	uint16	vPumpITECHigh;
	float	vPumpTempLow;
	float	vPumpTempHigh;
	float	vCaseTempLow;
	float	vCaseTempHigh;

}Hysteresis_t;

/************************************************************************/
/* ALARM Latch data type                                                */
/************************************************************************/
typedef struct {
	U8	ILD;
	U8	TMP;
	U8	MTH;
	U8	MTL;
	U8	LOS;
	U8	LOP;
	U8	RFL;
}alarmLatch_t;

/************************************************************************/
/* External Memory Record Type                                          */
/************************************************************************/
typedef enum {
	rec_AppSpec,
	rec_AppCfg,
	rec_Hys,
	rec_NUPhoton,
	rec_ALL
} rec_type_t;

/** EEPROM operation status, each operation returns one of the following status */
typedef enum eeprom_status {
	EEPROM_SUCCESS = 0,  /** Current operation successful */
	EEPROM_ERROR,  /** Current operation failed */
	EEPROM_SECTOR_PROTECTED, /** Sector protected */
	EEPROM_SECTOR_UNPROTECTED,  /** Sector unprotected */
	EEPROM_ERROR_INIT,  /** Initialization error: p_at25->pdesc is not initialized */
	EEPROM_ERROR_NOT_FOUND,  /** The specific EEPROM Not found  */
	EEPROM_ERROR_WRITE, /** Write error returned by the SerialFlash */
	EEPROM_ERROR_BUSY,  /** Current operation failed, SerialFlash is busy */
	EEPROM_ERROR_PROTECTED,  /** Current operation failed, SerialFlash is protected */
	EEPROM_ERROR_SPI  /** SPI transfer failed */
} eeprom_status_t;

/************************************************************************/
/* NUPHOTON FLASH and power-up Command data structure                                     */
/************************************************************************/
typedef struct {
	//OnOff_state_t	iniIMSG;		// Initialization message display at On or Off when power up only set by command IMSG.
	OnOff_state_t	iniStatusLD;	// Initial EDFA at On or Off when power up only set by command PWRUP.

	float		SetCurrent_ACC;		// Set by PLDP/PLDC/PAOPC for power-up. 
	float		SetPower_APC;		// 
	float		SetPower_AOPC;	    //
	float		powerAD_APC;
	float		powerAD_AOPC;
	float		powerAD_ACC;
	
	op_mode_t	MODE;				// MODE record.
	OnOff_state_t	statePump;		//Pump status
	OnOff_state_t	statePrompt;	//Prompt status
	OnOff_state_t	headMsg;
	
	/* 2016-05-12, command cmd_EDFA turn on/off. */
	// OnOff_state_t	statePump_edfa_on_off;
	
	/* 2016-05-17 */
	op_mode_t	MODE_cmd;				// MODE changed to, set by cmd_MODE().
	
	/* 2016-06-17, implement latch, change when execute "FLASH", cmd_FLASH(). */
	float SetPower_APC_cmd;
	float SetPower_AOPC_cmd;
	
	/* 2016-08-08, add AGC mode. */
	float SetPower_AGC;
	float SetPower_AGC_cmd;
	
} npMODE_t;



/************************************************************************/
/* Global Variables														*/
/************************************************************************/
extern volatile uint32_t g_ul_ms_ticks;
extern volatile eeprom_status_t	x1;
extern volatile AppCfgMap_t		AppCfgMap;
extern volatile Hysteresis_t	Hys;
extern volatile AppCfgMap_t		factory_AppCfgMap;
extern volatile APP_RT_DATA_t	AppRtData;
extern volatile	APP_SPEC_PARAM_t	sysvars_AppSpecParam;
extern volatile uint32_t dac_val;
/* ============   ALRM Ouucred Flags: Occured(1), Released(0)   =========== */
extern volatile uint8_t	InputOpt_hysteresis;
extern volatile uint8_t	OutputOpt_hysteresis;
extern volatile uint8_t	PumpBias_hysteresis;
//extern volatile uint8_t	PumpITEC_hysteresis;
extern volatile uint8_t	PumpTemp_hysteresis;
extern volatile uint8_t	CaseTemp_hysteresis;
extern volatile alarmLatch_t alarmLatch;

extern volatile npMODE_t	NP_iniMODE;
extern volatile npMODE_t	NP_rtMODE;

/* ========== SPI DAC ============ */
int32_t  DAC_Set_Value, SetCh_DAC;
uint8_t  Index_TL, Index_TH, Dir_L, Dir_H, SetCh_Dir, Ch_Dir;
float	 fDataL, fDataH, fData;
/* ========== Functions ========== */
void EDFA_Init(void);
void ShowApplicationLogo(void);
void Console_Prompt_Private(void);
void Get_EDFA_Monitor(void);

/************************************************************************/
/* command RST                                                          */
/************************************************************************/
#define activeRST	0xF2

/************************************************************************/
/* Console                                                              */
/************************************************************************/
/*  Key ASCII Definition */
#define	BACK_Key	0x08
#define ENTER_Key	0x0D

#define STRING_EOL    "\r"
#define STRING_HEADER "-- PDC_UART Example --\n\r" \
"-- "BOARD_NAME" --\n\r" \
"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL
#define MAX_CMD_LEN		(128)
#define MAX_CMD_ARGS	(10)
extern volatile uint8_t	tmpECHO, tmpSTORE;

void configure_console(void);
int Console_ParseCmd(char *s);
int CmdStrCompare(const char *s1, const char *s2);

typedef enum {
	STAT_SPACE = 0x00,
	STAT_WORD  = 0x11
}PARSE_STAT_t;

/************************************************************************/
/* Global                                                               */
/************************************************************************/
void save(rec_type_t);
void sys_RestoreAppSpecParam(void);

/************************************************************************/
/* IOtest                                                               */
/************************************************************************/
void configure_IO(void);


/************************************************************************/
/* External Serial Memory                                               */
/************************************************************************/
void testExtMemory(void);
eeprom_status_t OnEEPROM_Read_Page (uint8_t *value, uint16_t size, uint16_t address);
eeprom_status_t OnEEPROM_Write_Page (uint8_t *value , uint16_t size, uint16_t address);

/************************************************************************/
/* EDFA                                                                 */
/************************************************************************/
//extern APP_RT_DATA_t	AppRtData;
//extern AppCfgMap_t		AppCfgMap;
//extern Hysteresis_t		Hys;
//extern AppCfgMap_t		factory_AppCfgMap;
void Check_Alarm_Status(void);
void Glimcontrol(void);

/************************************************************************/
/* Internal DAC                                                         */
/************************************************************************/
/** The maximal digital value */
#define MAX_DIGITAL_12_BIT     (4095UL)
#define MAX_DIGITAL_15_BIT     (32767UL)
/** Reference voltage for AFEC in mv. */
#define VOLT_REF        (3000)
void DAC_Init(void);
/** The maximal digital value */
static uint32_t g_max_digital = MAX_DIGITAL_15_BIT;

/************************************************************************/
/* Internal ADC                                                         */
/************************************************************************/
void AFEC_Init(void);
void print_float(float temp);

/* 2016-05-27 */
/*
extern volatile float g_afec0_sample_data;
extern volatile float g_afec0_sample_data1;
extern volatile float g_afec0_sample_data2;
*/
extern volatile uint32_t g_afec0_sample_data;
extern volatile uint32_t g_afec0_sample_data1;
extern volatile uint32_t g_afec0_sample_data2;

extern volatile uint32_t g_afec0_sample_data3;

extern volatile float g_afec0_sample_data4;
extern volatile float g_afec0_sample_data5;
extern volatile uint32_t g_afec0_sample_data15;

/* 2016-05-16 */
// extern volatile float g_afec1_sample_data0;
extern volatile uint32_t g_afec1_sample_data0;

extern volatile float g_afec1_sample_data1;

/* 2016-08-02 */
// extern volatile bool is_temp_conversion_done;
extern volatile unsigned int adc0_conversion_done;
extern volatile unsigned int adc1_conversion_done;
/************************************************************************/
/*                                                                      */
/************************************************************************/
extern volatile uint32_t Sys_Elapsed_time;
void RTC_Init(void);

/************************************************************************/
/* SAM4 Internal DAC                                                    */
/************************************************************************/
extern void setLD_Value(uint32_t dac_val);

/************************************************************************/
/* SPI DAC                                                              */
/************************************************************************/
void DAC1_Output(uint16_t DAC_Value, DAC_CH_t CH);
void DAC2_Output(uint16_t DAC_Value, DAC_CH_t CH);
void DAC3_Output(uint16_t DAC_Value, DAC_CH_t CH);

#endif /* CONF_EXAMPLE_H_INCLUDED */
