/*
 * ADC.c
 *
 * Created: 2015/12/23 18:40:58
 *  Author: Administrator
 */ 
#include <asf.h>
#include "conf_example.h"

/** Reference voltage for AFEC in mv. */
//#define VOLT_REF        (3300)

/** The maximal digital value */
//#define MAX_DIGITAL_12_BIT     (4095UL)

/** The conversion data is done flag */
/* 2016-08-02 */
// volatile bool is_temp_conversion_done = false;
volatile unsigned int adc0_conversion_done = TRUE;
volatile unsigned int adc1_conversion_done = TRUE;

/** AFEC sample data */
/* 2016-05-27 */
/*
volatile float g_afec0_sample_data = 0;		//ILD (LASER current)
volatile float g_afec0_sample_data1 = 0;	//TLD (thermistor temperature)
volatile float g_afec0_sample_data2 = 0;	//LD_PD (back facet monitor PD)  
*/
volatile uint32_t g_afec0_sample_data = 0;		//ILD (LASER current)
volatile uint32_t g_afec0_sample_data1 = 0;	//TLD (thermistor temperature)
volatile uint32_t g_afec0_sample_data2 = 0;	//LD_PD (back facet monitor PD)  

// volatile float g_afec0_sample_data3 = 0;	//TCase
volatile uint32_t g_afec0_sample_data3 = 0;	//TCase

volatile uint32_t g_afec0_sample_data15 = 0;	//SAM4 internal temperature sensor

volatile float g_afec0_sample_data4 = 0;	//VTEC
volatile float g_afec0_sample_data5 = 0;	//ITEC in volt

/* 2016-05-16 */
// volatile float g_afec1_sample_data0 = 0;		//Pin
volatile uint32_t g_afec1_sample_data0 = 0;	// Pin

volatile float g_afec1_sample_data1 = 0;	//Pout


/**
 * \brief AFEC0 EOC interrupt callback function.
 */
void afec0_eoc_0(void)
{
	/* 2016-05-26 */
	AFEC0->AFEC_CSELR = 0;
	g_afec0_sample_data = AFEC0->AFEC_CDR;
	
#ifdef afec0_Debug
	printf("IPUMP: ");
	print_float(g_afec0_sample_data * VOLT_REF / g_max_digital);
	printf(" mV\n\r");
	//printf("====================> ADC0_CH0:%d\n\r",(AFEC0->AFEC_ISR) & AFEC_ISR_EOC0);
#endif
}


void afec0_eoc_1(void)
{
	/* 2016-05-26 */
	// g_afec0_sample_data1 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_1);
	AFEC0->AFEC_CSELR = 1;
	g_afec0_sample_data1 = AFEC0->AFEC_CDR;
		
#ifdef afec0_Debug	
	printf("TPUMP: ");
	print_float(g_afec0_sample_data1 * VOLT_REF / g_max_digital);
	printf(" mV\n\r");
#endif
}


void afec0_eoc_2(void)
{
	/* 2016-05-26 */
	// g_afec0_sample_data2 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_2);	
	AFEC0->AFEC_CSELR = 2;
	g_afec0_sample_data2 = AFEC0->AFEC_CDR;	
	
#ifdef afec0_Debug	
	printf("PUMP's PD: ");
	print_float(g_afec0_sample_data2 * VOLT_REF / g_max_digital);
	printf(" mV\n\r");
#endif	
}


void afec0_eoc_3(void)
{
	/* 2016-05-26 */	
	// g_afec0_sample_data3 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_3);
	AFEC0->AFEC_CSELR = 3;
	g_afec0_sample_data3 = AFEC0->AFEC_CDR;	
	
    #ifdef afec0_Debug	
	printf("TCase: %4.2f mV\n\r", g_afec0_sample_data3 * VOLT_REF / g_max_digital);
    #endif	
}


void afec0_eoc_4(void)
{	
	/* 2016-05-26 */
	// g_afec0_sample_data4 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_4);
	AFEC0->AFEC_CSELR = 4;
	g_afec0_sample_data4 = AFEC0->AFEC_CDR;	
	
	#ifdef afec0_Debug
	printf("VTEC: ");
	print_float(g_afec0_sample_data4 * VOLT_REF / g_max_digital);
	//print_float(g_afec0_sample_data4 * VOLT_REF / g_max_digital - 1650); //differential mode
	printf(" mV\n\r");
	#endif
}


void afec0_eoc_5(void)
{
	/* 2016-05-26 */
	// g_afec0_sample_data5 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_5);
	AFEC0->AFEC_CSELR = 5;
	g_afec0_sample_data5 = AFEC0->AFEC_CDR;	
	
	#ifdef afec0_Debug
	printf("ITEC: ");
	print_float(g_afec0_sample_data5 * VOLT_REF / g_max_digital);
	printf(" mV\n\r");
	#endif
}


void afec0_eoc_15(void)
{
	/* 2016-05-26 */
	// g_afec0_sample_data15 = afec_channel_get_value(AFEC0, AFEC_TEMPERATURE_SENSOR);
	AFEC0->AFEC_CSELR = 15;
	g_afec0_sample_data15 = AFEC0->AFEC_CDR;
	
	#ifdef afec0_Debug
	printf("SAM4 Temp.: %d\n\r", g_afec0_sample_data15);
	#endif
	
	/* 2016-05-26 */
	// AFEC0->AFEC_CR = AFEC_CR_START;
	
	/* 2016-08-02 */
	// is_temp_conversion_done = TRUE;
	adc0_conversion_done = TRUE;
}


/**
 * \brief AFEC1 EOC interrupt callback function.
 */
void afec1_eoc_0(void)
{
	/* 2016-05-26 */
	//printf("====================> ADC1_CH0:%d\n\r",(AFEC1->AFEC_ISR) & AFEC_ISR_EOC0);
	// g_afec1_sample_data0 = afec_channel_get_value(AFEC1, AFEC_CHANNEL_0);
	AFEC1->AFEC_CSELR = 0;
	g_afec1_sample_data0 = AFEC1->AFEC_CDR;
	
	
	//printf("AFEC1_CH0:");
	//print_float(g_afec1_sample_data0);
	//printf("====================> ADC1_CH0:%d\n\r",(AFEC1->AFEC_ISR) & AFEC_ISR_EOC0);
}

void afec1_eoc_1(void)
{	
	/* 2016-05-26 */
	// g_afec1_sample_data1 = afec_channel_get_value(AFEC1, AFEC_CHANNEL_1);
	AFEC1->AFEC_CSELR = 1;
	g_afec1_sample_data1 = AFEC1->AFEC_CDR;	
	
	//printf("AFEC1_CH1:");
	//print_float(g_afec1_sample_data1);
	
	/* 2016-05-26 */
	// AFEC1->AFEC_CR = AFEC_CR_START;
	
	/* 2016-08-02 */
	adc1_conversion_done = TRUE;
}



/**
 * \brief AFEC1 DRDY interrupt callback function.
 */
void afec0_data_ready(void) {
	g_afec0_sample_data = afec_get_latest_value(AFEC0);
	puts("AFEC0:");
	print_float(g_afec0_sample_data * VOLT_REF / g_max_digital);
}

void afec1_data_ready(void) {
	g_afec1_sample_data0 = afec_get_latest_value(AFEC1);
	
	/* 2016-05-16 */
	/*
	puts("AFEC1:");
	print_float(g_afec1_sample_data0 * VOLT_REF / g_max_digital);
	*/
}

/**
 * \brief AFEC0 Channel Difference DRDY interrupt callback function.
 */
void afec0_diff_data_ready(void) {
	g_afec0_sample_data = afec_get_latest_value(AFEC0);
	puts("AFEC0 Channel Differential Voltage:");
	print_float(g_afec0_sample_data * VOLT_REF / g_max_digital - 1650);
}


void AFEC_Init(void)
{
	struct afec_config afec_cfg;
	struct afec_ch_config afec_ch_cfg;
	struct afec_temp_sensor_config afec_temp_sensor_cfg;
	
	/** The conversion data is done flag */
	/* 2016-08-02 */
	// is_temp_conversion_done = false;
	adc0_conversion_done = TRUE;
	adc1_conversion_done = TRUE;
	
	afec_get_config_defaults(&afec_cfg);
	afec_ch_get_config_defaults(&afec_ch_cfg);
	
	// AFEC0
	afec_enable(AFEC0);
	afec_enable(AFEC1);
	
	afec_cfg.resolution = AFEC_15_BITS;
	afec_init(AFEC0, &afec_cfg);
	afec_init(AFEC1, &afec_cfg);


	afec_ch_set_config(AFEC1, AFEC_CHANNEL_0, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC1, AFEC_CHANNEL_0, 0x800);
	afec_channel_enable(AFEC1, AFEC_CHANNEL_0);
	// 2016-05-26, interrupt priority only bit7~bit4.
	afec_set_callback(AFEC1, AFEC_INTERRUPT_EOC_0, afec1_eoc_0, 0xB0);


	afec_ch_set_config(AFEC1, AFEC_CHANNEL_1, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC1, AFEC_CHANNEL_1, 0x800);
	afec_channel_enable(AFEC1, AFEC_CHANNEL_1);
	// 2016-05-26, interrupt priority only bit7~bit4.
	afec_set_callback(AFEC1, AFEC_INTERRUPT_EOC_1, afec1_eoc_1, 0xC0);


	
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_0, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_0, 0x800);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_0);

	afec_ch_set_config(AFEC0, AFEC_CHANNEL_1, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_1, 0x800);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_1);
	
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_2, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_2, 0x800);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_2);

	afec_ch_set_config(AFEC0, AFEC_CHANNEL_3, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_3, 0x800);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_3);

	afec_ch_set_config(AFEC0, AFEC_CHANNEL_4, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_4, 0x800);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_4);

	afec_ch_set_config(AFEC0, AFEC_CHANNEL_5, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_5, 0x800);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_5);
	
	afec_ch_set_config(AFEC0, AFEC_TEMPERATURE_SENSOR, &afec_ch_cfg);
	afec_channel_set_analog_offset(AFEC0, AFEC_TEMPERATURE_SENSOR, 0x800);
	afec_channel_enable(AFEC0, AFEC_TEMPERATURE_SENSOR);

	/* 2016-05-26, interrupt priority only bit7~bit4 */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_0, afec0_eoc_0, 0x40);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_1, afec0_eoc_1, 0x50);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_2, afec0_eoc_2, 0x60);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_3, afec0_eoc_3, 0x70);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_4, afec0_eoc_4, 0x80);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_5, afec0_eoc_5, 0x90);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_15, afec0_eoc_15, 0xA0);

	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
	/* 2016-05-27 */
	// AFEC0->AFEC_CR = AFEC_CR_START;
	
	afec_set_trigger(AFEC1, AFEC_TRIG_SW);
	/* 2016-05-27 */
	// AFEC1->AFEC_CR = AFEC_CR_START;


	/* 2016-05-27, before AFEC0 and AFEC1 begin, or 造成 Pout 有時有不正的 ADC 值, will check. */
	afec_start_calibration(AFEC0);
	while((afec_get_interrupt_status(AFEC0) & AFEC_ISR_EOCAL) != AFEC_ISR_EOCAL);
	afec_start_calibration(AFEC1);
	while((afec_get_interrupt_status(AFEC1) & AFEC_ISR_EOCAL) != AFEC_ISR_EOCAL);

	/* 2016-05-27, after calibration,  AFEC0 and AFEC0 begin. */
	// AFEC0->AFEC_CR = AFEC_CR_START;
	// AFEC1->AFEC_CR = AFEC_CR_START;

}