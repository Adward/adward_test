//------------------------------------------------------------------------------
// File:          flash.c
// Function:      Flash functions
// Supported chip(s):
//    - SAM3X8
//    - SAM3X4
//    - SAM3X2
// Supported toolchain(s):
//    - IAR Embedded Workbench
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------

#include "flash.h"
#include "flash_efc.h"

#ifdef USE_FLASH

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

/**
 * Name:      flash_init
 * Purpose:   Initializes the flash (nothing to do)
 */
void flash_init_bl() {

  //debug_printf("Flash: Initializing ...\n");
  //FLASHD_Initialize(BOARD_MCK, 1);
  flash_init(FLASH_ACCESS_MODE_128, 6);
  //debug_printf("Flash: Initialization done.\n");
}

/**
 * Name:      flash_cleanup
 * Purpose:   Cleans up the flash (nothing to do)
 */
void flash_cleanup() {

  //debug_printf("Flash: Cleaning up ...\n");
  //debug_printf("Flash: Cleanup done.\n");
}

/**
 * Name:      flash_write
 * Purpose:   Writes a page in the flash memory
 * Inputs:
 *  - Destination address in the flash
 *  - Source data buffer
 * Output: OK if write is successful, ERROR otherwise
 */
unsigned int flash_write_bl(void * vAddress, void * vData) {

    uint32_t rc;
    __DSB();
    __ISB();
    __disable_irq();
    __DMB();
//    rc = FLASHD_Write((uint32_t)vAddress, vData, IFLASH_PAGE_SIZE);
	rc = flash_write((uint32_t)vAddress, vData, IFLASH_PAGE_SIZE, 0);
    __DMB();
    __DSB();
    __ISB();
    __enable_irq();
    if (rc)
        return ERROR;
    return OK;
}

/**
 * Name:      flash_next
 * Purpose:   Computes next page address after (or starting at) specified address
 * Input:
 *  - Address
 * Output:    Next page address
 */
void * flash_next(void * vAddress) {

  // Local variables
  unsigned int address = (unsigned int) vAddress;

  if (address%MEMORY_PAGE_SIZE != 0) {

    address += (MEMORY_PAGE_SIZE - address%MEMORY_PAGE_SIZE);
  }

  // Check if address is remapped
  if (address < (unsigned int) MEMORY_START_ADDRESS) {

    address += (unsigned int) MEMORY_START_ADDRESS;
  }

  return (void *) address;
}

/**
 * Name:      flash_lock
 * Purpose:   Sets the lock bit for the specified memory region
 * Input:
 *  - Start address of region
 *  - End address of region
 * Output:    OK if lock is successful, ERROR otherwise
 */
unsigned int flash_lock_bl(void * vStart, void * vEnd) {

    uint32_t uStart = (uint32_t)vStart, uEnd = (uint32_t)vEnd;
    uint32_t rc;
    if (uStart < IFLASH0_ADDR)
        uStart = IFLASH0_ADDR;
    if (uEnd > IFLASH_END || uEnd == 0)
        uEnd = IFLASH_END;
    if (uStart >= uEnd)
        return ERROR;
    __DSB();
    __ISB();
    __disable_irq();
    __DMB();
    //rc = FLASHD_Lock(uStart, uEnd, NULL, NULL);
	rc = flash_lock(uStart, uEnd, NULL, NULL);
    __DMB();
    __DSB();
    __ISB();
    __enable_irq();
    if (rc)
        return ERROR;
    return OK;
}

/**
 * Name:      flash_unlock
 * Purpose:   Clears the lock bit for the specified memory region
 * Input:
 *  - Start address of the region to unlock
 *  - End address of the region to lock
 * Output:    OK if lock is successful, ERROR otherwise
 */
unsigned int flash_unlock_bl(void * vStart, void * vEnd) {

    uint32_t uStart = (uint32_t)vStart, uEnd = (uint32_t)vEnd;
    uint32_t rc;
    if (uStart < IFLASH0_ADDR)
        uStart = IFLASH0_ADDR;
    if (uEnd > IFLASH_END || uEnd == 0)
        uEnd = IFLASH_END;
    if (uStart >= uEnd)
        return ERROR;
    __DSB();
    __ISB();
    __disable_irq();
    __DMB();
    //rc = FLASHD_Unlock(uStart, uEnd, NULL, NULL);
	rc = flash_unlock(uStart, uEnd, NULL, NULL);
    __DMB();
    __DSB();
    __ISB();
    __enable_irq();
    if (rc)
        return ERROR;
    return OK;
}

#endif // USE_FLASH
