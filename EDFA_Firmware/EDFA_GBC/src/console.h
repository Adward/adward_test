/*
 * console.h
 *
 * Created: 2015/12/1 18:32:20
 *  Author: Administrator
 */ 
#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <asf.h>
#include "pio.h"

//--------------------------------------------------------------------------------------------
typedef int (*CMDPROC)(int argc, char *argv[]);
//--------------------------------------------------------------------------------------------
// Command Struct
//--------------------------------------------------------------------------------------------
typedef struct
{
	const char *cmd;
	const char *hlp;
	CMDPROC	cmdproc;
	int	isPubCmd;
} CMD_STRUCT_t;


/************************************************************************/
/* Operation Mode Macro                                                 */
/************************************************************************/
//U17
#define activeOE pio_set_pin_high(SW_Latch_Qout);
#define disactOE pio_set_pin_low(SW_Latch_Qout);
//U19
#define activeLE pio_set_pin_high(SW_Latch_Din);		//74373 Qout no changes.
#define disactLE pio_set_pin_low(SW_Latch_Din);			//74373 Qout=Din.
//U5B
#define activeACC pio_set_pin_low(SW_FEEDBACK);
#define activeALC pio_set_pin_high(SW_FEEDBACK);
//U6A
#define activeDIS pio_set_pin_low(SW_Output);
#define activeLASER pio_set_pin_high(SW_Output);
//U6B
#define activeMUTE pio_set_pin_high(SW_PID);
#define activeSET pio_set_pin_low(SW_PID);
//U5A
#define activeAPC pio_set_pin_low(SW_APC);
#define activeAOPC pio_set_pin_high(SW_APC);

#define swDIS {\
	activeDIS;\
	disactLE;\
	activeOE;\
	activeLE;\
}

#define swMUTE {\
	activeACC;\
	activeMUTE;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}

#define swACC {\
	activeACC;\
	activeSET;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}

#define swALC {\
	activeALC;\
	activeSET;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}

/* 2016-06-13, add activeLASER, activeSET */
/* 2016-05-12, add "activeALC;\". for NP-2000. */
#define swAPC {\
	activeSET;\
	activeAPC;\
	activeALC;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}
/* 2016-06-13, add activeALC, activeLASER, activeSET */
#define swAOPC {\
	activeSET;\
	activeAOPC;\
	activeALC;\
	activeLASER;\
	disactLE;\
	activeOE;\
	activeLE;\
}

//--------------------------------------------------------------------------------------------
// Private Command sets
//--------------------------------------------------------------------------------------------
int cmd_VER(int argc);
int cmd_help(int argc, char *argv[]);
int cmd_IV(int argc);
int cmd_RST(int argc);
int cmd_BOOT(int argc);
int cmd_ECHO(int argc, char *argv[]);
int cmd_BAUD(int argc, char *argv[]);
int cmd_Set_RTC(int argc, char *argv[]);
int cmd_STORE(int argc);
int cmd_MODE(int argc, char *argv[]);
int cmd_LOS(int argc, char *argv[]);
int cmd_MST(int argc);
int cmd_PIN(int argc);
int cmd_POUT(int argc);
int cmd_PUMP(int argc, char *argv[]);
int cmd_ALRM(int argc, char *argv[]);
int cmd_MT(int argc);
int cmd_AST(int argc);
int cmd_ASTM(int argc, char *argv[]);
int cmd_VERSION(int argc, char *argv[]);
int cmd_private(int argc, char *argv[]);
int cmd_exit(int argc);
int  cmd_REE(int argc, char *argv[]);
int  cmd_WEE(int argc, char *argv[]);
int  cmd_CC(int argc, char *argv[]);

/* 2016-07-22 */
int cmd_CAL(int argc, char *argv[]);
int cmd_DBG(int argc, char *argv[]);

/* 2016-08-09 */
int cmd_WDG(int argc, char *argv[]);

CMD_STRUCT_t  Private_Mode_CMD[] =
{
	{"VER","     Displays serial number and version information.",cmd_VER,TRUE},
	{"ECHO","    Sets and get the echoing of the command line to ON or OFF.",cmd_ECHO,TRUE},
	{"BAUD","    Sets and Get the baud rate.",cmd_BAUD,TRUE},
	{"MODE","    Sets and get the amplifier control with gain/output power val.",cmd_MODE,TRUE},
	//{"PLIM","    Sets and get the power limit in gain control mode to limit dBm.",cmd_PLIM,TRUE},
	//{"GLIM","    Sets and get the gain limit in power control mode to limit dB.",cmd_GLIM,TRUE},
	{"PIN","     Displays input power.",cmd_PIN,TRUE},
	{"POUT","    Displays total output power.",cmd_POUT,TRUE},
	//{"PSIG","    Displays signal output power.",cmd_PSIG,TRUE},
	//{"GAIN","    Displays the signal power is total power less estimated ASE power.",cmd_GAIN,TRUE},
	//{"OFG","     Displays the optimal flat gain setting.",cmd_OFG,TRUE},
	//{"RFL","     Displays the back reflection power in dB.",cmd_RFL,TRUE},
	{"MT","      Displays case temperature in degrees C.",cmd_MT,TRUE},
	{"PUMP","    Displays status of the pump.Sets pump current to mA.",cmd_PUMP,TRUE},
	{"ALRM","    Displays alarms information and sets threshold or hysteresis alarm.",cmd_ALRM,TRUE},
	{"AST","     Lists all alarms whose status is on normal or latching if alarm.",cmd_AST,TRUE},
	{"ASTM","    Sets and get alarm mode to normal (N) or latching (S-sticky) mode.",cmd_ASTM,TRUE},
	{"MST","     Displays amplifier status.",cmd_MST,TRUE},
	{"LOS","     Sets and get the behavior of the amplifier on input LOS.",cmd_LOS,TRUE},
	{"RST","     Restores all setting to factory default values.",cmd_RST,TRUE},
	//{"PD","      Get all PD power.",cmd_PD,TRUE},
	//{"AMP_TYPE","Set and get the Amp_type for B or P.",cmd_AMPTYPE,TRUE},
	//{"POUT_N","  Determines the pout for no input signal and AGC LOSN mode.",cmd_POUTN,TRUE},
	{"BOOT","    Resets the firmware.",cmd_BOOT,TRUE},
	{"HELP","    Requests all Supported Commands.",cmd_help,TRUE},
	//{"PRIVATE"," Enter Engineering mode",cmd_private,TRUE},
	//{"RECV","    Upgrade Application Code.",cmd_RECV,TRUE},
	//{"EXIT","    exit Engineering mode",cmd_exit,TRUE},
	//{"ADR","     ADC result read",ADC_read,TRUE},
	//{"ADCAL","   ADC Calibration value A,B,C,D,E,F,G,I,J",ADC_CAL,TRUE},
	//{"DVR","     Digital Variable resistor",cmd_DVR,TRUE},
	//{"DAC","     Digital to Analog",cmd_DAC,TRUE},
	//{"DACL","    Digital to Analog",cmd_DACL,TRUE},
	{"CC","      Current control",cmd_CC,TRUE},
	//{"xOUTP","   SET OUTPUT POWER",cmd_OUTP,FALSE},
	{"REE","     Read EEPROM ",cmd_REE,TRUE},
	{"WEE","     Write EEPROM ",cmd_WEE,TRUE},
	//{"ACC","     ACC / ALC ",cmd_ACC,FALSE},
	//{"RS","      Reset Register status ",cmd_RS,FALSE},
	{"IV","      Initial Value ",cmd_IV,FALSE},
	//{"CR","      Initial Value ",cmd_CR,FALSE},
	//{"SET_SN","  Set Serial Number ",cmd_Set_SN,FALSE},
	//{"SET_HW_VER","  Set HW Version ",cmd_Set_HW,FALSE},
	//{"SET_PUMP_EOL","  Set Pump EOL ",cmd_Set_EOL,FALSE},
	//{"SET_AUTO_PUMP","  Set Auto Pump ",cmd_Set_AutoPump,FALSE},
	{"RTC","     Set Real Time Clock:YYYY M D HH mm ss ",cmd_Set_RTC,FALSE},
	{"STORE","   Write current module settings to non-volatile memory.",cmd_STORE,TRUE},
	{"VERSION"," Firmware Upgrade or Switch Version.",cmd_VERSION,TRUE},
	
	/* 2016-07-22 */
	{"CAL", "     write calibration parameters to the EEPROM.", cmd_CAL, TRUE},
	{"DBG", "     debug.", cmd_DBG, TRUE},
	
	/* 2016-09-08 */	
	{"WDG", "     Test watchdog", cmd_WDG, TRUE},

	{"EXIT","    exit Engineering mode",cmd_exit,TRUE},		
	{NULL,NULL,NULL,FALSE}
};

//--------------------------------------------------------------------------------------------
//	Public Command sets
//--------------------------------------------------------------------------------------------
int cmd_FLINE(int argc);
int cmd_EDFA(int argc, char *argv[]);
int cmd_GEDFA(int argc);
int cmd_npMODE(int argc, char *argv[]);
int cmd_FLASH(int argc);
int cmd_GMODE(int argc);
int cmd_LDC(int argc, char *argv[]);
int cmd_GLDC(int argc);
int cmd_LDP(int argc, char *argv[]);
int cmd_GLDP(int argc);
int cmd_AOPC(int argc, char *argv[]);
int cmd_GAOPC(int argc);
int cmd_PLDP(int argc, char *argv[]);
int cmd_PAOPC(int argc, char *argv[]);
int cmd_PLDC(int argc, char *argv[]);
int cmd_LOS_Level(int argc, char *argv[]);
int cmd_LOP_Level(int argc, char *argv[]);
int cmd_GALRM(int argc);
int cmd_GALRMB(int argc);
int cmd_PWRUP(int argc, char *argv[]);
int cmd_LIPS(int argc, char *argv[]);
int cmd_DIAG(int argc);
int cmd_CRSR(int argc, char *argv[]);
int cmd_IMSG(int argc, char *argv[]);
int cmd_SAVES(int argc);
int cmd_Disable(int argc);

/* 2016-05-25 */
int cmd_GVER(int argc);
int cmd_GMOD(int argc);
int cmd_GSN(int argc);

/* 2016-08-09 */
int cmd_AGC(int argc, char *argv[]);
int cmd_GAGC(int argc);
int cmd_PAGC(int argc, char *argv[]);


CMD_STRUCT_t  SYS_INNER_CMD[] =
{
	{"ECHO","     Sets and get the echoing of the command line to ON or OFF.",cmd_ECHO,TRUE},
	{"HELP","     Requests all Supported Commands.",cmd_help,TRUE},
	{"FLASH","    Display operation parameters and update working mode.",cmd_FLASH,TRUE},
	{"FLINE","    Display operation parameters.",cmd_FLINE,TRUE},
	{"EDFA","     Turn EDFA On or OFF.",cmd_EDFA,TRUE},
	{"GEDFA","    Get/readback setting EDFA ON/OFF state.",cmd_GEDFA,TRUE},
	{"MODE","     Specify Operating mode.",cmd_npMODE,TRUE},
	{"GMODE","    Get/readback operation mode of EDFA.",cmd_GMODE,TRUE},	
	{"LDC","      Set LD operation current in ACC mode.",cmd_LDC,TRUE},
	{"GLDC","     Get/readback current setting of the Laser Pump in mA units.",cmd_GLDC,TRUE},
	{"LDP","      Set operating Laser Pump Power in APC mode.",cmd_LDP,TRUE},	
	{"GLDP","     Get/readback power setting of Laser Pump in mW.",cmd_GLDP,TRUE},	
	{"AOPC","     Set output power of EDFA in AOPC mode.",cmd_AOPC,TRUE},
	{"GAOPC","    Get/readback Output Power setting for AOPC mode in dBm units.",cmd_GAOPC,TRUE},
	
	/* 2016-08-09 */
	{"AGC","      Set/readback Output Power setting for AGC mode in dB units.",cmd_AGC,TRUE},
	{"GAGC","     Get/readback Output Power setting for AGC mode in dB units.",cmd_GAGC,TRUE},
	{"PAGC","     Preset/Default operating output power in AGC mode.",cmd_PAGC,TRUE},
		
	{"PLDP","     Preset/default LD operating power upon power up in APC mode.",cmd_PLDP,TRUE},
	{"PLDC","     Preset/default LD operating power upon power up in ACC mode.",cmd_PLDC,TRUE},
	{"PAOPC","    Preset/Default operating output power in AOPC mode.",cmd_PAOPC,TRUE},
	{"INPALMLVL","Set Input Power Alarm Level in dBm.",cmd_LOS_Level,TRUE},
	{"OPTALMLVL","Set Output Power Alarm Level in dBm.",cmd_LOP_Level,TRUE},
	{"GSN","      Get/Read Serial Number.",cmd_GSN,TRUE},
	{"GVER","     Get/Read Firmware Version.",cmd_GVER,TRUE},
	{"GMOD","     Get/Read EDFA Model Number.",cmd_GMOD,TRUE},
	{"GALARM","   Get/Read Alarms Detected.",cmd_GALRM,TRUE},
	{"GALARMB","  Get/Read Alarm bitwise display.",cmd_GALRMB,TRUE},
	{"SAVES","    Write parameters to non-voltile memory.",cmd_SAVES,TRUE},	
	{"PWRUP","    Set EDFA default ON/OFF state upon DC power-up.",cmd_PWRUP,TRUE},	
	{"LIPS","     Enable Automatic Laser shutdown during absence of input.",cmd_LIPS,TRUE},
	{"DIAG","     Show operating status and errors.",cmd_DIAG,TRUE},
	{"DISABLE","  Check the status of Amplifier disable input.",cmd_Disable,TRUE},	
	{"CRSR","     Turn prompt 'Optoplex:>' display ON/OFF.",cmd_CRSR,TRUE},	
	{"IMSG","     Set Initialization message display ON/OFF.",cmd_IMSG,TRUE},
	{"PRIVATE","  Enter Engineering mode.",cmd_private,FALSE},
	{"IV","       Initial Value ",cmd_IV,FALSE},
	{NULL,NULL,NULL,FALSE}
};

#endif // __CONSOLE_H__