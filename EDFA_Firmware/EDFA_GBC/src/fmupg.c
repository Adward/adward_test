/*
 * fmupg.c
 *
 * Created: 2016/2/4 17:05:32
 *  Author: Administrator
 */ 

#include <asf.h>
#include "stdlib.h"
//#include <string.h>
#include "conf_example.h"
//#include "console.h"
#include "flash.h"
#include "Signature.h"

#define WAITING_MAX_RETRY 15
#define FRAME_SIZE DATA_SIZE + 5
#define FRAME_HEADER_SIZE 3
#define SECTOR_SIZE 0x10000

static uint8_t rx_char, *frame;
static uint16_t DATA_SIZE;
static uint32_t start_address = TEMP_C_ADDRESS;


/**
 * Name:      flash_unlock
 * Purpose:   Clears the lock bit for the specified memory region
 * Input:
 *  - Start address of the region to unlock
 *  - End address of the region to lock
 * Output:    OK if lock is successful, ERROR otherwise
 */
unsigned int flash_unlock_bl(void * vStart, void * vEnd) {

    uint32_t uStart = (uint32_t)vStart, uEnd = (uint32_t)vEnd;
    uint32_t rc;
    if (uStart < IFLASH0_ADDR)
        uStart = IFLASH0_ADDR;
    if (uEnd > IFLASH_END || uEnd == 0)
        uEnd = IFLASH_END;
    if (uStart >= uEnd)
        return ERROR;
    __DSB();
    __ISB();
    __disable_irq();
    __DMB();
    //rc = FLASHD_Unlock(uStart, uEnd, NULL, NULL);
	rc = flash_unlock(uStart, uEnd, NULL, NULL);
    __DMB();
    __DSB();
    __ISB();
    __enable_irq();
    if (rc)
        return ERROR;
    return OK;
}

// Check frame header is valid
static bool xmodem_check_frame_header(uint8_t *frame) {
	return ((frame[1] + frame[2]) == 0xFF);
}

// Calculate the frame CRC and validate against the last 2 bytes
static bool xmodem_check_frame_crc(uint8_t *frame) {
	uint16_t crc = 0;
	char i;
	int count = DATA_SIZE;
	uint8_t *ptr = frame + FRAME_HEADER_SIZE;
	
	while (--count  >= 0) {
		crc = crc ^ (int) *ptr++ << 8;
		i = 8;
		do {
			if (crc & 0x8000) {
				crc = crc << 1 ^ (0x1021);
				}else{
				crc = crc << 1;
			}
		}while(--i);
	}

	// Compare the crc with the last 2 bytes of frame

	uint8_t crc_high =  crc >> 8;  // the HI CRC byte
	uint8_t crc_low =  crc & 0X00FF;  // The LOW CRC byte

	bool crc_high_equals = (crc_high == frame[DATA_SIZE + FRAME_HEADER_SIZE]);
	bool crc_low_equals  = (crc_low == frame[DATA_SIZE + FRAME_HEADER_SIZE + 1]);

	return (crc_high_equals && crc_low_equals);

}


static bool xmodem_receive_frame(uint8_t *frame_1K, uint8_t *frame_128) {
	
	uint16_t i = 0;
	uint8_t rx_char, rx_end;
	uint16_t vsize;
	
	// frame: SOH 0x01 0xFE (data, 1024 bit) CRC CRC

	rx_end = FALSE;
	frame = frame_1K;
	DATA_SIZE = 1024;
	vsize=1029;
	
	
	do {
		if(!uart_read(CONF_UART, &rx_char)) {
			
			frame[i++] = rx_char;
			
			if (i==1 && rx_char==(char) (0x01)) { //S0H
				DATA_SIZE = 128;
				vsize = 133;	//5+133
				frame_128[0] = frame_1K[0];
				frame = frame_128;
			}
			
			if (i==1 && rx_char == (char)(0x04)) { // first character is EOT, return false
				rx_end = TRUE;
				break;
			}
			
		}
	} while(i < vsize);
	return rx_end;
}

static void xmodem_send_char(char c) {
	uart_write(CONF_UART, c);
	while(!(CONF_UART->UART_SR & UART_SR_TXRDY));
}

static bool xmodem_firmware_write(uint8_t *frame) {
	
	uint8_t *ptr = frame;
	ptr +=  3;  // Skip frame header
	
	flash_write(start_address, ptr, DATA_SIZE ,0); //transfer firmware to firmware download buffer
	start_address = start_address + DATA_SIZE;
	
	return true;
}

static int xmodem_receive(uint8_t *frame_1K, uint8_t *frame_128){
	uint16_t c_sent;
	bool is_EOT, frame_valid;
	
	c_sent = 0;
	frame_valid = false;
	is_EOT = false;
	
	/************************************************************************/
	/* Initial state RECEIVER transmit 'C' to TRANSMITTER                    */
	/************************************************************************/
	g_ul_ms_ticks = 3001;
	do {
		// sending character c every 3 seconds
		if( g_ul_ms_ticks > 3000 ) {
			g_ul_ms_ticks = 0;
			xmodem_send_char((char)(0x43));	// 'C' start char
			c_sent++;
			
			// exit loop if all C's have been sent
			if( c_sent >= WAITING_MAX_RETRY ) {
				return 0; // by guo
				break;
			}
		}
	} while (!uart_is_rx_ready(CONF_UART));
	
	/************************************************************************/
	/* Start to receive all packets                                         */
	/************************************************************************/
	do {
		//is_EOT = xmodem_receive_frame(frame, FRAME_SIZE);
		is_EOT = xmodem_receive_frame(frame_1K, frame_128);
		
		frame_valid = xmodem_check_frame_header(frame) && xmodem_check_frame_crc(frame) ;
		
		if( is_EOT ) {
			xmodem_send_char((char)(0x06));		// send ACK
			//break;
			} else {
			if( frame_valid ) {
				xmodem_firmware_write(frame);	// Write frame to region c. (Assume region is already cleaned up)
				xmodem_send_char((char)(0x06)); // send ACK
				} else {
				xmodem_send_char((char)(0x15));	// send NAK
				return 0; // by guo
			}
		}
	} while(!is_EOT);
	
	return 1;	//by guo
}

// Reset the content before write
static void xmodem_clean_buffer_before_write(uint32_t address) {
	flash_erase_sector((uint32_t)address); //prepare program memory area for buffer copy
	flash_erase_sector((uint32_t)(address + SECTOR_SIZE));//allocated 2 sectors for program
}

// Clean prog?,  copy progC -> progA/B
static void xmodem_region_copy(uint32_t destAddress) {
	// clean prog?
	xmodem_clean_buffer_before_write(destAddress);
	
	// Copy
	flash_write(destAddress, (uint8_t *)TEMP_C_ADDRESS, SECTOR_SIZE * 2 ,0); //transfer firmware to firmware download buffer
}

int cmd_VERSION(int argc, char *argv[]) {
	
	
	signatureInfo_t *signature = malloc(sizeof(signatureInfo_t));
	
	
	
	flash_read_user_signature(signature, sizeof(*signature));
	if(argc == 1) {
		if ((signature->Loaded_Ver==VerA) || (signature->Loaded_Ver==VerDefault))
			printf("Loaded[%d]\n\r", 1);
		else
			printf("Loaded[%d]\n\r", 2);
		
		free(signature);	
		return 0;
	}
	else if(argc == 2) {
		uint8_t guo=FALSE;
		uint8_t saveSignature = FALSE;
		uint8_t *frame_1K = malloc(1029*sizeof(uint8_t));
		uint8_t *frame_128 = malloc(133*sizeof(uint8_t));
		uint32_t	PROG_ADDRESS;
		
		if(CmdStrCompare(argv[1],"DOWNLOAD")) {
			/* Enable UART interrupt */
			NVIC_DisableIRQ(CONSOLE_UART_IRQn);
			
			// Unlock all memory //
			memory_unlock(MEMORY_START_ADDRESS, NULL);
			xmodem_clean_buffer_before_write(TEMP_C_ADDRESS);
			
			// XMODEM Starts //
			PROG_ADDRESS = (signature->Loaded_Ver==VerA) ? PROG_B_ADDRESS : PROG_A_ADDRESS;
			guo=xmodem_receive(frame_1K, frame_128);
			
			// Receive done. Copy progC -> prog? and sign.
			if (guo) {
				xmodem_region_copy(PROG_ADDRESS);
				if ((PROG_ADDRESS == PROG_A_ADDRESS) || (signature->Loaded_Ver==VerDefault)) {
					signature->VerB_Info.s1 = 0xEE;
					signature->VerB_Info.s2 = 0x11;
					signature->VerB_Info.s3 = 0xAA;
					signature->VerB_Info.s4 = 0x55;
					saveSignature = TRUE;
				} else {
					signature->VerA_Info.s1 = 0x55;
					signature->VerA_Info.s2 = 0xAA;
					signature->VerA_Info.s3 = 0x11;
					signature->VerA_Info.s4 = 0xEE;
					saveSignature = TRUE;
				}	
			} // if (guo)
			
			
			
			/* Enable UART interrupt */
			NVIC_EnableIRQ(CONSOLE_UART_IRQn);
			printf("......DONE\n\r");
		}// if(CmdStrCompare(argv[1],"DOWNLOAD"))
		
		else if(CmdStrCompare(argv[1],"AUTO")) {
			signature->Requested_Ver = VerA;
			saveSignature = TRUE;
			printf("OK.\n\r");
		}
		else if(CmdStrCompare(argv[1],"1")) {
			signature->Requested_Ver = VerA;
			saveSignature = TRUE;
			printf("OK\n\r");
		}
		else if(CmdStrCompare(argv[1],"2")) {
			signature->Requested_Ver = VerB;
			saveSignature = TRUE;
			printf("OK\n\r");
		}
		else if(CmdStrCompare(argv[1],"B")) {
			signature->Loaded_Ver = VerDefault;	//Enter Bootloader
			saveSignature = TRUE;
			printf("OK\n\r");
		}
		else {
			printf("?Parameters Error\n\r");
		}
		
		if (saveSignature) {
			flash_erase_user_signature();
			flash_write_user_signature(signature,sizeof(*signature));
		}
		
		free(frame_1K);
		free(frame_128);
		free(signature);
		return 0;
	}
	else {
		printf("?Parameters Error\n\r");
		return 0;
	}
	
	// end firmware download
	
	
	return -1;
}
