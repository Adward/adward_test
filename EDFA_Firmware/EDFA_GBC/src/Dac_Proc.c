///////////////////////////////////////////////////////////////////////////////
//
//	Functions for DAC 
//
///////////////////////////////////////////////////////////////////////////////
#include <asf.h>
#include "arm_math.h"
#include "conf_example.h"
#include "Dac_Proc.h"
///////////////////////////////////////////////////////////////////////////////
struct spi_device SPI_DEVICE1 = {
	.id = SPI_CH_DAC1
};
struct spi_device SPI_DEVICE2 = {
	.id = SPI_CH_DAC3
};
struct spi_device SPI_DEVICE3 = {
	.id = SPI_CH_DAC2
};

uint16_t	Offset_DAC=0;

/************************************************************************
* @brief: Write to and update address of external DAC1
*         CHA: ILD_Limit
*         CHB: ASE Compensation value
************************************************************************/
void DAC1_Output(uint16_t DAC_Value, DAC_CH_t CH) {
	uint8_t DAC_data[3];

	DAC_data[0] = CH;
	DAC_data[1] = (uint8_t)(DAC_Value >> 8);
	DAC_data[2] = (uint8_t)(DAC_Value);
	
	spi_select_device(EDFA_SPI, &SPI_DEVICE1);
	OnSPI_Write(EDFA_SPI, DAC_data, 3);
	spi_deselect_device(EDFA_SPI, &SPI_DEVICE1);
}

/************************************************************************
* @brief: Write to and update address of external DAC2
*         CHA: 0~3Volt for 3rd LASER control
*         CHB: No used
************************************************************************/
void DAC2_Output(uint16_t DAC_Value, DAC_CH_t CH) {
	uint8_t DAC_data[3];

	DAC_data[0] = CH;
	DAC_data[1] = (uint8_t)(DAC_Value >> 8);
	DAC_data[2] = (uint8_t)(DAC_Value);
	
	spi_select_device(EDFA_SPI, &SPI_DEVICE2);
	OnSPI_Write(EDFA_SPI, DAC_data, 3);
	spi_deselect_device(EDFA_SPI, &SPI_DEVICE2);
}

/************************************************************************
* @brief: Write to and update address of external DAC3
*         CHA: Input optical power monitor analog out
*         CHB: Output optical power monitor analog out
************************************************************************/
void DAC3_Output(uint16_t DAC_Value, DAC_CH_t CH) {
	uint8_t DAC_data[3];

	DAC_data[0] = CH;
	DAC_data[1] = (uint8_t)(DAC_Value >> 8);
	DAC_data[2] = (uint8_t)(DAC_Value);
	
	spi_select_device(EDFA_SPI, &SPI_DEVICE3);
	OnSPI_Write(EDFA_SPI, DAC_data, 3);
	spi_deselect_device(EDFA_SPI, &SPI_DEVICE3);
}



void DAC_Output(uint16_t DAC_Value, uint8_t Dir)
{
	uint8_t DAC_data[3], DAC_Value_H, DAC_Value_L;

	//-------------------------------------------------
	DAC_Value_H = (DAC_Value) >> 8;
	DAC_Value_L = (DAC_Value);

	//----------------------------------------------
	if (Dir > 0)	// DAC Channel A control long-WL
	{
		//----------------------------------------------
		// Write DAC channel-C
		DAC_data[0] = DAC_WRITE_REGISTER_B;			// | ADDRESS_DAC_B;
		DAC_data[1] = (Offset_DAC) >> 8;
		DAC_data[2] = (Offset_DAC);
		//OnEDFA_SPI_CS(EDFA_SPI, SPI_CH_DAC1);
		spi_select_device(EDFA_SPI, &SPI_DEVICE1);
		OnSPI_Write(EDFA_SPI, DAC_data, 3);			// Write to SPI port to DAC
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE1);
		//----------------------------------------------
		// Write DAC channel-A and update ALL
		DAC_data[0] = DAC_WRITE_A_UPDATE_ALL;
		DAC_data[1] = DAC_Value_H;
		DAC_data[2] = DAC_Value_L;
		//OnEDFA_SPI_CS(EDFA_SPI, SPI_CH_DAC1);
		spi_select_device(EDFA_SPI, &SPI_DEVICE1);
		OnSPI_Write(EDFA_SPI, DAC_data, 3);			// Write to SPI port to DAC
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE1);
	}
	else	// DAC Channel C control short-WL
	{
		//----------------------------------------------
		// Write DAC channel-A
		DAC_data[0] = DAC_WRITE_REGISTER_A;			// | ADDRESS_DAC_A;
		DAC_data[1] = (Offset_DAC) >> 8;
		DAC_data[2] = (Offset_DAC);
		//OnEDFA_SPI_CS(EDFA_SPI, SPI_CH_DAC1);
		spi_select_device(EDFA_SPI, &SPI_DEVICE1);
		OnSPI_Write(EDFA_SPI, DAC_data, 3);			// Write to SPI port to DAC
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE1);

		//----------------------------------------------
		// Write DAC channel-C and update ALL
		DAC_data[0] = DAC_WRITE_B_UPDATE_ALL;
		DAC_data[1] = DAC_Value_H;
		DAC_data[2] = DAC_Value_L;
		//OnEDFA_SPI_CS(EDFA_SPI, SPI_CH_DAC1);
		spi_select_device(EDFA_SPI, &SPI_DEVICE1);
		OnSPI_Write(EDFA_SPI, DAC_data, 3);			// Write to SPI port to DAC
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE1);
	}
}