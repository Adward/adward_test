/*
 * IOtest.c
 *
 * Created: 2015/12/1 18:09:52
 *  Author: Administrator
 */ 
/* Get value from button and output it on led */
		//ioport_set_pin_level(EXAMPLE_LED,
		//		ioport_get_pin_level(EXAMPLE_BUTTON));
#include <asf.h>
#include "conf_example.h"

/** 
 * Get value from button and output it on led 
 * ioport_set_pin_level(EXAMPLE_LED,
 * ioport_get_pin_level(EXAMPLE_BUTTON));
*/
void pin_edge_handler(const uint32_t id, const uint32_t index)
{
	//if ((id == ID_PIOA) && (index == PIO_PA19)){
	if ((id == PIOD) && (index == PIO_PD25)){
		//if (pio_get(PIOA, PIO_TYPE_PIO_INPUT, PIO_PA19))
		if (pio_get(PIOD, PIO_TYPE_PIO_INPUT, PIO_PD25)) {
		//what to do
		//pio_clear(PIOA, PIO_PA0);
		} else {
		//what to do
		//pio_set(PIOA, PIO_PA0);
		}
	}
}

/**
 *  Configure IO for external interrupt test.
 */
void configure_IO(void)
{
	/* Insert application code here, after the board has been initialized. */
	

	/* Set direction and pullup on the given button IOPORT */
	//ioport_set_pin_dir(EXAMPLE_BUTTON, IOPORT_DIR_INPUT);
	//ioport_set_pin_mode(EXAMPLE_BUTTON, IOPORT_MODE_PULLUP);
	
	/* Configure PCK */
	/*
	pmc_switch_pck_to_pllack(PMC_PCK_0, PMC_PCK_PRES_CLK_1);
	ioport_set_pin_mode(PIN_PCK0, PIN_PCK0_MUX);
	ioport_disable_pin(PIN_PCK0);
	pmc_enable_pck(PMC_PCK_0);
	*/

	/* PIO Interrupt.  Serve for RESET, DISABLE and MUTE. */
	//pmc_enable_periph_clk(ID_PIOA);
	pmc_enable_periph_clk(ID_PIOD);
	
	//pio_set_output(PIOA, PIO_PA0, HIGH, DISABLE, ENABLE);
	
	//pio_set_input(PIOA, PIO_PA19, PIO_PULLUP);
	pio_set_input(PIOD, PIO_PD25, PIO_PULLUP);
	
	// pio_handler_set(PIOA, ID_PIOA, PIO_PA19, PIO_IT_EDGE, pin_edge_handler);
	// pio_handler_set(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_ID, PIN_PUSHBUTTON_1_MASK, PIO_IT_EDGE, pin_edge_handler);
	/* 2016-05-31, RESET pin from MSA pin12 */
	// pio_handler_set(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_ID, PIN_PUSHBUTTON_1_MASK, PIO_IT_EDGE, pin_edge_handler);
	pio_handler_set(PIOD, ID_PIOD, PIO_PD25, PIO_IT_EDGE, pin_edge_handler);

	// pio_enable_interrupt(PIOA, PIO_PA19);
	/* 2016-05-31 */
	// pio_enable_interrupt(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK);
	pio_enable_interrupt(PIOD, PIO_PD25);
	
	//NVIC_EnableIRQ(PIOA_IRQn);
	/* 2016-05-31 */
	// NVIC_EnableIRQ(PIN_PUSHBUTTON_1_IRQn);
	NVIC_EnableIRQ(PIOD_IRQn);

}


