/*
 * console.c
 *
 * Created: 2015/11/30 14:53:18
 *  Author: Administrator
 */ 

#include <asf.h>
#include <string.h>
#include <stdlib.h>
char *itoa(int value, char *string, int radix);
//#include "stdio_serial.h"
#include "conf_example.h"
#include "console.h"

/** All interrupt mask. */
#define ALL_INTERRUPT_MASK  0xffffffff


/** Byte mode read buffer. */
/* 2016-05-25 */
// static uint32_t gs_ul_read_buffer;
static volatile char gs_ul_read_buffer;

static uint32_t cmdlen = 0;

/* 2016-07-12 */
volatile unsigned int queue_head = 0, queue_tail = 0, queue_head_next = 0;
char rx_queue[QUEUE_SIZE][BUFFER_SIZE];
int cmd_length_queue[QUEUE_SIZE];
int cmd_got_enterkey[QUEUE_SIZE];


/* 2016-09-05 */
extern volatile float pwr_voltage;


/** Macro **/
#define Check_TmpStatus(a)	(a>0) ? "ON" : "OFF"
#define Check_TmpStatusLatch(a) (a>0) ? "ON" : "OFF"

/**
 *  Configure UART for a console.
 */
void configure_console(void) {
	 uint32_t BAUDRATE;

	if(sysvars_AppSpecParam.AppPrivate.BaudRate == 1)		BAUDRATE = 9600;
	else if(sysvars_AppSpecParam.AppPrivate.BaudRate == 2)	BAUDRATE = 19200;
	else if(sysvars_AppSpecParam.AppPrivate.BaudRate == 3)	BAUDRATE = 38400;
	else if(sysvars_AppSpecParam.AppPrivate.BaudRate == 4)	BAUDRATE = 57600;
	else													BAUDRATE = 115200;
	
	usart_serial_options_t uart_serial_options = {
		//.baudrate = CONF_UART_BAUDRATE,
		.baudrate = BAUDRATE,
		.paritytype = CONF_UART_PARITY
	};

	/* Disable all the UART interrupts. */
	uart_disable_interrupt(CONSOLE_UART, ALL_INTERRUPT_MASK);
	
	/* Enable UART IRQ */
	uart_enable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
	
	/* Enable the receiver and transmitter. */
	uart_enable_tx(CONSOLE_UART);
	uart_enable_rx(CONSOLE_UART);
	
	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Enable UART interrupt */
	NVIC_EnableIRQ(CONSOLE_UART_IRQn);

}


/**
 * \brief Interrupt handler for UART interrupt.
 */
void console_uart_irq_handler(void)
{
	/* Get UART status and check if UART received data coming. */
	if ( (uart_get_status(CONSOLE_UART) & UART_SR_RXRDY) == UART_SR_RXRDY ) {		
		uart_read(CONSOLE_UART, (uint32_t *)&gs_ul_read_buffer);
		
		/* 2016-05-25, enter "123", backspace 3 times, then backspace once, will erase NP2000:> '>'. */
		/*
		if ( 1 == AppCfgMap.v.ECHO ) {
			uart_write(CONSOLE_UART, gs_ul_read_buffer);
		}
		*/

		switch ( gs_ul_read_buffer ) {
					
			case BACK_Key:			
				/*
				if (cmdlen > 0) {
					
					// 2016-05-30, only "ECHO ON" support backspace, "ECHO OFF" not support.
					if ( 1 == AppCfgMap.v.ECHO ) {
						// 2016-05-25, use backspace to delete a key-in character.
						while (uart_write(CONSOLE_UART, BACK_Key))
							;
						while (uart_write(CONSOLE_UART, ' '))
							;
						while (uart_write(CONSOLE_UART, BACK_Key))
							;
					}
					
					cmdlen--;
				}
				*/
				/* 2016-05-25 */
				/*
				else {
					printf(">");
				}
				*/
				/* 2016-07-14 */
				// if ( queue_tail != queue_head ) { }
				if ( cmdlen ) {
					// 2016-05-30, only "ECHO ON" support backspace, "ECHO OFF" not support.
					if ( 1 == AppCfgMap.v.ECHO ) {
						// 2016-05-25, use backspace to delete a key-in character.
						while (uart_write(CONSOLE_UART, BACK_Key))
							;
						while (uart_write(CONSOLE_UART, ' '))
							;
						while (uart_write(CONSOLE_UART, BACK_Key))
							;
					}
						
					cmdlen--;						
				}
							
				break;
			
			
			case ENTER_Key:
				queue_head_next = (queue_head+1) % QUEUE_SIZE;							
				if ( queue_head_next != queue_tail ) {	
					/* 2016-07-13 */
					rx_queue[queue_head][cmdlen++] = '\0';
					cmd_length_queue[queue_head] = cmdlen;
					cmd_got_enterkey[queue_head] = 1;
					queue_head = queue_head_next;
				}
				
				cmdlen = 0;		//reset index to zero
				
				while (uart_write(CONSOLE_UART, 0x0A))
					;

				
			
				/* 2016-05-25, if "ECHO OFF",  return '\r' to console, this line and above line "\n\r". */
				/* there is no NP2000:>
				                    NP2000:>
								          NP2000:>
				*/
				/*
				if ( 0 == AppCfgMap.v.ECHO ) {
					while (uart_write(CONSOLE_UART, 0x0D));
				}
				*/
				while (uart_write(CONSOLE_UART, 0x0D))
					;

				break;
				
			
			default:
				// 2016-05-25
				if ( 1 == AppCfgMap.v.ECHO ) {
					/* 2016-08-08, a bug */
					// uart_write(CONSOLE_UART, gs_ul_read_buffer);
					while (uart_write(CONSOLE_UART, gs_ul_read_buffer))
						;
				}

				// converter lowercase alphabets to uppercase alphabets.
				if ( gs_ul_read_buffer >= 'a' && gs_ul_read_buffer <= 'z' ) {
					gs_ul_read_buffer = gs_ul_read_buffer - 'a' + 'A';
				}

				// preserved a byte for "Enter Key".
				if ( cmdlen < BUFFER_SIZE-1 ) {
					if ( ( queue_head + 1) % QUEUE_SIZE != queue_tail ) {
						rx_queue[queue_head][cmdlen] = gs_ul_read_buffer;
					}
					
					cmdlen++;
				}
						
				break;
				
		} //switch
		

	
	} //if
	
}

/**
* Parsing console input command line.  
Ex: A BC DEF ==> *argc=3, argv[0]=A, argv[1]=B, argv[2]=D
*/
void ParseArgs(char *s, int *argc, char *argv[]) {
	int i,tmpargc;
	PARSE_STAT_t state = STAT_SPACE;

	for(i = 0; s[i] != '\0'; i++)
		if(s[i] == '\t')s[i] = ' ';
	
	tmpargc = 0;
	for(i = 0; s[i] != '\0'; i++)
	{
		if(state == STAT_SPACE) {
			if(s[i] != ' ') {
				argv[tmpargc++] = s + i;
				state = STAT_WORD;
			}
		}
		else {
			if(s[i] == ' ') {
				s[i] = '\0';
				state = STAT_SPACE;
			}
		}
		if(tmpargc > MAX_CMD_ARGS) {
			tmpargc = MAX_CMD_ARGS;
			break;
		}
	}
	*argc = tmpargc;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
int CmdStrCompare(const char *s1, const char *s2) {
	int i = 0;
	while(1) {
		if(s1[i] != s2[i]) return 0;
		if(s1[i++] == '\0')return 1;
	}
}

/************************************************************************
* @brief: Concatenate 2 strings
************************************************************************/
char* concat(char *s1, char *s2)
{
	size_t len1 = strlen(s1);
	size_t len2 = strlen(s2);
	char *result = malloc(len1+len2+1);//+1 for the zero-terminator
	
	if (result == NULL) {
		printf("Out of Memory!!!\n\r");
		return NULL;
	}
	
	memcpy(result, s1, len1);
	memcpy(result+len1, s2, len2+1);//+1 to copy the null-terminator
	
	return result;
}

/************************************************************************
* @brief: Check the input command in Private Command Set
* @param: Input first string
* @return: Found Command index                                                                     
************************************************************************/
int  GetPrivateCmdProcIndex(const char *s) {
	int i = 0;

	while(Private_Mode_CMD[i].cmd != NULL) {
		if(CmdStrCompare(s, Private_Mode_CMD[i].cmd) != 0) {
			return i;
		}
		i++;
	}
	return -1;
}

/************************************************************************
* @brief: Check the input command in System Command Set
* @param: Input first string
* @return: Found Command index
*************************************************************************/

int  GetCmdProcIndex(const char *s) {
	int i = 0;

	while(SYS_INNER_CMD[i].cmd != NULL)
	{
		if(CmdStrCompare(s, SYS_INNER_CMD[i].cmd) != 0)
		{
			return i;
		}
		i++;
	}
	return -1;
}

/************************************************************************
* @brief: Parsing console input command
* @param: Input strings 
* @return: Processed command results                                                                   
*************************************************************************/
int Console_ParseCmd(char *s) {
	int  ProcIndex;
	int  argc;
	char *argv[MAX_CMD_ARGS];
	
	
	/* 2016-06-02 */
	/* Restart the WDT counter. */
	wdt_restart(WDT);
	
	ParseArgs(s, &argc, argv);
	
	if(argc == 0)	return 0;			//It is Enter Key input.
	
	//Private Mode
	if(AppCfgMap.v.Private == 1) {
		ProcIndex = GetPrivateCmdProcIndex(argv[0]);
	
		if(ProcIndex < 0)	return -1;  //It is not a Private Command.
	
		if(Private_Mode_CMD[ProcIndex].cmdproc != NULL) {
			return Private_Mode_CMD[ProcIndex].cmdproc(argc, argv);
		}
		else {
			return 0;					//It is an un-processed Command.
		}
	//} else if (AppCfgMap.v.Private == 0) {
	} else {
		ProcIndex = GetCmdProcIndex(argv[0]);
		
		if(ProcIndex < 0)	return -1;  //It is not a System Command.
		
		if(SYS_INNER_CMD[ProcIndex].cmdproc != NULL) {
			return SYS_INNER_CMD[ProcIndex].cmdproc(argc, argv);
		}
		else {
			return 0;					//It is an un-processed Command.
		}
	}
	
}

/************************************************************************/
/* EDFA Commands                                                        */
/************************************************************************/
//-------------------------------------------------------------------------------------------------
int cmd_help(int argc, char *argv[]) {
	int i;

	if(argc == 1) {
		if(AppCfgMap.v.Private == 1)
		{
			ShowApplicationLogo();
			/* 2016-07-11 */
			// puts("Command List:\r");
			puts("Command List:\n\r");
			for(i = 0; Private_Mode_CMD[i].cmd != NULL; i++) {
				if(Private_Mode_CMD[i].isPubCmd == 1)
				printf("[%s] %s\n\r",Private_Mode_CMD[i].cmd,Private_Mode_CMD[i].hlp);
			}
		}
		
		//if(AppCfgMap.v.Private == 0) {
		else {
			ShowApplicationLogo();
			/* 2016-07-11 */
			// puts("Command List:\r");
			puts("Command List:\n\r");
			for(i = 0; SYS_INNER_CMD[i].cmd != NULL; i++)
			{
				if(SYS_INNER_CMD[i].isPubCmd == 1)
				printf("[%s] %s\n\r",SYS_INNER_CMD[i].cmd,SYS_INNER_CMD[i].hlp);
			}
		}
	}

	return 0;
}


int cmd_CAL(int argc, char *argv[])
{
	// y = ax + b, coefficient a and b.
	double coeff_a, coeff_b;
	unsigned short coeff_c;
	
	printf("execute cmd_CAL().\n\r");
	
	// y = -1.0667x + 1532.8
	// "CAL ACC -1.0667 1532.8"
	if ( 4 == argc ) {
		
		if ( CmdStrCompare(argv[1], "ACC") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);
			
			AppCfgMap.v.R = coeff_a;
			AppCfgMap.v.S = coeff_b;
			
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);				
			printf ("ACC coefficient a and b are %f %f\n\r", AppCfgMap.v.R, AppCfgMap.v.S);
		}
		else if ( CmdStrCompare(argv[1], "AOPC") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);

			//set AOPC Pout, dBm -> DAC.
			AppCfgMap.v.AOPC_a_dac = 0.0;
			AppCfgMap.v.AOPC_b_dac = coeff_a;
			AppCfgMap.v.AOPC_c_dac = coeff_b;

			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);
			printf ("AOPC coefficient a and b are %f %f\n\r" , AppCfgMap.v.AOPC_b_dac, AppCfgMap.v.AOPC_c_dac);
		}
		else if ( CmdStrCompare(argv[1], "APC") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);
			
			AppCfgMap.v.K = 0.0;
			AppCfgMap.v.L = coeff_a;
			AppCfgMap.v.M = coeff_b;

			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);					
			printf ("APC coefficient a and b are %f %f\n\r" , AppCfgMap.v.L, AppCfgMap.v.M);
		}
		else if ( CmdStrCompare(argv[1], "PIN") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);
						
			AppCfgMap.v.A = 0.0;
			AppCfgMap.v.B = coeff_a;
			AppCfgMap.v.C = coeff_b;
			
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);
			printf ("PIN coefficient a and b are %f %f\n\r" , AppCfgMap.v.B, AppCfgMap.v.C);
		}
		else if ( CmdStrCompare(argv[1], "POUT") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);
				
			AppCfgMap.v.POUT_a_AOPC = 0.0;
			AppCfgMap.v.POUT_b_AOPC = coeff_a;
			AppCfgMap.v.POUT_c_AOPC = coeff_b;
			
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);
			printf ("POUT coefficient a and b are %f %f\n\r" , AppCfgMap.v.POUT_b_AOPC, AppCfgMap.v.POUT_c_AOPC);
		}
		/* 2016-08-12 "CAL EOL 1085 0", EOL 1085mA. */
		else if ( CmdStrCompare(argv[1], "EOL") ) {
			coeff_c = atof(argv[2]);
			AppCfgMap.v.PumpBiasEOL = (U16)coeff_c;
			
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);
			printf ("PUMP EOL: %d mA\n\r" , AppCfgMap.v.PumpBiasEOL);
		}
		else if ( CmdStrCompare(argv[1], "APCMW") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);
			
			AppCfgMap.v.APC_a = 0.0;
			AppCfgMap.v.APC_b = coeff_a;
			AppCfgMap.v.APC_c = coeff_b;
			
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);
			printf ("APCMW coefficient a and b are %f %f\n\r" , AppCfgMap.v.APC_b, AppCfgMap.v.APC_c);
		}
		else if ( CmdStrCompare(argv[1], "ILD") ) {
			coeff_a = atof(argv[2]);
			coeff_b = atof(argv[3]);
					
			AppCfgMap.v.I = coeff_a;
			AppCfgMap.v.J = coeff_b;
					
			OnEEPROM_Write_Page((unsigned char*)&AppCfgMap, sizeof(AppCfgMap), Addr_AppCfgMap);
			printf ("ILD coefficient a and b are %f %f\n\r" , AppCfgMap.v.I, AppCfgMap.v.J);
		}
		
	}
	
	if ( 3 == argc ) {
		if ( CmdStrCompare(argv[1], "READ") ) {			
			if ( CmdStrCompare(argv[2], "ACC") ) {
				printf ("ACC coefficient a and b are %f %f\n\r" , AppCfgMap.v.R , AppCfgMap.v.S);
			}
			else if ( CmdStrCompare(argv[2], "AOPC") ) {
				printf ("AOPC coefficient a and b are %f %f\n\r" , AppCfgMap.v.AOPC_b_dac , AppCfgMap.v.AOPC_c_dac);
			}
			else if ( CmdStrCompare(argv[2], "APC") ) {
				printf ("APC coefficient a and b are %f %f\n\r" , AppCfgMap.v.L , AppCfgMap.v.M);
			}
			else if ( CmdStrCompare(argv[2], "PIN") ) {
				printf ("PIN coefficient a and b are %f %f\n\r" , AppCfgMap.v.B , AppCfgMap.v.C);
			}
			else if ( CmdStrCompare(argv[2], "POUT") ) {
				printf ("POUT coefficient a and b are %f %f\n\r" , AppCfgMap.v.POUT_b_AOPC , AppCfgMap.v.POUT_c_AOPC);
			}
			else if ( CmdStrCompare(argv[2], "APCMW") ) {
				printf ("APCMW coefficient a and b are %f %f\n\r" , AppCfgMap.v.APC_b , AppCfgMap.v.APC_c);
			}
			else if ( CmdStrCompare(argv[2], "ILD") ) {
				printf ("PUMP ILD coefficient a and b are %f %f\n\r" , AppCfgMap.v.I , AppCfgMap.v.J);
			}			
			else if ( CmdStrCompare(argv[2], "ACC_DAC") ) {
				printf ("ACC DAC is %f\n\r" , AppCfgMap.v.AD5322Resister);
			}			
			else if ( CmdStrCompare(argv[2], "AOPC_DAC") ) {
				printf ("AOPC DAC is %f\n\r" , NP_rtMODE.powerAD_AOPC);
			}
			else if ( CmdStrCompare(argv[2], "APC_DAC") ) {
				printf ("APC DAC is %f\n\r" , NP_rtMODE.powerAD_APC);
			}
			
		}
		
	}

	return 0;
}


int cmd_DBG(int argc, char *argv[])
{
	int i;
	static uint32_t cmdlen_see = 0;

	/* 2016-07-12 */
	unsigned int queue_head_see, queue_tail_see;
	
	char rx_queue_see[QUEUE_SIZE][BUFFER_SIZE];
	int cmd_length_queue_see[QUEUE_SIZE];
	int cmd_got_enterkey_see[QUEUE_SIZE];
	
	uart_disable_interrupt(CONSOLE_UART, UART_IER_RXRDY);

	cmdlen_see = cmdlen;
	queue_head_see = queue_head;
	queue_tail_see = queue_tail;
	
	for (i=0; i<QUEUE_SIZE; i++) {
		cmd_got_enterkey_see[i] = cmd_got_enterkey[i];
		cmd_length_queue_see[i] = cmd_length_queue[i];
	}
	
	for (i=0; i<BUFFER_SIZE; i++) {
		rx_queue_see[queue_head_see][i] = rx_queue[queue_head][i];
		rx_queue_see[queue_tail_see][i] = rx_queue[queue_tail][i];
	}


	uart_enable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
	
	printf("queue_head is %d\n\r", queue_head_see);
	printf("queue_tail is %d\n\r", queue_tail_see);
	
	printf("cmd_got_enterkey: ");
	for (i=0; i<QUEUE_SIZE; i++) {
			printf("%d ", cmd_got_enterkey_see[i]);
	}
	printf("\n\r");
	
	printf("cmd_length_queue: ");
	for (i=0; i<QUEUE_SIZE; i++) {
		printf("%d ", cmd_length_queue_see[i]);
	}
	printf("\n\r");
	
	printf("rx_queue[queue_head]: ");
	for (i=0; i<BUFFER_SIZE; i++) {
		printf("%c ", rx_queue_see[queue_head_see][i]);
	}
	printf("\n\r");
	
	printf("rx_queue[queue_tail]: ");
	for (i=0; i<BUFFER_SIZE; i++) {
		printf("%c ", rx_queue_see[queue_tail_see][i]);
	}
	printf("\n\r");
	
	return 0;
}


/* 2016-09-08 */
int cmd_WDG(int argc, char *argv[])
{
	/* wait watchdog reset. */
	while (1)
		;
}

/************************************************************************/
/* Initial value for engineering purpose only                           */
/************************************************************************/
int  cmd_IV(int argc) {
/*
Factory Defaults
Booster
BAUD : 19200
OPERATION MODE : D
MODE M BIAS CURRENT : 400 mA
GLIM : D
PLIM : D
Pump Laser Alarm : 470 mA
Pump Laser Alarm Hysteresis : 5 mA
Pump Laser High Temperature Alarm : 80�J
Pump Laser High Temperature Alarm Hysteresis : 1�J
Case High Temperature Alarm : 80�J
Case High Temperature Alarm Hysteresis : 1�J
Case Low Temperature Alarm : -10�J
Case Low Temperature Alarm Hysteresis : 1�J
LOS Alarm: -15.0 dBm
LOS Alarm Hysteresis : 1 dB
LOP Alarm: Pset - 2.0 dB
LOP Alarm Hysteresis : 0.5 dB
LOS Mode : A
========================================
*/
	char FWVersion[] = "EDFA-A01.I26.000";
	char SerialNumber[] = "Alpha #02";
	char EntpLogo[] = "MSA EDFA";
	char HWVersion[] = "0.2";
	char FWDate[] = __DATE__ " "__TIME__;
		
	memcpy(sysvars_AppSpecParam.AppPrivate.EntpLogo,EntpLogo,strlen(EntpLogo)+1);
	memcpy(sysvars_AppSpecParam.AppPrivate.FWVersion,FWVersion,strlen(FWVersion)+1);
	memcpy(sysvars_AppSpecParam.AppPrivate.SerialNumber,SerialNumber,strlen(SerialNumber)+1);
	memcpy(sysvars_AppSpecParam.AppPrivate.HWVersion,HWVersion,strlen(HWVersion)+1);
	memcpy(sysvars_AppSpecParam.AppPrivate.FWDate,FWDate,strlen(FWDate)+1);
	

#define check_OK
#ifdef check_OK

	AppCfgMap.v.vPumpBiasLow  = 100.0;
	
	/* 2016-07-11, 1A Laser Pump. */
	// AppCfgMap.v.vPumpBiasHigh = 470.0
	AppCfgMap.v.vPumpBiasHigh = 1010.0;
	Hys.vPumpBiasHigh = 5.0;

	/* 2016-07-13, NP-2000 spec. */
	// AppCfgMap.v.vPumpTempHigh = 80.0;
	AppCfgMap.v.vPumpTempHigh = 70.0;
	
	/* 2016-07-13, NP-2000 spec. */
	// AppCfgMap.v.vCaseTempHigh = 80.0;
	AppCfgMap.v.vCaseTempHigh = 70.0;
	
	AppCfgMap.v.vCaseTempLow = -10.0;
	
	Hys.vPumpTempHigh = 1.0;
	Hys.vCaseTempHigh = 1.0;	
	Hys.vCaseTempLow = 1.0;

	AppCfgMap.v.vInputOptLowdBm = -15.0;
	Hys.vInputOptLowdBm = 1.0;
	
	AppCfgMap.v.vInputOptHighdBm = 10.0;
	//Hys.vInputOptHighdBm = 1;

	AppCfgMap.v.vOutputOptHighdBm = 18.0;
	
	/* 2016-06-13 */
	// AppCfgMap.v.vOutputOptLowdBm = 14.5;
	AppCfgMap.v.vOutputOptLowdBm = 10.0;
	
	Hys.vOutputOptLowdBm = 0.5;
	Hys.vOutputOptHighdBm = 0.5;

	tmpECHO=1;
	AppCfgMap.v.Private = 0;										//System Mode
	AppCfgMap.v.ECHO = 1;											//ECHO ON
	sysvars_AppSpecParam.AppPrivate.BaudRate = 5;					//115200
	AppCfgMap.v.GLIMstatus = 0;										//GLIM Disable
	
	/* 2016-07-11, 1A Laser Pump. */
	// AppCfgMap.v.PumpBiasEOL = (U16)750;
	AppCfgMap.v.PumpBiasEOL = (U16)1085;
	
	AppCfgMap.v.AutoISP = (U16)400;
	AppCfgMap.v.ACCcurrent = 400;		//default pump current in mode M is 400mA
	AppCfgMap.v.AmpType = 1;			//1:BOOST, 0:PREAMP
	AppCfgMap.v.bPumpAutoClose = LOS_A;
	
	/* 2016-05-12, test NP-2000 APC mode , and should "AppCfgMap.v.typeALC = MAPC". */
	/* 2016-05-11, NP-2000 no MDIS mode, default mode MACC. */
	/* 2016-05-11, NP2000 has no MDIS mode, default mode MACC. */
	// AppCfgMap.v.OperationMode = MDIS;
	// AppCfgMap.v.OperationMode = MACC;
	AppCfgMap.v.OperationMode = MALC;
	/* 2016-08-09 */
	// AppCfgMap.v.typeALC = MAPC;
	AppCfgMap.v.typeALC = MAOPC;
	
	/* 2016-07-07, 500mA Laser Diode. */
	// AppCfgMap.v.DACACCTHR = 135;//910;
	/* 2016-07-07, 1A Laser Diode. */
	AppCfgMap.v.DACACCTHR = 466;
	
	/* 2016-07-01 500mA Laser Pump. */
	/* 2016-06-20 */
	/* 2016-05-13, DACALCTHR = 950, value 380, AOPC more current, be careful!!! */
	/* 2016-05-13, IV -> RST -> BOOT to save parameters to flash. */
	/* 2016-05-11 */	
	// AppCfgMap.v.DACALCTHR = 2335;//910;
	// AppCfgMap.v.DACALCTHR = 1050;
	// AppCfgMap.v.DACALCTHR = 999;
	// AppCfgMap.v.DACALCTHR = 600;
	
	/* 2016-07-07 1A Laser Pump, AOPC DAC >=880, but APC DAC >= 390. */
	AppCfgMap.v.DACALCTHR = 400;
	
	AppCfgMap.v.AD5322Resister2 = 49060;		//for 12-bit DAC: 2250;	
	AppCfgMap.v.Latch = 0;	//0=N;1=S	
	
	
	
	/* 2016-05-16, Pin calibration. */
	/*
	AppCfgMap.v.A = 0.0;
	AppCfgMap.v.B = 0.01650596;
	AppCfgMap.v.C = 53.21568;
	*/
	// y = 0.0* x^2 + 0.0418x -19.299
	// AppCfgMap.v.A = 0.0;
	// AppCfgMap.v.B = 0.0418;
	// AppCfgMap.v.C = 19.299;
	/* 2015-05-20, from Vincent's data */
	// y = 0.029x - 44.642
	/*
	AppCfgMap.v.A = 0.0;
	AppCfgMap.v.B = 0.029;
	AppCfgMap.v.C = 44.642;
	*/
	/* 2016-05-20 */
	// y = 0.029x - 44.642
	// AppCfgMap.v.A = 0.0;
	// AppCfgMap.v.B = 0.049;
	// AppCfgMap.v.C = 44.608;
	/* 2016-07-12, Pin calibration parameters. */
	// y = 0.0494x - 50.407
	/*
	AppCfgMap.v.A = 0.0;
	AppCfgMap.v.B = 0.0494;
	AppCfgMap.v.C = 50.407;
	*/
	/* 2016-08-23, no#22 in Fremont, auto-calibration, Pin calibration parameters. */
	AppCfgMap.v.A = 0.0;
	AppCfgMap.v.B = 0.049616;
	AppCfgMap.v.C = -55.600264;	
	


/* 2016-06-23, Pout */
// AppCfgMap.v.E = 0.1505;
// AppCfgMap.v.E = 0.05565;
// AppCfgMap.v.F = 299.85;
// AppCfgMap.v.F = 61.5;
/* 2016-07-12, AOPC POUT. */
// AppCfgMap.v.E*(float)result -AppCfgMap.v.F;
/* 2016-08-05 */
/*
AppCfgMap.v.D = 0.0;
AppCfgMap.v.E = 0.0504;
AppCfgMap.v.F = 52.316;
*/
// AppCfgMap.v.POUT_b_AOPC*(float)result -AppCfgMap.v.POUT_c_AOPC;
/*
AppCfgMap.v.POUT_a_AOPC = 0;
AppCfgMap.v.POUT_b_AOPC = 0.0504;
AppCfgMap.v.POUT_c_AOPC = 52.316;
*/
/* 2016-08-23, no#22 in Fremont, auto-calibration, Pout calibration parameters. */
AppCfgMap.v.POUT_a_AOPC = 0;
AppCfgMap.v.POUT_b_AOPC = 0.051537;
AppCfgMap.v.POUT_c_AOPC = -51.823853;

AppCfgMap.v.G = 2.4990;

/* 2016-07-11, "PUMP ILD:" calibration.
AppRtData.vPump1Bias = (result -  1151.68) * 2 * 0.9525 + 1.6366;
= 1.905 * result - 2192.3138
*/
/* 2016-08-16, 500mA Laser Pump rw#04,
AppRtData.vPump1Bias = (result -  1158.73) * 0.9614 + 0.3383;
= 0.9614 * result - 1113.66
*/
// AppCfgMap.v.I = 0.9614;
// AppCfgMap.v.J = -1113.66;

/* 2016-09-29 */
/*
(x - 1156.44) * 2 * 0.9502 - 0.2254
= 1.9004 x �V 2197.924
*/
AppCfgMap.v.I = 1.9004;
AppCfgMap.v.J = -2197.924;


/* 2016-09-29 */
/*
No.29
(x - 1154.24) * 2 * 0.9503 + 0.2171
= 1.9006 x �V 2193.53
*/
/*
AppCfgMap.v.I = 1.9006;
AppCfgMap.v.J = -2193.53;
*/



/* 2016-05-11, APC calibration parameter update */
AppCfgMap.v.K = 0.0;
/*
AppCfgMap.v.L = 32.36882;
AppCfgMap.v.M = 3389.679;
*/
// y = -69.556x + 1231.7
// y = -153.63x + 1338.5
// y = -168.05x + 1659.5
// AppCfgMap.v.L = 168.05;
// AppCfgMap.v.M = 1659.5;

/* 2016-05-20, from Vincent'data. */
// y = -255.5x + 1335.9
// AppCfgMap.v.L = 255.5;
// AppCfgMap.v.M = 1335.9;

/* 2016-07-01, remote calibration, edfa in Fremont */
// y = -8.2821x + 1353.4
// AppCfgMap.v.L = 8.2821;
// AppCfgMap.v.M = 1353.4;

/* 2016-07-01, remote calibration, edfa in Fremont */
// y = -8.405x + 1361.9
// AppCfgMap.v.L = 8.405;
// AppCfgMap.v.M = 1361.9;

/* 2016-07-07, 1A Laser Pump, APC mode. */
// y = -6.7386x + 1407.7
// AppCfgMap.v.L = 6.7386;
// AppCfgMap.v.M = 1407.7;

/* 2016-08-05, 1A Laser Pump */
// for 20dBm, pump diode current = 450mA, pump laser output power = 300mA.
// for 23dBm, pump diode current = 940mA, pump laser output power = 600mA.
// mA -> DAC.
// y = -1.2099*x + 1483.5
/*
AppCfgMap.v.L = -1.2099;
AppCfgMap.v.M = 1483.5;
*/
/* 2016-08-19, no. 22 500mA calibration data. */
// y= -2.615x +1483.4
AppCfgMap.v.L = -2.615;
AppCfgMap.v.M = 1483.5;


/* 2016-09-08 */
AppCfgMap.v.N = 2.248118;
AppCfgMap.v.O = 2.061314;
AppCfgMap.v.P = 1.297362;
AppCfgMap.v.Q = 0.000000;
/*
AppCfgMap.v.N = 0.855;	// C3
AppCfgMap.v.O = 2.347;	// C2
AppCfgMap.v.P = 1.125;	// C1
AppCfgMap.v.Q = 0.000000;
*/

/* 2016-05-10, CONSTANT-CURRENT Mode Calibration Data.
 * DAC = AppCfgMap.v.Q * I * I - * AppCfgMap.v.S * I + AppCfgMap.v.R
 */
/*
AppCfgMap.v.R = 2.8767;
AppCfgMap.v.S = 1561;
*/
/*
 AppCfgMap.v.R = 2.955;
 AppCfgMap.v.S = 1433.1;
*/
/* 2016-07-01, Laser Pump spec 500mA, ACC remote calibration, edfa in Fremont. */
/*
// y = -2.4466x + 1206
 AppCfgMap.v.R = 2.4466;
 AppCfgMap.v.S = 1206;
*/
/* 2016-07-07, Laser Pump spec 1A, ACC remote calibration, ACC DAC limit: 466 */
// y = -1.0667x + 1532.8
/*
AppCfgMap.v.R = 1.0667;
AppCfgMap.v.S = 1532.8;
*/
/* 2016-08-22, 500mA Laser Pump in Fremont. */
AppCfgMap.v.R = -2.615;
AppCfgMap.v.S = 1483.4;

/* 2016-08-09, LD_PD mV -> mA. */
/* 2016-07-11, calibration APC Pout, "PUMP PWR: ? mW (13084)" */
/* APC_a, APC_b, APC_c data type are double !!!, check program. */
// y = 0 * x^2 + 0.3012x -365.31
/*
AppCfgMap.v.APC_a = 0;
AppCfgMap.v.APC_b = 0.3012;
AppCfgMap.v.APC_c= -365.31;
*/
// y = 0 * x^2 + 1.6664 * x -1952.9
AppCfgMap.v.APC_a = 0;
AppCfgMap.v.APC_b = 1.6664;
AppCfgMap.v.APC_c= -1952.9;

/* 2016-08-5, AOPC Pout, get DAC from setting dBm value. */
/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
/*
AppCfgMap.v.AOPC_a_dac = 0;
AppCfgMap.v.AOPC_b_dac = 39.813;
AppCfgMap.v.AOPC_c_dac = 1751.4;
*/
/* 2016-08-18, AOPC, reference #no.22(Fremont, calibrated) */
AppCfgMap.v.AOPC_a_dac = 0;
AppCfgMap.v.AOPC_b_dac = -59.930268;
AppCfgMap.v.AOPC_c_dac = 2026.674987;

#endif


/* 2016-08-22, lost AppCfgMap.v.ResFactorySet initialized. */
AppCfgMap.v.ResFactorySet = 0;



	//Save factory defaults to external SPI memory
#ifdef DG_EEPROM
	if (pio_get(PIOD, PIO_TYPE_PIO_INPUT, PIO_PD20))
		printf("EP_W: High.\n\r");
	else 
		printf("EP_W: Low.\n\r");
#endif
	x1= OnEEPROM_Write_Page((unsigned char*)&sysvars_AppSpecParam,sizeof(sysvars_AppSpecParam),Addr_sysvars_AppSpecParam_Ini);
	if (x1!=0) printf("EEROM Write Error: %d\n\r", x1);
	printf("EEROM Write AppSpec:\n\r");
	
#ifdef DG_EEPROM	
	if (pio_get(PIOD, PIO_TYPE_PIO_INPUT, PIO_PD20))
	printf("EP_W: High.\n\r");
	else
	printf("EP_W: Low.\n\r");
#endif
	x1 = OnEEPROM_Write_Page((uint8_t*)&AppCfgMap,sizeof(AppCfgMap),Addr_AppCfgMap_Ini);
	if (x1!=0) printf("EEROM Write Error: %d\n\r", x1);
	printf("EEROM Write AppCfgMap:\n\r");
	
#ifdef DG_EEPROM	
	if (pio_get(PIOD, PIO_TYPE_PIO_INPUT, PIO_PD20))
	printf("EP_W: High.\n\r");
	else
	printf("EP_W: Low.\n\r");
#endif
	x1 = OnEEPROM_Write_Page((unsigned char*)&Hys,sizeof(Hys),Addr_Hys_Ini);
	if (x1!=0) printf("EEROM Write Error: %d\n\r", x1);
	printf("EEROM Write Hys:\n\r");

	
#define NUPhoton
#ifdef NUPhoton

swDIS

NP_rtMODE.iniStatusLD = OFF;
NP_rtMODE.statePump = OFF;
NP_rtMODE.MODE = MAOPC;
NP_rtMODE.MODE_cmd = MAOPC;

NP_rtMODE.SetCurrent_ACC = 400;
NP_rtMODE.SetPower_APC = 0.1;
/* 2016-06-17, implement latch function. */
NP_rtMODE.SetPower_APC_cmd = 0.1;
NP_rtMODE.SetPower_AOPC = 15;
/* 2016-06-17, implement latch function. */
NP_rtMODE.SetPower_AOPC_cmd = 15;
/* 2016-08-09, add AGC mode. */
NP_rtMODE.SetPower_AGC = 10.0;
// now, no implement latch function.
NP_rtMODE.SetPower_AGC_cmd = 10.0;


NP_rtMODE.powerAD_APC = 3000;
NP_rtMODE.powerAD_AOPC = 3000;
NP_rtMODE.powerAD_ACC = 3000;


NP_rtMODE.headMsg = ON;
NP_rtMODE.statePrompt = ON;


#ifdef DG_EEPROM
if (pio_get(PIOD, PIO_TYPE_PIO_INPUT, PIO_PD20))
printf("EP_W: High.\n\r");
else
printf("EP_W: Low.\n\r");
#endif
// write to NP_iniMODE.
x1 = OnEEPROM_Write_Page((unsigned char*)&NP_rtMODE,sizeof(NP_rtMODE),Addr_npMODE);
if (x1!=0) printf("EEROM Write Error: %d\n\r", x1);
printf("EEROM Write NP:\n\r");
#endif

	
	return 0;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
int cmd_VER(int argc) {
	uint32_t rstSecond, rstMinute, rstHour, rstDay, rstSys_Elapsed_time;
	uint32_t hour, minute, second;
	uint32_t year, month, day;
	rtc_get_time(RTC, &hour, &minute, &second);
	rtc_get_date(RTC, &year, &month, &day, NULL);
	
	//Service Time Calculation
	rstSys_Elapsed_time = Sys_Elapsed_time;
	rstDay = rstSys_Elapsed_time/60/60/24;
	rstHour = rstSys_Elapsed_time/60/60 - rstDay*24;
	rstMinute = rstSys_Elapsed_time/60 - (rstHour + rstDay*24) * 60;
	rstSecond = rstSys_Elapsed_time - ((rstHour + rstDay*24) * 60 + rstMinute) * 60;
	
	if(argc == 1) {
		printf("Configuration:%s\n\r",(char *)sysvars_AppSpecParam.AppPrivate.EntpLogo);
		printf("Firmware Vers:%s\n\r",(char *)sysvars_AppSpecParam.AppPrivate.FWVersion);
		printf("Serial Number:%s\n\r",(char *)sysvars_AppSpecParam.AppPrivate.SerialNumber);
		printf("Hardware Vers:%s\n\r",(char *)sysvars_AppSpecParam.AppPrivate.HWVersion);
		//printf("Firmware Date:%s\n\r",(char *)sysvars_AppSpecParam.AppPrivate.FWDate);
		printf("Firmware Date:"__DATE__" "__TIME__"\n\r");
		
		printf("Date:%d-%d-%d\n\r", year,month,day);
		printf("Time: %d:%d:%d\n\r", hour,minute,second);
		
		printf("Service Time: %d days %d:%d:%d\n\r",rstDay,rstHour,rstMinute,rstSecond);
		return 0;
	}
	else if(argc > 1)	{
		printf("?Parameters Error\n\r");
		return 0;
	}
	
	return 0;
}


/* 2016-05-25 */
int cmd_GVER(int argc)
{
	printf("FW Version:  %s\n\r", (char *)sysvars_AppSpecParam.AppPrivate.FWVersion);
	
	return 0;
}


/* 2016-05-25 */
int cmd_GMOD(int argc)
{
	printf("Model: 001\n\r");
	
	return 0;
}


/* 2016-05-25 */
int cmd_GSN(int argc)
{
	printf("Serial Number:%s\n\r",(char *)sysvars_AppSpecParam.AppPrivate.SerialNumber);
	
	return 0;
}


/************************************************************************/
/* Restores all default factory parameters.                             */
/************************************************************************/
int cmd_RST(int argc) {
	//rec_type_t rec_type=rec_ALL;
	
	if(argc == 1) {
		AppCfgMap.v.ResFactorySet = (uint8_t)activeRST;
		save(rec_AppCfg);
		return 0;
	}
	return -1;
}

/************************************************************************/
/* Warm start                                                           */
/************************************************************************/
int cmd_BOOT(int argc) {
	if(argc == 1) {
		printf("MSA COMPATIBLE EDFA FIRMWARE VERSION 0.1 \n\r");
		printf("Booting ...\n\n\r");
		
		/* 2016-07-05 */
		// while (1) { }
		//SCB->AIRCR |= 0x04;
		NVIC_SystemReset();
			
		return 0;
	}
	return -1;
}

/************************************************************************/
/* Echo On/Off                                                          */
/************************************************************************/
int cmd_ECHO(int argc, char *argv[]) {
	
	if(argc == 1) {
		if(tmpECHO == 1)	printf("ECHO: ON\n\r");
		else				printf("ECHO: OFF\n\r");
		return 0;
	}
	
	if(argc == 2) {
		if(CmdStrCompare(argv[1],"OFF")) {
			AppCfgMap.v.ECHO = 0;
			tmpECHO=0;
		}
		else if(CmdStrCompare(argv[1],"ON")) {
			AppCfgMap.v.ECHO = 1;
			tmpECHO=1;
		}
		else {
			/* 2016-05-25 */
			// printf("?Parameters Error");
			printf("?Parameters Error\n\r");
		}
		
		//save(rec_AppCfg);
		
		/* 2016-05-25 */
		// printf("\n\r");
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}

/************************************************************************/
/* Baud                                                                 */
/************************************************************************/
int cmd_BAUD(int argc, char *argv[]) {
	if(argc == 1) {
		if (sysvars_AppSpecParam.AppPrivate.BaudRate == 1)		printf("BAUD:%s \n\r", "9600");
		else if (sysvars_AppSpecParam.AppPrivate.BaudRate == 2)	printf("BAUD:%s \n\r", "19200");
		else if (sysvars_AppSpecParam.AppPrivate.BaudRate == 3)	printf("BAUD:%s \n\r", "38400");
		else if (sysvars_AppSpecParam.AppPrivate.BaudRate == 4)	printf("BAUD:%s \n\r", "57600");
		else if (sysvars_AppSpecParam.AppPrivate.BaudRate == 5)	printf("BAUD:%s \n\r", "115200");
		else													printf("BAUD:%s \n\r", "115200");
		return 0;
	}
	else if(argc == 2) {
		if(CmdStrCompare(argv[1],"9600"))			sysvars_AppSpecParam.AppPrivate.BaudRate = 1;	
		else if(CmdStrCompare(argv[1],"19200"))		sysvars_AppSpecParam.AppPrivate.BaudRate = 2;
		else if(CmdStrCompare(argv[1],"38400"))		sysvars_AppSpecParam.AppPrivate.BaudRate = 3;
		else if(CmdStrCompare(argv[1],"57600"))		sysvars_AppSpecParam.AppPrivate.BaudRate = 4;
		else if(CmdStrCompare(argv[1],"115200"))	sysvars_AppSpecParam.AppPrivate.BaudRate = 5;
		else										printf("?Parameters Error\n\r");
		
		
		//save(rec_AppSpec);
		configure_console();
		delay_ms(10);
		return 0;
	}
	else if(argc > 2) {
		printf("?Parameters Error\n\r");
		return 0;
	}
	return -1;
}

/************************************************************************/
/* Set Real Time Cock                                                   */
/************************************************************************/
int cmd_Set_RTC(int argc, char *argv[]) {
	uint32_t hour, minute, second;
	uint32_t year, month, day;
	bool result=0;
	
	if(argc == 7) {
		year = (uint32_t)atoi(argv[1]);
		month = (uint32_t)atoi(argv[2]);
		day = (uint32_t)atoi(argv[3]);
		hour = (uint32_t)atoi(argv[4]);
		minute = (uint32_t)atoi(argv[5]);
		second = (uint32_t)atoi(argv[6]);
		
		result=rtc_set_time(RTC, hour, minute, second);
		if (result) printf("?RTC set time fail.\n\r");
		result=rtc_set_date(RTC, year, month, day, 1);
		if (result) printf("?RTC set date fail.\n\r");
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}

/************************************************************************/
/*                                                                      */
/************************************************************************/
int cmd_STORE(int argc) {

	if(argc == 1) {
		//save(rec_ALL);
		tmpSTORE = 1;
		printf("OK \n\r");
		return 0;
	} else {
		printf("?Parameters Error\n\r");
		return -1;
	}	
}

/************************************************************************/
/*  MODE C/P/DIS                                                        */
/************************************************************************/
int cmd_MODE(int argc, char *argv[]) {
	
	uint8_t opMode = AppCfgMap.v.OperationMode;
	uint8_t i, WR[7];//write Resistor 0~255
	char	*count;
	float	tmp_setPower;
	
  if(argc == 1) {
    if(opMode == MACC)		printf("MODE:C\n\r");
	else if(opMode == MALC)	printf("MODE:P %.1f dBm\n\r", AppCfgMap.v.setPower);
	else if(opMode == MAGC)	printf("MODE:G\n\r");
	else if(opMode == MDIS)	printf("MODE:D\n\r");
    return 0;
  } else if(argc == 2) {
	  
// MODE DIS----------------------------------------------------------------------------------------	  
	  if(CmdStrCompare(argv[1], "D") || CmdStrCompare(argv[1], "DNS")) { //NS: No Saved
		  swDIS;
		  if (CmdStrCompare(argv[1], "D")) {
			  AppCfgMap.v.OperationMode = MDIS;			//status is not kept for LOS occurred
		  }
		  AppRtData.RtFlag.Bits.OperationMode = MDIS;	
		  AppRtData.GlimitOccured = FALSE;
// MODE ACC----------------------------------------------------------------------------------------		
	  } else if(CmdStrCompare(argv[1], "M") || CmdStrCompare(argv[1], "MNS")) { //NS: No Saved
		 if (!AppRtData.RtFlag.Bits.FinalRequest) {		//No Output Disable situation then switch to output power.		
			swACC;
			AppRtData.RtFlag.Bits.OperationMode = MACC;
		 }
		 if (CmdStrCompare(argv[1], "M")) {
			AppCfgMap.v.OperationMode = MACC;			//status is not kept for LOS occurred
		 }
			AppRtData.GlimitOccured = FALSE;
			
			/* 2016-08-22 */
			// AppCfgMap.v.AD5322Resister = (uint16_t) ((-1) * AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S);
			AppCfgMap.v.AD5322Resister = (uint16_t) (AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S);
			
			setLD_Value((uint32_t)AppCfgMap.v.AD5322Resister);	//restore PUMP ISP setting point	
// Restore MODE ALC for Auto Disable Output Power -------------------------------------------------
	  } else if(CmdStrCompare(argv[1], "PNS")) {
			if (!AppRtData.RtFlag.Bits.FinalRequest) {	//No Output Disable situation then switch to output power.
				swALC;
				AppRtData.RtFlag.Bits.OperationMode = MALC;
			}
			setLD_Value((uint32_t)AppCfgMap.v.PowerAD);	//restored Pump current 
			AppRtData.GlimitOccured = FALSE;			//forced to reset output power for GLIM
		} else if(CmdStrCompare(argv[1], "P")) {
			printf("?Parameters Missed\n\r");
		} else {
			printf("Wrong operation mode!\n\r");
		}
		return 0;	//end 1 parameter
		
	 } else if(argc == 3) {
		count = argv[2];
		for(i = 0; (count[i] != '\0'); i++)	WR[i] = count[i];
	    WR[i] = '\0';
		 
		tmp_setPower = atof(WR);
// MODE ALC----------------------------------------------------------------------------------------		
		/* 2016-05-11 */
		if( CmdStrCompare(argv[1], "P") ) {
		// if( CmdStrCompare(argv[1], "P") || CmdStrCompare(argv[1], "PNS") ) {
			/* 2016-05-11, unit dBm. */
			// if ((tmp_setPower >= 10) && (tmp_setPower <= 20)) {
			if ((tmp_setPower >= 0) && (tmp_setPower <= 20)) {
				AppCfgMap.v.setPower = tmp_setPower;
				AppCfgMap.v.PowerAD = AppCfgMap.v.setPower*AppCfgMap.v.setPower*AppCfgMap.v.K - AppCfgMap.v.setPower*AppCfgMap.v.L + AppCfgMap.v.M;	
				
			if(AppCfgMap.v.PowerAD > AppCfgMap.v.DACALCTHR) {
				setLD_Value((int32_t)AppCfgMap.v.PowerAD);
				AppRtData.GlimitOccured = FALSE;	//forced to reset output power for GLIM
				AppCfgMap.v.vOutputOptLowdBm = AppCfgMap.v.setPower-2;
			} else if(AppCfgMap.v.PowerAD < AppCfgMap.v.DACALCTHR) {
				printf("Parameter Out of Range!\n\r");
			}
			
			if (!AppRtData.RtFlag.Bits.FinalRequest) {	//No Output Disable situation then switch to output power.
				swALC;
				AppRtData.RtFlag.Bits.OperationMode = MALC;
			}
			AppCfgMap.v.OperationMode = MALC;
		} else printf("?Setting Out of range\n\r"); //when set P value <10 or >18	
		} //end ALC mode
		else if(CmdStrCompare(argv[1], "G")) {
			/*swALC;										//ALC MODE
			AppRtDatas.RtFlag.Bits.OperationMode = 2;
			AppCfgMap.v.OperationMode = 2;
			puts("MODE:G");*/
			printf("?This operation mode is not support in this module\n\r");
		} else {
			printf("?Parameters Error\n\r");
		}
		return 0;
	}
	
    return -1;
}

/************************************************************************/
/*  LOS MODE                                                            */
/************************************************************************/
int cmd_LOS(int argc, char *argv[]) {
	
	if(argc == 1) {
		if(AppCfgMap.v.bPumpAutoClose == LOS_N)			printf("LOS:N\n\r");         
		else if(AppCfgMap.v.bPumpAutoClose == LOS_A)	printf("LOS:A\n\r"); 
      return 0;
    }
	else if(argc == 2) {
        if(CmdStrCompare(argv[1],"N")) {
            AppCfgMap.v.bPumpAutoClose = LOS_N; 
		} else if(CmdStrCompare(argv[1],"A")) {
            AppCfgMap.v.bPumpAutoClose = LOS_A; 
		} else if(CmdStrCompare(argv[1],"P")) {
			printf("?This command is not support in this module\n\r");
		} else if(CmdStrCompare(argv[1],"I")) {
			printf("?This command is not support in this module\n\r");
		} else {
			printf("?Parameters Error\n\r");
			return 0;
		}
		return 0;
	}
	else if(argc == 3) {
		if(CmdStrCompare(argv[1],"I")||CmdStrCompare(argv[1],"P")) {
			printf("?This command is not support in this module\n\r");
		} else {
			printf("?Parameters Error\n\r");
			return 0;
		}
		return 0;
   }
  else {
		printf("?Parameters Error\n\r");
		return 0;
	}
	return -1;
}

/************************************************************************/
/*  MST                                                                 */
/************************************************************************/
int cmd_MST(int argc) {

	if(argc == 1){
		printf("MST:");
		if(AppRtData.RtFlag.Bits.FinalRequest) {
			printf(" DIS\n\r");
		}
		if(AppRtData.RtFlag.Bits.PowerLimit  || AppRtData.GlimitOccured) {
			printf(" LIM\n\r");
		}
		if(AppRtData.RtFlag.Bits.RFLstatus ) {
			printf(" ES\n\r");
		}
		if ((!AppRtData.RtFlag.Bits.FinalRequest) && (!AppRtData.RtFlag.Bits.PowerLimit) && (!AppRtData.GlimitOccured) && (!AppRtData.RtFlag.Bits.RFLstatus)) {
			printf(" OK\n\r");
		}
		return 0;
	} else {
		printf("?Parameters Error\n\r");
		return 0;
	}
	return -1;
}


/************************************************************************/
/* Get Pin                                                              */
/************************************************************************/
int cmd_PIN(int argc)
{
	/* 2016-05-16 */
	volatile uint32_t g_afec1_sample_data0_tmp;
	float result;

	if ( 1 == argc ) {
		Get_EDFA_Monitor();
		
		//printf("PIN:%.1fdBm\n\r",AppRtData.vInputOptdBm);
		//printf("PIN:%.1fdBm\n\r",g_afec1_sample_data0);
		
		/*
		// 2016-05-16
		g_afec1_sample_data0_tmp = g_afec1_sample_data0;
		
		// 2016-05-16
		// printf("PIN: %4.2f mV (%d)\n\r", g_afec1_sample_data0 * VOLT_REF / g_max_digital, (uint32_t)g_afec1_sample_data);
		// 2016-05-27
		result = (double)g_afec1_sample_data0_tmp * VOLT_REF / g_max_digital;
		result = AppCfgMap.v.B * result - AppCfgMap.v.C;
		*/
		g_afec1_sample_data0_tmp = g_afec1_sample_data0;
		
		/* 2016-07-12 */
		/* 2016-06-17, calibration, it's easily parser. */
		/* 2016-05-23 */
		// printf("PIN: %.2f dBm (%.1f mV)\n\r", result, (uint32_t)g_afec1_sample_data0_tmp);
		// printf("PIN: %.2f dBm (%.1f mV)\n\r", AppRtData.vInputOptdBm, (uint32_t)g_afec1_sample_data0_tmp);
		
		printf("PIN: %.2f dBm (%.2f mV)\n\r", AppRtData.vInputOptdBm, (double)(g_afec1_sample_data0_tmp * VOLT_REF / MAX_DIGITAL_15_BIT));
		// printf("PIN: %.2f dBm\n\r", AppRtData.vInputOptdBm);
	}
	else if ( argc > 1 ) {
		printf("?Parameters Error\n");
	}
	return 0;
}

/************************************************************************/
/* Get Pout                                                             */
/************************************************************************/
int cmd_POUT(int argc) {
	/* 2016-05-20 */
	volatile float v1;
	
	/* 2016-05-20 */
	float g_afec1_sample_data1_tmp;
	
	/* 2016-05-26, ����᭱ */
	/*
	g_afec1_sample_data1_tmp = g_afec1_sample_data1;
	v1 = g_afec1_sample_data1_tmp * VOLT_REF / g_max_digital;
	v1 = v1 * 0.0556 - 61.5;
	*/
	
	if(argc == 1) {
		Get_EDFA_Monitor();
		
		//printf("POUT:%.1fdBm\n",AppRtData.vOutputOptdBm);
		//printf(" dBm\n\r",g_afec1_sample_data1);
		/* 2016-05-20 */
		// printf("POUT: %.1f dBm (%4.1f mV)\n\r", (float)10*log10f(AppRtData.vOutputOptdBm), g_afec1_sample_data1 * VOLT_REF / g_max_digital);
		
		
		/* 2016-07-07 */
		/* 2016-05-26 */
		g_afec1_sample_data1_tmp = g_afec1_sample_data1;
		v1 = g_afec1_sample_data1_tmp * VOLT_REF / g_max_digital;
		// v1 = v1 * 0.0556 - 61.5;
		v1 = v1 * 0.0504 - 52.316;
		
// 		AppCfgMap.v.K
		
				
		/* 2016-06-17, calibration, it's easily parser. */
		// printf("POUT: %.1f dBm (%4.1f mV)\n\r", v1, g_afec1_sample_data1_tmp * VOLT_REF / g_max_digital);
		printf("POUT: %.1f dBm (%.1f mV)\n\r", v1, g_afec1_sample_data1_tmp * VOLT_REF / g_max_digital);
	}
	else if (argc > 1) printf("?Parameters Error\n");
	return 0;
}

/************************************************************************/
/*  PUMP Current                                                        */
/************************************************************************/
int cmd_PUMP(int argc, char *argv[])
{
	int	i;
	uint8 WR[7];//write Resistor 0~255
	char	*count;
	float tmpISP;
	U16 tmpAD;
	
	/* 2016-05-26 */
	volatile uint32_t g_afec0_sample_data_tmp;
	volatile uint32_t g_afec0_sample_data1_tmp;
	volatile uint32_t g_afec0_sample_data4_tmp;
	volatile uint32_t g_afec0_sample_data5_tmp;
	
	g_afec0_sample_data_tmp = g_afec0_sample_data;
	g_afec0_sample_data1_tmp = g_afec0_sample_data1;
	g_afec0_sample_data4_tmp = g_afec0_sample_data4;
	g_afec0_sample_data5_tmp = g_afec0_sample_data5;
	
	float result;
	

	if(argc == 1) {
		Get_EDFA_Monitor();
		
		/* 2016-05-26 */
		// printf("PUMP ILD: %.1f mA (%d, %4.2fmV)\n\r", (AppRtData.vPump1Bias>0) ? AppRtData.vPump1Bias : 0, (uint32_t)g_afec0_sample_data, g_afec0_sample_data * VOLT_REF / g_max_digital);
		result = g_afec0_sample_data_tmp * VOLT_REF / g_max_digital;
		// printf("PUMP ILD: %.1f mA (%d, %4.2fmV)\n\r", (AppRtData.vPump1Bias>0) ? AppRtData.vPump1Bias : 0, (uint32_t)g_afec0_sample_data_tmp , g_afec0_sample_data_tmp * VOLT_REF / g_max_digital);
		/* 2016-07-11 */
		// printf("PUMP ILD: %.1f mA (%d, %4.2fmV)\n\r", (AppRtData.vPump1Bias>0) ? AppRtData.vPump1Bias : 0, (uint32_t)g_afec0_sample_data_tmp , result);
		/* 2016-09-22, Pump Laser 1A. */
		// printf("PUMP ILD: %.1f mA (%d, %4.2fmV)\n\r", (AppRtData.vPump1Bias>10) ? AppRtData.vPump1Bias : 0, (uint32_t)g_afec0_sample_data_tmp , result);
		printf("PUMP ILD: %.1f mA (%d, %4.2fmV)\n\r", (AppRtData.vPump1Bias>=50) ? AppRtData.vPump1Bias : 0, (uint32_t)g_afec0_sample_data_tmp , result);
		
		printf("PUMP EOL: %d mA\n\r", AppCfgMap.v.PumpBiasEOL);
		
		/* 2016-09-08 */
		/* 2016-05-26 */
		// printf("PUMP TMP: %.1f C (%d)\n\r", AppRtData.vPump1Temp, (uint32_t)g_afec0_sample_data1);
		// printf("PUMP TMP: %.1f C (%d)\n\r", AppRtData.vPump1Temp, (uint32_t)g_afec0_sample_data1_tmp);
		printf("PUMP TMP: %.1f C (%d)\n\r", (float)AppRtData.vPump1Temp, (uint32_t)g_afec0_sample_data1_tmp);
		
		printf("PUMP ISP: %5.1f mA\n\r", AppCfgMap.v.ACCcurrent);
		
		/* 2016-07-12 */
		/* 2016-05-26 */
		/* 2016-05-19 */
		// printf("PUMP TEC: %5.1f mV\n\r", (float)(g_afec0_sample_data5 * VOLT_REF / g_max_digital));
		/*
		printf("PUMP TEC I: %5.1f mV PUMP�@V�@%5.1f mV\n\r", (float)(g_afec0_sample_data5 * VOLT_REF / g_max_digital),
				(float)(g_afec0_sample_data4 * VOLT_REF / g_max_digital));
		*/
		result = (float)(g_afec0_sample_data5_tmp * VOLT_REF / g_max_digital);
		result = 1200 - result;	// Reference Voltage: 1200mV.
		result = result / (10 * 0.02);	// Gain = 10, resistor: 0.02 ohms.
		printf("PUMP TEC I:%.1f mA PUMP, V:%.1f mV\n\r", result,
				(float)(g_afec0_sample_data4_tmp * VOLT_REF / g_max_digital));
		
		return 0;
	}
	else if(argc == 2) {
		Get_EDFA_Monitor();
		
		if(CmdStrCompare(argv[1], "1")) {
			printf("PUMP ILD: %.1f mA\n\r", (AppRtData.vPump1Bias>0) ? AppRtData.vPump1Bias : 0);
			printf("PUMP EOL: %d mA\n\r", AppCfgMap.v.PumpBiasEOL);
			printf("PUMP TMP: %.1f C\n\r", AppRtData.vPump1Temp);
			printf("PUMP ISP: %5.1f mA\n\r", AppCfgMap.v.ACCcurrent);
		}
		else if(CmdStrCompare(argv[1], "ILD"))	printf("PUMP ILD: %.1f mA\n\r", (AppRtData.vPump1Bias>=0)?AppRtData.vPump1Bias:0);
		else if(CmdStrCompare(argv[1], "EOL"))	printf("PUMP EOL: %d mA\n\r", AppCfgMap.v.PumpBiasEOL);
		else if(CmdStrCompare(argv[1], "TMP")) 	printf("PUMP TMP: %.1f C\n\r", AppRtData.vPump1Temp);
		else if(CmdStrCompare(argv[1], "ISP"))  printf("PUMP ISP: %5.1f mA\n\r", AppCfgMap.v.ACCcurrent);
		/* 2016-09-05, display APC Pump Laser Power and voltage, and for auto-calibration. */
		else if ( CmdStrCompare(argv[1], "PWR") ) {
			printf("PUMP PWR: %.1f mW, %.2f mV\n\r", (AppRtData.vOutputOptmW >0 ) ? AppRtData.vOutputOptmW : 0,
				(float)pwr_voltage);
		}
		else if(CmdStrCompare(argv[1], "AUTO")) {
			
			AppCfgMap.v.ACCcurrent = (float)AppCfgMap.v.AutoISP;
			
			/* 2016-08-22 */
			// AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q - AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
			AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q + AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
			
			
			if(AppRtData.RtFlag.Bits.OperationMode == MACC) setLD_Value((uint32_t)AppCfgMap.v.AD5322Resister);	//restore PUMP ISP setting point
			
			return 0;
		}
		else printf("?Parameters Error\n\r");
		return 0;
	}
	else if(argc == 3) {
		if(CmdStrCompare(argv[1], "1")) {			
			Get_EDFA_Monitor();
			
			if(CmdStrCompare(argv[2], "ILD"))		printf("PUMP ILD: %.1f mA\n\r", (AppRtData.vPump1Bias>=0)? AppRtData.vPump1Bias : 0);
			else if(CmdStrCompare(argv[2], "EOL"))	printf("PUMP EOL: %d mA\n\r", AppCfgMap.v.PumpBiasEOL);
			else if(CmdStrCompare(argv[2], "TMP"))	printf("PUMP TMP: %.1f C\n\r", AppRtData.vPump1Temp);
			else if(CmdStrCompare(argv[2], "ISP"))	printf("PUMP ISP: %5.1f mA\n\r", AppCfgMap.v.ACCcurrent);
			else if(CmdStrCompare(argv[2], "AUTO"))	{
				AppCfgMap.v.ACCcurrent = (float)AppCfgMap.v.AutoISP;
				
				/* 2016-08-22 */
				// AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q - AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
				AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q + AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
				
				if(AppRtData.RtFlag.Bits.OperationMode == MACC) setLD_Value((uint32_t)AppCfgMap.v.AD5322Resister);
				
				return 0;
			}
			else printf("?Parameters Error\n\r");
			return 0;
		}
		//PUMP #2
		else if(CmdStrCompare(argv[1], "2")) {
			if(CmdStrCompare(argv[2], "ILD"))		printf("?There is no Pump 2 in this module\n\r");
			else if(CmdStrCompare(argv[2], "EOL"))	printf("?There is no Pump 2 in this module\n\r");
			else if(CmdStrCompare(argv[2], "TMP"))	printf("?There is no Pump 2 in this module\n\r");
			else if(CmdStrCompare(argv[2], "ISP"))	printf("?There is no Pump 2 in this module\n\r");
			else if(CmdStrCompare(argv[2], "AUTO"))	printf("?There is no Pump 2 in this module\n\r");
			else printf("?Parameters Error\n\r");
			return 0;
		}
		//PUMP #1
		else if(CmdStrCompare(argv[1], "ISP")) {
			if (CmdStrCompare(argv[2], "xAUTO")) {
				
			}
			else {
				count = argv[2];
				for(i = 0; (count[i] != '\0'); i++)
				if (isdigit(count[i])||count[i]==0x2E) WR[i] = count[i];	//check if a number
				else {
					printf("?Setting Out of Range\n\r");
					return 0;
				}
				//}
				WR[i] = '\0';
				tmpISP = atof(WR);
				if (tmpISP<=650 && tmpISP>=0)AppCfgMap.v.ACCcurrent = (float)tmpISP;
				else {
					printf("?Setting Out of Range\n\r");
					return 0;
				}
				
				tmpAD = AppCfgMap.v.AD5322Resister;
				
				/* 2016-08-22 */
				// AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q - AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
				AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q + AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
				
				if(AppRtData.RtFlag.Bits.OperationMode == MACC) {
					if(AppCfgMap.v.AD5322Resister > AppCfgMap.v.DACACCTHR) {
						
						setLD_Value((uint32_t)AppCfgMap.v.AD5322Resister);
						
					}
					else if(AppCfgMap.v.AD5322Resister < AppCfgMap.v.DACACCTHR) {
						printf("DAC count:%d\n\r",AppCfgMap.v.AD5322Resister);
						printf("Over Range!\n\r");
					}
					
					return 0;
				}
				else { //not in ACC mode.
					if(AppCfgMap.v.AD5322Resister > AppCfgMap.v.DACACCTHR) {
						
					}
					else if(AppCfgMap.v.AD5322Resister < AppCfgMap.v.DACACCTHR) {
						printf("DAC count:%d\n",AppCfgMap.v.AD5322Resister);
						printf("Over Range!\n");
					}
					
					return 0;
				}
			}
		}
		else printf("?Parameters Error\n");
		return 0;
	}
	else if(argc == 4) {
		if(CmdStrCompare(argv[1], "1") && CmdStrCompare(argv[2], "ISP")) {
			if (CmdStrCompare(argv[3], "xAUTO")) {
				
			}
			else {
				count = argv[3];
				for(i = 0; (count[i] != '\0'); i++)
				if (isdigit(count[i])||count[i]==0x2E) WR[i] = count[i];	//check if a number
				else {
					printf("?Setting Out of Range\n\r");
					return 0;
				}
				WR[i] = '\0';
				tmpISP = atof(WR);
				if (tmpISP<=650 && tmpISP>=0)AppCfgMap.v.ACCcurrent = (float)tmpISP;
				else {
					printf("?Setting Out of Range\n");
					return 0;
				}
				
				/* 2016-08-22 */
				// AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q - AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
				AppCfgMap.v.AD5322Resister = AppCfgMap.v.ACCcurrent*AppCfgMap.v.ACCcurrent*AppCfgMap.v.Q + AppCfgMap.v.ACCcurrent*AppCfgMap.v.R + AppCfgMap.v.S;
				
				if(AppRtData.RtFlag.Bits.OperationMode == MACC) {
					if(AppCfgMap.v.AD5322Resister > AppCfgMap.v.DACACCTHR) {
						setLD_Value(AppCfgMap.v.AD5322Resister);
					}
					else if(AppCfgMap.v.AD5322Resister < AppCfgMap.v.DACACCTHR) {
						printf("DAC count:%d\n\r",AppCfgMap.v.AD5322Resister);
						printf("Over Range!\n\r");
					}
					
					return 0;
				}
				else {//not in ACC mode. Save the setting only.
					if(AppCfgMap.v.AD5322Resister > AppCfgMap.v.DACACCTHR) {
						
					}
					else if(AppCfgMap.v.AD5322Resister < AppCfgMap.v.DACACCTHR)	printf("Over Range!\n");
					
					return 0;
				}
			}
		}
		else if (CmdStrCompare(argv[1], "2") && CmdStrCompare(argv[2], "ISP")) printf("?There is no Pump 2 in this module\n");
		else printf("?Parameters Error\n");
		return 0;
	}
	
	else { //argc > 4
		printf("?Parameters Error\n");
		return 0;
	}
	return -1;
}


/************************************************************************/
/* ALRM                                                                 */
/************************************************************************/
int cmd_ALRM(int argc, char *argv[]) {
	uint16	i;
	char	*pTmpAlarm;
	float	TmpDataHigh;
	float	TmpDataLow;
	uint8_t tmp;
	float tmpValue;
 
	if(argc == 1) {
	    Check_Alarm_Status();

//ILD, Check Status High only
		printf("ALRM ILD STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Bias));
//ILD, Check Status High only	//Latch
		printf("ALRM ILD SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.ILD));
//ILD 
		printf("ALRM ILD THR: %5.1f mA\n\r", AppCfgMap.v.vPumpBiasHigh);	
//ILD 
		printf("ALRM ILD HYS: %5.1f mA\n\r", Hys.vPumpBiasHigh);

//************************************************************************************************************
//MTH, Check Status High only
		printf("ALRM MTH STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.CaseTemp==2 ? 1:0));
//MTH, Check Status High only
		printf("ALRM MTH SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.MTH));
//MTH
		TmpDataHigh = AppCfgMap.v.vCaseTempHigh * 1;
		printf("ALRM MTH THR: %5.1f C\n\r", TmpDataHigh);
//MTH
		TmpDataHigh = Hys.vCaseTempHigh * 1;
		printf("ALRM MTH HYS: %5.1f C\n\r", TmpDataHigh);

//************************************************************************************************************
//MTL, Check Status Low only			   
		printf("ALRM MTL STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Case2Temp==1 ? 1:0));
//MTL, Check Status Low only
		printf("ALRM MTL SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.MTL));
//MTL
		TmpDataLow = AppCfgMap.v.vCaseTempLow * 1;
		printf("ALRM MTL THR: %5.1f C\n\r", TmpDataLow);									
//MTL
		TmpDataLow = Hys.vCaseTempLow * 1;									
		printf("ALRM MTL HYS: %5.1f C\n\r", TmpDataLow);

//************************************************************************************************************
//TMP, Check Status High only
		printf("ALRM TMP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Temp));
//TMP, Check Status High only
		printf("ALRM TMP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.TMP));
//TMP
		TmpDataHigh = AppCfgMap.v.vPumpTempHigh * 1;
		printf("ALRM TMP THR: %5.1f C\n\r", TmpDataHigh);
//TMP
		TmpDataHigh =Hys.vPumpTempHigh * 1;
		printf("ALRM TMP HYS: %5.1f C\n\r", TmpDataHigh);
		
//************************************************************************************************************		
//Loss of input power alarm
//STA,Current status			   
		printf("ALRM LOS STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.InputOpt));
//SST,Latching status
		printf("ALRM LOS SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOS));
//THR,Threshold
		printf("ALRM LOS THR: %5.1f dBm\n\r", AppCfgMap.v.vInputOptLowdBm);
//HYS,Hysteresis
		printf("ALRM LOS HYS: %5.1f dB\n\r", Hys.vInputOptLowdBm);
		
//************************************************************************************************************
//LOP, Check Status Low only	   
		printf("ALRM LOP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.OutputOpt));
//LOP, Check Status Low only
		printf("ALRM LOP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOP));
//LOP
		printf("ALRM LOP THR: %5.1f dBm\n\r", AppCfgMap.v.vOutputOptLowdBm);
//LOP
		printf("ALRM LOP HYS: %5.1f dB\n\r", Hys.vOutputOptLowdBm);
		
//************************************************************************************************************
/*
//RFL, Check Status	
		printf("ALRM STA RFL: %s\n", Check_TmpStatus(AppRtData.RtFlag.Bits.RFLstatus));

//RFL, Check Status	
		Check_TmpStatusLatch(AppRtDatas.RtFlag.Bits.RFLstatus);				   
		printf("ALRM SST RFL: %s\n", pTmpStatus);
		printf("ALRM THR RFL: %5.1f dB\n",AppCfgMap.v.RFLLimit);	
*/
        return 0;
    }
	else if(argc == 2) {
    if(CmdStrCompare(argv[1],"ILD")) {
			printf("ALRM ILD STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Bias==2?1:0));
			printf("ALRM ILD SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.ILD));
			printf("ALRM ILD THR: %5.1f mA\n\r", AppCfgMap.v.vPumpBiasHigh);
			printf("ALRM ILD HYS: %5.1f mA\n\r", Hys.vPumpBiasHigh);
		}
		else if(CmdStrCompare(argv[1],"MTH")) {				   
			printf("ALRM MTH STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.CaseTemp==2 ? 1:0));
			printf("ALRM MTH SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.MTH));
			TmpDataHigh = AppCfgMap.v.vCaseTempHigh * 1;
			printf("ALRM MTH THR: %5.1f C\n\r", TmpDataHigh);
			TmpDataHigh = Hys.vCaseTempHigh * 1;
			printf("ALRM MTH HYS: %5.1f C\n\r", TmpDataHigh);
		}
		else if(CmdStrCompare(argv[1],"MTL")) {
			printf("ALRM MTL STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Case2Temp));
			printf("ALRM MTL SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.MTL));
			TmpDataLow = AppCfgMap.v.vCaseTempLow * 1;
			printf("ALRM MTL THR: %5.1f C\n\r", TmpDataLow);
			TmpDataLow = Hys.vCaseTempLow * 1;
			printf("ALRM MTL HYS: %5.1f C\n\r", TmpDataLow);
		}
		else if(CmdStrCompare(argv[1],"TMP")) {
			;				   
			printf("ALRM TMP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Temp));
			printf("ALRM TMP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.TMP));
			TmpDataHigh = AppCfgMap.v.vPumpTempHigh * 1;
			printf("ALRM TMP THR: %5.1f C\n\r", TmpDataHigh);
			TmpDataHigh =Hys.vPumpTempHigh * 1;
			printf("ALRM TMP HYS: %5.1f C\n\r", TmpDataHigh);
		}
/*		else if(CmdStrCompare(argv[1],"TMPL"))	
		{

			Check_TmpStatus(AppRtDatas.RtFlag.Bits.Pump1Temp);				   
			printf("ALRM TMPL STA: %s\n", pTmpStatus);
			Check_TmpStatusLatch(AppRtDatas.RtFlag.Bits.Pump1Temp);				   
			printf("ALRM TMPL SST: %s\n", pTmpStatus);
			TmpDataLow = AppCfgMap.v.vPumpTempLow * 1;
			printf("ALRM TMPL THR: %5d C\n", TmpDataLow);
			TmpDataLow =Hys.vPumpTempLow * 1;
			printf("ALRM TMPL HYS: %5d C\n", TmpDataLow);
		}
*/
		else if(CmdStrCompare(argv[1],"LOS")) {
			printf("ALRM LOS STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.InputOpt));
			printf("ALRM LOS SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOS));
			printf("ALRM LOS THR: %5.1f dBm\n\r", AppCfgMap.v.vInputOptLowdBm);
			printf("ALRM LOS HYS: %5.1f dBm\n\r", Hys.vInputOptLowdBm);
		}
		else if(CmdStrCompare(argv[1],"LOP")) {
			printf("ALRM LOP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.OutputOpt));
			printf("ALRM LOP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOP));
			printf("ALRM LOP THR: %5.1f dBm\n\r", AppCfgMap.v.vOutputOptLowdBm);
			printf("ALRM LOP HYS: %5.1f dBm\n\r", Hys.vOutputOptLowdBm);
		}

		else if(CmdStrCompare(argv[1],"RFL")) {
			/*
			Check_TmpStatusLatch(AppRtDatas.RtFlag.Bits.RFLstatus);				   
			printf("ALRM RFL STA: %s\n", pTmpStatus);
			Check_TmpStatusLatch(AppRtDatas.RtFlag.Bits.RFLstatus);				   
			printf("ALRM RFL SST: %s\n", pTmpStatus);
			printf("ALRM RFL THR: %5.1f dB\n",AppCfgMap.v.RFLLimit);	
			*/
			//printf("?Back reflection is not monitored in this module.\n");
			printf("?Back reflection is not monitored in this module\n\r");
		}

		else if(CmdStrCompare(argv[1],"STA")) {
			printf("ALRM ILD STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Bias));
			printf("ALRM TMP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Temp));
			printf("ALRM MTH STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.CaseTemp));
			printf("ALRM MTL STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Case2Temp));
			printf("ALRM LOS STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.InputOpt));
			printf("ALRM LOP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.OutputOpt));
			//Check_TmpStatus(AppRtDatas.RtFlag.Bits.RFLstatus);				   
			//printf("ALRM RFL STA: %s\n", pTmpStatus);
		}
		else if(CmdStrCompare(argv[1],"SST")) {
			printf("ALRM ILD SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.ILD));
			printf("ALRM TMP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.TMP));
			printf("ALRM MTH SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.MTH));
			printf("ALRM MTL SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.MTL));
			printf("ALRM LOS SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOS));
			printf("ALRM LOP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOP));
			//Check_TmpStatusLatch(AppRtDatas.RtFlag.Bits.RFLstatus);				   
			//printf("ALRM RFL SST: %s\n", pTmpStatus);
		}
		else if(CmdStrCompare(argv[1],"THR")) {
			printf("ALRM ILD THR: %5.1f mA\n\r", AppCfgMap.v.vPumpBiasHigh);
			TmpDataHigh = AppCfgMap.v.vPumpTempHigh * 1;
			printf("ALRM TMP THR: %5.1f C\n\r", TmpDataHigh);
			TmpDataHigh = AppCfgMap.v.vCaseTempHigh * 1;
			printf("ALRM MTH THR: %5.1f C\n\r", TmpDataHigh);
			TmpDataLow = AppCfgMap.v.vCaseTempLow * 1;
			printf("ALRM MTL THR: %5.1f C\n\r", TmpDataLow);
			printf("ALRM LOS THR: %5.1f dBm\n\r", AppCfgMap.v.vInputOptLowdBm);
			printf("ALRM LOP THR: %5.1f dBm\n\r", AppCfgMap.v.vOutputOptLowdBm);
			//printf("ALRM RFL THR: %5.1f dB\n",AppCfgMap.v.RFLLimit);	
		}
		else if(CmdStrCompare(argv[1],"HYS")){
			printf("ALRM ILD HYS: %5.1f mA\n\r", Hys.vPumpBiasHigh);
			printf("ALRM TMP HYS: %5.1f C\n\r", Hys.vPumpTempHigh);
			printf("ALRM MTH HYS: %5.1f C\n\r", Hys.vCaseTempHigh);
			printf("ALRM MTL HYS: %5.1f C\n\r", Hys.vCaseTempLow);
			printf("ALRM LOS HYS: %5.1f dB\n\r", Hys.vInputOptLowdBm);
			printf("ALRM LOP HYS: %5.1f dB\n\r", Hys.vOutputOptLowdBm);
		}

		else if(CmdStrCompare(argv[1],"CLR"))	{
			//"If "CLR" is set - alrm values go to only factory default. "
			//"CLR" resets the latching status only

			OnEEPROM_Read_Page((unsigned char*)&factory_AppCfgMap,sizeof(factory_AppCfgMap), Addr_AppCfgMap_Ini);
			AppCfgMap.v.vPumpBiasHigh = factory_AppCfgMap.v.vPumpBiasHigh;
			AppCfgMap.v.vCaseTempHigh = factory_AppCfgMap.v.vCaseTempHigh;
			AppCfgMap.v.vCaseTempLow = factory_AppCfgMap.v.vCaseTempLow;
			AppCfgMap.v.vPumpTempHigh = factory_AppCfgMap.v.vPumpTempHigh;
			AppCfgMap.v.vInputOptLowdBm = factory_AppCfgMap.v.vInputOptLowdBm;
			
			alarmLatch.ILD=0;	PumpBias_hysteresis=0;
			alarmLatch.TMP=0;	PumpTemp_hysteresis=0;
			alarmLatch.MTH=0; CaseTemp_hysteresis=0;
			alarmLatch.MTL=0;
			alarmLatch.LOS=0;	InputOpt_hysteresis=0;
			alarmLatch.LOP=0;	OutputOpt_hysteresis=0;
			alarmLatch.RFL=0;
		}
		else{
			printf("?Parameters Error\n\r");
		}
		return 0;
	}
	else if(argc == 3) {
		if(CmdStrCompare(argv[2],"CLR")) {
			OnEEPROM_Read_Page((unsigned char*)&factory_AppCfgMap,sizeof(factory_AppCfgMap), Addr_AppCfgMap_Ini);
		}
		
		if(CmdStrCompare(argv[1],"ILD")) {
			if(CmdStrCompare(argv[2],"STA")) {
				printf("ALRM ILD STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Bias));
			}
			else if(CmdStrCompare(argv[2],"SST")) {				   
				printf("ALRM ILD SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.ILD));
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				printf("ALRM ILD THR: %5.1f mA\n", AppCfgMap.v.vPumpBiasHigh);
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				printf("ALRM ILD HYS: %5.1f mA\n", Hys.vPumpBiasHigh);
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				AppRtData.RtFlag.Bits.CLRstatus = 0;
				AppCfgMap.v.CLRstatus = 0;
				AppCfgMap.v.vPumpBiasHigh = factory_AppCfgMap.v.vPumpBiasHigh;
				alarmLatch.ILD = 0;
				PumpBias_hysteresis=0;
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1],"MTH")) {
			if(CmdStrCompare(argv[2],"STA")) {
				printf("ALRM MTH STA: %s\n", Check_TmpStatus(AppRtData.RtFlag.Bits.CaseTemp));
			}
			else if(CmdStrCompare(argv[2],"SST")) {
				printf("ALRM MTH SST: %s\n", Check_TmpStatusLatch(alarmLatch.MTH));
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				TmpDataHigh = AppCfgMap.v.vCaseTempHigh * 1;
				printf("ALRM MTH THR: %5.1f C\n", TmpDataHigh);
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				TmpDataHigh = Hys.vCaseTempHigh * 1;
				printf("ALRM MTH HYS: %5.1f C\n", TmpDataHigh);
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				AppRtData.RtFlag.Bits.CLRstatus = 0; 
				AppCfgMap.v.CLRstatus = 0;
				AppCfgMap.v.vCaseTempHigh = factory_AppCfgMap.v.vCaseTempHigh;
				alarmLatch.MTH = 0;
				CaseTemp_hysteresis=0;
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1],"MTL")) {
			if(CmdStrCompare(argv[2],"STA"))	{
				printf("ALRM MTL STA: %s\n", Check_TmpStatus(AppRtData.RtFlag.Bits.Case2Temp));
			}
			else if(CmdStrCompare(argv[2],"SST")) {
				printf("ALRM MTL SST: %s\n", Check_TmpStatusLatch(alarmLatch.MTL));
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				TmpDataLow = AppCfgMap.v.vCaseTempLow * 1;
				printf("ALRM MTL THR: %5.1f C\n", TmpDataLow);
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				TmpDataLow = Hys.vCaseTempLow * 1;
				printf("ALRM MTL HYS: %5.1f C\n", TmpDataLow);
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				AppRtData.RtFlag.Bits.CLRstatus = 0; //CLR Disable
				AppCfgMap.v.CLRstatus = 0;
				AppCfgMap.v.vCaseTempLow = factory_AppCfgMap.v.vCaseTempLow;
				alarmLatch.MTL = 0;
				CaseTemp_hysteresis=0;
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1],"TMP")) {
			if(CmdStrCompare(argv[2],"STA")) {
				printf("ALRM TMP STA: %s\n", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Temp));
			}
			else if(CmdStrCompare(argv[2],"SST")) {
				printf("ALRM TMP SST: %s\n", Check_TmpStatusLatch(alarmLatch.TMP));
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				TmpDataHigh = AppCfgMap.v.vPumpTempHigh * 1;
				printf("ALRM TMP THR: %5.1f C\n", TmpDataHigh);
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				TmpDataHigh =Hys.vPumpTempHigh * 1;
				printf("ALRM TMP HYS: %5.1f C\n", TmpDataHigh);
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				AppRtData.RtFlag.Bits.CLRstatus = 0; 
				AppCfgMap.v.CLRstatus = 0;
				AppCfgMap.v.vPumpTempHigh = factory_AppCfgMap.v.vPumpTempHigh;
				alarmLatch.TMP = 0;
				PumpTemp_hysteresis=0;
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1],"LOS")) {
			if(CmdStrCompare(argv[2],"STA")) {
				printf("ALRM LOS STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.InputOpt));
			}
			else if(CmdStrCompare(argv[2],"SST")) {
				printf("ALRM LOS SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOS));
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				printf("ALRM LOS THR: %5.1f dBm\n\r", AppCfgMap.v.vInputOptLowdBm);
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				printf("ALRM LOS HYS: %5.1f dBm\n\r", Hys.vInputOptLowdBm);
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				AppRtData.RtFlag.Bits.CLRstatus = 0; //CLR Disable
				AppCfgMap.v.CLRstatus = 0;
				AppCfgMap.v.vInputOptLowdBm = factory_AppCfgMap.v.vInputOptLowdBm;
				alarmLatch.LOS = 0;
				InputOpt_hysteresis=0;
			}
			else printf("?Parameters Error\n");
		}
		else if(CmdStrCompare(argv[1],"LOP")) {
			if(CmdStrCompare(argv[2],"STA")) {
				;				   
				printf("ALRM LOP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.OutputOpt));
			}
			else if(CmdStrCompare(argv[2],"SST")) {
				printf("ALRM LOP SST: %s\n\r", Check_TmpStatusLatch(alarmLatch.LOP));
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				printf("ALRM LOP THR: %5.1f dBm\n\r", AppCfgMap.v.vOutputOptLowdBm);
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				printf("ALRM LOP HYS: %5.1f dBm\n\r", Hys.vOutputOptLowdBm);
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				AppRtData.RtFlag.Bits.CLRstatus = 0; //CLR Disable
				AppCfgMap.v.CLRstatus = 0;
				printf("?The Setting of LOP THR Should be Pset-2 dB.\n");
				alarmLatch.LOP = 0;
				OutputOpt_hysteresis=0;
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1],"RFL")) {
			if(CmdStrCompare(argv[2],"STA")) {			   
				printf("?Back reflection is not monitored in this module.\n\r");
			}
			else if(CmdStrCompare(argv[2],"SST")) {			   
				printf("?Back reflection is not monitored in this module.\n\r");
			}
			else if(CmdStrCompare(argv[2],"THR")) {
				printf("?Back reflection is not monitored in this module.\n\r");
			}
			else if(CmdStrCompare(argv[2],"HYS")) {
				printf("?Back reflection is not monitored in this module.\n\r");
			}
			else if(CmdStrCompare(argv[2],"CLR")) {
				printf("?Back reflection is not monitored in this module.\n\r");
			}
		}
		else {
			printf("?Parameters Error\n\r");
		}
       
		return 0;
	}

	else if(argc == 4) {
		tmp=1;
		pTmpAlarm = argv[3];
		 
		
		for(i = 0; (pTmpAlarm[i] != '\0'); i++) {
			if (isdigit(pTmpAlarm[i])||pTmpAlarm[i]==0x2E ||pTmpAlarm[i]==0x2D) AppRtData.TmpSetValue[i] = pTmpAlarm[i];	//check if a number,"." or "-"
			else tmp=0;
		}
		AppRtData.TmpSetValue[i] = '\0';
		
		tmpValue = atof(AppRtData.TmpSetValue);	//floating point is not allow for THR & HYS except "LOS" and "LOP"
		tmpValue = ((CmdStrCompare(argv[1], "LOS"))||(CmdStrCompare(argv[1], "LOP"))) ? 10*tmpValue : tmpValue;
		tmpValue = (tmpValue>0) ?  tmpValue : (-1)*tmpValue;
			if(tmp && (tmpValue!=(float) tmpValue)) {
				printf("?Setting Out of Range\n\r");
				return 0;
			}
		tmpValue = atof(AppRtData.TmpSetValue);	//floating point is not allow for THR & HYS
		
		
					
		if(CmdStrCompare(argv[1], "ILD")) {	
			if(tmp && CmdStrCompare(argv[2], "THR")) {
				if ((tmpValue<0)||(tmpValue>600)) {	//ILD<0 or ILD>600 is not allowed //20140116 chenghan
					printf("?Setting Out of Range\n\r");
				}
				else {
					AppCfgMap.v.vPumpBiasHigh = (float)tmpValue;
					AppRtData.RtFlag.Bits.Pump1Bias = 0; 	//normal
					PumpBias_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Bias1 Current Alarm LED. */
					ioport_set_pin_level(SW_PumpBias, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;					
				}
			}
			else if(tmp && CmdStrCompare(argv[2], "HYS")) {
				if ((tmpValue<2)||(tmpValue>10)) {	//HYS<2 or HYS>10 is not allowed //20140116 chenghan
					printf("?Setting Out of Range\n\r");
				}
				else {
					Hys.vPumpBiasHigh = (float)tmpValue;
					AppRtData.RtFlag.Bits.Pump1Bias = 0; 	//normal
					PumpBias_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Bias1 Current Alarm LED. */
					ioport_set_pin_level(SW_PumpBias, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;					
				}
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1], "MTH")) {
			if(tmp && CmdStrCompare(argv[2], "THR")) {
				if ((tmpValue>80.0)||(tmpValue<60.0)) 	//20140116 chenghan 
				{
					printf("?Setting Out of Range\n\r"); 
				}
				else	
				{	
					AppCfgMap.v.vCaseTempHigh = tmpValue;
					AppRtData.RtFlag.Bits.CaseTemp = 0;		//normal
					AppRtData.RtFlag.Bits.Case2Temp = 0;	//normal
					CaseTemp_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Case Temp Alarm LED. */
					ioport_set_pin_level(SW_CaseTemp, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;
				}
			}
			else if(tmp && CmdStrCompare(argv[2], "HYS")) {
				if ((tmpValue>3.0)||(tmpValue<1.0)) {	//HYS<1 or HYS>3is not allowed  //20140116 chenghan 
					printf("?Setting Out of Range\n\r");
				}
				else {
					Hys.vCaseTempHigh = tmpValue;
					AppRtData.RtFlag.Bits.CaseTemp = 0;		//normal
					AppRtData.RtFlag.Bits.Case2Temp = 0;	//normal
					CaseTemp_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Case Temp Alarm LED. */
					ioport_set_pin_level(SW_CaseTemp, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;
				}
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1], "MTL")) {
			if(tmp && CmdStrCompare(argv[2], "THR")) {
				if ((tmpValue<-10.0)||(tmpValue>10.0))	// THR<-10 or THR>10 is not allowed 20140116 chenghan 
				{
					printf("?Setting Out of Range\n"); 
				}
				else {
					AppCfgMap.v.vCaseTempLow = tmpValue;
					AppRtData.RtFlag.Bits.CaseTemp = 0;		//normal
					AppRtData.RtFlag.Bits.Case2Temp = 0;	//normal
					CaseTemp_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Case Temp Alarm LED. */
					ioport_set_pin_level(SW_CaseTemp, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;					
				}
			}
			else if	(tmp && CmdStrCompare(argv[2], "HYS")) {
				if ((tmpValue<1.0)||(tmpValue>3.0)) {	//HYS<1 OR HYS >3 is not allowed 20140116 chenghan 
					printf("?Setting Out of Range\n");
				}
				else {
					Hys.vCaseTempLow = tmpValue;
					AppRtData.RtFlag.Bits.CaseTemp = 0;	//normal
					AppRtData.RtFlag.Bits.Case2Temp = 0;	//normal
					CaseTemp_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Case Temp Alarm LED. */
					ioport_set_pin_level(SW_CaseTemp, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;
				}
      }
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1], "TMP")) {
			if(tmp && CmdStrCompare(argv[2], "THR")) {
				if ((tmpValue<60)||(tmpValue>80)) {		//THR<60 or THR>80 is not allowed //20140116 chenghan
					printf("?Setting Out of Range\n\r");
				}
				else {
					AppCfgMap.v.vPumpTempHigh = tmpValue;
					AppRtData.RtFlag.Bits.Pump1Temp = 0;	//normal
					PumpTemp_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Pump1 Temp Alarm LED. */
					ioport_set_pin_level(SW_PumpTemp, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;
				}
			}
			else if	(tmp && CmdStrCompare(argv[2], "HYS")) {
				if ((tmpValue<1.0)||(tmpValue>3.0)) {	//HYS<1 OR HYS>3 is not allowed //20140116 chenghan
					printf("?Setting Out of Range\n\r");
				}
				else {
					Hys.vPumpTempHigh = tmpValue;
					AppRtData.RtFlag.Bits.Pump1Temp = 0;	//normal
					PumpTemp_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Pump1 Temp Alarm LED. */
					ioport_set_pin_level(SW_PumpTemp, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;					
				}
      }
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1], "LOS")) {
			if(tmp && CmdStrCompare(argv[2], "THR")) {
				/* 2016-05-16, debug, delete later. */
				if (tmpValue>-10 || tmpValue<-20) {
				// if (tmpValue>5 || tmpValue<-20) {
					printf("?Setting Out of Range\n\r");
				}
				else {
					AppCfgMap.v.vInputOptLowdBm = tmpValue;
					AppRtData.RtFlag.Bits.InputOpt = 0;		//normal
					InputOpt_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Input Power Alarm LED. */
					ioport_set_pin_level(SW_LOI, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;
				}
			}
			else if(tmp && CmdStrCompare(argv[2], "HYS")) {
				if (tmpValue<0.5 || tmpValue>3) {	
					printf("?Setting Out of Range\n\r");
				}
				else {
					Hys.vInputOptLowdBm = tmpValue;
					AppRtData.RtFlag.Bits.InputOpt = 0;		//normal
					InputOpt_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Input Power Alarm LED. */
					ioport_set_pin_level(SW_LOI, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;
				}
			}
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1], "LOP")) {
			if(tmp && CmdStrCompare(argv[2], "THR")) {
				
				/* 2016-05-26 */
				if ( tmpValue > 20 || tmpValue < 0 ) {
					printf("?Parameters Error\n\r");
					
					return -1;
				}

				//printf("?The Setting of LOP THR Should be Pset-2 dB\n\r");		//for NP command, OPTALMLVL
				AppCfgMap.v.vOutputOptLowdBm = tmpValue;
				AppRtData.RtFlag.Bits.OutputOpt = 0;		//normal
				OutputOpt_hysteresis = 0;					//Guo: ALARM released
				
				/* 2016-08-23, turn off Output Power Alarm LED. */
				ioport_set_pin_level(SW_LOP, SW_ALARM_DeAct);
				disactLE;
				activeOE;
				activeLE;
			}
			else if	(tmp && CmdStrCompare(argv[2], "HYS")) {
				if ((tmpValue-0.5<0) || (tmpValue-3>0)){	//0.5<= HYS <=3 
					printf("?Setting Out of Range\n");
				}
				else {
					Hys.vOutputOptLowdBm = tmpValue;
					AppRtData.RtFlag.Bits.OutputOpt = 0;	//normal
					OutputOpt_hysteresis = 0;				//Guo: ALARM released
					
					/* 2016-08-23, turn off Output Power Alarm LED. */
					ioport_set_pin_level(SW_LOP, SW_ALARM_DeAct);
					disactLE;
					activeOE;
					activeLE;					
				}
      }
			else printf("?Parameters Error\n\r");
		}
		else if(CmdStrCompare(argv[1], "RFL")) {
			printf("?Back reflection is not monitored in this module\n\r");
    }
		else{
			printf("?Parameters Error\n\r");
		}																										//only for both ALRM alrm THR/HYS val
    return 0;
	}
  else {
		printf("?Parameters Error\n\r");
		return 0;
	}
	return -1;
}

/************************************************************************/
/* Module Temperature                                                   */
/************************************************************************/
int cmd_MT(int argc)
{	
	float mv;
	
	float vcase = 0;
	float tcase = 0;
	/* 2016-05-26 */
	volatile uint32_t g_afec0_sample_data15_temp;
	volatile uint32_t g_afec0_sample_data3_temp;
	
	if(argc == 1) {		
		Get_EDFA_Monitor();
		
		/* 2016-05-26 */
		g_afec0_sample_data15_temp = g_afec0_sample_data15;
		mv = (double)g_afec0_sample_data15_temp* VOLT_REF / g_max_digital;
		
		mv = (float)((mv - 1440)*100/470+27);
		
		
		g_afec0_sample_data3_temp = g_afec0_sample_data3;

		/* 2016-04-26, use ADC AFEC_15_BITS mode, adc value divided by 8(g_max_digital = 4095 * 8).
		 *
		 */
		/* 2016-04-22, temperature-sensor LM60(U7) output voltage is multiple 2 by op-amp U43B,
		 * Tcase adc value(from MCU) should be divided by 2 to get right temperature-sensor output voltage.
		 * tcase_v = (g_afec0_sample_data3_temp * 3.0 / 4095 / 2);
		 */
		vcase = (double)(g_afec0_sample_data3_temp * 3.0 / 2 / g_max_digital);
	
		/* 2016-04-26, LM60 spec, vcase converts to tcase. */
		tcase = (vcase * 1000 - 424) / 6.25;
		
		/* 2016-04-26, use ADC AFEC_15_BITS mode, adc value divided by 8. */
		/* 2016-04-22, temperature-sensor LM60(U7) output voltage is multiple 2 by op-amp U43B,
		 * Tcase adc value(from MCU) should be divided by 2 to get right temperature-sensor output voltage.
		*/
		
		
		//printf("MT: ");
		//print_float(mv);
		//printf(" mV,   ");
		//print_float((mv - 1440)  * 100 / 470 + 27);
		//printf("C\n\r");
		/* 2016-07-11, send the EDFA to Fremont. */
		printf("MT: (SAM4)%4.2f C, (LM60)%4.2f C\n\r", mv, tcase);
		// printf("MT: %4.2f C\n\r", tcase);
		
	}
	else if (argc > 1) printf("?ParameCters Error\n\r");
	return 0;
}

//-------------------------------------------------------------------------------------------------
int cmd_AST(int argc)	 {
	char allPass = 1;
	
  if(argc == 1) {
		printf("AST:");
		
	if(AppCfgMap.v.Latch) {
		if(alarmLatch.ILD) {
			allPass = 0;
			printf(" ILD");
		}
		if(alarmLatch.TMP) {
			allPass = 0;
			printf(" TMP");
		}
		if(alarmLatch.MTH) {
			allPass = 0;
			printf(" MTH");
		}			   
		if(alarmLatch.MTL) {
			allPass = 0;
			printf(" MTL");
		}
		if(alarmLatch.LOS) {
			allPass = 0;
			printf(" LOS");
		}
		if(alarmLatch.LOP) {
			allPass = 0;
			printf(" LOP");
		}
	}
		else {
		if(AppRtData.RtFlag.Bits.Pump1Bias == 2) {
			allPass = 0;
			printf(" ILD");
		}
		if(AppRtData.RtFlag.Bits.Pump1Temp==2) {
			allPass = 0;
			printf(" TMP");
		}
		if(AppRtData.RtFlag.Bits.CaseTemp == 2) {
			allPass = 0;
			printf(" MTH");
		}			   
		if(AppRtData.RtFlag.Bits.Case2Temp == 1)	{
			allPass = 0;
			printf(" MTL");
		}
		if(AppRtData.RtFlag.Bits.InputOpt==1) {
			allPass = 0;
			printf(" LOS");
		}
		if(AppRtData.RtFlag.Bits.OutputOpt==1) {
			allPass = 0;
			printf(" LOP");
		}
/*
		if(AppRtData.RtFlag.Bits.RFLstatus!=0) {
			allPass = 0;
			printf(" RFL");
		}
*/
	}
		if(allPass) {
			printf(" OK");
		}	
		printf("\n\r");
		
		return 0;
	}
    return -1;
}

//-------------------------------------------------------------------------------------------------
int cmd_ASTM(int argc, char *argv[]) {

    if(argc == 1) {
        if(AppCfgMap.v.Latch == 1) {
			/* 2016-05-25 */
            // printf("ASTM: S\n");
			printf("ASTM: S\n\r");
        }
		else if(AppCfgMap.v.Latch == 0) {
			/* 2016-05-25 */
			// printf("ASTM: N\n");
            printf("ASTM: N\n\r");
        }
        
        return 0;
    }
	else if(argc == 2) {
        if(CmdStrCompare(argv[1],"N")) {
            AppCfgMap.v.Latch = 0;
        }
		else if(CmdStrCompare(argv[1],"S")) {
            AppCfgMap.v.Latch = 1;
        }
		
		save(rec_AppCfg);
		return 0;
    }
    return -1;
}

/************************************************************************
* @brief: Enter Private mode
* @param: Password is 3.14
************************************************************************/
int cmd_private(int argc, char *argv[])
{
	if(argc == 1)
	{
		printf("Please Enter Passward:\n\r");
	}

	else if(argc == 2)
	{
		if(CmdStrCompare(argv[1],"3.14"))
		{
			printf("Private Mode!\n\r");
			AppCfgMap.v.Private = 1;
		}
		else
		{
			printf("Error Password!\n\r");
		}

	}
	
	return 0;
}

/************************************************************************
* @brief: Exit private mode and enter System mode.
************************************************************************/
int cmd_exit(int argc)
{
	if(argc == 1)
	{
		AppCfgMap.v.Private = 0;
		printf("Public Mode!\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP Command to turn the EDFA ON or OFF.                                                                    
************************************************************************/
int cmd_EDFA(int argc, char *argv[])
{
	char ModeD[16] = "MODE DNS\0";
	char ModeP[16] = "MODE PNS\0";
	char ModeM[16] = "MODE MNS\0";
			
	if ( 2 == argc ) {
				
		AppCfgMap.v.Private = 1;
		
		if ( CmdStrCompare(argv[1],"OFF") ) {
			// AppRtData.RtFlag.Bits.FinalRequest = TRUE;
			
			NP_rtMODE.statePump = OFF;
			
			/* 2016-05-17 */
			// NP_rtMODE.statePump_edfa_on_off = OFF;
				
			Console_ParseCmd(&ModeD[0]);
		}
		else if ( CmdStrCompare(argv[1],"ON") ) {
			/* 2016-05-18, NO MODE CHANGED. */
			// if ( NP_rtMODE.MODE_cmd == MACC) {
			if ( NP_rtMODE.MODE == MACC) {
				// NP_rtMODE.MODE = MACC;
				AppCfgMap.v.OperationMode = MACC;
				
				/* 2016-07-20, set AppCfgMap.v.ACCcurrent in cmd_LDC(). */
				/* 2016-06-17, implement latch, only change in cmd_FLASH(). */
				// AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
						
				Console_ParseCmd(&ModeM[0]);
				
				// printf("MODE ACC OK\n\r");
			}
			else {
				/* 2016-05-18, NO MODE CHANGED. */
				// if ( NP_rtMODE.MODE_cmd == MAPC ) {
				if ( NP_rtMODE.MODE == MAPC ) {
					// NP_rtMODE.MODE = MAPC;
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAPC;
					
					/* 2016-06-17 */		
					/* 2016-05-12, private mode no method to switch ACC/APC-AOPC, switch here! */
					// activeAPC
					swAPC
								
					// printf("MODE APC OK\n\r");
				}
				/* 2016-05-18, NO MODE CHANGED. */
				// else if ( NP_rtMODE.MODE_cmd == MAOPC ) {
				else if ( NP_rtMODE.MODE == MAOPC ) {
					// NP_rtMODE.MODE = MAOPC;
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAOPC;
					
					/* 2016-06-17, implement latch, only change in cmd_FLASH(). */
					/* 2016-05-13 */
					/*
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 44.18  + 1426.1;
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 65.869  + 1910.6;
					// 2016-07-01
					NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 64.654  + 2077;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					*/
					
					/* 2016-06-17 */	
					/* 2016-05-12, private mode no method to switch ACC/APC-AOPC, switch here! */
					// activeAOPC
					swAOPC
								
					// printf("MODE AOPC OK\n\r");
				}
				else if ( NP_rtMODE.MODE == MAGC ) {
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAGC;
					
					swAOPC
		
					// printf("MODE AOPC OK\n\r");
				}
							
				Console_ParseCmd(&ModeP[0]);
			}
			
			NP_rtMODE.statePump = ON;
				
		}		
		else {
			printf("?Parameters Error\n\r");
		}
			
		AppCfgMap.v.Private = 0;
	}
	else
		printf("?Parameters Error\n\r");
	
	return 0;
}


/************************************************************************
* @brief: NP Command to get/readback setting EDFA ON/OFF state.
************************************************************************/
int cmd_GEDFA(int argc) {
	if(argc == 1) {
		if (NP_rtMODE.statePump == OFF)
			printf("LD: OFF\n\r");
		else
			printf("LD: ON\n\r");
	}
	else
		printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP Command, MODE, only defined ACC/AGC/APC/AOPC.
************************************************************************/
int cmd_npMODE(int argc, char *argv[])
{
	if ( argc == 2 ) {
		if ( CmdStrCompare(argv[1],"ACC") ) {
			/* 2016-05-13, implemented in cmd_FLASH(). */
			// activeACC;
			
			/* 2016-05-17 */
			// NP_rtMODE.MODE = MACC;
			NP_rtMODE.MODE_cmd = MACC;
			
			/* 2016-05-17 */
			// printf("SW ACC\n\r");
		}
		else if ( CmdStrCompare(argv[1],"APC") ) {
			/* 2016-05-13, implemented in cmd_FLASH(). */
			// activeAPC;
			
			/* 2016-05-17 */
			// NP_rtMODE.MODE = MAPC;
			NP_rtMODE.MODE_cmd = MAPC;
			
			/* 2016-05-17 */
			// printf("SW APC\n\r");
		}
		else if ( CmdStrCompare(argv[1],"AOPC") ) {
			/* 2016-05-13, implemented in cmd_FLASH(). */
			// activeAOPC;
			
			/* 2016-05-17 */
			// NP_rtMODE.MODE = MAOPC;
			NP_rtMODE.MODE_cmd = MAOPC;
			
			/* 2016-05-13, implemented in cmd_FLASH(). */
			// NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC*AppCfgMap.v.L + AppCfgMap.v.M;
			
			/* 2016-05-17 */						
			// printf("SW AOPC\n\r");
		}
		/* 2016-08-08, add AGC mode. */
		else if( CmdStrCompare(argv[1],"AGC") ) {
			NP_rtMODE.MODE_cmd = MAGC;

			// printf("SW AGC\n\r");
		}
		else {
			printf("?Parameters Error\n\r");
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP Command to display and update EDFA working mode.
************************************************************************/
int cmd_FLASH(int argc)
{
	/* 2016-05-11, CmdStrCompare(), only MODE "P" 10. */
	char  ModeP[16] = "MODE PNS\0";
	// char  ModeP[16] = "MODE P 13\0";
		
	char  ModeM[16] = "MODE MNS\0";
	char  FLINECmd[8] = "FLINE\0";
		
	/* 2016-05-12 */
	char ModeD[] = "MODE D\0";
	
	float SetPower_APC_mA;
	
	/* 2016-08-08 */
	float SetPower_AOPC_AGC;
			
	if ( argc == 1 ) {	
		AppCfgMap.v.Private = 1;
		
		if ( OFF == NP_rtMODE.statePump ) {
			
			/* 2016-05-16, add, now MDIS MODE, but MODE change(ACC, APC, AOPC). */
			if ( NP_rtMODE.MODE_cmd == MAPC ) {
				NP_rtMODE.MODE = MAPC;
				
				/* 2016-08-08, lost, but no affect? */				
				AppCfgMap.v.OperationMode = MALC;
				
				AppCfgMap.v.typeALC = MAPC;
				
				/* 2016-08-16, 500mA Laser Pump, APC mode, laser pump current mW -> mA. */
				/*
				For 20dBm EDFA, pump diode current=450mA, pump laser output power=300mw.
				For 23dBm EDFA, pump diode current=940mA, pump laser output power=600mw.
				*/
				/* 2016-08-05, APC mode, laser pump current mW -> mA. */
				/* 23dBm edfa, 940mA / 600mW */
				// NP_rtMODE.powerAD_APC = (-1) * NP_rtMODE.SetPower_APC*AppCfgMap.v.L + AppCfgMap.v.M;
				SetPower_APC_mA = NP_rtMODE.SetPower_APC * 940 / 600;
				// SetPower_APC_mA = NP_rtMODE.SetPower_APC * 450 / 300;
				// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
				
				NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
				
				AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
			}
			else if ( NP_rtMODE.MODE_cmd == MAOPC ) {
				NP_rtMODE.MODE = MAOPC;
				
				/* 2016-08-10, lost, but no affect? */
				AppCfgMap.v.OperationMode = MALC;
				
				AppCfgMap.v.typeALC = MAOPC;
				
				/* 2016-07-20, no "FLASH" after executing "AOPC xxx". */
				/* 2016-06-17, implement latch function. */
				// NP_rtMODE.SetPower_AOPC = NP_rtMODE.SetPower_AOPC_cmd;
				
				/* 2016-07-01 */
				// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 64.654  + 2077;				
				/* 2016-07-07, 1A Laser Pump */
				// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1749.4;
				/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
				// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1751.4;
				/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
				/* 2016-08-18, modify, auto-calibration */
				// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
				NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
				
				AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
			}
			/* 2016-08-08, add AGC mode. */
			else if ( NP_rtMODE.MODE_cmd == MAGC ) {
				NP_rtMODE.MODE = MAGC;
				AppCfgMap.v.OperationMode = MALC;
				AppCfgMap.v.typeALC = MAGC;
								
				/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
				SetPower_AOPC_AGC = AppRtData.vInputOptdBm + NP_rtMODE.SetPower_AGC;
				if ( SetPower_AOPC_AGC > 20.0 ) {
					SetPower_AOPC_AGC = 20.0;
				}
				else if ( SetPower_AOPC_AGC < 15.0 ) {
					SetPower_AOPC_AGC = 15.0;
				}

				/* 2016-08-08, add AGC mode. */									
				/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
				/* 2016-08-18, modify, auto-calibration */
				// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
				NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
									
				AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
			}
			/* 2016-05-16, add ACC MODE. */
			else if ( NP_rtMODE.MODE_cmd == MACC) {
				NP_rtMODE.MODE = MACC;
				AppCfgMap.v.OperationMode = MACC;
				AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
			}
			
			// AppRtData.RtFlag.Bits.FinalRequest = TRUE;
			Console_ParseCmd(ModeD);
			// swDIS
		}
		else if ( ON == NP_rtMODE.statePump ) {
			
			if ( NP_rtMODE.MODE_cmd == MACC) {
				NP_rtMODE.MODE = MACC;
				AppCfgMap.v.OperationMode = MACC;
				AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
				
				// swAPC
				
				Console_ParseCmd(&ModeM[0]);
				
				// printf("MODE ACC OK\n\r");
			}
			else {
				if ( NP_rtMODE.MODE_cmd == MAPC ) {
					NP_rtMODE.MODE = MAPC;
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAPC;
					
					/* 2016-08-16, 500mA Laser Pump, APC mode, laser pump current mW -> mA. */
					/*
					For 20dBm EDFA, pump diode current=450mA, pump laser output power=300mw.
					For 23dBm EDFA, pump diode current=940mA, pump laser output power=600mw.
					*/
					/* 2016-08-05, APC mode, laser pump current mW -> mA. */
					/* 23dBm edfa, 940mA / 600mW */
					// NP_rtMODE.powerAD_APC = (-1) * NP_rtMODE.SetPower_APC*AppCfgMap.v.L + AppCfgMap.v.M;
					SetPower_APC_mA = NP_rtMODE.SetPower_APC * 940 / 600;
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * 450 / 300;
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
					NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
					

					/* 2016-07-12, may be a bug. */
					/* 2016-05-12, private mode no method to switch ACC/APC-AOPC, switch here! */
					// activeAPC
					swAPC
					
					
					// printf("MODE APC OK\n\r");
				}
				else if ( NP_rtMODE.MODE_cmd == MAOPC ) {
					NP_rtMODE.MODE = MAOPC;
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAOPC;
					
					/* 2016-07-20, no "FLASH" after executing "AOPC xxx". */
					/* 2016-06-17, implement latch function. */
					// NP_rtMODE.SetPower_AOPC = NP_rtMODE.SetPower_AOPC_cmd;
					
					/* 2016-07-01 */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 64.654  + 2077;					
					/* 2016-07-07, 1A Laser Pump */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1749.4;
					/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1751.4;
					/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					/* 2016-08-18, modify, auto-calibration */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					
					/* 2016-05-13 */
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					
					/* 2016-07-12, may be a bug. */
					/* 2016-05-12, private mode no method to switch ACC/APC-AOPC, switch here! */
					// activeAOPC
					swAOPC
					
					// printf("MODE AOPC OK\n\r");
				}
				/* 2016-08-08, add AGC mode. */
				else if ( NP_rtMODE.MODE_cmd == MAGC ) {
					NP_rtMODE.MODE = MAGC;
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAGC;
					
					
					
					
					/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
					SetPower_AOPC_AGC = AppRtData.vInputOptdBm + NP_rtMODE.SetPower_AGC;
					if ( SetPower_AOPC_AGC > 20.0 ) {
						SetPower_AOPC_AGC = 20.0;
					}
					else if ( SetPower_AOPC_AGC < 15.0 ) {
						SetPower_AOPC_AGC = 15.0;
					}
					
					/* 2016-08-08, add AGC mode. */
					/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					/* 2016-08-18, modify, auto-calibration */
					// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
									
					/* 2016-07-12, may be a bug. */
					/* 2016-05-12, private mode no method to switch ACC/APC-AOPC, switch here! */
					// activeAOPC
					swAOPC
									
					// printf("MODE AOPC OK\n\r");
				}
				
				Console_ParseCmd(&ModeP[0]);
			}
			
			// NP_rtMODE.statePump = ON;
		}
		
		AppCfgMap.v.Private = 0;
		Console_ParseCmd(FLINECmd);

	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP command to get/readback operation mode of EDFA.
************************************************************************/
int cmd_GMODE(int argc)
{
	if ( argc == 1 ) {
		
		if ( AppCfgMap.v.OperationMode == MACC ) {
			printf("MODE: ACC \n\r");
		}
		else if ( AppCfgMap.v.OperationMode == MALC ) {
			if ( AppCfgMap.v.typeALC == MAPC ) {
				printf("MODE: APC \n\r");
			}
			else if ( AppCfgMap.v.typeALC == MAOPC ) {
				printf("MODE: AOPC \n\r");
			}
			/* 2016-08-08, add AGC mode. */
			else if ( AppCfgMap.v.typeALC == MAGC ) {
				printf("MODE: AGC \n\r");
			}
		}
		else if ( MDIS == AppCfgMap.v.OperationMode ) {
			if ( MACC == NP_rtMODE.MODE ) {
				printf("MODE: ACC \n\r");
			}
			else if ( MAPC == NP_rtMODE.MODE ) {
				printf("MODE: APC \n\r");
			}
			else if ( MAOPC == NP_rtMODE.MODE ) {
				printf("MODE: AOPC \n\r");
			}
			/* 2016-08-08, add AGC mode. */
			else if ( MAGC == NP_rtMODE.MODE ) {
				printf("MODE: AGC \n\r");
			}
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}

/************************************************************************
* @brief: NP command to display and update EDFA working mode.
************************************************************************/
int cmd_FLINE(int argc)
{
	/* 2016-05-16 */
	char EDFACmd[] = "GEDFA\0";
	char GMODECmd[] = "GMODE\0";
	char PUMPCmd[] = "PUMP\0";
	char PINCmd[] = "PIN\0";
	char POUTCmd[] = "POUT\0";
	char MTCmd[] = "MT\0";
	
	if ( 1 == argc ) {
		
		Console_ParseCmd(EDFACmd);
		
		Console_ParseCmd(GMODECmd);
		
		AppCfgMap.v.Private = 1;
				
		Console_ParseCmd(PUMPCmd);
		
		/* 2016-07-29, APC calibration. */
		/* 2016-07-11 */
		// printf("PUMP PWR: ? mW (%d)\n\r", (uint32_t)g_afec0_sample_data2);
		// printf("PUMP PWR: %.1f mW\n\r", (AppRtData.vOutputOptmW >0 ) ? AppRtData.vOutputOptmW : 0);
		/* 2016-09-05 */
		/*
		printf("PUMP PWR: %.1f mW, %.2f mV\n\r", (AppRtData.vOutputOptmW >0 ) ? AppRtData.vOutputOptmW : 0,
					(float)(g_afec0_sample_data2*VOLT_REF/MAX_DIGITAL_15_BIT));
		*/
		printf("PUMP PWR: %.1f mW, %.2f mV\n\r", (AppRtData.vOutputOptmW >0 ) ? (float)AppRtData.vOutputOptmW : 0,
					(float)pwr_voltage);
					
		
		Console_ParseCmd(PINCmd);
		
		Console_ParseCmd(POUTCmd);
		
		Console_ParseCmd(MTCmd);
		
		AppCfgMap.v.Private = 0;
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP Command to set operating Laser Pump current in ACC mode.
************************************************************************/
/* 2016-07-20, no "FLASH" after execute "LDC xxx". */
int cmd_LDC(int argc, char *argv[]) {
	int		i;
	uint8	WR[7];
	char	*count;
	float	tmpISP;
	
	char ModeM[16] = "MODE MNS\0";
	
	if(argc == 2) {
		count = argv[1];
		for(i = 0; (count[i] != '\0'); i++)
		
		if (isdigit(count[i])||count[i]==0x2E) {
			WR[i] = count[i];	//check if a number or '.'
		}
		else {
			printf("?Setting Out of Range\n\r");
			
			return 0;
		}
		
		WR[i] = '\0';
		tmpISP = atof(WR);
		
		/* 2016-08-16, 500mA Laser Pump. */
		/* 2016-07-07, 1A Laser Pump. */
		/* 2016-07-01, remote calibration, edfa in Fremont. */
		/* 2016-05-11, Laser Pump: 50 ~ 400 mA */
		/* 2016-08-24 */
		if ( tmpISP <= LDC_MAX && tmpISP >= LDC_MIN ) {
			AppCfgMap.v.Private = 1;

			if ( OFF == NP_rtMODE.statePump ) {
				if ( NP_rtMODE.MODE == MACC) {
					// NP_rtMODE.MODE = MACC;
					// AppCfgMap.v.OperationMode = MACC;
					
					NP_rtMODE.SetCurrent_ACC = tmpISP;
					AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
				}
				else {
					NP_rtMODE.SetCurrent_ACC = tmpISP;
					AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
				}
			}
			else if ( ON == NP_rtMODE.statePump ) {
				if ( NP_rtMODE.MODE == MACC) {
					// NP_rtMODE.MODE = MACC;
					AppCfgMap.v.OperationMode = MACC;
					
					NP_rtMODE.SetCurrent_ACC = tmpISP;
					AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
					
					Console_ParseCmd(&ModeM[0]);
				}
				else {
					NP_rtMODE.SetCurrent_ACC = tmpISP;
					AppCfgMap.v.ACCcurrent = NP_rtMODE.SetCurrent_ACC;
				}
			}
			
			/* 2016-05-20 */
			AppCfgMap.v.Private = 0;			
		}
		else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to get/readback current setting of the Laser Pump 
*         in mA units.
************************************************************************/
int cmd_GLDC(int argc) {
	if(argc == 1) {
		printf("PUMP Current Setting: %5.1f mA\n\r", AppCfgMap.v.ACCcurrent);
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}


/************************************************************************
* @brief: NP Command to set operating Laser Pump Power in APC mode.
************************************************************************/
int cmd_LDP(int argc, char *argv[]) {
	int i;
	uint8 WR[7];
	char *count;
	float tmpAPC;
	
	char ModeP[] = "MODE PNS\0";
	
	float SetPower_APC_mA;
	
	if ( 2 == argc ) {
		count = argv[1];
		
		for(i = 0; (count[i] != '\0'); i++) {
			
			if (isdigit(count[i])||count[i]==0x2E) {
				WR[i] = count[i];	//check if a number or '.'
			}
			else {
				printf("?Setting Out of Range\n\r");
				
				return 0;
			}
		}
		
		WR[i] = '\0';
		tmpAPC = atof(WR);
		
		/* 2016-08-16, 20dBm EDFA, pump diode current 450mA, pump laser output power 300mW. */
		/* 2016-08-05, 23dBm EDFA, pump diode current 940mA, pump laser output power 600mW. */
		/* 2016-08-24 */
		if (tmpAPC <= LDP_MAX && tmpAPC >= LDP_MIN ) {
			NP_rtMODE.SetPower_APC = (float)tmpAPC;	//20dBm equals to 100mW
			
			AppCfgMap.v.Private = 1;

			if ( OFF == NP_rtMODE.statePump ) {
				
				if ( NP_rtMODE.MODE == MAPC ) {
					AppCfgMap.v.typeALC = MAPC;
					
					/* 2016-08-16, 20dBm EDFA, pump diode current 450mA, pump laser output power 300mW. */
					/* 2016-08-05, APC mode, laser pump current mW -> mA. */
					/* 23dBm edfa, 940mA / 600mW */
					// NP_rtMODE.powerAD_APC = (-1) * NP_rtMODE.SetPower_APC*AppCfgMap.v.L + AppCfgMap.v.M;
					SetPower_APC_mA = NP_rtMODE.SetPower_APC * 940 / 600;
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * 450 / 300;
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
					NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
				}
				else {
				}
			}
			else if ( ON == NP_rtMODE.statePump ) {
				
				if ( NP_rtMODE.MODE == MAPC) {
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAPC;

					/* 2016-08-16, 20dBm EDFA, pump diode current 450mA, pump laser output power 300mW. */										
					/* 2016-08-05, APC mode, laser pump current mW -> mA. */
					/* 23dBm edfa, 940mA / 600mW */
					// NP_rtMODE.powerAD_APC = (-1) * NP_rtMODE.SetPower_APC*AppCfgMap.v.L + AppCfgMap.v.M;
					SetPower_APC_mA = NP_rtMODE.SetPower_APC * 940 / 600;
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * 450 / 300;
					// SetPower_APC_mA = NP_rtMODE.SetPower_APC * APC_SLOPE_mA_mW;
					NP_rtMODE.powerAD_APC = SetPower_APC_mA * AppCfgMap.v.L + AppCfgMap.v.M;
										
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_APC;
					
					/* 2016-07-21, may a bug. */
					// activeAPC
					swAPC
					
					Console_ParseCmd(&ModeP[0]);
				}
				else {
				}
			}

			AppCfgMap.v.Private = 0;
		}
		else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP command to get/readback power setting of Laser Pump in mW.
************************************************************************/
int cmd_GLDP(int argc) {
	if(argc == 1) {
		printf("APC PUMP Power Setting: %5.1f mW\n\r", NP_rtMODE.SetPower_APC);
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}


/* 2016-08-08 */
/************************************************************************
* @brief: NP Command to set operating Laser Pump Power in AGC mode.
************************************************************************/
int cmd_AGC(int argc, char *argv[])
{
	int i;
	uint8 WR[7];
	char *count;
	float tmpAGC;
	
	float SetPower_AOPC_AGC;
	
	char ModeP[] = "MODE PNS\0";

	if ( 2 == argc ) {
		count = argv[1];
		
		for (i = 0; (count[i] != '\0'); i++) {
			if (isdigit(count[i])||count[i]==0x2E) {
				WR[i] = count[i];	//check if a number or '.'
			}
			else {
				printf("?Setting Out of Range\n\r");
				
				return 0;
			}
		}
		
		WR[i] = '\0';
		tmpAGC = atof(WR);
		
		/* 2016-08-16, 500mA Laser Pump, unit dB, maximum Gain 17dB. */
		/* 2016-08-08, AGC Gain: 0 ~ 20 dB. */
		if ( tmpAGC <= AGC_MAX && tmpAGC >= AGC_MIN ) {
			
			NP_rtMODE.SetPower_AGC = (float)tmpAGC;
			
			/* 2016-05-20 */
			AppCfgMap.v.Private = 1;

			if ( OFF == NP_rtMODE.statePump ) {
				
				if ( NP_rtMODE.MODE == MAGC ) {
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAGC;			
					
					/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
					SetPower_AOPC_AGC = AppRtData.vInputOptdBm + NP_rtMODE.SetPower_AGC;
					if ( SetPower_AOPC_AGC > 20.0 ) {
						SetPower_AOPC_AGC = 20.0;
					}
					else if ( SetPower_AOPC_AGC < 15.0 ) {
						SetPower_AOPC_AGC = 15.0;
					}
					
					/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					/* 2016-08-18, modify, auto-calibration */
					// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
				}
				else {
				}
			}
			else if ( ON == NP_rtMODE.statePump ) {				
				if ( NP_rtMODE.MODE == MAGC) {
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAGC;
					
					/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
					SetPower_AOPC_AGC = AppRtData.vInputOptdBm + NP_rtMODE.SetPower_AGC;
					if ( SetPower_AOPC_AGC > 20.0 ) {
						SetPower_AOPC_AGC = 20.0;
					}	
					else if ( SetPower_AOPC_AGC < 15.0 ) {
						SetPower_AOPC_AGC = 15.0;
					}

					/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					/* 2016-08-18, modify, auto-calibration */
					// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					
					/* 2016-07-21, may a bug. */
					// activeAOPC
					swAOPC

					Console_ParseCmd(&ModeP[0]);
				}
				else {
				}
			}

			/* 2016-05-20 */
			AppCfgMap.v.Private = 0;
			
		}
		else {
			printf("?Setting Out of Range\n\r");
			
			return 0;
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/* 2016-08-09 */
/************************************************************************
* @brief: NP command to get/readback Output Power setting for AOPC mode
*         in dBm units.
************************************************************************/
int cmd_GAGC(int argc) {
	if(argc == 1) {
		printf("AGC Gain Setting: %5.1f dBm\n\r", NP_rtMODE.SetPower_AGC);
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP Command to set operating Laser Pump Power in AOPC mode.
************************************************************************/
int cmd_AOPC(int argc, char *argv[])
{
	int i;
	uint8 WR[7];
	char *count;
	float tmpAOPC;
	
	char ModeP[] = "MODE PNS\0";

	
	if ( 2 == argc ) {
		count = argv[1];
		
		for (i = 0; (count[i] != '\0'); i++) {
			if (isdigit(count[i])||count[i]==0x2E) {
				WR[i] = count[i];	//check if a number or '.'
			}
			else {
				printf("?Setting Out of Range\n\r");
				
				return 0;
			}
		}
		
		WR[i] = '\0';
		tmpAOPC = atof(WR);
		
		/* 2016-08-16, 500mA Laser Pump, maximum output +20dBm, limit to 20dBm due to 500A. */
		/* 2016-07-07, 1A Laser Pump, maximum output +23dBm, limit to 21dBm due to 1A. */
		/* 2016-08-24 */
		if ( tmpAOPC <= AOPC_MAX && tmpAOPC >= AOPC_MIN ) {
			
			NP_rtMODE.SetPower_AOPC = (float)tmpAOPC;
			
			/* 2016-05-20 */
			AppCfgMap.v.Private = 1;

			if ( OFF == NP_rtMODE.statePump ) {
				
				if ( NP_rtMODE.MODE == MAOPC ) {
					AppCfgMap.v.OperationMode = MALC;
					AppCfgMap.v.typeALC = MAOPC;
					
					/* 2016-05-20, from Vincent'data */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 44.18  + 1426.1;
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 65.869  + 1910.6;
					/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1751.4;
					/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					/* 2016-08-18, modify, auto-calibration */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
										
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
				}
				else {
				}
			}
			else if ( ON == NP_rtMODE.statePump ) {
				
				if ( NP_rtMODE.MODE == MAOPC) {
					/* 2016-08-08, lost, but no affect. */
					AppCfgMap.v.OperationMode = MALC;
					
					AppCfgMap.v.typeALC = MAOPC;

					/* 2016-05-20, from Vincent'data */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 44.18  + 1426.1;
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 65.869  + 1910.6;
					/* 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * 39.813  + 1751.4;
					/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
					/* 2016-08-18, modify, auto-calibration */
					// NP_rtMODE.powerAD_AOPC = (-1) * NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					NP_rtMODE.powerAD_AOPC = NP_rtMODE.SetPower_AOPC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
					
					
					AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;
					
					/* 2016-07-21, may a bug. */
					/* 2016-06-08, a bug. */
					// activeAPC
					// activeAOPC
					swAOPC


					Console_ParseCmd(&ModeP[0]);
				}
				else {
				}
			}

			/* 2016-05-20 */
			AppCfgMap.v.Private = 0;
			
		}
		else {
			printf("?Setting Out of Range\n\r");
			
			return 0;
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP command to get/readback Output Power setting for AOPC mode 
*         in dBm units.
************************************************************************/
int cmd_GAOPC(int argc) {
	if(argc == 1) {
		printf("AOPC PUMP Power Setting: %5.1f dBm\n\r", NP_rtMODE.SetPower_AOPC);
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to preset/default LD operating power upon power up 
*         in APC mode
************************************************************************/
int cmd_PLDP(int argc, char *argv[]) {
	int		i;
	uint8	WR[7];
	char	*count;
	float	tmpAPC;
	
	if(argc == 2) {
		count = argv[1];
		for(i = 0; (count[i] != '\0'); i++)
		if (isdigit(count[i])||count[i]==0x2E) WR[i] = count[i];	//check if a number or '.'
		else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
		
		WR[i] = '\0';
		tmpAPC = atof(WR);

		/* 2016-08-15, 940mA / 600mW. */
		/* 2016-07-07, 1A Laser Pump, maximum output +23dBm, limit to 21dBm due to 1A, APC DAC minimum 390.*/
		// if (tmpAPC<=100 && tmpAPC>=0) {
		// if (tmpAPC<=550 && tmpAPC>=0) {
		/* 2016-09-02 */
		if ( tmpAPC <= LDP_MAX && tmpAPC>= LDP_MIN ) {
		
			NP_iniMODE.SetPower_APC = tmpAPC;
			NP_iniMODE.MODE = MAPC;
			
			/* 2016-05-10, add when execute cmd_FLASH(). */
			/*
			NP_rtMODE.MODE = MAPC;
			NP_rtMODE.SetPower_APC = tmpAPC;
			// NP_rtMODE.powerAD_APC;
			*/
		} else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}


/* 2016-08-08, add AGC mode. */
/************************************************************************
* @brief: NP command to preset/default operating output power in AGC mode
************************************************************************/
int cmd_PAGC(int argc, char *argv[]) {
	int		i;
	uint8	WR[7];
	char	*count;
	float	tmpAGC;
	
	if ( argc == 2 ) {
		count = argv[1];
		
		for(i = 0; (count[i] != '\0'); i++) {
			if (isdigit(count[i])||count[i]==0x2E) {
				WR[i] = count[i];	//check if a number or '.'
			}
			else {
				printf("?Setting Out of Range\n\r");
				return 0;
			}
		}
		
		WR[i] = '\0';
		tmpAGC = atof(WR);
		
		/* 2016-09-02 */
		/* 2016-08-16, 500mA Laser Pump, unit dB, maximum Gain 17dB. */
		// if (tmpAGC<=17 && tmpAGC>=0) {
		if (tmpAGC <= AGC_MAX && tmpAGC >= AGC_MIN ) {
			NP_iniMODE.SetPower_AGC = tmpAGC;
			NP_iniMODE.MODE = MAGC;
		}
		else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP command to preset/default operating output power in AOPC mode
************************************************************************/
int cmd_PAOPC(int argc, char *argv[]) {
	int		i;
	uint8	WR[7];
	char	*count;
	float	tmpAOPC;
	
	if(argc == 2) {
		count = argv[1];
		for(i = 0; (count[i] != '\0'); i++)
		if (isdigit(count[i])||count[i]==0x2E) WR[i] = count[i];	//check if a number or '.'
		else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
		
		WR[i] = '\0';
		tmpAOPC = atof(WR);
		
		/* 2016-08-16, 500mA Laser Pump, maximum output +20dBm, limit to 20dBm due to 500A. */
		/* 2016-07-07, 1A Laser Pump, maximum output +23dBm, limit to 21dBm due to 1A. */
		/* 2016-05-13, unit dBm */
		// if (tmpAOPC<=20.0 && tmpAOPC>=0) {
		if ( tmpAOPC <= AOPC_MAX && tmpAOPC >= AOPC_MIN ) {
		
			NP_iniMODE.SetPower_AOPC = tmpAOPC;
			NP_iniMODE.MODE = MAOPC;
		} else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to Preset/default LD operating current upon powerup
*         in ACC mode
************************************************************************/
int cmd_PLDC(int argc, char *argv[]) {
	int		i;
	uint8	WR[7];
	char	*count;
	float	tmpACC;
	
	if(argc == 2) {
		count = argv[1];
		for(i = 0; (count[i] != '\0'); i++)
		if (isdigit(count[i])||count[i]==0x2E) WR[i] = count[i];	//check if a number or '.'
		else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
		
		WR[i] = '\0';
		tmpACC = atof(WR);
		
		/* 2016-08-16, 1A Laser Pump. */
		/* 2016-07-07, 1A Laser Pump. */
		/* 2016-05-11, Laser Pump 50 ~ 400 mA */
		// if (tmpACC<=500 && tmpACC>=10) {
		// if (tmpACC<=1000 && tmpACC>=10) {
		if ( tmpACC <= LDC_MAX && tmpACC >= LDC_MIN ) {					
			NP_iniMODE.SetCurrent_ACC = tmpACC;
			NP_iniMODE.MODE = MACC;
		} else {
			printf("?Setting Out of Range\n\r");
			return 0;
		}
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to Set Input Power Alarm Level in dBm
************************************************************************/
int cmd_LOS_Level(int argc, char *argv[])
{
	/* 2016-05-26 */
	volatile char cmd[BUFFER_SIZE +2] = "ALRM LOS THR ";
	int len_str = 0;
		
	if(argc == 2) {
		AppCfgMap.v.Private = 1;
		
		/* 2016-05-26 */
		/*
		char* cmd = concat("ALRM LOS THR ", argv[1]);
		
		// 2016-05-24
		// 2016-05-16
		if ( NULL != cmd ) {
			Console_ParseCmd(cmd);
			
			free(cmd);
			cmd = NULL;
		}
		*/
		if ( (len_str = strlen(argv[1])) && len_str < (BUFFER_SIZE - 14) ) {
			/* 2016-05-26, debug, delete later. */
			// cmd[13 + len_str] = 0x88;
					
			// 13 is the size of the string "ALRM LOS THR ".
			strncpy(&cmd[13], argv[1], len_str);
			cmd[13 + len_str] = '\0';
					
			Console_ParseCmd(cmd);
		}
		else {
			printf("?Parameters Error\n\r");
		}
		
		AppCfgMap.v.Private = 0;
	} 
	else
		printf("?Parameters Error\n\r");
		
	return 0;
}

/************************************************************************
* @brief: NP command to Set Output Power Alarm Level in dBm
************************************************************************/
int cmd_LOP_Level(int argc, char *argv[])
{
	/* 2016-05-26 */
	volatile char cmd[BUFFER_SIZE +2] = "ALRM LOP THR ";
	int len_str = 0;
	
	if ( argc == 2 ) {
		AppCfgMap.v.Private = 1;
		
		/* 2016-05-26 */
		/*
		char* cmd = concat("ALRM LOP THR ", argv[1]);

		// 2016-05-24
		// 2016-05-16
		if ( NULL != cmd ) {
			Console_ParseCmd(cmd);
			
			free(cmd);
			cmd = NULL;
		}
		*/
		if ( (len_str = strlen(argv[1])) && len_str < (BUFFER_SIZE - 14) ) {
			/* 2016-05-26, debug, delete later. */
			// cmd[13 + len_str] = 0x88;
			
			// 13 is the size of the string "ALRM LOP THR ".
			strncpy(&cmd[13], argv[1], len_str);
			cmd[13 + len_str] = '\0';
			
			Console_ParseCmd(cmd);
		}
		else {
			printf("?Parameters Error\n\r");
		}

		AppCfgMap.v.Private = 0;
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}

/************************************************************************
* @brief: NP command to Get/Read Alarms Detected 
*		  1:Optical Input, 
*		  2:Optical output, 
*		  4:LD temperature, 
*		  8:LD drive current, 
*		 16:EDFA Case temperature
************************************************************************/
int cmd_GALRM(int argc) {
	if(argc == 1) {
		/* 2016-07-12
		 1:Optical Input,
		 2:Optical output,
		 4:LD temperature,
		 8:LD drive current,
		 16:EDFA Case temperature
		*/		
		/*
		printf("ALRM LOS STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.InputOpt));
		printf("ALRM LOP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.OutputOpt));		
		printf("ALRM TMP STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Temp));
		printf("ALRM ILD STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Bias));
		printf("ALRM MTH STA: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.CaseTemp));
		*/
		printf("1:Optical Input: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.InputOpt));
		printf("2:Optical output: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.OutputOpt));
		printf("4:LD temperature: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Temp));
		printf("8:LD drive current: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.Pump1Bias));
		printf("16:EDFA Case temperature: %s\n\r", Check_TmpStatus(AppRtData.RtFlag.Bits.CaseTemp));
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to Get/Read Alarm bitwise display
*		  1:Optical Input,
*		  2:Optical output,
*		  4:LD temperature,
*		  8:LD drive current,
*		 16:EDFA Case temperature
************************************************************************/
int cmd_GALRMB(int argc) {
	int8_t tmp=0;
	
	if(argc == 1) {
		if (AppRtData.RtFlag.Bits.InputOpt)		tmp|=1;
		if (AppRtData.RtFlag.Bits.OutputOpt)	tmp|=2;
		if (AppRtData.RtFlag.Bits.Pump1Temp)	tmp|=4;
		if (AppRtData.RtFlag.Bits.Pump1Bias)	tmp|=8;
		if (AppRtData.RtFlag.Bits.CaseTemp)		tmp|=16;
		
		char buff[5];
		itoa(tmp, buff, 2);
		
		printf("ALRMS: %5s\n\r", buff);
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to Set EDFA default ON/OFF state upon DC power-up
************************************************************************/
int cmd_PWRUP(int argc, char *argv[]) {
	if(argc == 2) {
		if(CmdStrCompare(argv[1],"OFF")){
			/* 2016-05-10 */		
			// NP_iniMODE.statePump = OFF;
			NP_iniMODE.iniStatusLD = OFF;
			
			save(rec_NUPhoton);
		} else if(CmdStrCompare(argv[1],"ON")){
			/* 2016-05-10 */
			// NP_iniMODE.statePump = ON;
			NP_iniMODE.iniStatusLD = ON;
			
			save(rec_NUPhoton);
		} else {
			printf("?Parameters Error\n\r");
		}
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: NP command to Enable Automatic Laser shutdown during absence 
*         of input
************************************************************************/
int cmd_LIPS(int argc, char *argv[])
{
	/* 2016-05-25 */
	char cmdLOSN[] = "LOS N";
	char cmdLOSA[] = "LOS A";
	
	if ( 2 == argc ) {
		AppCfgMap.v.Private = 1;
		
		if ( CmdStrCompare(argv[1],"OFF") ) {
			/* 2016-05-25 */
			/*
			char* cmd = concat("LOS ", "N");

			// 2016-05-24
			// 2016-05-16
			if ( NULL != cmd ) {
				Console_ParseCmd(cmd);
				
				free(cmd);
				cmd = NULL;
			}
			*/
			
			/* 2016-09-22, a bug. */
			/* 2016-08-23, save "LIPS OFF". */
			// AppCfgMap.v.bPumpAutoClose = LOS_A;
			AppCfgMap.v.bPumpAutoClose = LOS_N;
			save(rec_AppCfg);
			
			
			Console_ParseCmd(cmdLOSN);
		}
		else if ( CmdStrCompare(argv[1],"ON") ) {
			/* 2016-05-25 */
			/*
			char* cmd = concat("LOS ", "A");

			// 2016-05-24
			// 2016-05-16
			if ( NULL != cmd ) {
				Console_ParseCmd(cmd);
				
				free(cmd);
				cmd = NULL;
			}
			*/
			
			/* 2016-09-22, a bug. */
			/* 2016-08-23, save "LIPS OFF". */
			// AppCfgMap.v.bPumpAutoClose = LOS_N;
			AppCfgMap.v.bPumpAutoClose = LOS_A;
			
			save(rec_AppCfg);
			
			Console_ParseCmd(cmdLOSA);
		}
		else {
			printf("?Parameters Error\n\r");
		}
		
		AppCfgMap.v.Private = 0;
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}


/************************************************************************
* @brief: NP command to Show operating status and errors
************************************************************************/
int cmd_DIAG(int argc) {
	char allPass = 1;
	
  if(argc == 1) {
	printf("Alarm Status:");  		
	if(AppCfgMap.v.Latch) {
		if(alarmLatch.ILD) {
			allPass = 0;
			printf(" ILD");
		}
		if(alarmLatch.TMP) {
			allPass = 0;
			printf(" TMP");
		}
		if(alarmLatch.MTH) {
			allPass = 0;
			printf(" MTH");
		}			   
		if(alarmLatch.MTL) {
			allPass = 0;
			printf(" MTL");
		}
		if(alarmLatch.LOS) {
			allPass = 0;
			printf(" LOS");
		}
		if(alarmLatch.LOP) {
			allPass = 0;
			printf(" LOP");
		}
	} else {
		if(AppRtData.RtFlag.Bits.Pump1Bias == 2) {
			allPass = 0;
			printf(" ILD");
		}
		if(AppRtData.RtFlag.Bits.Pump1Temp==2) {
			allPass = 0;
			printf(" TMP");
		}
		if(AppRtData.RtFlag.Bits.CaseTemp == 2) {
			allPass = 0;
			printf(" MTH");
		}			   
		if(AppRtData.RtFlag.Bits.Case2Temp == 1) {
			allPass = 0;
			printf(" MTL");
		}
		if(AppRtData.RtFlag.Bits.InputOpt==1) {
			allPass = 0;
			printf(" LOS");
		}
		if(AppRtData.RtFlag.Bits.OutputOpt==1) {
			allPass = 0;
			printf(" LOP");
		}
	}
		if(allPass) {
			printf(" OK (No alarms)");
		}	
		printf("\n\r");
		
		return 0;
	} else
	printf("?Parameters Error\n\r");
	
    return -1;
}

/************************************************************************
* @brief: NP command to Set EDFA default ON/OFF state upon DC power-up
************************************************************************/
int cmd_CRSR(int argc, char *argv[])
{	
	if ( argc == 2 ) {
		if ( CmdStrCompare(argv[1],"OFF") ) {
			NP_rtMODE.statePrompt = OFF;
			
			/* 2016-09-06 */
			// save(rec_NUPhoton);
		}
		else if ( CmdStrCompare(argv[1],"ON") ) {
			NP_rtMODE.statePrompt = ON;
			
			/* 2016-09-06 */
			// save(rec_NUPhoton);
		}
		else {
			printf("?Parameters Error\n\r");
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}

/************************************************************************
* @brief: NP command to Set EDFA default ON/OFF state upon DC power-up
************************************************************************/
int cmd_IMSG(int argc, char *argv[])
{
	if ( argc == 2 ) {
		
		if ( CmdStrCompare(argv[1],"OFF") ) {
			NP_iniMODE.headMsg = OFF;
			
			/* 2016-09-06 */
			save(rec_NUPhoton);
		}
		else if ( CmdStrCompare(argv[1],"ON") ) {
			NP_iniMODE.headMsg = ON;
			
			/* 2016-09-06 */
			save(rec_NUPhoton);
		}
		else {
			printf("?Parameters Error\n\r");
		}
	}
	else {
		printf("?Parameters Error\n\r");
	}
	
	return 0;
}

/************************************************************************
* @brief: Check the status of Amplifier disable input.
************************************************************************/
int cmd_Disable(int argc) {
	if(argc == 1) {
		if (pio_get(PIN_PUSHBUTTON_1_PIO, PIO_TYPE_PIO_INPUT, PIN_PUSHBUTTON_1_MASK)) {
			printf("DISABLE_Pin: High.\n\r");
		} else {
			printf("DISABLE_Pin: Low.\n\r");
		}
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: write parameters to non-voltile memory
************************************************************************/
int cmd_SAVES(int argc) {
	if(argc == 1) {
		save(rec_ALL);
	}
	else
	printf("?Parameters Error\n\r");
	
	return 0;
}

/************************************************************************
* @brief: Direct Current Control for engineering  
************************************************************************/
int  cmd_CC(int argc, char *argv[])
{
	int i;
	uint8 WR[7];//write Resistor 0~4095
	char *count;

	if(argc == 1)
	{
		printf("ASE Set:%d\n\r",AppCfgMap.v.DACASE);
		printf("Resistor Set:%d\n\r",AppCfgMap.v.AD5322Resister);
		printf("DAC ACC THR Set:%d\n\r",AppCfgMap.v.DACACCTHR);
		printf("DAC ALC THR Set:%d\n\r",AppCfgMap.v.DACALCTHR);
	}
	else if(argc == 3)
	{
		
		count = argv[2];
		for(i = 0; (count[i] != '\0'); i++)
		{
			WR[i] = count[i];
		}
		
		if(CmdStrCompare(argv[1],"0")) {
			WR[i] = '\0';
			AppCfgMap.v.DACASE = atoi(WR);
			DAC1_Output(AppCfgMap.v.DACASE, CH_B);
			printf("ASECompensation Set:%d\n\r",AppCfgMap.v.DACASE);
		}
		else if(CmdStrCompare(argv[1],"1"))
		{
			WR[i] = '\0';
			AppCfgMap.v.AD5322Resister = atoi(WR);
			if((NP_rtMODE.MODE == MAOPC) || (NP_rtMODE.MODE == MAPC))
			{
				if(AppCfgMap.v.AD5322Resister > AppCfgMap.v.DACALCTHR) {
					setLD_Value(AppCfgMap.v.AD5322Resister);
					printf("AOPC Set:%d\n\r",AppCfgMap.v.AD5322Resister);
				}
				else if(AppCfgMap.v.AD5322Resister < AppCfgMap.v.DACALCTHR) {
					printf("ALC Over Range!\n\r");
				}
			}
			else if(NP_rtMODE.MODE == MACC)
			{
				if(AppCfgMap.v.AD5322Resister > AppCfgMap.v.DACACCTHR)
				{
					
					setLD_Value(AppCfgMap.v.AD5322Resister);
					printf("ACC Set:%d\n\r",AppCfgMap.v.AD5322Resister);
				}
				else if(AppCfgMap.v.AD5322Resister < AppCfgMap.v.DACACCTHR)
				{
					printf("ACC Over Range!\n\r");
				}
			}
		}
		if(CmdStrCompare(argv[1],"ACCTHR"))
		{
			WR[i] = '\0';
			AppCfgMap.v.DACACCTHR = atoi(WR);
			printf("DAC ACC THR Set:%d\n\r",AppCfgMap.v.DACACCTHR);
		}
		if(CmdStrCompare(argv[1],"ALCTHR"))
		{
			WR[i] = '\0';
			AppCfgMap.v.DACALCTHR = atoi(WR);
			printf("DAC ALC THR Set:%d\n\r",AppCfgMap.v.DACALCTHR);
		}
	}
	//OnEEPROM_Write_Page(Addr_AppCfgMap_Ini,(unsigned char*)&AppCfgMap,sizeof(AppCfgMap));
	//OnEEPROM_Write_Page(Addr_Hys_Ini,(unsigned char*)&Hys,sizeof(Hys));
	//save(rec_ALL);
	return 0;
}

/**
  * @brief Write Test for SPI EEPROM
  * 
  */
int  cmd_WEE(int argc, char *argv[]) {
	uint16_t page_addr, i, j, k;
	uint8_t DATA[64], PG[4];//read Page 0~255
	char *tmp;
	
	if(argc == 1) {
		puts("Usage: WEE #page_no. data_array\n\r");
		return 0;
	}

	if(argc > 2) {
		tmp = argv[1];
		for(i=0; (tmp[i] != '\0'); i++) {
			PG[i] = tmp[i];
		}
		PG[i] = '\0';
		page_addr = atoi(PG) << 6;	//1 page = 64 bytes, the 'PG'th page
		
		tmp = argv[2];
		j=0;
		for(i=0; i<sizeof(DATA); i++) {
			for (k=0;k<4;k++) PG[k]='\0';
			k = 0;
			while (tmp[j] != '\0') {
				PG[k++] = tmp[j++];
			}
			DATA[i] = atoi(PG);
			j++;											//bypass all space '\0' chars
		}
		
		OnEEPROM_Write_Page(DATA,sizeof(DATA),page_addr);
		
		printf("WEE: Done \n\r");
	}
	return 0;
}
/**
  * @brief Read Test for SPI EEPROM
  * 
  */
int  cmd_REE(int argc, char *argv[])
{
	uint16_t page_addr, i;
	uint8_t tmp[64],PG[7];//read Page 0~255
	char *count;
	
	if(argc == 1) {
		puts("Usage: REE #page_no.\n\r");
		return 0;
	}

	if(argc == 2) {
		count = argv[1];
		for(i=0; (count[i] != '\0'); i++) {
			PG[i] = count[i];
		}
		PG[i] = '\0';
		page_addr = atoi(PG) << 6;	//1 page = 64 bytes
		
		OnEEPROM_Read_Page(tmp,sizeof(tmp),page_addr);
		
		//printf("REE:");
		//for (i=0;i<sizeof(tmp);i++) {
		//	SBUF0 = tmp[i];
		//	while (!TI0);	// Wait for transmit complete
		//	TI0 = 0;
		//}
		printf("\n\r");
	}
	return 0;
}
