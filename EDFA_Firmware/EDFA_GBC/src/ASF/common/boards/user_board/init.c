/**
 * \file
 *
 * \brief User board initialization template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include "compiler.h"
#include "board.h"
#include "conf_board.h"
#include "ioport.h"

#include <asf.h>
//#include <board.h>
//#include <conf_board.h>


/**
 * \brief Set peripheral mode for IOPORT pins.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param port IOPORT port to configure
 * \param masks IOPORT pin masks to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_port_peripheral_mode(port, masks, mode) \
	do {\
		ioport_set_port_mode(port, masks, mode);\
		ioport_disable_port(port, masks);\
	} while (0)

/**
 * \brief Set peripheral mode for one single IOPORT pin.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param pin IOPORT pin to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 */
#define ioport_set_pin_peripheral_mode(pin, mode) \
	do {\
		ioport_set_pin_mode(pin, mode);\
		ioport_disable_pin(pin);\
	} while (0)

/**
 * \brief Set input mode for one single IOPORT pin.
 * It will configure port mode and disable pin mode (but enable peripheral).
 * \param pin IOPORT pin to configure
 * \param mode Mode masks to configure for the specified pin (\ref ioport_modes)
 * \param sense Sense for interrupt detection (\ref ioport_sense)
 */
#define ioport_set_pin_input_mode(pin, mode, sense) \
	do {\
		ioport_set_pin_dir(pin, IOPORT_DIR_INPUT);\
		ioport_set_pin_mode(pin, mode);\
		ioport_set_pin_sense_mode(pin, sense);\
	} while (0)


/**
 *  Initial EDFA SPI
 */
void OnEDFA_SPI_Initial(void) {
	
	spi_master_init(EDFA_SPI);
	
	spi_enable_clock(EDFA_SPI);
	spi_set_lastxfer(EDFA_SPI);
	spi_set_master_mode(EDFA_SPI);
	spi_disable_mode_fault_detect(EDFA_SPI);
	//spi_set_peripheral_chip_select_value(EDFA_SPI, NO_CH_SELECTED);
	//spi_get_pcs(0);
	spi_set_fixed_peripheral_select(EDFA_SPI);

	spi_disable_peripheral_select_decode(EDFA_SPI);
	//spi_enable_peripheral_select_decode(EDFA_SPI);

	spi_set_delay_between_chip_select(EDFA_SPI, CONFIG_SPI_MASTER_DELAY_BCS);

	// SPI for EEPROM M95128
	spi_set_clock_polarity(EDFA_SPI, SPI_CH0, SPI_CH0_CLK_POL);
	spi_set_clock_phase(EDFA_SPI, SPI_CH0, SPI_CH0_CLK_PHASE);
	spi_set_bits_per_transfer(EDFA_SPI, SPI_CH0, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(EDFA_SPI, SPI_CH0, (sysclk_get_cpu_hz()/SPI_CLK_CH0));
	spi_set_transfer_delay(EDFA_SPI, SPI_CH0, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(EDFA_SPI, SPI_CH0, SPI_CS_KEEP_LOW);

	// SPI for DAC1
	spi_set_clock_polarity(EDFA_SPI, SPI_CH1, SPI_CH1_CLK_POL);
	spi_set_clock_phase(EDFA_SPI, SPI_CH1, SPI_CH1_CLK_PHASE);
	spi_set_bits_per_transfer(EDFA_SPI, SPI_CH1, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(EDFA_SPI, SPI_CH1, (sysclk_get_cpu_hz()/SPI_CLK_CH1));
	spi_set_transfer_delay(EDFA_SPI, SPI_CH1, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(EDFA_SPI, SPI_CH1, SPI_CS_KEEP_LOW);

	// SPI for DAC2
	spi_set_clock_polarity(EDFA_SPI, SPI_CH2, SPI_CH2_CLK_POL);
	spi_set_clock_phase(EDFA_SPI, SPI_CH2, SPI_CH2_CLK_PHASE);
	spi_set_bits_per_transfer(EDFA_SPI, SPI_CH2, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(EDFA_SPI, SPI_CH2, (sysclk_get_cpu_hz()/SPI_CLK_CH2));
	spi_set_transfer_delay(EDFA_SPI, SPI_CH2, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(EDFA_SPI, SPI_CH2, SPI_CS_KEEP_LOW);
	
	// SPI for DAC3
	spi_set_clock_polarity(EDFA_SPI, SPI_CH3, SPI_CH3_CLK_POL);
	spi_set_clock_phase(EDFA_SPI, SPI_CH3, SPI_CH3_CLK_PHASE);
	spi_set_bits_per_transfer(EDFA_SPI, SPI_CH3, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(EDFA_SPI, SPI_CH3, (sysclk_get_cpu_hz()/SPI_CLK_CH3));
	spi_set_transfer_delay(EDFA_SPI, SPI_CH3, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(EDFA_SPI, SPI_CH3, SPI_CS_KEEP_LOW);

	spi_enable(EDFA_SPI);
}

void board_init(void)
{
	#ifndef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
	/* Disable the watchdog */
	// WDT->WDT_MR = WDT_MR_WDDIS;
	#endif
	
	/* 2016-05-24, enable watchdog. */
	/*
	// bit15 = 0, bit13 = 1, enable watchdog, and watchdog reset enable.
	WDT->WDT_CR = ( 0xA5000000 | WDT_CR_WDRSTT);
	nop();
	nop();
	nop();
	nop();
	nop();
	
	WDT->WDT_MR = WDT_MR_WDRSTEN;
	
	while (1) {
		// WDT->WDT_CR |= WDT_CR_KEY_PASSWD;
		// WDT->WDT_CR = ( WDT_CR_KEY_PASSWD | WDT_CR_WDRSTT);
		WDT->WDT_CR = ( 0xA5000000 | WDT_CR_WDRSTT);
	}
	*/


	/* Initialize IOPORTs */
	ioport_init();
	
	/* 2016-05-19, MCU wait +2.4V stable. */

	int i, j;
	/*
	for (i=0; i<1000; i++)
		for (j=0; j<1000; j++)
			;
	*/

	
	/* 2016-05-19, disable  PUMP first, Disable_C low */
	ioport_set_pin_dir(SW_Output, IOPORT_DIR_OUTPUT);
	pio_set_pin_low(SW_Output);
	
	for (i=0; i<1000; i++)
		;
	
	// LE
	ioport_set_pin_dir(SW_Latch_Din, IOPORT_DIR_OUTPUT);
	pio_set_pin_low(SW_Latch_Din);
	
		for (i=0; i<1000; i++)
		;
		
	// OE
	ioport_set_pin_dir(SW_Latch_Qout, IOPORT_DIR_OUTPUT);
	pio_set_pin_high(SW_Latch_Qout);
	

		for (i=0; i<1000; i++)
		;
		
	pio_set_pin_high(SW_Latch_Din);		
		for (i=0; i<1000; i++)
		;

	// pio_set_pin_low(SW_Latch_Qout);
	// pio_set_pin_low(SW_Latch_Din);

	
	/*
	#define activeOE pio_set_pin_high(SW_Latch_Qout);
	#define disactOE pio_set_pin_low(SW_Latch_Qout);
	//U19
	#define activeLE pio_set_pin_high(SW_Latch_Din);		//74373 Qout no changes.
	#define disactLE pio_set_pin_low(SW_Latch_Din);
	disactLE;
	activeOE;
	activeLE;
	*/
	

	/* Configure the pins connected to PID control switches and set their
	 * default initial state .
	 */
	ioport_set_pin_dir(SW_APC, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_APC, SW_MODE_OAPC);
	ioport_set_pin_dir(SW_FEEDBACK, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_FEEDBACK, SW_FB_C);
	ioport_set_pin_dir(SW_PID, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_PID, SW_Control_Set);
	ioport_set_pin_dir(SW_Output, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_Output, SW_O_EN);
	
	/* Configure the pins connected to LATCh and EEPROM control switches and set their
	 * default initial state .
	 */
	ioport_set_pin_dir(SW_Latch_Din, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_Latch_Din, SW_Latch_Active);
	ioport_set_pin_dir(SW_Latch_Qout, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_Latch_Qout, SW_Latch_Active);
	ioport_set_pin_dir(SW_EEPROM, IOPORT_DIR_INPUT);
	ioport_set_pin_mode(SW_EEPROM, IOPORT_MODE_PULLUP);
	//ioport_set_pin_level(SW_EEPROM, SW_Read);
	
	/* Configure the pins connected to Alarm Output control switches and set their
	 * default initial state .
	 */
	ioport_set_pin_dir(SW_CaseTemp, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_CaseTemp, SW_ALARM_DeAct);
	ioport_set_pin_dir(SW_PumpTemp, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_PumpTemp, SW_ALARM_DeAct);
	ioport_set_pin_dir(SW_PumpBias, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_PumpBias, SW_ALARM_DeAct);
	ioport_set_pin_dir(SW_LOI, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_LOI, SW_ALARM_DeAct);
	ioport_set_pin_dir(SW_LOP, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_LOP, SW_ALARM_DeAct);
	ioport_set_pin_dir(SW_CommonALRM, IOPORT_DIR_OUTPUT);
	/* 2016-07-12 */
	// ioport_set_pin_level(SW_CommonALRM, SW_ALARM_DeActN);
	ioport_set_pin_level(SW_CommonALRM, SW_ALARM_DeAct);
	
	/* Configure the pins connected to High Power LASER control switches and set their
	 * default initial state .
	 */
	ioport_set_pin_dir(SW_Seed, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_Seed, SW_O_DIS);
	ioport_set_pin_dir(SW_3rd_PUMP, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(SW_3rd_PUMP, SW_O_DIS);

	/* Configure 3 input pins: DISABLE,  MUTE and RESET */
	ioport_set_pin_input_mode(GPIO_PUSH_BUTTON_1, GPIO_PUSH_BUTTON_1_FLAGS,
			GPIO_PUSH_BUTTON_1_SENSE);
	ioport_set_pin_input_mode(GPIO_PUSH_BUTTON_2, GPIO_PUSH_BUTTON_2_FLAGS,
			GPIO_PUSH_BUTTON_2_SENSE);
	ioport_set_pin_input_mode(GPIO_PUSH_BUTTON_3, GPIO_PUSH_BUTTON_3_FLAGS,
			GPIO_PUSH_BUTTON_3_SENSE);
			
	/* 2016-09-19, PA22 detecting ADN8830 pin5(templock) if high. */
	ioport_set_pin_input_mode(PIO_PA22_IDX, GPIO_PUSH_BUTTON_1_FLAGS,
			GPIO_PUSH_BUTTON_1_SENSE);
	

#ifdef CONF_BOARD_UART_CONSOLE
	/* Configure UART pins */
	ioport_set_port_peripheral_mode(PINS_UART0_PORT, PINS_UART0,
			PINS_UART0_FLAGS);
#endif





#ifdef CONF_BOARD_SPI
	ioport_set_pin_peripheral_mode(SPI_MISO_GPIO, SPI_MISO_FLAGS);
	ioport_set_pin_peripheral_mode(SPI_MOSI_GPIO, SPI_MOSI_FLAGS);
	ioport_set_pin_peripheral_mode(SPI_SPCK_GPIO, SPI_SPCK_FLAGS);

#ifdef CONF_BOARD_SPI_NPCS0
	ioport_set_pin_peripheral_mode(SPI_NPCS0_GPIO, SPI_NPCS0_FLAGS);
#endif
#ifdef CONF_BOARD_SPI_NPCS1
ioport_set_pin_peripheral_mode(SPI_NPCS1_PA31_GPIO, SPI_NPCS1_PA31_FLAGS);
#endif
#ifdef CONF_BOARD_SPI_NPCS2
ioport_set_pin_peripheral_mode(SPI_NPCS2_PA30_GPIO, SPI_NPCS2_PA30_FLAGS);
#endif
#ifdef CONF_BOARD_SPI_NPCS3
	ioport_set_pin_peripheral_mode(SPI_NPCS3_PA3_GPIO, SPI_NPCS3_PA3_FLAGS);
#endif

OnEDFA_SPI_Initial();

#endif	//#ifdef CONF_BOARD_SPI



}


