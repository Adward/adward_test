/**
 * \file
 *
 * \brief User board definition template
 *
 */

 /* This file is intended to contain definitions and configuration details for
 * features and devices that are available on the board, e.g., frequency and
 * startup time for an external crystal, external memory devices, LED and USART
 * pins.
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef USER_BOARD_H
#define USER_BOARD_H

#include <conf_board.h>

#include "compiler.h"
#include "system_sam4e.h"
#include "exceptions.h"

/*----------------------------------------------------------------------------*/
/**
 *  \page sam4e_ek_opfreq "SAM4E-EK - Operating frequencies"
 *  This page lists several definition related to the board operating frequency
 *
 *  \section Definitions
 *  - \ref BOARD_FREQ_*
 *  - \ref BOARD_MCK
 */

/** Board oscillator settings */
#define BOARD_FREQ_SLCK_XTAL            (32768U)
#define BOARD_FREQ_SLCK_BYPASS          (32768U)
#define BOARD_FREQ_MAINCK_XTAL          (12000000U)
#define BOARD_FREQ_MAINCK_BYPASS        (12000000U)

/** Master clock frequency */
#define BOARD_MCK                       CHIP_FREQ_CPU_MAX

/** board main clock xtal startup time */
#define BOARD_OSC_STARTUP_US            15625

/** Enable Watch Dog function*/
//#define CONF_BOARD_KEEP_WATCHDOG_AT_INIT

/*----------------------------------------------------------------------------*/
/**
 * \page sam4e_ek_board_info "SAM4E-EK - Board informations"
 * This page lists several definition related to the board description.
 *
 * \section Definitions
 * - \ref BOARD_NAME
 */

/** Name of the board */
#define BOARD_NAME "USER_BOARD"
/** Board definition */
#define userboard
/** Family definition (already defined) */
#define sam4e
/** Core definition */
#define cortexm4

/*----------------------------------------------------------------------------*/

/** UART0 pins (UTXD0 and URXD0) definitions, PA10,9. */
#define PINS_UART0        (PIO_PA9A_URXD0 | PIO_PA10A_UTXD0)
#define PINS_UART0_FLAGS  (IOPORT_MODE_MUX_A)

#define PINS_UART0_PORT   IOPORT_PIOA
#define PINS_UART0_MASK   (PIO_PA9A_URXD0 | PIO_PA10A_UTXD0)
#define PINS_UART0_PIO    PIOA
#define PINS_UART0_ID     ID_PIOA
#define PINS_UART0_TYPE   PIO_PERIPH_A
#define PINS_UART0_ATTR   PIO_DEFAULT



/** LED #0 pin definition (Blue). */
#define LED_0_NAME      "Blue LED D2"
#define PIN_LED_0       {PIO_PA0, PIOA, ID_PIOA, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_LED_0_MASK  PIO_PA0
#define PIN_LED_0_PIO   PIOA
#define PIN_LED_0_ID    ID_PIOA
#define PIN_LED_0_TYPE  PIO_OUTPUT_1
#define PIN_LED_0_ATTR  PIO_DEFAULT

#define LED0_GPIO            (PIO_PA0_IDX)
#define LED0_FLAGS           (0)
#define LED0_ACTIVE_LEVEL    IOPORT_PIN_LEVEL_LOW
#define LED0_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/** LED #1 pin definition (Amber). */
#define LED_1_NAME      "Amber LED D3"
#define PIN_LED_1       {PIO_PD20, PIOD, ID_PIOD, PIO_OUTPUT_1, PIO_DEFAULT}
#define PIN_LED_1_MASK  PIO_PD20
#define PIN_LED_1_PIO   PIOD
#define PIN_LED_1_ID    ID_PIOD
#define PIN_LED_1_TYPE  PIO_OUTPUT_1
#define PIN_LED_1_ATTR  PIO_DEFAULT

//#define LED1_GPIO            (PIO_PD20_IDX)
#define LED1_FLAGS           (0)
#define LED1_ACTIVE_LEVEL    IOPORT_PIN_LEVEL_LOW
#define LED1_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/** LED #2 pin definition (Green). */
#define LED_2_NAME      "Green LED D4"
#define PIN_LED_2_MASK  PIO_PD21
#define PIN_LED_2_PIO   PIOD
#define PIN_LED_2_ID    ID_PIOD
#define PIN_LED_2_TYPE  PIO_OUTPUT_1
#define PIN_LED_2_ATTR  PIO_DEFAULT

#define LED2_GPIO            (PIO_PD21_IDX)
#define LED2_FLAGS           (0)
#define LED2_ACTIVE_LEVEL    IOPORT_PIN_LEVEL_LOW
#define LED2_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_HIGH

/** LED #3 pin definition (Red). */
#define LED_3_NAME      "Red LED D5"
#define PIN_LED_3_MASK  PIO_PD22
#define PIN_LED_3_PIO   PIOD
#define PIN_LED_3_ID    ID_PIOD
#define PIN_LED_3_TYPE  PIO_OUTPUT_0
#define PIN_LED_3_ATTR  PIO_DEFAULT

#define LED3_GPIO            (PIO_PD22_IDX)
#define LED3_FLAGS           (0)
#define LED3_ACTIVE_LEVEL    IOPORT_PIN_LEVEL_HIGH
#define LED3_INACTIVE_LEVEL  IOPORT_PIN_LEVEL_LOW


/**
 * Push button #0 definition. Attributes = pull-up + debounce + interrupt on
 * rising edge.
 */
#define PUSHBUTTON_1_NAME        "EDFA DISABLE"
//#define PUSHBUTTON_1_WKUP_LINE   (9)
//#define PUSHBUTTON_1_WKUP_FSTT   (PMC_FSMR_FSTT9)
#define GPIO_PUSH_BUTTON_1       (PIO_PD1_IDX)
#define GPIO_PUSH_BUTTON_1_FLAGS (IOPORT_MODE_PULLDOWN | IOPORT_MODE_DEBOUNCE)
#define GPIO_PUSH_BUTTON_1_SENSE (IOPORT_SENSE_RISING)

#define PIN_PUSHBUTTON_1       {PIO_PD1, PIOD, ID_PIOD, PIO_INPUT, \
		IOPORT_MODE_PULLDOWN | PIO_DEBOUNCE | PIO_IT_RISE_EDGE}
#define PIN_PUSHBUTTON_1_MASK  PIO_PD1
#define PIN_PUSHBUTTON_1_PIO   PIOD
#define PIN_PUSHBUTTON_1_ID    ID_PIOD
#define PIN_PUSHBUTTON_1_TYPE  PIO_INPUT
#define PIN_PUSHBUTTON_1_ATTR  (PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)
#define PIN_PUSHBUTTON_1_IRQn  PIOD_IRQn

/**
 * Push button #1 definition. Attributes = pull-up + debounce + interrupt on
 * falling edge.
 */
#define PUSHBUTTON_2_NAME        "EDFA MUTE"
//#define PUSHBUTTON_2_WKUP_LINE   (10)
//#define PUSHBUTTON_2_WKUP_FSTT   (PMC_FSMR_FSTT10)
#define GPIO_PUSH_BUTTON_2       (PIO_PD2_IDX)
#define GPIO_PUSH_BUTTON_2_FLAGS (IOPORT_MODE_PULLDOWN | IOPORT_MODE_DEBOUNCE)
#define GPIO_PUSH_BUTTON_2_SENSE (IOPORT_SENSE_RISING)

#define PIN_PUSHBUTTON_2       {PIO_PD2, PIOD, ID_PIOD, PIO_INPUT, \
		IOPORT_MODE_PULLDOWN | PIO_DEBOUNCE | PIO_IT_RISE_EDGE}
#define PIN_PUSHBUTTON_2_MASK  PIO_PD2
#define PIN_PUSHBUTTON_2_PIO   PIOD
#define PIN_PUSHBUTTON_2_ID    ID_PIOD
#define PIN_PUSHBUTTON_2_TYPE  PIO_INPUT
#define PIN_PUSHBUTTON_2_ATTR  (PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)
#define PIN_PUSHBUTTON_2_IRQn  PIOD_IRQn

/**
 * Push button #2 definition. Attributes = pull-up + debounce + interrupt on
 * both edges.
 */
#define PUSHBUTTON_3_NAME        "EDFA RESET"
//#define PUSHBUTTON_3_WKUP_LINE   (1)
//#define PUSHBUTTON_3_WKUP_FSTT   (PMC_FSMR_FSTT1)
#define GPIO_PUSH_BUTTON_3       (PIO_PD25_IDX)
#define GPIO_PUSH_BUTTON_3_FLAGS (IOPORT_MODE_PULLDOWN | IOPORT_MODE_DEBOUNCE)
#define GPIO_PUSH_BUTTON_3_SENSE (IOPORT_SENSE_RISING)

#define PIN_PUSHBUTTON_3       {PIO_PD25, PIOD, ID_PIOD, PIO_INPUT, \
		IOPORT_MODE_PULLDOWN | PIO_DEBOUNCE | PIO_IT_RISE_EDGE}
#define PIN_PUSHBUTTON_3_MASK  PIO_PD25
#define PIN_PUSHBUTTON_3_PIO   PIOD
#define PIN_PUSHBUTTON_3_ID    ID_PIOD
#define PIN_PUSHBUTTON_3_TYPE  PIO_INPUT
#define PIN_PUSHBUTTON_3_ATTR  (IOPORT_MODE_PULLDOWN | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)
#define PIN_PUSHBUTTON_3_IRQn  PIOD_IRQn




/************************************************************************/
/* 4 Output GPIO Pins for PID Closed Loop Control                       */
/************************************************************************/
#define SW_APC			(PIO_PA25_IDX)
#define SW_MODE_APC     IOPORT_PIN_LEVEL_LOW
#define SW_MODE_OAPC	IOPORT_PIN_LEVEL_HIGH

#define SW_FEEDBACK		(PIO_PA26_IDX)
#define SW_FB_C			IOPORT_PIN_LEVEL_LOW
#define SW_FB_P			IOPORT_PIN_LEVEL_HIGH

#define SW_PID			(PIO_PA27_IDX)
#define SW_Control_Set	IOPORT_PIN_LEVEL_LOW
#define SW_Control_Mute	IOPORT_PIN_LEVEL_HIGH

#define SW_Output		(PIO_PA28_IDX)
#define SW_O_EN			IOPORT_PIN_LEVEL_HIGH
#define SW_O_DIS		IOPORT_PIN_LEVEL_LOW

/************************************************************************/
/* 2 Output GPIO Pins as Latch Control for all MCU Output Pins          */
/************************************************************************/
#define SW_Latch_Din	(PIO_PA29_IDX)
#define SW_Latch_Qout	(PIO_PD9_IDX)
#define SW_Latch_Active	IOPORT_PIN_LEVEL_HIGH
#define SW_Latch_DeAct	IOPORT_PIN_LEVEL_LOW

/************************************************************************/
/* 1 Output GPIO Pin as EEPROM Read/Write Control Pin                   */
/************************************************************************/
#define SW_EEPROM		(PIO_PD20_IDX)
#define SW_Read			IOPORT_PIN_LEVEL_HIGH
#define SW_Write_Prot	IOPORT_PIN_LEVEL_LOW

/************************************************************************/
/* 6 Output GPIO Pins as ALARM Output Pins                                */
/************************************************************************/
#define SW_CaseTemp		(PIO_PD3_IDX)
#define SW_CommonALRM	(PIO_PD4_IDX)
#define SW_PumpTemp		(PIO_PD5_IDX)
#define SW_PumpBias		(PIO_PD6_IDX)
#define SW_LOI			(PIO_PD7_IDX)
#define SW_LOP			(PIO_PD8_IDX)
#define SW_ALARM_Act	IOPORT_PIN_LEVEL_HIGH
#define SW_ALARM_DeAct	IOPORT_PIN_LEVEL_LOW
#define SW_ALARM_ActN	IOPORT_PIN_LEVEL_LOW
#define SW_ALARM_DeActN	IOPORT_PIN_LEVEL_HIGH

/************************************************************************/
/* 2 Output GPIO Pins for High Power LASER                              */
/************************************************************************/
#define SW_Seed			(PIO_PD10_IDX)
#define SW_3rd_PUMP		(PIO_PD0_IDX)



/** 
  * SPI MISO pin definition. 
  */
#define SPI_MISO_GPIO         (PIO_PA12_IDX)
#define SPI_MISO_FLAGS        (IOPORT_MODE_MUX_A)
/** SPI MOSI pin definition. */
#define SPI_MOSI_GPIO         (PIO_PA13_IDX)
#define SPI_MOSI_FLAGS        (IOPORT_MODE_MUX_A)
/** SPI SPCK pin definition. */
#define SPI_SPCK_GPIO         (PIO_PA14_IDX)
#define SPI_SPCK_FLAGS        (IOPORT_MODE_MUX_A)

/** SPI chip select 0 pin definition. (Only one configuration is possible) */
#define SPI_NPCS0_GPIO        (PIO_PA11_IDX)
#define SPI_NPCS0_FLAGS       (IOPORT_MODE_MUX_A)

/** SPI chip select 1 pin definition. (multiple configurations are possible) */
//#define SPI_NPCS1_PA9_GPIO    (PIO_PA9_IDX)
//#define SPI_NPCS1_PA9_FLAGS   (IOPORT_MODE_MUX_B)
#define SPI_NPCS1_PA31_GPIO   (PIO_PA31_IDX)
#define SPI_NPCS1_PA31_FLAGS  (IOPORT_MODE_MUX_A)
//#define SPI_NPCS1_PB14_GPIO   (PIO_PB14_IDX)
//#define SPI_NPCS1_PB14_FLAGS  (IOPORT_MODE_MUX_A)
//#define SPI_NPCS1_PC4_GPIO    (PIO_PC4_IDX)
//#define SPI_NPCS1_PC4_FLAGS   (IOPORT_MODE_MUX_B)

/** SPI chip select 2 pin definition. (multiple configurations are possible) */
//#define SPI_NPCS2_PA10_GPIO   (PIO_PA10_IDX)
//#define SPI_NPCS2_PA10_FLAGS  (IOPORT_MODE_MUX_B)
#define SPI_NPCS2_PA30_GPIO   (PIO_PA30_IDX)
#define SPI_NPCS2_PA30_FLAGS  (IOPORT_MODE_MUX_B)
//#define SPI_NPCS2_PB2_GPIO    (PIO_PB2_IDX)
//#define SPI_NPCS2_PB2_FLAGS   (IOPORT_MODE_MUX_B)

/** SPI chip select 3 pin definition. (multiple configurations are possible) */
#define SPI_NPCS3_PA3_GPIO    (PIO_PA3_IDX)
#define SPI_NPCS3_PA3_FLAGS   (IOPORT_MODE_MUX_B)
//#define SPI_NPCS3_PA5_GPIO    (PIO_PA5_IDX)
//#define SPI_NPCS3_PA5_FLAGS   (IOPORT_MODE_MUX_B)
//#define SPI_NPCS3_PA22_GPIO   (PIO_PA22_IDX)
//#define SPI_NPCS3_PA22_FLAGS  (IOPORT_MODE_MUX_B)

/* Select the SPI module */
//#define EDFA_SPI			SPI

/* Chip select used by components on the SPI bus */
//#define SPI_EEPROM			0x00
//#define SPI_DAC1			0x00
//#define SPI_DAC2			0x02
//#define SPI_DAC3			0x03




/*----------------------------------------------------------------------------*/

#define CONSOLE_UART               UART0
#define CONSOLE_UART_ID            ID_UART0



#endif // USER_BOARD_H
