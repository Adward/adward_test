/*
 * SerialMemory.c
 *
 * Created: 2015/12/8 09:32:11
 *  Author: Administrator
 */ 
#include <asf.h>
#include "conf_example.h"

#define  EEPROM_CMD_WRSR   0x01        // Write Status Register Command WRSR Write Status Register  
#define  EEPROM_CMD_WRITE  0x02        // Write Command						WRITE Write to Memory Array 
#define  EEPROM_CMD_READ   0x03        // Read Command						READ Read from Memory Array 
#define  EEPROM_CMD_WRDI   0x04        // Reset Write Enable Latch			Command WRDI Write Disable 
#define  EEPROM_CMD_RDSR   0x05        // Read Status Register Command		RDSR Read Status Register 
#define  EEPROM_CMD_WREN   0x06        // Set Write Enable Latch Command	WREN Write Enable 

struct spi_device SPI_DEVICE0 = {
	.id = SPI_CH_EEPROM
};

uint8_t OnEEPROM_Read_Status(void);


/**
 * \brief Read the Status Register of the EEROM.  For continuous polling status,
 *        the caller needs to select and deselect the device. 
 *
 * \return Content of the Status Register.
 */
uint8_t OnEEPROM_Read_Status(void)
{
	uint8_t Cmd[1];
	char tmpResult;
	
	Cmd[0] = EEPROM_CMD_RDSR;
	
	tmpResult=OnSPI_Write(EDFA_SPI, Cmd, 1);
	if (tmpResult!=0) printf("EEPROM Read Status W ERR:%d\n\r",tmpResult);
	tmpResult=OnSPI_Read(EDFA_SPI, Cmd, 1);
	if (tmpResult!=0) printf("EEPROM Read Status R ERR:%d\n\r",tmpResult);
	
	return Cmd[0];
}

/**
 * \brief Read data from the specified address on the EEROM.
 *
 * \param value  Data buffer.
 * \param size  Number of bytes to read.
 * \param address  Read address.
 *
 * \return EEPROM_SUCCESS if successful; otherwise, failed.
 */
eeprom_status_t OnEEPROM_Read_Page (uint8_t *value, uint16_t size, uint16_t address) {
	uint8_t EEPROM_data[3], EEPROM_Value_H, EEPROM_Value_L;
	eeprom_status_t result = EEPROM_SUCCESS; 
	char tmpResult;
	
	EEPROM_Value_H = (uint8_t)((address & 0xFF00) >> 8);
	EEPROM_Value_L = (uint8_t)(address & 0x00FF);
#ifdef DG_EEPROM
	printf("EEPROM_Value_H: %x\n\r",EEPROM_Value_H);
	printf("EEPROM_Value_L: %x\n\r",EEPROM_Value_L);
#endif
	
	// Step1: Send the READ command and 16-bit source address (MSB first)
	EEPROM_data[0] = EEPROM_CMD_READ;
	EEPROM_data[1] = EEPROM_Value_H;
	EEPROM_data[2] = EEPROM_Value_L;
	spi_select_device(EDFA_SPI, &SPI_DEVICE0);
	tmpResult = OnSPI_Write(EDFA_SPI, EEPROM_data, 3);
	if (tmpResult!=0) printf("EEPROM Page Read W ERR:%d\n\r",tmpResult);
	
	// Step2: Read the value returned
	tmpResult = OnSPI_Read(EDFA_SPI, value, size);
	spi_deselect_device(EDFA_SPI, &SPI_DEVICE0);
	if (tmpResult!=0) printf("EEPROM Page Read R ERR:%d\n\r",tmpResult);
	
	
	result = (tmpResult==0) ? EEPROM_SUCCESS:EEPROM_ERROR;
	return result;
}

/**
 * \brief Write data from the specified address on the EEROM.
 *
 * \param value  Data buffer.
 * \param size  Number of bytes to write.
 * \param address  Write address.
 *
 * \return EEPROM_SUCCESS if successful; otherwise, failed.
 */
eeprom_status_t OnEEPROM_Write_Page (uint8_t *value , uint16_t size, uint16_t address) {
	
	uint32_t SPI_read_Timeout = SPI_TIMEOUT;
	uint8_t EEPROM_data[3], EEPROM_Value_H, EEPROM_Value_L, status_RDSR;
	eeprom_status_t result = EEPROM_SUCCESS;
	
	uint16_t pages,BytesToWrite;
	uint16_t i,res;
	
	char tmpResult=0;
	
	pages=(uint16_t)(size>>6);	// 1 page= 64 bytes
	res=size-(pages<<6);			//res= total bytes - 64bytes*pages
	
	pages+=1;
	for(i=0;i<pages;i++) {
		EEPROM_Value_H = (uint8_t)((address & 0xFF00) >> 8);
		EEPROM_Value_L = (uint8_t)(address & 0x00FF);
#ifdef DG_EEPROM
		printf("EEPROM_Value_H: %x\n\r",EEPROM_Value_H);
		printf("EEPROM_Value_L: %x\n\r",EEPROM_Value_L);
#endif
		
		// Step1: Set the Write Enable Latch to 1
		EEPROM_data[0] = EEPROM_CMD_WREN;
		spi_select_device(EDFA_SPI, &SPI_DEVICE0);
		tmpResult = OnSPI_Write(EDFA_SPI, EEPROM_data, 1);
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE0);
		if (tmpResult!=0) printf("EEPROM Page Write cmd_WREN ERR:%d\n\r",tmpResult);
		
		
		
		// Step2: Send the WRITE command and destination address (MSB first)
		EEPROM_data[0] = EEPROM_CMD_WRITE;
		EEPROM_data[1] = EEPROM_Value_H;
		EEPROM_data[2] = EEPROM_Value_L;		
		spi_select_device(EDFA_SPI, &SPI_DEVICE0);
		tmpResult = OnSPI_Write(EDFA_SPI, EEPROM_data, 3);
		if (tmpResult!=0) printf("EEPROM Page Write cmd_WRITE ERR:%d\n\r",tmpResult);
		

		// Step3: Send the value to write
		BytesToWrite = (i==(pages-1)) ? res : 64;
		tmpResult = OnSPI_Write(EDFA_SPI, (value+64*i), BytesToWrite);
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE0);
		if (tmpResult!=0) printf("EEPROM Page Write W ERR:%d\n\r",tmpResult);
		
		
		
		// Step4: Poll on the Write In Progress (WIP) bit in Read Status Register
		spi_select_device(EDFA_SPI, &SPI_DEVICE0);
		do{
			status_RDSR = OnEEPROM_Read_Status();
			if (!SPI_read_Timeout--) return ERR_TIMEOUT;
#ifdef DG_EEPROM
			printf("STA=%d, Timeout=%d\n\r",status_RDSR, (int)SPI_read_Timeout);
#endif
		} while( (status_RDSR & 0x01) == 0x01 );
		spi_deselect_device(EDFA_SPI, &SPI_DEVICE0);
		address+=64;
	}
	result = (tmpResult==0) ? EEPROM_SUCCESS:EEPROM_ERROR;
	return result;
}

