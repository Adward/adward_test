///////////////////////////////////////////////////////////////////////
// Dac_Proc.h
//
///////////////////////////////////////////////////////////////////////
#ifndef __DAC_H__
#define __DAC_H__

/////////////////////////////////////////////////////////////////////////
// DAC control constant
#define DAC_WRITE_REGISTER_N				0x00
#define DAC_UPDATE_REGISTER_N				0x10
#define DAC_WRITE_REGISTER_N_UPDATE_ALL		0x20
#define DAC_WRITE_UPDATE_REGISTER_N			0x30

#define ADDRESS_DAC_A						0x00
#define ADDRESS_DAC_B						0x01
#define ADDRESS_DAC_C						0x02
#define ADDRESS_DAC_D						0x03
#define ADDRESS_DAC_ALL						0x0F

#define DAC_WRITE_REGISTER_A				0x00
#define DAC_WRITE_REGISTER_B				0x01
#define DAC_WRITE_REGISTER_C				0x02
#define DAC_WRITE_REGISTER_D				0x03

#define DAC_WRITE_A_UPDATE_ALL				0x20
#define DAC_WRITE_B_UPDATE_ALL				0x21
#define DAC_WRITE_C_UPDATE_ALL				0x22
#define DAC_WRITE_D_UPDATE_ALL				0x23




#define DAC_OFFSET				0				// DAC offset = 0V for PCBA Rev 2.0
#define MAX_SET_VOLTAGE			75000UL			// MAX V = 75000mv
#define DAC_DATA_SCALE			(65536.f/MAX_SET_VOLTAGE)	// DAC scale factor
#define V_STEP0					(100 * DAC_DATA_SCALE)		// V-STEP = 0.1v
//////////////////////////////////////////////////////////////
// define Thermistor coefficient
#define	THE_A			(2.695f/1000)
#define THE_B			(2.92155f/10000)
#define THE_C			(5.10885f/10000000)
#define THE_R			(0.5f)
#define THE_ADJ			(2711.5f)
//////////////////////////////////////////////////////////////
// DAC control and read temperature functions
void OnRead_Tmp125(void);
void OnRead_TmpR(void);
void OnRead_Temp(void);
void DAC_Output(uint16_t DAC_Value, uint8_t Dir);
void DAC1_Output(uint16_t DAC_Value, uint8_t MemsCH);
void DAC2_Output(uint16_t DAC_Value, uint8_t Dir);
void OnSet_Voltage(int32_t DAC_Value, uint8_t MemsCH);


#endif // __Dac_H__