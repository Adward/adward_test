/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */

#include <asf.h>
#include "stdio_serial.h"
#include "conf_example.h"

volatile static uint8_t do_cmd_decode = 0;	//console decode console
volatile uint8_t	tmpECHO;
volatile uint8_t	tmpSTORE=0;

/* 2016-07-12 */
extern volatile unsigned int queue_head, queue_tail, queue_head_next;
extern char rx_queue[QUEUE_SIZE][BUFFER_SIZE];
extern int cmd_length_queue[QUEUE_SIZE];
extern int cmd_got_enterkey[QUEUE_SIZE];
static char cmd_string[BUFFER_SIZE];

/** Test DAC Variable. */
volatile uint32_t dac_val=DACC_MAX_DATA;



/** The maximal sine wave sample data (no sign) */
#define MAX_DIGITAL   (0x7fff)
/** The maximal (peak-peak) amplitude value */
#define MAX_AMPLITUDE ((1 << 16)-1)	//(DACC_MAX_DATA)
/** The minimal (peak-peak) amplitude value */
#define MIN_AMPLITUDE (100)
/** SAMPLES per cycle */
#define SAMPLES (100)
const int16_t gc_us_triangle_data[SAMPLES] = {
	0x0,	0x51F,	0xA3D,	0xF5C,	0x147B,	0x1999,	0x1EB8,	0x23D7,	0x28F5,	0x2E14,
	0x3333,	0x3851,	0x3D70,	0x428F,	0x47AE,	0x4CCC,	0x51EB,	0x570A,	0x5C28,	0x6147,
	0x6666,	0x6B84,	0x70A3,	0x75C2,	0x7AE0,	0x7FFF,	0x7AE0,	0x75C2,	0x70A3,	0x6B84,
	0x6666,	0x6147,	0x5C28,	0x570A,	0x51EB,	0x4CCC,	0x47AE,	0x428F,	0x3D70,	0x3851,
	0x3333,	0x2E14,	0x28F5,	0x23D7,	0x1EB8,	0x1999,	0x147B,	0xF5C,	0xA3D,	0x51F,

	-0x0,   -0x51F,	-0xA3D,	-0xF5C,	-0x147B,	-0x1999,	-0x1EB8,	-0x23D7,	-0x28F5,	-0x2E14,
	-0x3333,-0x3851,	-0x3D70,	-0x428F,	-0x47AE,	-0x4CCC,	-0x51EB,	-0x570A,	-0x5C28,	-0x6147,
	-0x6666,-0x6B84,	-0x70A3,	-0x75C2,	-0x7AE0,	-0x7FFF,	-0x7AE0,	-0x75C2,	-0x70A3,	-0x6B84,
	-0x6666,-0x6147,	-0x5C28,	-0x570A,	-0x51EB,	-0x4CCC,	-0x47AE,	-0x428F,	-0x3D70,	-0x3851,
	-0x3333,-0x2E14,	-0x28F5,	-0x23D7,	-0x1EB8,	-0x1999,	-0x147B,	-0xF5C,	-0xA3D,	-0x51F
};
#define wave_to_dacc(wave, amplitude, max_digital, max_amplitude) \
(((int)(wave) * (amplitude) / (max_digital)) + (amplitude / 2))


volatile uint32_t g_ul_ms_ticks = 0;
volatile uint32_t g_ul_index_sample = 0;


void testDAC3(void) {
	uint32_t dac_val;
	static uint32_t old_value;
	
	if (old_value!= g_ul_index_sample) {
		if (g_ul_index_sample >= SAMPLES) {
			g_ul_index_sample = 0;
		}
		dac_val =
#ifdef testDAC_3_constant
		wave_to_dacc(0x7FFF,
#else
		wave_to_dacc(gc_us_triangle_data[g_ul_index_sample],
#endif
		24530,
		MAX_DIGITAL*2, MAX_AMPLITUDE);
		
//#define driveB		
		#ifdef driveB
		DAC3_Output(dac_val, CH_B);
		#elif  driveA
		DAC3_Output(dac_val, CH_A);
		# else
		DAC3_Output(dac_val, CH_ALL);
		#endif
		
		old_value= g_ul_index_sample;
	}
}





void SysTick_Handler(void) {
	g_ul_ms_ticks++;
	g_ul_index_sample++;
}


/* 2016-05-31 */
static volatile int flag_disable_edfa = 0;
static volatile int flag_mute_edfa = 0;
static void disable_handler(uint32_t id, uint32_t mask)
{
	/* Set button event flag (g_b_button_event). */
	// if ((PIOD == id) && (PIO_PD1 == mask)) {
	if ( PIO_PD1 == mask ) {
		flag_disable_edfa = 1;
	}
}


static void disable_edfa_interrupt(void)
{
	// Enable the peripheral clock for the push button on board.
	pmc_enable_periph_clk(ID_PIOD);

	// Configure PIOs as input pins.
	pio_configure(PIOD, PIO_INPUT, PIO_PD1, (PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE));
	// pio_configure(PIOD, PIO_INPUT, PIO_PD1, PIO_IT_RISE_EDGE);

	// Adjust PIO debounce filter parameters.
	pio_set_debounce_filter(PIOD, PIO_PD1, 10);

	// Initialize PIO interrupt handler, interrupt on rising edge.
	pio_handler_set(PIOD, ID_PIOD, PIO_PD1, (PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE), disable_handler);

}


/* 2016-06-01 */
void WDT_Handler(void)
{
	/* 2016-07-05, enter WDT_Handler(), the system may be halt, reboot */
	/*
	// Clear status bit to acknowledge interrupt by dummy read.
	wdt_get_status(WDT);
	
	// Restart the WDT counter.
	wdt_restart(WDT);
	*/
	NVIC_SystemReset();
}


/* 2016-08-02 */
static void configure_rtt(void)
{
	/* Configure RTT for a 1 second tick interrupt */
	#if SAM4N || SAM4S || SAM4E || SAM4C || SAM4CP || SAM4CM || SAMV71 || SAMV70 || SAME70 || SAMS70
	rtt_sel_source(RTT, false);
	#endif
	
	/* counter adds 1024 per second. */
	// rtt_init(RTT, 32768);
	rtt_init(RTT, 32);

	/*
	uint32_t ul_previous_time;
	ul_previous_time = rtt_read_timer_value(RTT);
	while (ul_previous_time == rtt_read_timer_value(RTT));
	*/

	/*
	while (1) {
		ul_previous_time = rtt_read_timer_value(RTT);
		printf("Timer Counter is %d\n\r", ul_previous_time);
	}
	*/
	
}

static volatile unsigned int counter_now = 0, counter_prev = 0;
static volatile unsigned int counter_edfa_disabled = 0, counter_now2 = 0;
static unsigned int detected_disable = 0, check_back_to_normal = 0;

void AGC_AOGC_OutputPower(void);

void AGC_AOGC_OutputPower(void)
{
	char ModeP[] = "MODE PNS";
	
	/* 2016-08-09, add AGC mode. */
	float SetPower_AOPC_AGC, vInputOptdBm_tmp;
					
	vInputOptdBm_tmp = AppRtData.vInputOptdBm;
						
	/* 2016-08-08, AGC mode, 23dBm Laser Pump. */
	SetPower_AOPC_AGC = vInputOptdBm_tmp + NP_rtMODE.SetPower_AGC;
	if ( SetPower_AOPC_AGC > 20.0 ) {
		SetPower_AOPC_AGC = 20.0;
	}
	else if ( SetPower_AOPC_AGC < 15.0 ) {
		SetPower_AOPC_AGC = 15.0;
	}

	/* 2016-08-05, 2016-07-20, 1A Laser Pump, offset add 2dB for Fremont test environment. */
	/* 2016-08-22 */
	// NP_rtMODE.powerAD_AOPC = (-1) * SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
	NP_rtMODE.powerAD_AOPC = SetPower_AOPC_AGC * AppCfgMap.v.AOPC_b_dac  + AppCfgMap.v.AOPC_c_dac;
						
	AppCfgMap.v.PowerAD = NP_rtMODE.powerAD_AOPC;

	AppCfgMap.v.Private = 1;
						
	/* 2016-08-09, �Ȯɤ��[�J */
	/* 2016-07-21, may a bug. */
	// activeAOPC
	// swAOPC

	Console_ParseCmd(ModeP);
						
	AppCfgMap.v.Private = 0;

}


static void edfa_disable(void)
{
	/* 2016-05-31, when disable EDFA interrupt, execute private command. */
	char ModeD[] = "MODE DNS\0";
		
	NP_rtMODE.statePump = OFF;
	AppCfgMap.v.Private = 1;
	Console_ParseCmd(ModeD);
	AppCfgMap.v.Private = 0;
}


void watchdog_disable(void)
{
	uint32_t wdt_mode;
	
	/* Configure WDT to trigger an interrupt (or reset). */
	wdt_mode =
		WDT_MR_WDFIEN |		/* Enable WDT fault interrupt. */
		WDT_MR_WDRPROC   |	/* WDT fault resets processor only. */
		WDT_MR_WDDBGHLT  |	/* WDT stops in debug state. */
		WDT_MR_WDIDLEHLT;	/* WDT stops in idle state. */
		
	wdt_init(WDT, wdt_mode, 2048, 2048);
		
	/* 2016-06-01, Configure and enable WDT interrupt. */
	NVIC_DisableIRQ(WDT_IRQn);
	NVIC_ClearPendingIRQ(WDT_IRQn);
}


void watchdog_enable(void)
{
	/* 2016-06-01 */
	uint32_t wdt_mode, timeout_value;
	
	/* Get timeout value. */
	timeout_value = wdt_get_timeout_value(3000 * 1000, BOARD_FREQ_SLCK_XTAL);
	if (timeout_value == WDT_INVALID_ARGUMENT) {
		while (1) {
			/* Invalid timeout value, error. */
		}
	}
	
	/* Configure WDT to trigger an interrupt (or reset). */
	wdt_mode =
	WDT_MR_WDFIEN |		/* Enable WDT fault interrupt. */
	WDT_MR_WDRPROC   |		/* WDT fault resets processor only. */
	WDT_MR_WDDBGHLT  |	/* WDT stops in debug state. */
	WDT_MR_WDIDLEHLT	|	/* WDT stops in idle state. */
	WDT_MR_WDRSTEN;
	
	/* Initialize WDT with the given parameters. */
	/* 2016-06-02 */
	wdt_init(WDT, wdt_mode, 2048, 2048);
	/* 2016-06-01, Configure and enable WDT interrupt. */
	NVIC_DisableIRQ(WDT_IRQn);
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_SetPriority(WDT_IRQn, 0);
	NVIC_EnableIRQ(WDT_IRQn);
}


/* 2016-09-19, solve inrush-current, "EDFA ON" after TEC Control IC ADN8830 pin5 TEMPLOCK high,
even "PWRUP ON".
*/
int tec_lock = FALSE;

/* 2016-09-19, execute "EDFA ON" when "PWRUP ON". */
int edfa_init_edfa_on = FALSE;

int main (void)
{
	/* 2016-07-14 */
	int i, j;
	volatile unsigned int AGC_counter_now = 0, AGC_counter_prev = 0;
	int do_AGC_once = 0, AGC_turn_on_edfa = 1;
	
	/* 2016-08-09, delete later. */
	int debug_toogle = 0;
	
	
	do_cmd_decode = 0;
	
	
	/* 2016-09-08 */
	watchdog_disable();

	
	SysTick_Config(sysclk_get_cpu_hz() / 1000);
	
	/* Insert system clock initialization code here (sysclk_init()). */
	sysclk_init();
	board_init();
	

	// wdt_restart(WDT);

	
	configure_rtt();


	/************************************************************/
	/* 2016-08-09, add some delay to avoid edfa turn-on noise. */
	/************************************************************/
	
	
	/* Configure an IO for external interrupt, RESET, DISABLE and MUTE . */
	/* 2015-05-31, PD1, Ext_PIN14_Output Disable Input. */
	disable_edfa_interrupt();
	
	/* Initialize and enable push button (PIO) interrupt. */
	pio_handler_set_priority(PIOD, PIOD_IRQn, 0);
	/* 2016-05-31, or Disable(PD1, external IRQ not enable). */
	pio_enable_interrupt(PIOD, PIO_PD1);
	
	
	/* 2016-07-14 */
	for (i=0; i<QUEUE_SIZE; i++) {
		cmd_length_queue[i] = 0;
		cmd_got_enterkey[i] = 0;
	}
		
	/* 2016-07-19 */
	for (j=0; j<QUEUE_SIZE; j++) {
		for (i=0; i<BUFFER_SIZE; i++) {
			rx_queue[j][i] = 0;
		}
	}

	/* Internal DAC initialization*/
	DAC_Init();
	
	/* External DAC3 initialization */
	DAC3_Output(0, Off_ALL);
	
	/* Internal ADC initialization */
	AFEC_Init();
	
	/* 2016-09-22, move here, before EDFA_Init(), when "PWRUP ON", execute AGC. */
	/* 2016-08-09, set AppRtData.vInputOptdBm = 0dBm before complete a ADC conversion. */
	AppRtData.vInputOptdBm = 0;
	
	/* EDFA System Initialization */
	/* 2016-08-22, EDFA_Init() implemnts "PWRUP ON" command, DAC_Init() should be first executed. */
	EDFA_Init();
	
	
	/* 2016-09-19, execute configure_console() here, after EDFA_Init(),
	and now ADN8830 templock is high.(temperature lock)
	*/
	/* Configure UART for system message output. */
	configure_console();
	
	
	
	
	/* Initial RTC */
	//RTC_Init();
	
	
	printf("\n\rSystem Ready .....\n\r");

	if (NP_iniMODE.headMsg == ON) {
		Console_Prompt_Private();
		ShowApplicationLogo();
	}

	/* 2016-08-03 */
	if ( ( AppCfgMap.v.Private != 1 ) && ( NP_rtMODE.statePrompt == ON ) ) {
		/* 2016-07-19 */
		// printf("NP2000:>");
		printf("\n\r");
		printf(PROMPT);
	}
	
	
	/* 2016-09-08 */
	// watchdog_enable();
		

	while (true) {
		/* 2016-06-02 */
		/* Restart the WDT counter. */
		wdt_restart(WDT);
		
		
		/* 2016-08-02, test, delete later. */
		/*
		uart_disable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
		if ( queue_tail != queue_head ) {
			ioport_set_pin_level(SW_CommonALRM, SW_ALARM_Act);
		}
		else {
			ioport_set_pin_level(SW_CommonALRM, SW_ALARM_DeAct);
		}
		uart_enable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
		*/
		
		
		
		/* 2016-05-31 */
		if ( 1 == flag_disable_edfa ) {
			edfa_disable();
					
			/* 2016-08-02 */
			if ( 0 == detected_disable ) {
				counter_edfa_disabled = rtt_read_timer_value(RTT);
				detected_disable = 1;
				check_back_to_normal = 0;
			}

			/* 2016-08-02 */
			counter_now2 = rtt_read_timer_value(RTT);
			if ( 0 == check_back_to_normal ) {
				/* 10 second later. */
				if ( counter_now2 > counter_edfa_disabled + 1024*10 ) {
					check_back_to_normal = 1;
				}
				else if ( counter_now2 < counter_edfa_disabled ) {
					if ( 0xFFFFFFFF - counter_edfa_disabled + counter_now2 + 1 > 1024*10 ) {
						check_back_to_normal = 1;
					}
				}
			}

			/* 2016-05-31, should have de-bounce, and avoid switch Disable / Enable Laser Pump so rapidly. */
			/* recovery to normal state, 10 seconds later when disable. */
			if ( 1 == check_back_to_normal ) {
				if ( 0 == pio_get(PIN_PUSHBUTTON_1_PIO, PIO_TYPE_PIO_INPUT, PIN_PUSHBUTTON_1_MASK) ) {
					detected_disable = 0;
					check_back_to_normal = 0;
					flag_disable_edfa = 0;
				}
			}
			
		}
		else if ( 1 == flag_mute_edfa ) {
		}
		
		
		/* 2016-08-09 */
		wdt_restart(WDT);
		
		
		if ( adc0_conversion_done && adc1_conversion_done ) {
			counter_now = rtt_read_timer_value(RTT);
			// printf("Timer Counter is %d\n\r", counter_now);
						
			/* 2016-08-02, about 100ms execute once ADC0, ADC1 Conversion. */
			if ( counter_now > counter_prev + 100 ) {
				counter_prev = counter_now;
				
				adc0_conversion_done = FALSE;
				adc1_conversion_done = FALSE;
							
				AFEC0->AFEC_CR = AFEC_CR_START;
				AFEC1->AFEC_CR = AFEC_CR_START;	
			}
			else if ( counter_now < counter_prev) {
				if ( 0xFFFFFFFF - counter_prev + counter_now + 1 > 100 ) {
					counter_prev = counter_now;
					
					adc0_conversion_done = FALSE;
					adc1_conversion_done = FALSE;
									
					AFEC0->AFEC_CR = AFEC_CR_START;
					AFEC1->AFEC_CR = AFEC_CR_START;
				}
			}
			
		}

				

		if (tmpSTORE) {
			save(rec_ALL);	//For command ECHO and consistency, do save in main() loop.
			tmpSTORE = 0;
		}
				
		uart_disable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
		if ( queue_tail != queue_head && cmd_got_enterkey[queue_tail] == 1 ) {
			do_cmd_decode = 1;
		}
		else {
			uart_enable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
			do_cmd_decode = 0;
		}
		
		if ( do_cmd_decode ) {
			tmpECHO = AppCfgMap.v.ECHO;		// Activate console during processing command
			
			/* 2016-05-24, note: there is no "ECHO OFF" in private commands. */
			AppCfgMap.v.ECHO = 1;
			
			/* 2016-07-14 */
			for (i=0; i<cmd_length_queue[queue_tail]; i++) {
				cmd_string[i] = rx_queue[queue_tail][i];
			}
			
			/* 2016-07-19 */
			for (i=1; i<BUFFER_SIZE; i++) {
				rx_queue[queue_tail][i] = 0;
			}
			rx_queue[queue_tail][0] = '#';
						
			cmd_length_queue[queue_tail] = 0;
			cmd_got_enterkey[queue_tail] = 0;
			queue_tail = (queue_tail + 1) % QUEUE_SIZE;
					
			/* 2016-07-14 */
			uart_enable_interrupt(CONSOLE_UART, UART_IER_RXRDY);
									
			if ( Console_ParseCmd(&cmd_string[0]) < 0 )
				printf("Command Error!\n\r");
				
			for (i=0; i<BUFFER_SIZE; i++) {
				cmd_string[BUFFER_SIZE] = 0;
			}
			
			AppCfgMap.v.ECHO=tmpECHO;	// Restore console state
			
			if ( AppCfgMap.v.ECHO == 1 )
				Console_Prompt_Private();
				
			if ( ( AppCfgMap.v.Private != 1 ) && ( NP_rtMODE.statePrompt == ON ) ) {
				/* 2016-07-19 */
				// printf("NP2000:>");
				printf(PROMPT);
			}

			do_cmd_decode = 0;
		}
		
		
		
		/* 2016-08-09 */
		wdt_restart(WDT);
		
		
		/* 2016-08-09, implement AGC here, inside Main-Loop. */
		if ( ON == NP_rtMODE.statePump ) {
			if ( MAGC == NP_rtMODE.MODE ) {
				AGC_counter_now = rtt_read_timer_value(RTT);

				/* 2016-08-09, every 30 second, do AGC once. */
				if ( AGC_counter_now > AGC_counter_prev + 1024*30 ) {
					AGC_counter_prev = AGC_counter_now;
					
					do_AGC_once = 1;
					
					if ( 0 == debug_toogle ) {
						/* 2016-09-22, no debug led to customer. */
						// ioport_set_pin_level(SW_CommonALRM, SW_ALARM_Act);
						
						debug_toogle = 1;
					}
					else if ( 1 == debug_toogle ) {
						/* 2016-09-22, no debug led to customer. */
						// ioport_set_pin_level(SW_CommonALRM, SW_ALARM_DeAct);
						
						debug_toogle = 0;
					}
					
				}
				else if ( AGC_counter_now < AGC_counter_prev) {
					if ( 0xFFFFFFFF - AGC_counter_prev + AGC_counter_now + 1 > 1024*30 ) {
						AGC_counter_prev = AGC_counter_now;
						
						do_AGC_once = 1;
												
						if ( 0 == debug_toogle ) {
							/* 2016-09-22, no debug led to customer. */
							// ioport_set_pin_level(SW_CommonALRM, SW_ALARM_Act);
							
							debug_toogle = 1;
						}
						else if ( 1 == debug_toogle ) {
							/* 2016-09-22, no debug led to customer. */
							// ioport_set_pin_level(SW_CommonALRM, SW_ALARM_DeAct);
							
							debug_toogle = 0;
						}
						
					}
					
				}
				
				/* 2016-08-09, solve when turn-on inside 30 second, AGC not function. */
				if ( 1 == AGC_turn_on_edfa ) {
					AGC_turn_on_edfa = 0;
					do_AGC_once = 1;
				}
				
				
				if ( 1 == do_AGC_once) {
					do_AGC_once = 0;
					AGC_AOGC_OutputPower();
				}
			}
		}		
		
		
		/* 2016-08-09 */
		wdt_restart(WDT);
		
		Check_Alarm_Status();

	}
	
}


//=================================================================================
/*



static void mdelay(uint32_t ul_dly_ticks)
{
	uint32_t ul_cur_ticks;
	ul_cur_ticks = g_ul_ms_ticks;
	while ((g_ul_ms_ticks - ul_cur_ticks) < ul_dly_ticks);
}


//mdelay(3000);
		
//#define testADC
#ifdef testADC
afec_start_software_conversion(AFEC0);
delay_ms(10000);
afec_start_software_conversion(AFEC1);
delay_ms(10000);
#endif


//#define testDAC_3_constant
#ifdef testDAC_3_constant		
  testDAC3();
#endif


//#define testDAC_3
#ifdef testDAC_3
testDAC3();
#endif


		//if (AppRtData.RtFlag.Bits.OperationMode==MALC) Glimcontrol();	//P Mode


*/

//=================================================================================