///////////////////////////////////////////////////////////////////////////////
//
//	Dpram_Proc.c: Interface for DPRAM inside of FPGA
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
///////////////////////////////////////////////////////////////////////////////
void OnDpram_Write_data(void)
{
	uint8_t Cmd[20], Index;

	Index = 0;
	Cmd[Index ++] = 'W';
	Cmd[Index ++] = 'R';
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 156;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 2;

	Cmd[Index ++] = 'T';
	Cmd[Index ++] = 'E';
	Cmd[Index ++] = 'S';
	Cmd[Index ++] = 'T';

	//	pio_set_pin_low(PIO_PA3_IDX);
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DPRAM);
	OnSPI_Write(MOpm_SPI, Cmd, 10);
	
	OnRead_Tmp125();
	//	Delay_us(2);
	//	pio_set_pin_high(PIO_PA3_IDX);
	//	Delay_ms(10);
}
///////////////////////////////////////////////////////////////////////////////
void OnDpram_Read_data(void)
{
	uint8_t Cmd[20], Databuf[10], Index;

	Index = 0;
	Cmd[Index ++] = 'R';
	Cmd[Index ++] = 'D';
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 156;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 2;

	Databuf[0] = '\0';
	Databuf[1] = '\0';
	Databuf[2] = '\0';
	Databuf[3] = '\0';
	Databuf[4] = '\0';

	//	pio_set_pin_low(PIO_PA3_IDX);
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DPRAM);
	OnSPI_Write(MOpm_SPI, Cmd, 6);

	OnSPI_Read(MOpm_SPI, Databuf, 4);
	OnRead_Tmp125();
	
	//	Delay_us(2);
	//	pio_set_pin_high(PIO_PA3_IDX);
	//	Delay_ms(10);
}
///////////////////////////////////////////////////////////////////////////////
