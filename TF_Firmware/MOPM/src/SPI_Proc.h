///////////////////////////////////////////////////////////////////////
// SPI_Proc.h
//
///////////////////////////////////////////////////////////////////////
#ifndef __SPI_H__
#define __SPI_H__

///////////////////////////////////////////////////////////////////////////////
#define SPI_CLK_CH0_3			24000000UL		// for SPI ch0 ~ ch3
#define SPI_CLK_CH4_7			 8000000UL		// for SPI ch4 ~ ch7
#define SPI_CLK_CH8_11			24000000UL		// for SPI ch8 ~ ch11
#define SPI_CLK_CH12_14			24000000UL		// for SPI ch12 ~ ch14

///////////////////////////////////////////////////////////////////////////////
#define MOpm_SPI					SPI0
#define SPI_ID						ID_SPI0
#define SPI_CH_DAC1					0x0
#define SPI_CH_TMP125				0x1
#define SPI_CH_FLASH				0x3	// for use chip AT45DB161E
#define SPI_CH_DPRAM				0x7
#define SPI_CH_NONE					0xF
///////////////////////////////////////////////////////////////////////////////
#define SPI_CH0_3					0x00
#define SPI_CH4_7					0x01
#define SPI_CH8_11					0x02
#define SPI_CH12_14					0x03
///////////////////////////////////////////////////////////////////////////////
#define SPI_CH0_CLK_POL				0
#define SPI_CH4_CLK_POL				0
#define SPI_CH8_CLK_POL				0
#define SPI_CH12_CLK_POL			0
#define SPI_CH0_CLK_PHASE			1
#define SPI_CH4_CLK_PHASE			1
#define SPI_CH8_CLK_PHASE			1
#define SPI_CH12_CLK_PHASE			0
///////////////////////////////////////////////////////////////////////////////
#define NO_CH_SELECTED				0xF
#define SPI_DLYBSP					0x8	//0x50
#define SPI_DLYBCT					0x3
#define SPI_DLYBCS					0X8	//0x88

//////////////////////////////////////////////////////////////
// SPI interface functions
void OnMTF_SPI_Initial(void);
void OnMTF_SPI_CS(Spi *pSPI, uint32_t Channel);
char OnSPI_Read(Spi *pSPI, uint8_t *Data, uint16_t Length);
char OnSPI_Write(Spi *pSPI, uint8_t *Data, uint16_t Length);
char OnSPI_TxEmpty(Spi *pSPI);
//////////////////////////////////////////////////////////////
#endif // __SPI_H__