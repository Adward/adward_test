﻿#ifndef SORT_H_
#define SORT_H_

typedef struct data_type
{
	int index;
	float data;
	char type_peak_valley;
} Data_Type;

void swap(int *a, int *b);
void sort(int *data, int left, int right);
void swap_struct(Data_Type *a, Data_Type *b);
void sort_structure(Data_Type *data, int left, int right);
void BubbleSort(Data_Type A[], int n);

int reverse(int *array, unsigned int size);



#endif /* SORT_H_ */
