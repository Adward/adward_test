///////////////////////////////////////////////////////////////////////
//	Ch_Proc.c: process channel related request
//
///////////////////////////////////////////////////////////////////////
#include "MTF.h"
extern uint16_t WSNR;
///////////////////////////////////////////////////////////////////////
void OnSetTF_Position(uint8_t SetChNum, int32_t SetWL)
{
	//-------------------------------------------------------------------
	// Used for regular TOF tuning
//	gpio_set_pin_low(TEST_PIN13);
	OnGet_ChControlV_DAC(SetChNum, SetWL);
	OnSet_Voltage(SetCh_DAC, SetCh_Dir);
//	gpio_set_pin_low(TEST_PIN13);
	//------------------------------------------------------------------
	MTF_Mode = RUN_NORMAL;
	Pre_Tmp = CurrentTmp;
	
	Ch_Num = SetChNum;					// Update Current Ch-number
	Ch_Wavelength = SetWL;				// and wavelength
	Ch_Voltage = SetCh_Voltage;
}
///////////////////////////////////////////////////////////////////////
void OnGet_ChControlV_DAC(uint8_t SetChNum, int32_t SetWL)
{
	OnGetTempTableIndex();
	//--------------------------------------------------------------------------------------
	// Interpolate XY voltage between two adjacent channels of "SetChNum" and "SetChNum + 1"
	OnGet_V_atWL(SetChNum, SetWL, Index_TL, &Ch_VL);							// Will calculate Ch_VL
	//--------------------------------------------------------------------------------------
	// Interpolate set voltage if current temperature is not equal to calibration point temperature
	if (Index_TL != Index_TH)
	{
		OnGet_V_atWL(SetChNum, SetWL, Index_TH, &Ch_VH);						// will calculate Ch_VH

		if (TTable[Index_TH] == TTable[Index_TL]) SetCh_Voltage = Ch_VL;		// Wrong Temperature Table case
		else SetCh_Voltage = Ch_VL + (Ch_VH - Ch_VL)*(CurrentTmp - TTable[Index_TL])/(TTable[Index_TH] - TTable[Index_TL]);
	}
	else SetCh_Voltage = Ch_VL;
	//-------------------------------------------------------------------
	if (SetCh_Voltage < 0)		// SetCh_Voltage can be (+) or (-)
	{
		Ch_VL = - SetCh_Voltage;
		SetCh_Dir = 1;
	}
	else
	{
		Ch_VL = SetCh_Voltage;
		SetCh_Dir = 0;
	}
	//-------------------------------------------------------------------
	if (Ch_VL > MAX_SET_VOLTAGE) Ch_VL = MAX_SET_VOLTAGE;
	//----------------------------------------------------------------------------------------
	// Convert set voltage value to DAC input number, it is always (+), use DIR for direction
	SetCh_DAC = (Ch_VL * DAC_DATA_SCALE + DAC_OFFSET);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void OnGet_V_atWL(uint8_t SetChNum, int32_t SetWL, uint16_t Index_T, float* DataV)
{
	// Use Linear V^2 to do interpolation
	Data32L  = ChV_Table[Index_T][SetChNum];
	Data32H  = ChV_Table[Index_T][SetChNum + 1];

	if (Data32L < 0) fDataL = -(float)(Data32L) * (float)(Data32L);
	else fDataL = (float)(Data32L) * (float)(Data32L);
	if (Data32H < 0) fDataH = -(float)(Data32H) * (float)(Data32H);
	else fDataH = (float)(Data32H) * (float)(Data32H);

	//-----------------------------------------------------------------------------------------------
	WL_L  = ITU_WL_Table[SetChNum];
	WL_H  = ITU_WL_Table[SetChNum + 1];
	//-----------------------------------------------------------------------------------------------
	// Use WL and V^2 relation for control voltage interpolation
	if (SetWL == WL_L) *DataV = (float)Data32L;
	else if (SetWL == WL_H) *DataV = (float)Data32H;
	else if (WL_L != WL_H)						// Correct calibration table must meet it
	{
		fData = fDataL + (fDataH - fDataL)*(SetWL - WL_L)/(WL_H - WL_L);
		if (fData >= 0) *DataV = sqrtf(fData);
		else *DataV = -sqrtf(-fData);
	} //--------------------------------------------------------------------
	else	// Wrong calibration table, set ERROR code if necessary
	{
		*DataV = (float)Data32L;
	}
};
////////////////////////////////////////////////////////////////////////////////////
void OnGetInsertionLoss(uint8_t ChNum, int32_t ILoss_WL)		// Get I-Loss at WL
{
	int16_t ILoss_L, ILoss_H;

	OnGetTempTableIndex();
	//----------------------------------------------------------------------------------
	// Get two adjacent data, then interpolate at WL
	WL_L  = ITU_WL_Table[ChNum];						// IL at ITU WL
	WL_H  = ITU_WL_Table[ChNum + 1];

	//-----------------------------------------------------------------------------------
	Data32L  = (int32_t)(IL_Table[Index_TL][ChNum]);
	Data32H  = (int32_t)(IL_Table[Index_TL][ChNum + 1]);

	ILoss_L = 0;
	ILoss_H = 0;
	//-----------------------------------------------------------------------------------
	if (WL_L != WL_H)
	{
		if (ILoss_WL == WL_L) ILoss_L = Data32L;
			else if (ILoss_WL == WL_H) ILoss_L = Data32H;
			else ILoss_L = Data32L + (Data32H - Data32L)*(ILoss_WL - WL_L)/(WL_H - WL_L);
	}
	else
	{
		//-------------------- Calibration data error, set error code here
		ILoss_L = Data32L;
	}
	//-----------------------------------------------------------------------------------
	if (Index_TL == Index_TH) Ch_ILoss = ILoss_L;
	else
	{
		Data32L  = (int32_t)(IL_Table[Index_TH][ChNum]);
		Data32H  = (int32_t)(IL_Table[Index_TH][ChNum + 1]);

		if (WL_L != WL_H)
		{
			if (ILoss_WL == WL_L) ILoss_H = Data32L;
			else if (ILoss_WL == WL_H) ILoss_H = Data32H;
			else ILoss_H = Data32L + (Data32H - Data32L)*(ILoss_WL - WL_L)/(WL_H - WL_L);
		}
		else
		{
			//-------------------- Calibration data error, set error code here
			ILoss_H = Data32L;
		}
		//--------------------------------------------------------------------------------------------------------------------
		// Interpolate ILoss for Temperature
		Ch_ILoss = ILoss_L + (ILoss_H - ILoss_L)*(CurrentTmp - TTable[Index_TL])/(TTable[Index_TH] - TTable[Index_TL]);
	}
}
////////////////////////////////////////////////////////////////////////////////////
void OnGetFilterBW(uint8_t ChNum, int32_t Bandwidth_WL)		// Get Filter BW at WL
{
	int16_t Bandwidth_L, Bandwidth_H;

	OnGetTempTableIndex();
	//----------------------------------------------------------------------------------
	// Get two adjacent data, then interpolate at WL
	WL_L  = ITU_WL_Table[ChNum];						// IL at ITU WL
	WL_H  = ITU_WL_Table[ChNum + 1];

	//-----------------------------------------------------------------------------------
	Data32L  = (int32_t)(BW_Table[Index_TL][ChNum]);
	Data32H  = (int32_t)(BW_Table[Index_TL][ChNum + 1]);

	Bandwidth_L = 0;
	Bandwidth_H = 0;
	//-----------------------------------------------------------------------------------
	if (WL_L != WL_H)
	{
		if (Bandwidth_WL == WL_L) Bandwidth_L = Data32L;
		else if (Bandwidth_WL == WL_H) Bandwidth_H = Data32H;
		else Bandwidth_L = Data32L + (Data32H - Data32L)*(Bandwidth_WL - WL_L)/(WL_H - WL_L);
	}
	else
	{
		//-------------------- Calibration data error, set error code here
		Bandwidth_L = Data32L;
	}
	//-----------------------------------------------------------------------------------
	if (Index_TL == Index_TH) Ch_BW = Bandwidth_L;
	else
	{
		Data32L  = (int32_t)(BW_Table[Index_TH][ChNum]);
		Data32H  = (int32_t)(BW_Table[Index_TH][ChNum + 1]);

		if (WL_L != WL_H)
		{
			if (Bandwidth_WL == WL_L) Bandwidth_H = Data32L;
			else if (Bandwidth_WL == WL_H) Bandwidth_H = Data32H;
			else Bandwidth_H = Data32L + (Data32H - Data32L)*(Bandwidth_WL - WL_L)/(WL_H - WL_L);
		}
		else
		{
			//-------------------- Calibration data error, set error code here
			Bandwidth_H = Data32L;
		}
		//--------------------------------------------------------------------------------------------------------------------
		// Interpolate F_Bandwidth for Temperature
		Ch_BW = Bandwidth_L + (Bandwidth_H - Bandwidth_L)*(CurrentTmp - TTable[Index_TL])/(TTable[Index_TH] - TTable[Index_TL]);
	}
}
///////////////////////////////////////////////////////////////////////////////////
void OnGetTempTableIndex(void)
{
	uint8_t Index;

	if(refTmp125)
	{
		Tmp125X10 = simTmp;
		CurrentTmp = Tmp125X10;
	}
	else
		CurrentTmp = TmpRX10;
	
	// Change T table to 5 temperature values and using (10 X actual_T)
	//------------------------------------------------------------------
	for (Index = 0; Index < NUM_OF_TEMP-1; Index ++)
	{
		if (CurrentTmp < TTable[0])
		{
			Index_TL = 0;	Index_TH = 1;
			break;
		}
		else if (CurrentTmp == TTable[Index])
		{
			Index_TL = Index;	Index_TH = Index;
			break;
		}
		else if (CurrentTmp == TTable[NUM_OF_TEMP-1])
		{
			Index_TL = NUM_OF_TEMP-1;	Index_TH = NUM_OF_TEMP-1;
			break;
		}
		else if ((CurrentTmp > TTable[Index]) && (CurrentTmp < TTable[Index+1]))
		{
			Index_TL = Index;	Index_TH = Index + 1;
			break;
		}
		else if (CurrentTmp > TTable[NUM_OF_TEMP-1])
		{
			Index_TL = NUM_OF_TEMP - 2;
			Index_TH = NUM_OF_TEMP - 1;
			break;
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////
uint8_t OnGetWL_ChNum(int32_t SetWL)	// Calculate Channel Number from WL
{
	uint8_t ChNum, Index;

	// Customer Channel number: 1 to 88
	// Calibration channel data from 0 to 89, there is ONE ch before and ONE ch after
	// for extra tuning position: ChNum = 0, for WL before the first customer CH
	// and ChNum = Total_CH_NUM, for WL after the last customer CH
	// The found ChNum is lower boundary, that: WL(ChNum) <= SetWL <= WL(ChNum+1)
	//-------------------------------------------------------------------------------
	ChNum = 0;

	Data32L = ITU_WL_Table[1];						// The first Customer CH

	if (SetWL >= Data32L)							// WL >= first channel
	{
		//for (Index = 2; Index < Total_ChNum; Index ++)
		for (Index = 2; Index < (Total_ChNum+1); Index ++)						//VG:
		{
			Data32H = ITU_WL_Table[Index];

			if ((SetWL >= Data32L) && (SetWL < Data32H))
			{
				ChNum = Index - 1; break;
			}
			//else if ((SetWL >= Data32H) && (Index == Total_ChNum - 1))		// WL >= last ch
			else if ((SetWL >= Data32H) && (Index == Total_ChNum))				// WL >= last ch, Guo
			{
				ChNum = Total_ChNum; break;
			}
			Data32L = Data32H;
		}
	}
	return ChNum;	// Returned ChNum is "Calibration channel" number: from 0 to 89
}
//////////////////////////////////////////////////////////////////////////////////////
int32_t OnGetChNum_WL(uint8_t SetChNum)		// Calculate WL from Channel Number
{
	// Customer Channel number starts from 1 (not from 0)
	return (int32_t)(1000*(C_SPEED/(float)(Ch0_Freq - Ch_Space * SetChNum)) + 0.5f);
}
//////////////////////////////////////////////////////////////////////////////////////
void OnGet_Head_Info(void)
{
	int16_t Index;

	PageNum = 0;
	OnFlash_Read_Memory(RxTx_buf, FLASH_PAGE_SIZE, PageNum, 0);

	for (Index = 0; Index < VENDOR_NAME_LENGTH; Index ++)
		VendorName[Index] = RxTx_buf[Index + VENDOR_NAME_ADDR];

	for (Index = 0; Index < HW_VER_LENGTH; Index ++)
		HW_ver[Index] = RxTx_buf[Index + HW_VER_ADDR];

	for (Index = 0; Index < FLASH_VER_LENGTH; Index ++)
		Flash_ver[Index] = RxTx_buf[Index + FLASH_VER_ADDR];

	for (Index = 0; Index < MODULE_SN_LENGTH; Index ++)
		Module_SN[Index] = RxTx_buf[Index + MODULE_SN_ADDR];

	for (Index = 0; Index < MODULE_PN_LENGTH; Index ++)
		Module_PN[Index] = RxTx_buf[Index + MODULE_PN_ADDR];

	for (Index = 0; Index < M_DATE_LENGTH; Index ++)
		Manufacture_date[Index] = RxTx_buf[Index + M_DATE_ADDR];

	for (Index = 0; Index < CUSTOMER_SN_LENGTH; Index ++)
		Customer_SN[Index] = RxTx_buf[Index + CUSTOMER_SN_ADDR];

	for (Index = 0; Index < CUSTOMER_PN_LENGTH; Index ++)
		Customer_PN[Index] = RxTx_buf[Index + CUSTOMER_PN_ADDR];

	for (Index = 0; Index < ORACLE_NUM_LENGTH; Index ++)
		Oracle_Num[Index] = RxTx_buf[Index + ORACLE_NUM_ADDR];

	for (Index = 0; Index < WID_NUM_LENGTH; Index ++)
		Wid_Num[Index] = RxTx_buf[Index + WID_NUM_ADDR];
		
	for (Index = 0; Index < MODULE_TYPE_LENGTH; Index ++)
		Module_Type = RxTx_buf[Index + MODULE_TYPE_ADDR];
Module_Type=1;
	//----------------------------------------------------
	Ch_Space = RxTx_buf[CH_SPACE_ADDR];
Ch_Space=50;	// C or L band	
//Ch_Space=100;	// C+L band
//Ch_Space=150;	// O band

	Total_ChNum = RxTx_buf[CUSTOMER_CH_NUM_ADDR];
Total_ChNum=80;	 //C or L band, 50GHz spacing
//Total_ChNum=86;//O band, 200GHz spacing
//Total_ChNum=115;//O band, 150GHz spacing

	Total_ChNum_Plus = Total_ChNum + 2;
	ScanStartCh = 0;		
	ScanEndCh =  Total_ChNum;
	ETableLength = Total_ChNum_Plus;		//161207 VG: added for MOPM frequency interpolation functions
	
	Ch0_Freq =  (RxTx_buf[CH1_FREQ_ADDR+0] << 24);
	Ch0_Freq += (RxTx_buf[CH1_FREQ_ADDR+1] << 16);
	Ch0_Freq += (RxTx_buf[CH1_FREQ_ADDR+2] << 8);
	Ch0_Freq +=  RxTx_buf[CH1_FREQ_ADDR+3];
	Ch0_Freq +=  Ch_Space;					// extend one channel before customer CH
Ch0_Freq=196100;	//C band, Ch0_Freq > ChN_Freq
//Ch0_Freq=237900;	//O band, Ch0_Freq > ChN_Freq

	ChN_Freq =  (RxTx_buf[CHN_FREQ_ADDR+0] << 24);
	ChN_Freq += (RxTx_buf[CHN_FREQ_ADDR+1] << 16);
	ChN_Freq += (RxTx_buf[CHN_FREQ_ADDR+2] << 8);
	ChN_Freq +=  RxTx_buf[CHN_FREQ_ADDR+3];
	ChN_Freq -=  Ch_Space;					// extend one channel after customer CH
ChN_Freq=192050;	//C band, Ch0_Freq > ChN_Freq
//ChN_Freq=220500;	//O band, Ch0_Freq > ChN_Freq

	if (Ch0_Freq > 0)
		Ch0_WL = (uint32_t)(1000*(C_SPEED/(float)Ch0_Freq) + 0.5f);
	else
	{
		Ch0_WL =  (RxTx_buf[CH1_WL_ADDR+0] << 24);
		Ch0_WL += (RxTx_buf[CH1_WL_ADDR+1] << 16);
		Ch0_WL += (RxTx_buf[CH1_WL_ADDR+2] << 8);
		Ch0_WL +=  RxTx_buf[CH1_WL_ADDR+3];
	}
	if (ChN_Freq > 0)
		ChN_WL = (uint32_t)(1000*(C_SPEED/(float)ChN_Freq) + 0.5f);
	else
	{
		ChN_WL =  (RxTx_buf[CHN_WL_ADDR+0] << 24);
		ChN_WL += (RxTx_buf[CHN_WL_ADDR+1] << 16);
		ChN_WL += (RxTx_buf[CHN_WL_ADDR+2] << 8);
		ChN_WL +=  RxTx_buf[CHN_WL_ADDR+3];
	}
	//----------------------------------------------------
	if (Module_Type >= 100)		// OCM type module
	{
		Scan_Pos_Offset =   (RxTx_buf[SCAN_POS_OFFSET_ADDR+0] << 24);
		Scan_Pos_Offset +=  (RxTx_buf[SCAN_POS_OFFSET_ADDR+1] << 16);
		Scan_Pos_Offset +=  (RxTx_buf[SCAN_POS_OFFSET_ADDR+2] << 8);
		Scan_Pos_Offset +=   RxTx_buf[SCAN_POS_OFFSET_ADDR+3];
		
		Scan_Pos_Resolution =  (RxTx_buf[SCAN_POS_RESOLUTION_ADDR+0] << 24);
		Scan_Pos_Resolution += (RxTx_buf[SCAN_POS_RESOLUTION_ADDR+1] << 16);
		Scan_Pos_Resolution += (RxTx_buf[SCAN_POS_RESOLUTION_ADDR+2] << 8);
		Scan_Pos_Resolution +=  RxTx_buf[SCAN_POS_RESOLUTION_ADDR+3];
	
		EQWL_deltaWL =  (RxTx_buf[EQWL_deltaWL_ADDR+0] << 8);
		EQWL_deltaWL +=  RxTx_buf[EQWL_deltaWL_ADDR+1];
			
		EQS_Space =  (RxTx_buf[EQS_Space_ADDR+0] << 8);
		EQS_Space +=  RxTx_buf[EQS_Space_ADDR+1];
		
		MinPeakLevel = (RxTx_buf[MinPeakLevel_ADDR+0] << 8);
		MinPeakLevel += RxTx_buf[MinPeakLevel_ADDR+1];
		
		Contrast = (RxTx_buf[Contrast_ADDR+0] << 8);
		Contrast += RxTx_buf[Contrast_ADDR+1];
		
		Sidelobe_lmt = (RxTx_buf[Sidelobe_lmt_ADDR+0] << 8);
		Sidelobe_lmt += RxTx_buf[Sidelobe_lmt_ADDR+1];
		
		Sidelobe_alpha = (RxTx_buf[Sidelobe_alpha_ADDR] << 8);
		Sidelobe_alpha += RxTx_buf[Sidelobe_alpha_ADDR+1];
	}
	else	// OSA type module
	{
		
		Scan_Pos_Resolution=13600;		// NB C band
		Scan_Pos_Offset=4556;			// NB C band
		
		//Scan_Pos_Resolution=50000;	// L band
		//Scan_Pos_Offset=11192;		// L band
		
		//Scan_Pos_Resolution=50000;	// L band
		//Scan_Pos_Offset=11192;		// L band
		
		//Scan_Pos_Resolution=30000;	// C+L band
		//Scan_Pos_Offset=6648;			// C+L band
		
		//xScan_Pos_Resolution=28000;	// O band 1 (NG)
		//xScan_Pos_Offset=2800;				
		//xScan_Pos_Resolution=23000;	// O band 2 (NG)
		//xScan_Pos_Offset=1500;
		//Scan_Pos_Resolution=36000;	// O band 3
		//Scan_Pos_Offset=5500;
		
		EQS_Space=1;
	}
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoad_CalibrationTables(void)
{
	uint8_t Index;
	
	OnGet_Head_Info();
	OnLoadTemp_Table();
	OnLoadITU_Table(ITU_WL_Table);
	//------------------------------------------------------------------------
	for (Index = 0; Index < NUM_OF_TEMP; Index ++)
	{
		OnLoadChV_Table(ChV_Table[Index], Index);
		OnLoadIL_dB_Table(IL_Table[Index], Index);
		OnLoad_BW_Table(BW_Table[Index], Index);
		
	//----------------------------------------------------
	/* Restart watchdog */
	    wdt_restart(WDT);
		   
	//-------------------------------------------------------------------------------------------------
	// Calibration table used for half-cycle fast switch, not used in regular TOF
	//	PageNum = (Index * RC_TABLE_SIZE * BT_BYTES * NUM_OF_RC_TABLE + RC_TABLE_ADDR)/FLASH_PAGE_SIZE;
	//	OnLoadRcT_Table(CycleT_Table0[Index], PageNum);
	//	OnLoadRcV_Table(RefV_Table0[Index], PageNum+1);
	//	OnLoadRcT_Table(CycleT_Table1[Index], PageNum+2);
	//	OnLoadRcV_Table(RefV_Table1[Index], PageNum+3);
	//	OnLoadRcT_Table(CycleT_Table2[Index], PageNum+4);
	//	OnLoadRcV_Table(RefV_Table2[Index], PageNum+5);
	}
	//---------------------------------------------
	if (forceModuleType)	Module_Type=ModuleType_NarrowBW;
	//---------------------------------------------
	if (Module_Type==ModuleType_NarrowBW)
	{
		sWindow = 0;				// for MOSA, only report peak power as channel total power.
		calWindow = 0;
		MinPeakLevel = 14848;		//debug only.  -70dB => 256*(128-70)=14848
		//MinPeakLevel = 17408;		//debug only.  -60dB => 256*(128-60)=17408
		Contrast = 1280;			//debug only.  2.5dB => 256*2.5=640; 5dB=> 256*5=1280 (narrow band OPM)

		/* 2017-11-24 */
		#undef SIDE_LOBE_SUPPRESSION_NON_NARROW_BAND
		adc_filter_level_db = -70.0;
		EDGE_AND_ADC_PEAK_RANGE = 20;
		PEAK_DECONVOLUTION_STEP = 4;
		LEFT_EDGE_STEP = 4;
		RIGHT_EDGE_STEP = 4;
		LEFT_EDGE_BEGIN = 8;
		RIGHT_EDGE_BEGIN = 8;
	}
	else
	{
		sWindow = 0;				// for MOSA, only report peak power as channel total power.
		calWindow = 0;
		MinPeakLevel = 14848;		//debug only.  -70dB => 256*(128-70)=14848
		//MinPeakLevel = 17408;		//debug only.  -60dB => 256*(128-60)=17408
		Contrast = 640;				//debug only.  2.5dB => 256*2.5=640; 5dB=> 256*5=1280 (narrow band OPM)
		
		/* 2017-11-24 */
		#define SIDE_LOBE_SUPPRESSION_NON_NARROW_BAND
		adc_filter_level_db = -80.0;
		
		/* 2018-01-10 */
		EDGE_AND_ADC_PEAK_RANGE = 4;
		
		PEAK_DECONVOLUTION_STEP = 4;
		LEFT_EDGE_STEP = 10;
		RIGHT_EDGE_STEP = 10;
		LEFT_EDGE_BEGIN = 18;
		RIGHT_EDGE_BEGIN = 18;
	}
	//------------------------------------------------------------------------
	if (Module_Type)
	{
		// OLD Cal. method ref. TLS only with flash Version V12
		   //OnLoadPower_Table();
		
		OnMTF_Creat_ETable();
		// NEW Cal. method ref. a power meter with flash Version V13
		   OnLoadPower5T_Table();
		OnMOPM_RenewPower_Table();
		
		OnLoadPSF_Table();	
	}
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoadPSF_Table(void)
{
	uint16_t indexP;
	float32_t maxP, minP, rangeP;
	
	maxP=-1; minP=1;
	
	PageNum = PSFADC_TABLE_ADDR/FLASH_PAGE_SIZE;
	Offset = PSFADC_TABLE_ADDR - FLASH_PAGE_SIZE * PageNum;
	OnFlash_Read_Memory((uint8_t*)PSF_ADC, (PSF_BYTES * PSF_TABLE_SIZE/2), PageNum, Offset);
	
	PageNum = PSFADC_TABLE_ADDR2/FLASH_PAGE_SIZE;
	Offset = PSFADC_TABLE_ADDR2 - FLASH_PAGE_SIZE * PageNum;
	OnFlash_Read_Memory((uint8_t*)(PSF_ADC+256), (PSF_BYTES * PSF_TABLE_SIZE/2), PageNum, Offset);
	
	//Normalized PSF
	for (indexP=0; indexP<PSF_TABLE_SIZE; indexP++) {
		data_psf[indexP] = powf(10.0, PSF_ADC[indexP] / 1801.27 - 9.49);		
		if(data_psf[indexP]>maxP)	maxP = data_psf[indexP];
		if(data_psf[indexP]<minP)	minP = data_psf[indexP];
	}
	
	rangeP = maxP-minP;
	for (indexP=0; indexP<PSF_TABLE_SIZE; indexP++) {
		data_psf[indexP] = (data_psf[indexP]-minP)/(rangeP);
	}	
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoadPower5T_Table(void)
{
	uint8_t ch, index;
	uint16_t TempIndex;
	
	// 2-Table per page for 512-bytes/page flash
	// Load Cal. ADC counts
	for (TempIndex = 0; TempIndex < NUM_OF_TEMP; TempIndex ++)
	{
		PageNum = (TempIndex * POWER_TABLE_SIZE * PW_BYTES + PowerADC_TABLE_ADDR)/FLASH_PAGE_SIZE;
		Offset = (POWER_TABLE_SIZE * PW_BYTES) * (TempIndex - (uint8_t)(TempIndex/2)*2);
		OnFlash_Read_Memory(&ChP_Table[TempIndex], (PW_BYTES * POWER_TABLE_SIZE), PageNum, Offset);
	}
	//------------------------------------------------------------------------
	
	// Load Cal. Input_Power in Q8 format ( Q8=256*(128+Input_Power)).
	// The power is a deconvoluted peak power, a ratio of total power for each channel.
	for (TempIndex = 0; TempIndex < NUM_OF_TEMP; TempIndex ++)
	{
		PageNum = (TempIndex * POWER_TABLE_SIZE * PW_BYTES + ChPower_ADDR)/FLASH_PAGE_SIZE;
		//PageNum = (POWER_TABLE_SIZE * PW_BYTES + ChPower_ADDR)/FLASH_PAGE_SIZE;
		Offset = (POWER_TABLE_SIZE * PW_BYTES) * (TempIndex - (uint8_t)(TempIndex/2)*2);
		OnFlash_Read_Memory(&ChP_PMeter[TempIndex], (PW_BYTES * POWER_TABLE_SIZE), PageNum, Offset);
	}
}
//////////////////////////////////////////////////////////////////////////////////////
void OnMOPM_RenewPower_Table(void)
{
	uint8_t		ch, index;
	uint16_t	Ch_PL, Ch_PH;
	int16_t		X1, X2;
	
	OnGetTempTableIndex();
	X1 = TTable[Index_TL];		X2 = TTable[Index_TH];
	
	//------------------------------------------------------------------------
	//CH0 input with different optical power
	for (index = 0; index < CAL_HEAD_SIZE; index ++) //CH#0 ADC-count
	{
		Ch_PL = ChP_Table[Index_TL][index];   Ch_PH= ChP_Table[Index_TH][index];
		if (X2 != X1)
			PTable_ADC[0][index] = Ch_PL + (CurrentTmp-X1)*(Ch_PH-Ch_PL)/(X2-X1);
		else
			PTable_ADC[0][index] = Ch_PL;		
	}
	//------------------------------------------------------------------------
	//All channel input with the same max input optical power
	ch=0;
	for (index=CAL_HEAD_SIZE; index<(Total_ChNum_Plus+CAL_HEAD_SIZE); index++)
	{
		Ch_PL = ChP_Table[Index_TL][index];   Ch_PH= ChP_Table[Index_TH][index];
		if (X2 != X1)		
			PTable_ADC[ch][0] = Ch_PL + (CurrentTmp-X1)*(Ch_PH-Ch_PL)/(X2-X1);
		else
			PTable_ADC[ch][0] = Ch_PL;
			
		ch++;
	}
	
	//------------------------------------------------------------------------
	//------------------------------------------------------------------------
	// CH0 input with different optical power
	for (index = 0; index < CAL_HEAD_SIZE; index ++) //CH#0 ADC-count
	{
		Ch_PL = ChP_PMeter[Index_TL][index];   Ch_PH= ChP_PMeter[Index_TH][index];
		if (X2 != X1)
		{
			PTable_head[index] = Ch_PL + (CurrentTmp-X1)*(Ch_PH-Ch_PL)/(X2-X1);
			Laser_Ch0Power[index] = Ch_PL + (CurrentTmp-X1)*(Ch_PH-Ch_PL)/(X2-X1);
		}
		else
		{
			PTable_head[index] = Ch_PL;
			Laser_Ch0Power[index] = Ch_PL;		//256*(128-10)
		}
	}
	//------------------------------------------------------------------------
	//All channel input with different max input optical power
	ch=0;
	for (index=CAL_HEAD_SIZE; index<(Total_ChNum_Plus+CAL_HEAD_SIZE); index++)
	{
		Ch_PL = ChP_PMeter[Index_TL][index];   Ch_PH= ChP_PMeter[Index_TH][index];
		if (X2 != X1)
			Laser_ChPower[ch] = Ch_PL + (CurrentTmp-X1)*(Ch_PH-Ch_PL)/(X2-X1);
		else
			Laser_ChPower[ch] = Ch_PL;	// non-equal input power for each channel due to splitter
		
		ch++;
	}
	
	OnMTF_Create_PTable();
}
//////////////////////////////////////////////////////////////////////////////////////
/*
void OnLoadPower_Table(void)
{
	uint16_t Index, k;
	
	PageNum = PowerADC_TABLE_ADDR/FLASH_PAGE_SIZE;
	Offset = PowerADC_TABLE_ADDR - FLASH_PAGE_SIZE * PageNum;
	OnFlash_Read_Memory((uint8_t*)RxTx_buf, (PW_BYTES * POWER_TABLE_SIZE), PageNum, Offset);
	
	k = 0;
	for (Index = 0; Index < CAL_HEAD_SIZE; Index ++) //CH#0 ADC-count
	{
		PTable_ADC[0][Index] = RxTx_buf[k ++];
		PTable_ADC[0][Index] += RxTx_buf[k ++] << 8;
		
		Laser_Ch0Power[Index] = PTable_head[Index];	//256*(128-10)
	}
	for (Index = 0; Index < ETableLength; Index ++)	//profile ADC-count
	{
		PTable_ADC[Index][0] = RxTx_buf[k ++];
		PTable_ADC[Index][0] += RxTx_buf[k ++] << 8;
		
		Laser_ChPower[Index] = PTable_head[0];		//256*(128-10), equal input power for each channel, -10dBm
	}
	
	OnMTF_Create_PTable();
	OnMTF_Creat_ETable();
}
*/
//////////////////////////////////////////////////////////////////////////////////////
void OnLoadTemp_Table(void)
{
	PageNum = TEMP_TABLE_ADDR/FLASH_PAGE_SIZE;
	Offset = TEMP_TABLE_ADDR - FLASH_PAGE_SIZE * PageNum;
	OnFlash_Read_Memory((uint8_t*)TTable, (TT_BYTES * TEMP_TABLE_SIZE), PageNum, Offset);
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoadChV_Table(int32_t* Data, uint8_t TempIndex)
{
	PageNum = (TempIndex * CH_V_TABLE_SIZE * CHV_BYTES + CH_V_TABLE_ADDR)/FLASH_PAGE_SIZE;
	OnFlash_Read_Memory((uint8_t*)Data, FLASH_PAGE_SIZE, PageNum, 0);
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoadIL_dB_Table(int16_t* Data, uint8_t TempIndex)
{
	// 2-Table per page for 512-bytes/page flash
	PageNum = (TempIndex * IL_TABLE_SIZE * IL_BYTES + IL_TABLE_ADDR)/FLASH_PAGE_SIZE;
	Offset = (IL_BYTES * IL_TABLE_SIZE) * (TempIndex - (uint8_t)(TempIndex/2)*2);
	OnFlash_Read_Memory((uint8_t*)Data, (IL_TABLE_SIZE * IL_BYTES), PageNum, Offset);
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoad_BW_Table(int16_t* Data, uint8_t TempIndex)
{
	// 2-Table per page for 512-bytes/page flash
	PageNum = (TempIndex * BW_TABLE_SIZE * BW_BYTES + BW_TABLE_ADDR)/FLASH_PAGE_SIZE;
	Offset = (BW_BYTES * BW_TABLE_SIZE) * (TempIndex - (uint8_t)(TempIndex/2)*2);
	OnFlash_Read_Memory((uint8_t*)Data, (BW_TABLE_SIZE * BW_BYTES), PageNum, Offset);
}
//////////////////////////////////////////////////////////////////////////////////////
void OnLoadITU_Table(uint32_t* Data)
{
	// Flash page size = 512, TOTAL_CH_NUM <= 128
	PageNum = ITU_WL_TABLE_ADDR/FLASH_PAGE_SIZE;
	OnFlash_Read_Memory((uint8_t*)Data, FLASH_PAGE_SIZE, PageNum, 0);
}
/////////////////////////////////////////////////////////////////////////////////////
void OnLoadRcT_Table(float* Data, uint8_t Page_Num)
{
	OnFlash_Read_Memory((uint8_t*)Data, FLASH_PAGE_SIZE, Page_Num, 0);
}
/////////////////////////////////////////////////////////////////////////////////////
void OnLoadRcV_Table(int32_t* Data, uint8_t Page_Num)
{
	OnFlash_Read_Memory((uint8_t*)Data, FLASH_PAGE_SIZE, Page_Num, 0);
}
//////////////////////////////////////////////////////////////////////////////////////


