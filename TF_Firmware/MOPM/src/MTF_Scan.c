///////////////////////////////////////////////////////////////////////////////
//
//	Mems Scan functions for MEMS TF
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"

#ifdef  simADC
	//	#include "HuaWai_Simulate.h"
		#include "SNA0217_HuaWai2_Simulate.h"
	//	#include "SNA0215_Simulate.h"
#endif
#include "Pusdo_Deconv.h"
///////////////////////////////////////////////////////////////////////////////
// Interrupt Service Routing when Signal Buffer FULL.
void MOpmDMA_Handler(void){OnMTF_DMA();}
///////////////////////////////////////////////////////////////////////////////

/**
 * \brief TC0 to software trigger ADC conversion for 100-pin M7 evaluation version only.
 */

/*
// Interrupt Service Routing for MOpm_TC0 to generate a software trigger for AFEC0
void MOpmTC0_Handler(void){OnMTF_TC0();}
void OnMTF_TC0(void)
{
	volatile uint32_t ul_dummy;

	// Clear status bit to acknowledge interrupt
	ul_dummy = tc_get_status(TC0, 0);
	// Avoid compiler warning
	UNUSED(ul_dummy);
	
	afec_start_software_conversion(AFEC0);
}
*/

///////////////////////////////////////////////////////////////////////////////
/**
 * \brief XDMAC interrupt handler.
 */
void OnMTF_DMA(void)
{
	uint32_t dma_status;

	dma_status = xdmac_channel_get_interrupt_status(XDMAC, XDMAC_SignalCH);

	if (dma_status & XDMAC_CIS_BIS) {
		tc_stop(TC0, 0);
		OnMTF_Scan_Finish();
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_StartScan(void)
{
	updateT = FALSE;
	//---------------------------------------------
	
	// Enable the XDMA transfer
	ScanData_Ready = FALSE;
	xdmac_channel_enable(XDMAC, XDMAC_SignalCH);
	tc_start(TC0, 0);
	ScanPosIndex = 0;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_MEMS_Control(void)
{
	// Clears DRDY by reading AFEC_LCDR
	afec_get_latest_value(AFEC0);

	// Clear status bit to acknowledge interrupt
	afec_channel_get_value(AFEC0, AFEC_CHANNEL_0);
	afec_get_interrupt_status(AFEC0);

	if (MTF_Mode == MTF_SCAN) 
		OnMTF_SetScanPos_EqWL(ScanPosIndex); 
	else 
		OnMTF_SetScanPos2(ScanPosIndex);
				
	ScanPosIndex ++;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_SetScanPos_EqWL(uint16_t StepIndex)
{
	float32_t ScanPos;

	if (ScanDir == 0) ScanPos = EqWL_ScanPos[StepIndex];
	else ScanPos = EqWL_ScanPos[ScanPosLength - StepIndex - 1];
	//-----------------------------------------------------------------------------
	if (ScanPos < 32768)
	{
		Data32 = (int32_t)(sqrtf((32768.f - ScanPos)/32768.f)*(65535 - Offset_DAC) + Offset_DAC+0.5f);
		if (Data32 > 65535) ScanDAC = 65535;
		else ScanDAC = (uint16_t)Data32;
		DAC1_Output(ScanDAC, MEMS_CH_A);
	}
	else
	{
		Data32 = (int32_t)(sqrtf((ScanPos - 32768.f)/32768.f)*(65535 - Offset_DAC) + Offset_DAC+0.5f);
		if (Data32 > 65535) ScanDAC = 65535;
		else ScanDAC = (uint16_t)Data32;
		DAC1_Output(ScanDAC, MEMS_CH_C);
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_SetScanPos(uint16_t StepIndex)
{
	uint32_t ScanPos;

	//if (StepIndex & 0x01) pio_set_pin_low(PIO_PA1_IDX);
	//else pio_set_pin_high(PIO_PA1_IDX);

	ScanPos = (StepIndex*ScanEndPos)/SCAN_MAX_LENGTH;
	//----------------------------------------------------
	if (ScanPos < 32768)
	{
		Data32 = (int32_t)((32768.f - ScanPos)*(65535-Offset_DAC)/32768.f + Offset_DAC + 0.5f);

		if (Data32 > 65535) ScanDAC = 65535;
		else ScanDAC = (uint16_t)Data32;
		DAC1_Output(ScanDAC, MEMS_CH_A);
	}
	else
	{
		Data32 = (int32_t)((ScanPos - 32768.f)*(65535-Offset_DAC)/32768.f + Offset_DAC + 0.5f);
		if (Data32 > 65535) ScanDAC = 65535;
		else ScanDAC = (uint16_t)Data32;
		DAC1_Output(ScanDAC, MEMS_CH_C);
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_SetScanPos2(uint16_t StepIndex)
{
	uint32_t ScanPos;

	//if (StepIndex & 0x01) pio_set_pin_low(PIO_PA1_IDX);
	//else pio_set_pin_high(PIO_PA1_IDX);
	
	if (ScanDir == 0) ScanPos = ((StepIndex+Scan_Pos_Offset)*ScanEndPos)/Scan_Pos_Resolution;
	else ScanPos = ((ScanPosLength - StepIndex - 1)+Scan_Pos_Offset)*ScanEndPos/Scan_Pos_Resolution;
	//-----------------------------------------------------------------------------
	//ScanPos = ((StepIndex+Scan_Pos_Offset)*ScanEndPos)/Scan_Pos_Resolution;	//20170314 VG: replaced with partial scan.
	//-----------------------------------------------------------------------------
	
	if (ScanPos < 32768)
	{
		//Data32 = (int32_t)(sqrtf((32768.f - ScanPos)/32768.f)*(65535 - Offset_DAC) + Offset_DAC + 0.5f);
		Data32 = (int32_t)(sqrtf((32768.f - ScanPos)/32768.f)*(65535 - Offset_DAC) + Offset_DAC);
		if (Data32 > 65535) ScanDAC = 65535;
		else ScanDAC = (uint16_t)Data32;
		DAC1_Output(ScanDAC, MEMS_CH_A);
		Ch_Dir=1;
	}
	else
	{
		//Data32 = (int32_t)(sqrtf((ScanPos - 32768.f)/32768.f)*(65535 - Offset_DAC) + Offset_DAC + 0.5f);
		Data32 = (int32_t)(sqrtf((ScanPos - 32768.f)/32768.f)*(65535 - Offset_DAC) + Offset_DAC);
		if (Data32 > 65535) ScanDAC = 65535;
		else ScanDAC = (uint16_t)Data32;
		DAC1_Output(ScanDAC, MEMS_CH_C);
		Ch_Dir=0;
	}
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_SetEqWL_Table(void)
{
	uint16_t Index;
	uint8_t Ch, deltaWL;
	float32_t fWL;
	
	OnRead_TmpR();
	OnGetTempTableIndex();
	Pre_Tmp = CurrentTmp;
	//-------------------------------------------------------
	deltaWL = 10;		// use 10pm as step size, // (float32_t)(ChN_WL - Ch0_WL)/SCAN_MAX_LENGTH;
	for (Index = 0; Index <= SCAN_MAX_LENGTH; Index ++)
	{
		//fWL = (float32_t)(ScanStartWL + Index * deltaWL);
		fWL = (float32_t)(ScanStartWL + Index * EQWL_deltaWL);
		if (fWL > ScanEndWL) break;
		//---------------------------------------------------		
		for (Ch = 0; Ch < Total_ChNum-1; Ch ++)
		{
			fData = 0;
			if (fWL <= ITU_WL_Table[0])
			{
				WL_L = ITU_WL_Table[0];  
				WL_H = ITU_WL_Table[1];
				fDataL = (float32_t)(ChV_Table[Index_TL][0] + ChV_Table[Index_TH][0])/2.f;
				fDataH = (float32_t)(ChV_Table[Index_TL][1] + ChV_Table[Index_TH][1])/2.f;
				
				if (fDataL >= 0) fDataL *= fDataL;
				else fDataL *= -fDataL;
				if (fDataH >= 0) fDataH *= fDataH;
				else fDataH *= -fDataH; 
				
				fData = fDataL + (float32_t)(fWL - WL_L)*(fDataH - fDataL)/(WL_H - WL_L);
				break;
			}
			else if (((fWL > ITU_WL_Table[Ch])) && (fWL <= ITU_WL_Table[Ch + 1]))
			{
				WL_L = ITU_WL_Table[Ch];
				WL_H = ITU_WL_Table[Ch+1];
				fDataL = (float32_t)(ChV_Table[Index_TL][Ch] + ChV_Table[Index_TH][Ch])/2.f;
				fDataH = (float32_t)(ChV_Table[Index_TL][Ch+1] + ChV_Table[Index_TH][Ch+1])/2.f;
				
				if (fDataL >= 0) fDataL *= fDataL;
				else fDataL *= -fDataL;
				if (fDataH >= 0) fDataH *= fDataH;
				else fDataH *= -fDataH; 
				
				fData = fDataL + (float32_t)(fWL - WL_L)*(fDataH- fDataL)/(WL_H - WL_L);
				break;
			}
			else if (fWL > ITU_WL_Table[Total_ChNum-1])
			{
				WL_L = ITU_WL_Table[Total_ChNum-2];
				WL_H = ITU_WL_Table[Total_ChNum-1];
				fDataL = (float32_t)(ChV_Table[Index_TL][Total_ChNum-2] + ChV_Table[Index_TH][Total_ChNum-2])/2.f;
				fDataH = (float32_t)(ChV_Table[Index_TL][Total_ChNum-1] + ChV_Table[Index_TH][Total_ChNum-1])/2.f;
				
				if (fDataL >= 0) fDataL *= fDataL;
				else fDataL *= -fDataL;
				if (fDataH >= 0) fDataH *= fDataH;
				else fDataH *= -fDataH; 
				
				fData = fDataL + (float32_t)(fWL - WL_L)*(fDataH - fDataL)/(WL_H - WL_L);
				break;
			}
		}
		//-----------------------------------------------------
		if (fData>= 0) fData = sqrtf(fData) * (DAC_DATA_SCALE);
		else fData = -sqrtf(-fData) * (DAC_DATA_SCALE);
		
		if (fData > 0) EqWL_ScanPos[Index] = (int16_t)(32768.f + 32768.f*(fData/(65535 - Offset_DAC))*(fData/(65535 - Offset_DAC)));
		else EqWL_ScanPos[Index] = (int16_t)(32768.f - 32768.f*(fData/(65535 - Offset_DAC))*(fData/(65535 - Offset_DAC)));
	}
	//----------------------------------
	ScanPosLength = Index;
	MTF_Mode = MTF_SCAN;
	OnMTF_XDMAC_Initial(AFEC0, ScanPosLength);
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_SetEqS_Table(void)
{
	uint16_t Index;

	//-------------------------------------------------------
	for (Index = 0; Index <= SCAN_MAX_LENGTH; Index ++)
	{
		// To save space, using buffer EqWL_ScanPos for EQSpacing scan.
		EqWL_ScanPos[Index] = ((Index+Scan_Pos_Offset)*ScanEndPos)/Scan_Pos_Resolution;	
	}
	//----------------------------------
	ScanPosLength = Index;
	//MTF_Mode = MTF_SCAN;
	MTF_Mode = MOPM_RW_SCAN;
	OnMTF_XDMAC_Initial(AFEC0, ScanPosLength);
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_SetEqS_Table_Cal(void)
{
	uint16_t Index;

	//-------------------------------------------------------
	for (Index = 0; Index <= SCAN_MAX_LENGTH; Index ++)
	{
		// To save space, using buffer EqWL_ScanPos for EQSpacing scan.
		EqWL_ScanPos[Index] = ((Index+Scan_Pos_Offset)*ScanEndPos)/Scan_Pos_Resolution;
	}
	//----------------------------------
	ScanPosLength = Index;
	//MTF_Mode = MTF_SCAN;
	//MTF_Mode = MOPM_RW_SCAN;
	OnMTF_XDMAC_Initial(AFEC0, ScanPosLength);
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Scan_Finish(void)
{
	uint32_t afec_status;

	/* Clear status bit to acknowledge interrupt */
	//afec_status = afec_get_interrupt_status(AFEC0);

	
		xdmac_channel_disable(XDMAC, XDMAC_SignalCH);
		//xdmac_channel_disable(XDMAC, XDMAC_NoiselCH);
				
		//reload ADC XDMA to prepare for next transfer
		xdmac_channel_set_destination_addr(XDMAC, XDMAC_SignalCH, (uint32_t)SignalCh_buf);
		
		// Clears DRDY by reading AFEC_LCDR
		afec_status = afec_get_latest_value(AFEC0);
		// g_afec1_sample_data = afec_get_latest_value(AFEC1);
		// Clears EC0 by reading CDR

		afec_status = afec_channel_get_value(AFEC0, AFEC_CHANNEL_0);
		//---------------------------------------------
		if (MTF_Mode == MTF_SCAN) 
		{
			if (ScanDir == 0)
			{
				Ch_Wavelength = ScanEndWL;
				Ch_Num = ScanEndCh;
			}
			else
			{
				Ch_Wavelength = ScanStartWL;
				Ch_Num = ScanStartCh;
			}
		}
		else 
		{
			// C band (M178 and NB)
			Ch_Wavelength = ChN_WL;
			OnMTF_SetScanPos2(ScanStartPos);
			// O band
//			OnRead_Temp();
//			OnGet_ChControlV_DAC(ScanStartCh, ScanStartWL);
//			OnSet_Voltage(SetCh_DAC, SetCh_Dir);
		}
		
		//---------------------------------------------
		RawDataLength = ScanPosIndex;
		ScanCount += 1;
		ScanData_Ready = TRUE;
		MOpm_ScanDone = 1;
		//---------------------------------------------
		updateT = TRUE;
}
////////////////////////////////////////////////////////////////////////////////
void OnMTF_ProcessUserCmd(void)
{
	uint16_t *SignalCh_buf_tmp, index;
	
	if ((Uart_CmdID == RW_SIGNAL_CMD) || (Uart_CmdID == MTF_SCAN_CMD))
	{
		OnMTF_UART_TxData_2(SignalCh_buf, RawDataLength, RW_SIGNAL_CMD);
	}//----------------------------------------------------------------------------------------------------------------
//	else if (Uart_CmdID == RW_NOISE_CMD)
//	{
//		OnMTF_UART_TxData_2(NoiseCh_buf, RawDataLength, Uart_CmdID);
//	}
	else if (Uart_CmdID == MOPM_SCAN_CMD)
	{
		if (ScanDir)
		{
			for (index=0; index<ScanPosLength; index++)
				NoiseCh_buf[index] = SignalCh_buf[ScanPosLength-index-1];
			SignalCh_buf_tmp = NoiseCh_buf;
		}
		else
		{
			SignalCh_buf_tmp = SignalCh_buf;
		}
//0. No Deconvolution
/*		OnMTF_Convert_RawData2EQS(SignalCh_buf_tmp, SignalCh_Eqs);
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		OnMTF_UART_TxData_2(SignalCh_Eqs, EqsDataLength, EQS_SIGNAL_CMD);
*/		
//1. Run Normally
#ifndef simADC
		OnMTF_Convert_RawData2EQS(SignalCh_buf_tmp, SignalCh_Eqs);
#else
		OnMTF_Convert_RawData2EQS(dgSS_SN0222, SignalCh_Eqs);
#endif
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		OnMTF_UART_TxData_2(SignalCh_Eqs, EqsDataLength, EQS_SIGNAL_CMD);
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == EQS_SIGNAL_CMD)	//161207 VG: Added for spectrum data
	{
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);		
		OnMTF_UART_TxData_2(SignalCh_Eqs, EqsDataLength, EQS_SIGNAL_CMD);		
		MTF_Mode = RUN_NORMAL;
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == SIGNAL_FILTER_WF_CMD)
	{						
#ifndef simADC
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
#else
		OnMTF_Convert_RawData2EQS(dgSS_SN0222,SignalCh_Eqs);
#endif							
		Reply_to_UART(MEASURE_SIGNAL_FWF, RX_OK);
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == ANALYZE_SIGNAL_CMD)	//170112 VG: Added for finding multi-peaks
	{
		OnMTF_Analyse_Signal(SignalCh_Eqs, EqsDataLength);
		OnMTF_UART_TxData_2(MultiPeaks_Eqs, NumOfPeaks, ANALYZE_SIGNAL_CMD);
		MTF_Mode = RUN_NORMAL;
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == USER1_SCAN_Cmd8 || Uart_CmdID == USER1_SCAN_Cmd9)
	{
		Sidelobe_lmt=0x00;
		Sidelobe_alpha=0x40;
		//ScanStartFreq=0x1A2C;
		//ScanStopFreq=0x2C24;
		ScanStartFreq = ChN_Freq - Freq_Offset;
		ScanStopFreq  = Ch0_Freq - Freq_Offset;
#ifndef simADC
// 0. Real-time operation
		// OnMOSA_Deconvolution(SignalCh_buf,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
#else
// 1. Simulation only
		// OnMOSA_Deconvolution(SN0217_HuaWai_case11,SignalCh_EqsDeconv,RawDataLength,RawDataLength);		// HuWai Case 1~11
		// OnMOSA_Deconvolution(ZTE_case6,SignalCh_EqsDeconv,RawDataLength,RawDataLength);					// ZTE Case 4 and 6
		// OnMOSA_Deconvolution(Wuhan_100G_ruler,SignalCh_EqsDeconv,RawDataLength,RawDataLength);		    // Wuhan 100G ruler.
		// OnMOSA_Deconvolution(SN0217_ZTE_case2_80CH100G,SignalCh_EqsDeconv,RawDataLength,RawDataLength);	// SN0217_ZTE_case1~3_80CH100G
		// OnMOSA_Deconvolution(SN0217_HuaWai2_case1,SignalCh_EqsDeconv,RawDataLength,RawDataLength);		// SN0217_HuaWai2_case1~8
		// OnMOSA_Deconvolution(SN0215_4CH50G,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		// OnMOSA_Deconvolution(SN_A0215_1560nm,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		// OnMOSA_Deconvolution(NB_test,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		// OnMOSA_Deconvolution(dgSS_SN0222,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		OnMTF_Convert_RawData2EQS(dgSS_SN0222, SignalCh_Eqs);
#endif			
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		OnMTF_Analyse_Signal(SignalCh_Eqs, EqsDataLength);
		OnMOPM_ReportPW(sWindow);				// 3dB BW
		//OnMOPM_GetBW(Ch_Space, SignalCh_Eqs);	// Q8 format
		OnMTF_GetOSNR(Ch_Space/EQS_Space, SignalCh_Eqs);	// Q8 format
		User1_totalP = OnMTF_GetAvgP(Ch_Space/EQS_Space, SignalCh_Eqs);	// dBm = 256*(128-totalP)
		OnMTF_UART_TxData_user1(Uart_CmdID, RX_OK);
		MTF_Mode = RUN_NORMAL;
	}//----------------------------------------------------------------------------------------------------------------
	else if ((Uart_CmdID == USER1_SCAN_CmdF) && (decimateN > 0))
	{
		Sidelobe_lmt=0x00;
		Sidelobe_alpha=0x40;
		ScanStartFreq = Freq_Begin + Freq_Offset_USER1 - Freq_Offset;
		ScanStopFreq  = Freq_End   + Freq_Offset_USER1 - Freq_Offset;
		decimateNplusONE = decimateN + 1;
#ifndef simADC
// 0. Real-time operation
		//OnMTF_Convert_RawData2EQS_User1ScanCmd9(SignalCh_buf, SignalCh_Eqs, (uint8_t)decimateN);
		OnMTF_Convert_RawData2EQS_User1ScanCmd9(SignalCh_buf, SignalCh_Eqs, (uint8_t)decimateNplusONE);
#else		
		//OnMTF_Convert_RawData2EQS_User1ScanCmd9(dgSS_SN0222, SignalCh_Eqs, (uint8_t)decimateN);
		OnMTF_Convert_RawData2EQS_User1ScanCmd9(dgSS_SN0222, SignalCh_Eqs, (uint8_t)decimateNplusONE);
#endif
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		OnMTF_UART_TxData_user1(Uart_CmdID, RX_OK);
		
		//restore frequency to process entire range
		ScanStartFreq = ChN_Freq - Freq_Offset;
		ScanStopFreq  = Ch0_Freq - Freq_Offset;
		
		MTF_Mode = RUN_NORMAL;
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == USER1_SCAN_Cmd1)
	{
		Sidelobe_lmt=0x00;
		Sidelobe_alpha=0x40;
		ScanStartFreq = ChN_Freq - Freq_Offset;
		ScanStopFreq  = Ch0_Freq - Freq_Offset;
		#ifndef simADC
		// 0. Real-time operation
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
		#else
		OnMTF_Convert_RawData2EQS(dgSS_SN0222, SignalCh_Eqs);
		#endif
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		OnMTF_Analyse_Signal(SignalCh_Eqs, EqsDataLength);
		OnMOPM_ReportPW(sWindow);	// 3dB BW
		OnMTF_UART_TxData_user1(Uart_CmdID, RX_OK);
		
		MTF_Mode = RUN_NORMAL;
	}//----------------------------------------------------------------------------------------------------------------
	else if ((Uart_CmdID == M178_SCAN_Signal) || (Uart_CmdID == M178_SCAN_SignalNoise))
	{
		//ScanStartFreq = 0x1A2C;
		//ScanStopFreq  = 0x2C24;
		ScanStartFreq = ChN_Freq - Freq_Offset;
		ScanStopFreq  = Ch0_Freq - Freq_Offset;

//SIGNAL
#ifndef simADC		
// 0. Run normally
		// OnMOSA_Deconvolution(SignalCh_buf,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
#else
// 1. for simulation only 
		// OnMOSA_Deconvolution(Wuhan_100G_ruler,SignalCh_EqsDeconv,RawDataLength,RawDataLength);		    // Wuhan 100G ruler.
		// OnMOSA_Deconvolution(SN0217_ZTE_case2_80CH100G,SignalCh_EqsDeconv,RawDataLength,RawDataLength);	// SN0217_ZTE_case1~3_80CH100G
		// OnMOSA_Deconvolution(SN0217_HuaWai_case11,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		// OnMOSA_Deconvolution(SN0217_HuaWai2_case1,SignalCh_EqsDeconv,RawDataLength,RawDataLength);		// SN0217_HuaWai2_case1~8
		// OnMOSA_Deconvolution(NB_test,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		// OnMOSA_Deconvolution(dgSS_SN0222,SignalCh_EqsDeconv,RawDataLength,RawDataLength);
		OnMTF_Convert_RawData2EQS(dgSS_SN0222, SignalCh_Eqs);
#endif		   
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		OnMTF_Analyse_Signal(SignalCh_Eqs, EqsDataLength);
		OnMOPM_ReportPW(sWindow);				// 3dB BW
//		OnMOPM_SignalPowerCheck_M178(fReportSignal);			// removed peaks which power divergence are greater than 20dB.  For M178 only.
			
		if (Uart_CmdID == M178_SCAN_Signal){		
			OnMOPM_UART_TxData_M178(ID_ScanSignal, M178Cmd_OK, NumOfPeaks);
		}
		else
		{
//NOISE
#ifndef simADC						
// 0. Run normally
			   OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
			   OnMOPM_NoiseDataGen_M178(SignalCh_Eqs, Pusdo_Deconvolition);
#else
// 1. for simulation only
			   OnMTF_Convert_RawData2EQS(dgSS_SN0222,SignalCh_Eqs);
			// OnMTF_Convert_RawData2EQS(NB_test, SignalCh_Eqs);
			   OnMOPM_NoiseDataGen_M178(SignalCh_Eqs, Pusdo_Deconvolition);
			// OnMOPM_NoiseDataGen_M178(testNoise, Pusdo_Deconvolition);
#endif			
			OnMOPM_UART_TxData_M178(ID_ScanSignalNoise, M178Cmd_OK, 80);
		}
		MTF_Mode = RUN_NORMAL;
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == LOG_ADC_CMD)
	{
		OnMTF_UART_TxData_2(SignalCh_buf, RawDataLength, RW_SIGNAL_CMD);
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == LOG_DECONV_CMD)
	{
		OnMTF_UART_TxData_2(SignalCh_buf, RawDataLength, RW_SIGNAL_CMD);
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == LOG_ResampleP_CMD)
	{
		OnMTF_UART_TxData_2(SignalCh_Eqs, EqsDataLength, RW_SIGNAL_CMD);
	}//----------------------------------------------------------------------------------------------------------------
	else if (Uart_CmdID == LOG_ResampleWL_CMD)
	{
#ifndef simADC			
		//0. No Deconvolution
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
#else	
		OnMTF_Convert_RawData2EQS(dgSS_SN0222,SignalCh_Eqs);
#endif	
		//1.
//		OnMTF_Convert_RawData2EQS(SignalCh_EqsDeconv, SignalCh_Eqs);
		
		OnMTF_UART_TxData_2(SignalCh_Eqs, EqsDataLength, RW_SIGNAL_CMD);
	}//----------------------------------------------------------------------------------------------------------------
	else if(Uart_CmdID == NB_DEMO_rawADC)
	{
		// 0. Run normally (No Deconvolution)
		   OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
		   OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		
		// 1. for simulation only
		// OnMTF_Convert_RawData2EQS(NB_test, SignalCh_Eqs);
		
		OnMTF_UART_TxData_2(SignalCh_Eqs, EqsDataLength, RW_SIGNAL_CMD);
	}
}
/////////////////////////////////////////////////////////////////////////////////
void OnMTF_ScanNext(void)
{
	if ((ScanMode == MTF_SCAN_MODE_2) && (ScanDir == 0))
	{
		ScanDir = 1;
		//pio_set_pin_high(PIO_PA1_IDX);
		OnMTF_StartScan();
		delay_ms(10);
		//pio_set_pin_low(PIO_PA1_IDX);
	}
	else if ((ScanMode == MTF_SCAN_MODE_3) && (ScanDir == 0))
	{
		ScanDir = 1;
		OnMTF_StartScan();
	}
	else if (ScanMode == MTF_SCAN_MODE_4)
	{
		ScanDir = !ScanDir;
		//pio_set_pin_high(PIO_PA1_IDX);
		OnMTF_StartScan();
		delay_ms(10);
		//pio_set_pin_low(PIO_PA1_IDX);
	}
	else if (ScanMode == MTF_SCAN_MODE_5)
	{
		if (ScanDir == 0)
		{
			ScanDir = 1;
			//pio_set_pin_high(PIO_PA1_IDX);
			OnMTF_StartScan();
			delay_ms(10);
			//pio_set_pin_low(PIO_PA1_IDX);
		}
		else
		{
			ScanDir = 0;
			OnMTF_StartScan();
		}
	}
	else if (ScanMode == MOPM_SCAN)
	{
		ScanDir = 0;
		OnMTF_StartScan();
	}
}
/////////////////////////////////////////////////////////////////////////////////