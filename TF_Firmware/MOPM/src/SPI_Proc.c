///////////////////////////////////////////////////////////////////////////////
//
//	Functions for SPI interface
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
char OnSPI_Write(Spi *pSPI, uint8_t *Data, uint16_t Length)
{
	uint16_t Index;
	uint32_t SPI_read_Timeout = SPI_TIMEOUT;

	for (Index = 0; Index < Length; Index ++)
	{
		SPI_read_Timeout = SPI_TIMEOUT;
		pSPI->SPI_TDR = SPI_TDR_TD((uint16_t)Data[Index]);
		while (!(pSPI->SPI_SR & SPI_SR_TDRE))
		{
			if (!SPI_read_Timeout--) return ERR_TIMEOUT;
		}
	}
	return 0;	//OnSPI_TxEmpty(pSPI);
}
///////////////////////////////////////////////////////////////////////////////
char OnSPI_TxEmpty(Spi *pSPI)		// wait for last byte TX empty
{
	uint32_t SPI_read_Timeout = SPI_TIMEOUT;

	while ((pSPI->SPI_SR & SPI_SR_TXEMPTY) != SPI_SR_TXEMPTY)
	{
		if (!SPI_read_Timeout--) return ERR_TIMEOUT;
	}
	return 0;
}
///////////////////////////////////////////////////////////////////////////////
char OnSPI_Read(Spi *pSPI, uint8_t *Data, uint16_t Length)
{
	uint16_t Index;
	uint32_t SPI_read_Timeout = SPI_TIMEOUT;

	for (Index = 0; Index < Length; Index ++)
	{
		pSPI->SPI_TDR = SPI_TDR_TD(0xFF);	// Write Dummy byte
		//----------------------------------------------------------------------
		// Need to check both of RDRF and TEXMPTY
		SPI_read_Timeout = SPI_TIMEOUT;
		while ((pSPI->SPI_SR & (SPI_SR_RDRF|SPI_SR_TXEMPTY)) != (SPI_SR_RDRF|SPI_SR_TXEMPTY))
		{
			if (!SPI_read_Timeout--) return ERR_TIMEOUT;
		}
		Data[Index] = pSPI->SPI_RDR & SPI_RDR_RD_Msk;
	}
	return 0;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_SPI_CS(Spi *pSPI, uint32_t SPI_Ch)
{
	pSPI->SPI_MR &= (~SPI_MR_PCS_Msk);
	pSPI->SPI_MR |= SPI_MR_PCS(SPI_Ch);
}
///////////////////////////////////////////////////////////////////////////////
