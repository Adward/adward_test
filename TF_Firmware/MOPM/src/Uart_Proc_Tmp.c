 /////////////////////////////////////////////////////////////////////////////
 //  Uart_Proc.c															//
 //																			//
 /////////////////////////////////////////////////////////////////////////////
#include "MOpm.h"
#include "Uart_Cmd.h"
//////////////////////////////////////////////////////////////////////////////
void OnMOpm_UART_Decode_2(void)
{
	uint8_t Cmd_H, Cmd_L, Data8;
	uint16_t Data16;

	//------------------------------------------------
	// Get Command Bytes
	Cmd_H = RxTx_buf[0];			// Command High Byte
	Cmd_L = RxTx_buf[1];			// Command Low Byte
	Rx_Checksum = RxTx_buf[Rx_Length + CMD_OVERHEAD -1];
	Data8 = 0;
	//------------------------------------------------
	Data_Checksum = 0;
	for (UartData16 = 0; UartData16 < (Rx_Length + CMD_OVERHEAD - 1); UartData16 ++)
	{
		Data_Checksum += RxTx_buf[UartData16];
	}
	//------------------------------------------------
	if ((Cmd_H == 'S') && (Cmd_L == 'V'))
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(SET_VOLTAGE, RX_CKS_ERROR);
		else
		{
			Data8 = RxTx_buf[4];						// Direction of Set Voltage
			UartData32  = (int32_t)(RxTx_buf[5]) << 24;
			UartData32 += (int32_t)(RxTx_buf[6]) << 16;
			UartData32 += (int32_t)(RxTx_buf[7]) << 8;
			UartData32 += (int32_t)(RxTx_buf[8]);

			if (UartData32 > MAX_SET_VOLTAGE) UartData32 = MAX_SET_VOLTAGE;
			//----------------------------------------------------------------------
			// Convert set voltage value to DAC input number with a Gain factor
			Pre_DAC_Value = UartData32;
			if (Data8 == 0)
			{
				UartData16 = (uint16_t)(UartData32 * DAC_DATA_SCALE + Offset_DAC1_A);
				DAC1_Output(UartData16, MEMS_CH_A);
			}
			else if (Data8 == 1)
			{
				UartData16 = (uint16_t)(UartData32 * DAC_DATA_SCALE + Offset_DAC1_C);
				DAC1_Output(UartData16, MEMS_CH_C);
			}
			else if (Data8 == 2)
			{
				UartData16 = (uint16_t)(UartData32 * DAC_DATA_SCALE + Offset_DAC2_A);
				DAC2_Output(UartData16, MEMS_CH_A);
			}
			else if (Data8 == 3)
			{
				UartData16 = (uint16_t)(UartData32 * DAC_DATA_SCALE + Offset_DAC2_C);
				DAC2_Output(UartData16, MEMS_CH_C);
			}
			else
			{
				Reply_to_MOpm_UART(SET_VOLTAGE, DATA_OUT_RAMGE);
				return;
			}
			Reply_to_MOpm_UART(SET_VOLTAGE, RX_OK);
		}
	} //---------------------------------------------------------------------------
	else if ((Cmd_H == 'O') && (Cmd_L == 'F'))	// Set DAC channel Offset as DAC count
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(SET_OFFSET, RX_CKS_ERROR);
		else
		{
			Data8 = RxTx_buf[4];		// DAC_1 or DAC_2
			if (Data8 == 1)
			{
				Offset_DAC1_A  = ((int16_t)(RxTx_buf[5]) << 8) + RxTx_buf[6];
				Offset_DAC1_C  = ((int16_t)(RxTx_buf[7]) << 8) + RxTx_buf[8];
				DAC1_Output(Offset_DAC1_A, MEMS_CH_A);
			}
			else if (Data8 == 2)
			{
				Offset_DAC2_A  = ((int16_t)(RxTx_buf[5]) << 8) + RxTx_buf[6];
				Offset_DAC2_C  = ((int16_t)(RxTx_buf[7]) << 8) + RxTx_buf[8];
				DAC2_Output(Offset_DAC2_A, MEMS_CH_A);
			}
			Reply_to_MOpm_UART(SET_OFFSET, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'G') && (Cmd_L == 'O'))	// Read channel offset number
	{
		Data8 = RxTx_buf[4];
		if ((Data_Checksum != Rx_Checksum)&&(Data8==0)) Reply_to_MOpm_UART(GET_OF_1, RX_CKS_ERROR);
		else
		{
			Reply_to_MOpm_UART((GET_OF_1 + Data8), RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'R') && (Cmd_L == 'W'))	//Read Signal RAW data
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(GET_RW, RX_CKS_ERROR);
		else
		{
			OnMOpm_StartScan();
			Uart_CmdID = RW_CMD;
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'N') && (Cmd_L == 'S')) // Read Noise RAW data
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(GET_NS, RX_CKS_ERROR);
		else
		{
			OnMOpm_StartScan();
			Uart_CmdID = NS_CMD;
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'W') && (Cmd_L == 'D'))
	{
		OnDpram_Write_data();
//		FlashBlockID = (uint16_t)RxTx_buf[4] << 8;
//		FlashBlockID += RxTx_buf[5];

//		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_DF, RX_CKS_ERROR);
//		else
//		{
//			for (Data8 = 0; Data8 < 16; Data8 ++)
//			{
//				OnFlash_WritePage(&RxTx_buf[6+Data8*FLASH_PAGE_SIZE], FLASH_PAGE_SIZE, (FlashBlockID*16 + Data8));
//			}
//			Reply_to_MOpm_UART(FLASH_DF, RX_OK);
//		}
	} //----------------------------------------------------------------------------------
	else if ((Cmd_H == 'R') && (Cmd_L == 'D'))
	{
		OnDpram_Read_data();
	}
	//------------------------------------------------------------------------------------
	else if ((Cmd_H == 'D') && (Cmd_L == 'F'))			// download flash data 256-bytes per page
	{
		FlashBlockID = (uint16_t)RxTx_buf[4] << 8;		// page number (256 bytes / page)
		FlashBlockID += RxTx_buf[5];					// download 16 pages per transfer

		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_DF, RX_CKS_ERROR);
		else
		{
			for (Data8 = 0; Data8 < 16; Data8 ++)
			{
				OnFlash_WritePage(&RxTx_buf[6+Data8*FLASH_PAGE_SIZE], FLASH_PAGE_SIZE, (FlashBlockID*16 + Data8));
			}
			Reply_to_MOpm_UART(FLASH_DF, RX_OK);
		}
	} //----------------------------------------------------------------------------------
	else if ((Cmd_H == 'U') && (Cmd_L == 'F'))			// upload flash data
	{
		FlashBlockID = (uint16_t)RxTx_buf[4] << 8;
		FlashBlockID += RxTx_buf[5];

		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_UF, RX_CKS_ERROR);
		else
		{
			OnGet_Flash_Data(&RxTx_buf[8], FLASH_PAGE_SIZE*16, FlashBlockID*16);
			Reply_to_MOpm_UART(FLASH_UF, RX_OK);
		}
	} //----------------------------------------------------------------------------------
	else if ((Cmd_H == 'E') && (Cmd_L == 'F'))			// Erase flash sector
	{
		Data8 = RxTx_buf[4];

		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_EF, RX_CKS_ERROR);
		else
		{
			HW_ErrorCode = 0;
			OnSet_Flash_SE(Data8);
			Reply_to_MOpm_UART(FLASH_EF, RX_OK);
		}
	}
	else if ((Cmd_H == 'B') && (Cmd_L == 'T'))	// Read Board Temperature sensor
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(GET_BT, RX_CKS_ERROR);
		else
		{
			OnRead_TMP125();
			Reply_to_MOpm_UART(GET_BT, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'W') && (Cmd_L == 'P'))	//
	{
		Data16 = RxTx_buf[4]*256 + RxTx_buf[5];

		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_WP, RX_CKS_ERROR);
		else
		{
			HW_ErrorCode = 0;
			OnSet_Test_Flash_Write(Data16);
			Reply_to_MOpm_UART(FLASH_WP, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'R') && (Cmd_L == 'P'))	//
	{
		ReadPageNum = RxTx_buf[4]*256 + RxTx_buf[5];

		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_RP, RX_CKS_ERROR);
		else
		{
			HW_ErrorCode = 0;
			OnGet_Flash_Data(&RxTx_buf[8], 21, ReadPageNum);
			Reply_to_MOpm_UART(FLASH_RP, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'V') && (Cmd_L == 'N'))	// Get Vendor Name From flash
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(GET_V_NAME, RX_CKS_ERROR);
		else
		{
			OnGet_Head_Info();
			Reply_to_MOpm_UART(GET_V_NAME, HW_ErrorCode);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_H == 'F') && (Cmd_L == 'S'))	// Read Flash Status: FDID
	{
		if (Data_Checksum != Rx_Checksum) Reply_to_MOpm_UART(FLASH_ID, RX_CKS_ERROR);
		else
		{
			OnGet_Flash_ID();
			FlashID[19] = OnGet_Flash_Status();
			Reply_to_MOpm_UART(FLASH_ID, HW_ErrorCode);
		}
	} //--------------------------------------------------------------------------------
	else Reply_to_MOpm_UART(TF_HELLO, RX_OK);

	//--------------------------------------------------------------------------------
}
//////////////////////////////////////////////////////////////////////////////////
void Reply_to_MOpm_UART(uint8_t CmdID, uint8_t ErrorCode)
{
	uint16_t Index, Length;

	Index = 0;
	Tx_Checksum = 0;
	UartData32 = 0;
	//------------------------------------------
	if (CmdID == SET_VOLTAGE)
	{
		UartData32 = (uint32_t)((float)(Pre_DAC_Value - DAC_OFFSET) / DAC_DATA_SCALE + 0.5f);

		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'V';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[1] = HW_ErrorCode;
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(UartData32);
		}
		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_RW)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		if (ErrorCode == RX_OK)
		{
		}
		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == SET_OFFSET)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_OF_1)
	{
		Offset32 = (uint32_t)((float)(Offset_DAC1_A - DAC_OFFSET) / DAC_DATA_SCALE + 0.5f);
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 0;
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Offset32);
		}
		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_OF_2)
	{
		Offset32 = (uint32_t)((float)(Offset_DAC1_C - DAC_OFFSET) / DAC_DATA_SCALE + 0.5f);
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 1;
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Offset32);
		}
		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_OF_3)
	{
		Offset32 = (uint32_t)((float)(Offset_DAC2_A - DAC_OFFSET) / DAC_DATA_SCALE + 0.5f);
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 2;
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Offset32);
		}
		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_OF_3)
	{
		Offset32 = (uint32_t)((float)(Offset_DAC2_C - DAC_OFFSET) / DAC_DATA_SCALE + 0.5f);
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 3;
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Offset32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Offset32);
		}
		RxTx_buf[5] = Index - 6;		// Number of bytes for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_BT)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = Tmp125 >> 8;
		RxTx_buf[Index ++] = Tmp125;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_V_NAME)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'V';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (uint8_t k=0; k < VENDOR_NAME_LENGTH; k ++)
		RxTx_buf[Index ++] = HeadInfo[k];

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == FLASH_ID)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (uint8_t k=0; k < 20; k ++)
		RxTx_buf[Index ++] = FlashID[k];

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == FLASH_WP)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == FLASH_RP)							// Upload flash page
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ReadPageNum >> 8;			// echo back "upload page number"
		RxTx_buf[Index ++] = ReadPageNum;

		if (ErrorCode == RX_OK) Index = 21 + 8;
		else
		{
			RxTx_buf[Index ++] = Data_Checksum;
			RxTx_buf[Index ++] = Rx_Checksum;
		}
		RxTx_buf[4] = (Index - 6) >> 8;
		RxTx_buf[5] = (Index - 6);
	}
	else if (CmdID == FLASH_EF)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == FLASH_DF)							// download flash page
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = FlashBlockID >> 8;			// echo back "download page number"
		RxTx_buf[Index ++] = FlashBlockID;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == FLASH_UF)							// Upload flash page
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'U';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = FlashBlockID >> 8;			// echo back "upload page number"
		RxTx_buf[Index ++] = FlashBlockID;

		if (ErrorCode == RX_OK) Index = 16*FLASH_PAGE_SIZE + 8;
		else
		{
			RxTx_buf[Index ++] = Data_Checksum;
			RxTx_buf[Index ++] = Rx_Checksum;
		}
		RxTx_buf[4] = (Index - 6) >> 8;
		RxTx_buf[5] = (Index - 6);
	}
	else if (CmdID == SET_OFFSET)		// Set Channel Offset
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[4] = (Index - 6) >> 8;
		RxTx_buf[5] = (Index - 6);
	}
/*
	else if (CmdID == SET_TARGET)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Ch_Num;
		RxTx_buf[Index ++] = SetCh_Dir;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (int32_t)Ch_Voltage >> 24;
			RxTx_buf[Index ++] = (int32_t)Ch_Voltage >> 16;
			RxTx_buf[Index ++] = (int32_t)Ch_Voltage >> 8;
			RxTx_buf[Index ++] = (int32_t)Ch_Voltage;

			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = Ch_Wavelength >> 16;
			RxTx_buf[Index ++] = Ch_Wavelength >> 8;
			RxTx_buf[Index ++] = Ch_Wavelength;
		}
		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_CHANNEL)					// "Get Channel" is the same as "Get Target"
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = Ch_Num;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Ch_Wavelength >> 16;
		RxTx_buf[Index ++] = Ch_Wavelength >> 8;
		RxTx_buf[Index ++] = Ch_Wavelength;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_TARGET)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = Ch_Num;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Ch_Wavelength >> 16;
		RxTx_buf[Index ++] = Ch_Wavelength >> 8;
		RxTx_buf[Index ++] = Ch_Wavelength;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_ILOSS)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'I';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = User_Ch_Num;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = UartData32 >> 16;
		RxTx_buf[Index ++] = UartData32 >> 8;
		RxTx_buf[Index ++] = UartData32;

		RxTx_buf[Index ++] = Ch_ILoss >> 8;
		RxTx_buf[Index ++] = Ch_ILoss;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_FBW)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = User_Ch_Num;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = UartData32 >> 16;
		RxTx_buf[Index ++] = UartData32 >> 8;
		RxTx_buf[Index ++] = UartData32;

		RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10+0.5) >> 8;
		RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10+0.5);

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_FLASH_VER)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (k=0; k < FLASH_VER_LENGTH; k ++)
			RxTx_buf[Index ++] = Flash_ver[k];

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_M_DATE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'M';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (k=0; k < M_DATE_LENGTH; k ++)
			RxTx_buf[Index ++] = Manufacture_date[k];

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_SN_PN)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (k=0; k < MODULE_SN_LENGTH; k ++)
			RxTx_buf[Index ++] = Module_SN[k];

		for (k=0; k < MODULE_PN_LENGTH; k ++)
			RxTx_buf[Index ++] = Module_PN[k];

		for (k=0; k < M_DATE_LENGTH; k ++)
			RxTx_buf[Index ++] = Manufacture_date[k];
		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_PN_NUM)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (k=0; k < MODULE_PN_LENGTH; k ++)
			RxTx_buf[Index ++] = Module_PN[k];

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_HW_ST)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'H';			// Hardware Status
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = HW_Failure_Reg >> 8;
		RxTx_buf[Index ++] = HW_Failure_Reg;

		RxTx_buf[Index ++] = InputVcc >> 8;
		RxTx_buf[Index ++] = InputVcc;

		RxTx_buf[5] = Index - 6;
	} */

/*	else if (CmdID == GET_ADC_XY)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'A';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ADC_XY_Dir0 >> 8;
		RxTx_buf[Index ++] = ADC_XY_Dir0;

		RxTx_buf[Index ++] = ADC_XY_Dir1 >> 8;
		RxTx_buf[Index ++] = ADC_XY_Dir1;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_ADC_CH)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'A';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ADC_data_Ch0 >> 8;
		RxTx_buf[Index ++] = ADC_data_Ch0;
		RxTx_buf[Index ++] = ADC_data_Ch1 >> 8;
		RxTx_buf[Index ++] = ADC_data_Ch1;

		RxTx_buf[Index ++] = (InputVcc >> 8);
		RxTx_buf[Index ++] = (InputVcc);
		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == SEND_SPI_DATA)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'K';
		RxTx_buf[Index ++] = 'K';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		for (k=0; k < User_SPI_data.SPI_Rx_Length; k ++)
		{
			RxTx_buf[Index ++] = User_SPI_data.SPI_Rxbuf[k] >> 8;
			RxTx_buf[Index ++] = User_SPI_data.SPI_Rxbuf[k];
		}
		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == SET_CAL_MODE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'A';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = CalMode;

		RxTx_buf[5] = Index - 6;
	}
	else if (CmdID == GET_T_TABLE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		for (k=0; k < TEMP_TABLE_SIZE; k++)
		{
			RxTx_buf[Index ++] = TTable[k] >> 8;
			RxTx_buf[Index ++] = TTable[k];
		}
		RxTx_buf[5] = Index - 6;
	}*/
	else if (CmdID == TF_HELLO)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'O';

		RxTx_buf[5] = Index - 6;
	}
	else
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD;
		RxTx_buf[Index ++] = 1;	//RX_UNKNOW_CMD;

		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
	}
	//-------------------------------------
	Length = Index;
	//---------------------------------------------------------------------
	for (Index = 1; Index < Length; Index ++) Tx_Checksum += RxTx_buf[Index];
	RxTx_buf[Length ++] = Tx_Checksum;

	//---------------------------------------------
	OnMOpm_UART_Tx(Length);
	//---------------------------------------------
}
//////////////////////////////////////////////////////////////////////////////////
void OnMOpm_UART_Tx(uint16_t Length)
{
	uint16_t Index;

	for (Index = 0; Index < Length; Index ++)
	{
		while (!(MOPM_UART->UART_SR & UART_SR_TXRDY));
		MOPM_UART->UART_THR = RxTx_buf[Index];
	}
}
//////////////////////////////////////////////////////////////////////////////////
void OnMOpm_UART_ScanData(uint16_t *pData, uint16_t DataLength, uint8_t CmdType)
{
	uint16_t Index, Length;

	Index = 0;
	RxTx_buf[Index ++] = SYS_UART_HEAD;
	RxTx_buf[Index ++] = 0;				// Error Code

	if (CmdType == RW_CMD)
	{
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'W';
	}
	else if (CmdType == NS_CMD)
	{
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 'S';
	}
	RxTx_buf[Index ++] = 0;
	RxTx_buf[Index ++] = 0;

	RxTx_buf[4] = (Index + (2*DataLength) - 6) >> 8;
	RxTx_buf[5] = (Index + (2*DataLength) - 6);
	//------------------------------------------------
	Length = Index;		Tx_Checksum = 0;
	for (Index = 0; Index < Length; Index ++)
	{
		while (!(MOPM_UART->UART_SR & UART_SR_TXRDY));
		MOPM_UART->UART_THR = RxTx_buf[Index];
		if (Index > 0) Tx_Checksum += RxTx_buf[Index];
	}
	for (Index = 0; Index < DataLength; Index ++)
	{
		while (!(MOPM_UART->UART_SR & UART_SR_TXRDY));
		MOPM_UART->UART_THR = pData[Index] >> 8;
		Tx_Checksum += (pData[Index] >> 8) & 0x00FF;

		while (!(MOPM_UART->UART_SR & UART_SR_TXRDY));
		MOPM_UART->UART_THR = pData[Index];
		Tx_Checksum += (pData[Index] & 0x00FF);
	}
	while (!(MOPM_UART->UART_SR & UART_SR_TXRDY));
	MOPM_UART->UART_THR = Tx_Checksum;
}
//////////////////////////////////////////////////////////////////////////////////
/*
void OnSend_RcTData(uint8_t* TxData, uint16_t DataLength)
{
	uint16_t Index, Length;

	Index = 0;
	Length = DataLength;

	TxData[Index ++] = SYS_UART_HEAD;
	TxData[Index ++] = 0;				// Error Code

	TxData[Index ++] = 'R';
	TxData[Index ++] = 'T';
	TxData[Index ++] = 0;
	TxData[Index ++] = 0;

	TxData[Index ++] = RcAveNum;
	TxData[Index ++] = 0;

	TxData[Index ++] = Length >> 8;
	TxData[Index ++] = Length;
	Index += Length;

	TxData[4] = (Index - 6) >> 8;
	TxData[5] = (Index - 6);
	//-------------------------------------
	Length = Index;
	//---------------------------------------------------------------------
	Tx_Checksum = 0;
	for (Index = 1; Index < Length; Index ++) Tx_Checksum += TxData[Index];

	TxData[Length ++] = Tx_Checksum;
	for (Index = 0; Index < Length; Index ++) usart_putchar(pSys_UART, TxData[Index]);
}
//////////////////////////////////////////////////////////////////////////////////
void OnSend_ScanData(uint8_t* TxData, uint16_t DataLength)
{
	uint16_t Length, Index;

	Index = 0;
	Tx_Checksum = 0;
	TxData[Index ++] = SYS_UART_HEAD;
	TxData[Index ++] = 0;

	TxData[Index ++] = 'S';
	TxData[Index ++] = 'D';
	TxData[Index ++] = 0;
	TxData[Index ++] = 0;

	TxData[Index ++] = ScanDataID;
	TxData[Index ++] = 0;
	TxData[Index ++] = DataLength >> 8;
	TxData[Index ++] = DataLength;

	Index += DataLength;
	TxData[4] = (Index - 6) >> 8;
	TxData[5] = (Index - 6);

	Length = Index;
	for (Index = 1; Index < Length; Index ++) Tx_Checksum += TxData[Index];

	TxData[Length ++] = Tx_Checksum;
	for (Index = 0; Index < Length; Index ++) usart_putchar(pSys_UART, TxData[Index]);
}*/
//////////////////////////////////////////////////////////////////////////////////