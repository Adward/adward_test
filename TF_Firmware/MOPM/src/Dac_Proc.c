///////////////////////////////////////////////////////////////////////////////
//
//	Functions for DAC and TMP125 control
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
///////////////////////////////////////////////////////////////////////////////
void OnRead_Temp(void)
{
	OnRead_TmpR();
	OnRead_Tmp125();
	
	if(refTmp125)
	{
		Tmp125X10 = simTmp;
		CurrentTmp = Tmp125X10;
	}
	else
		CurrentTmp = TmpRX10;
}
///////////////////////////////////////////////////////////////////////////////
void OnRead_Tmp125(void)
{
	uint8_t data[2], k;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_TMP125);
	OnSPI_Read(MOpm_SPI, data, 2);

	Tmp125 = (((int16_t)data[0] << 8) + data[1]) << 1;		// shift out "Leading-0" bit-15
	Tmp125 = (int16_t)((Tmp125 * TMP125_FACTOR)/2);			// convert to temperature X 10

	Tmp125_buf[Tmp125_Index ++] = Tmp125;

	if (Tmp125_ReadNum < TEMP_AVERAGE_N) Tmp125_ReadNum ++;
	if (Tmp125_Index >= TEMP_AVERAGE_N) Tmp125_Index = 0;

	//------------------------------------------------------------------
	if (Tmp125_ReadNum >= TEMP_AVERAGE_N)		// Do moving average
	{
		Tmp125X10 = 0;
		for (k=0; k < TEMP_AVERAGE_N; k++) Tmp125X10 += Tmp125_buf[k];
		Tmp125X10 /= TEMP_AVERAGE_N;				// TempX10 is in 0.1C
	}
	else Tmp125X10 = Tmp125;
}
///////////////////////////////////////////////////////////////////////////////
void OnRead_TmpR(void)
{
	uint16_t Data16;
	uint8_t k;
	
	Disable_global_interrupt();
	
	afec_channel_disable(AFEC0, AFEC_CHANNEL_0);	
	afec_channel_enable(AFEC0, AFEC_CHANNEL_1);
	afec_set_trigger(AFEC0, AFEC_TRIG_SW); //temporary switch to software trigger to acquire temperature data
		
	afec_start_software_conversion(AFEC0);
	while (!(afec_get_interrupt_status(AFEC0) & (1 << AFEC_CHANNEL_1)));	// wait until conversion is completed	
	Data16 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_1);
	thmist_ADC = Data16;	
					//	afec_start_software_conversion(AFEC0);
					//	while (!(afec_get_interrupt_status(AFEC0) & (1 << AFEC_CHANNEL_1)));	// wait until conversion is completed
					//	Data16 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_1);
		
	fData = (float32_t)(Data16*3)/16383.f;
	fData = 30.1f*fData/(4.096f-fData) - THE_R;
	fDataL = log10f(fData)/(0.4342944819f);
	fDataH = THE_A + THE_B*fDataL + THE_C*fDataL*fDataL*fDataL;
	TmpR = 10.f/fDataH - THE_ADJ;
			
	afec_channel_disable(AFEC0, AFEC_CHANNEL_1);
	afec_channel_enable(AFEC0, AFEC_CHANNEL_0);
	
	afec_start_software_conversion(AFEC0);
	while (!(afec_get_interrupt_status(AFEC0) & (1 << AFEC_CHANNEL_0)));	// wait until conversion is completed
	Data16 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_0);
	
					// afec_start_software_conversion(AFEC0);
					// while (!(afec_get_interrupt_status(AFEC0) & (1 << AFEC_CHANNEL_0)));	// wait until conversion is completed
					// Data16 = afec_channel_get_value(AFEC0, AFEC_CHANNEL_0);
	
					
					// Clears DRDY by reading AFEC_LCDR
					///afec_get_latest_value(AFEC0);

					// Clear status bit to acknowledge interrupt
					///afec_channel_get_value(AFEC0, AFEC_CHANNEL_0);
					///afec_get_interrupt_status(AFEC0);
					
	afec_set_trigger(AFEC0, AFEC_TRIG_TIO_CH_0); //restore trigger setting
	Enable_global_interrupt();
	
	//--------------------------------------------------
	TmpR_buf[TmpR_Index ++] = TmpR;

	if (TmpR_ReadNum < TEMP_AVERAGE_N) TmpR_ReadNum ++;
	if (TmpR_Index >= TEMP_AVERAGE_N) TmpR_Index = 0;

	//------------------------------------------------------------------
	if (TmpR_ReadNum >= TEMP_AVERAGE_N)		// Do moving average
	{
		TmpRX10 = 0;
		for (k=0; k < TEMP_AVERAGE_N; k++) TmpRX10 += TmpR_buf[k];
		TmpRX10 /= TEMP_AVERAGE_N;				// TempX10 is in 0.1C
	}
	else TmpRX10 = TmpR;
}
///////////////////////////////////////////////////////////////////////////////
void DAC_Output(uint16_t DAC_Value, uint8_t Dir)
{
		uint8_t DAC_data[3], DAC_Value_H, DAC_Value_L;

		//-------------------------------------------------
		DAC_Value_H = (DAC_Value) >> 8;
		DAC_Value_L = (DAC_Value);

		//----------------------------------------------
		if (Dir > 0)	// DAC Channel A control long-WL
		{
			//----------------------------------------------
			// Write DAC channel-C
			DAC_data[0] = DAC_WRITE_REGISTER_B;			// | ADDRESS_DAC_B;
			DAC_data[1] = (Offset_DAC) >> 8;
			DAC_data[2] = (Offset_DAC);
			OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DAC1);
			OnSPI_Write(MOpm_SPI, DAC_data, 3);		// Write to SPI port to DAC
			//----------------------------------------------
			// Write DAC channel-A and update ALL
			DAC_data[0] = DAC_WRITE_A_UPDATE_ALL;
			DAC_data[1] = DAC_Value_H;
			DAC_data[2] = DAC_Value_L;
			OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DAC1);
			OnSPI_Write(MOpm_SPI, DAC_data, 3);		// Write to SPI port to DAC
		}
		else	// DAC Channel C control short-WL
		{
			//----------------------------------------------
			// Write DAC channel-A
			DAC_data[0] = DAC_WRITE_REGISTER_A;			// | ADDRESS_DAC_A;
			DAC_data[1] = (Offset_DAC) >> 8;
			DAC_data[2] = (Offset_DAC);
			OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DAC1);
			OnSPI_Write(MOpm_SPI, DAC_data, 3);		// Write to SPI port to DAC
			//----------------------------------------------
			// Write DAC channel-C and update ALL
			DAC_data[0] = DAC_WRITE_B_UPDATE_ALL;
			DAC_data[1] = DAC_Value_H;
			DAC_data[2] = DAC_Value_L;
			OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DAC1);
			OnSPI_Write(MOpm_SPI, DAC_data, 3);		// Write to SPI port to DAC
		}
}
///////////////////////////////////////////////////////////////////////////////
void DAC1_Output(uint16_t DAC_Value, uint8_t MemsCH)
{
	uint8_t DAC_data[3];

	//-------------------------------------------------
	DAC_data[0] = MemsCH;
	DAC_data[1] = (uint8_t)(DAC_Value >> 8);
	DAC_data[2] = (uint8_t)(DAC_Value);
	Pre_DAC_Value = DAC_Value;
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DAC1);
	OnSPI_Write(MOpm_SPI, DAC_data, 3);

	if (MemsCH == MEMS_CH_A)
	{
		DAC_data[0] = MEMS_CH_C;
		DAC_data[1] = (uint8_t)(Offset_DAC >> 8);
		DAC_data[2] = (uint8_t)(Offset_DAC);
	}
	else
	{
		DAC_data[0] = MEMS_CH_A;
		DAC_data[1] = (uint8_t)(Offset_DAC >> 8);
		DAC_data[2] = (uint8_t)(Offset_DAC);
	}
	//-------------------------------------------------------------
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_DAC1);
	OnSPI_Write(MOpm_SPI, DAC_data, 3);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void OnSet_Voltage(int32_t DAC_Value, uint8_t Dir)	// SINE control with variable step size and delay
{
	int32_t deltaDAC, Set_DAC_0;					// Work with cross 0-V case
	
	// Use SINE shape control voltage raising and falling:
	if (Ch_Dir == Dir)								// for voltage goes UP, it can get very small oscillation
	{												// But, for voltage goes down, it still cause big oscillation
		if (DAC_Value > Pre_DAC_Value + V_STEP0)
		{
			Set_DAC_0 = 0;
			deltaDAC = (DAC_Value - Pre_DAC_Value);
			fData = (float32_t)deltaDAC / 1.571f;		// PI/2

			while (Set_DAC_0 < deltaDAC)
			{
				if (deltaDAC - Set_DAC_0 > 60*V_STEP0) Set_DAC_0 = Set_DAC_0 + 7*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 50*V_STEP0) Set_DAC_0 = Set_DAC_0 + 6*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 40*V_STEP0) Set_DAC_0 = Set_DAC_0 + 5*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 30*V_STEP0) Set_DAC_0 = Set_DAC_0 + 4*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 10*V_STEP0) Set_DAC_0 = Set_DAC_0 + 2*V_STEP0;
				else Set_DAC_0 = Set_DAC_0 + V_STEP0;
				
				if (Set_DAC_0 < deltaDAC) DAC_Set_Value = (uint16_t)(Pre_DAC_Value + deltaDAC * (float32_t)sinf((float32_t)(Set_DAC_0)/fData));
				else DAC_Set_Value = DAC_Value;

				if (DAC_Set_Value >= DAC_Value)
				{
					DAC_Output(DAC_Value, Dir);
					DAC_Set_Value = DAC_Value;
					break;
				}
				else DAC_Output(DAC_Set_Value, Dir);
			}
		} //------------------------------------------------------------------------------------
		else if (Pre_DAC_Value > DAC_Value + V_STEP0)
		{
			Set_DAC_0 = 0;
			deltaDAC = (Pre_DAC_Value - DAC_Value);
			fData = (float)deltaDAC / 1.571f;
			while (Set_DAC_0 < deltaDAC)
			{
				if (deltaDAC - Set_DAC_0 > 60*V_STEP0) Set_DAC_0 = Set_DAC_0 + 7*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 50*V_STEP0) Set_DAC_0 = Set_DAC_0 + 6*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 40*V_STEP0) Set_DAC_0 = Set_DAC_0 + 5*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 30*V_STEP0) Set_DAC_0 = Set_DAC_0 + 4*V_STEP0;
				else if (deltaDAC - Set_DAC_0 > 10*V_STEP0) Set_DAC_0 = Set_DAC_0 + 2*V_STEP0;
				else Set_DAC_0 = Set_DAC_0 + V_STEP0;

				if (Set_DAC_0 < deltaDAC) DAC_Set_Value = (uint16_t)(Pre_DAC_Value - deltaDAC * (float32_t)sinf((float32_t)(Set_DAC_0)/fData));
				else DAC_Set_Value = DAC_Value;

				if (DAC_Set_Value <= DAC_Value)
				{
					DAC_Output(DAC_Value, Dir);
					DAC_Set_Value = DAC_Value;
					break;
				}
				else DAC_Output(DAC_Set_Value, Dir);
			}
		}
		else
		{
			DAC_Output(DAC_Value, Dir);				   //Guo: less than V_STEP0	
			DAC_Set_Value = DAC_Value;
		}
	}	//--------------------------------------------- Different DIR case
	else
	{
		DAC_Set_Value = Pre_DAC_Value;						// Use EVEN-STEP for falling voltage
		deltaDAC = (Pre_DAC_Value - DAC_OFFSET);			// Using even step for voltage falling is better.

		while (DAC_Set_Value > DAC_OFFSET)
		{
			if (Pre_DAC_Value >= V_STEP0) DAC_Set_Value = Pre_DAC_Value - 2*V_STEP0;
			else DAC_Set_Value = DAC_OFFSET;

			if (DAC_Set_Value <= DAC_OFFSET)
			{
				DAC_Output(DAC_OFFSET, Ch_Dir);
				DAC_Set_Value = DAC_OFFSET;
				break;
			}
			else DAC_Output(DAC_Set_Value, Ch_Dir);
			Pre_DAC_Value = DAC_Set_Value;
		}
		//---------------------------------------------------------
		Set_DAC_0 = 0;
		deltaDAC = (DAC_Value - DAC_OFFSET);
		fData = (float)deltaDAC / 1.571f;

		while (Set_DAC_0 < deltaDAC)
		{
			if (deltaDAC - Set_DAC_0 > 60*V_STEP0) Set_DAC_0 = Set_DAC_0 + 7*V_STEP0;
			else if (deltaDAC - Set_DAC_0 > 50*V_STEP0) Set_DAC_0 = Set_DAC_0 + 6*V_STEP0;
			else if (deltaDAC - Set_DAC_0 > 40*V_STEP0) Set_DAC_0 = Set_DAC_0 + 5*V_STEP0;
			else if (deltaDAC - Set_DAC_0 > 30*V_STEP0) Set_DAC_0 = Set_DAC_0 + 4*V_STEP0;
			else if (deltaDAC - Set_DAC_0 > 10*V_STEP0) Set_DAC_0 = Set_DAC_0 + 3*V_STEP0;
			else Set_DAC_0 = Set_DAC_0 + 2*V_STEP0;

			if (Set_DAC_0 < deltaDAC) DAC_Set_Value = (uint16_t)(DAC_OFFSET + deltaDAC * (float32_t)sinf((float32_t)(Set_DAC_0)/fData));
			else DAC_Set_Value = DAC_Value;

			if (DAC_Set_Value >= DAC_Value)
			{
				DAC_Output(DAC_Value, Dir);
				DAC_Set_Value = DAC_Value;
				break;
			}
			else DAC_Output(DAC_Set_Value, Dir);
		}
	}
	//-----------------------------------------------------
	Pre_DAC_Value = DAC_Set_Value;
	Ch_Dir = Dir;
}//////////////////////////////////////////////////////////////////////////////////////////////////
