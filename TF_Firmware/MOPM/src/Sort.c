﻿#include "Sort.h"

void swap(int *a, int *b)
{
	int temp;
	
	temp = *a;
	*a = *b;
	*b = temp;
}

void swap_struct(Data_Type *a, Data_Type *b)
{
	Data_Type temp;
	
	temp.index = a->index;
	temp.data = a->data;
	temp.type_peak_valley = a->type_peak_valley;
	
	a->index = b->index;
	a->data = b->data;
	a->type_peak_valley = b->type_peak_valley;
	
	b->index = temp.index;
	b->data = temp.data;
	b->type_peak_valley = temp.type_peak_valley;
}

void BubbleSort(Data_Type A[], int n)
{
	int i, j, sort_finish;

	for (i=n-1; i>0; i--) {
		sort_finish = 1;

		for (j =0; j <=i-1; j++) {

			if ( A[j].index > A[j+1].index ) {
				swap_struct(&A[j], &A[j+1]);
				sort_finish = 0;
			}
		}
		
		if ( 1 == sort_finish )
		break;
	}

}


void sort(int *data, int left, int right)
{
	int pivot, i, j;

	if (left >= right) {
		return;
	}

	pivot = data[left];

	i = left+1;
	j = right;

	while (1)
	{
		while (i <= right)
		{
			if (data[i] > pivot) {
				break;
			}

			i = i+1;
		}

		while (j > left)
		{
			if (data[j] < pivot) {
				break;
			}

			j = j-1;
		}

		if (i > j) {
			break;
		}

		swap(&data[i], &data[j]);
	}

	swap(&data[left], &data[j]);

	sort(data, left, j-1);
	sort(data, j+1, right);
}


void sort_structure(Data_Type *data, int left, int right)
{
	int pivot, i, j;

	if (left >= right) {
		return;
	}

	pivot = data[left].index;

	i = left+1;
	j = right;

	while (1)
	{
		while (i <= right)
		{
			if (data[i].index > pivot) {
				break;
			}

			i = i+1;
		}

		while (j > left)
		{
			if (data[j].index < pivot) {
				break;
			}

			j = j-1;
		}

		if (i > j) {
			break;
		}

		swap_struct(&data[i], &data[j]);
	}

	swap(&data[left], &data[j]);

	sort_structure(data, left, j-1);
	sort_structure(data, j+1, right);
}


int reverse(int *array, unsigned int size)
{
	unsigned int i;
	int temp;
	
	if ( size <= 0 )
	return -1;
	
	for (i=0; i<size/2; i++) {
		temp = *(array+i);
		*(array+i) = *(array+size-1-i);
		*(array+size-1-i) = temp;
	}
	
	return 0;
}

