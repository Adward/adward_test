///////////////////////////////////////////////////////////////////////////////
//
//	MOpm_Head.h: define constant used in MOpm
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __MTF_HEAD_H__
#define __MTF_HEAD_H__

/////////////////////////////////////////////////////////////////////////////////////
#define FW_VER_BYTE_1		'a'				// Version Number: xx.xx.xx
#define FW_VER_BYTE_2		'2'				//
#define FW_VER_BYTE_3		'0'				// ------------------------
#define FW_VER_BYTE_4		'0'				//
#define FW_VER_BYTE_5		'6'				// Bug-fix number byte-H
#define FW_VER_BYTE_6		'0'				// Bug-fix number byte-L
///////////////////////////////////////////////////////////////////////////////
#define SYSTEM_CLOCK_HZ			300000000UL
#define SAMPLE_FREQ				24000			// O band: 96kHz;  C band 24kHz
#define SQUARE_65535			4294836225UL	// =65535*65535

///////////////////////////////////////////////////////////////////////////////
#define MTF_OK						0						//no error status
#define TRUE						1
#define FALSE						0
///////////////////////////////////////////////////////////////////////////////
#define MTF_UART					UART0
#define MTF_UART_ID					ID_UART0
#define MTF_UART_PARITY				UART_MR_PAR_NO
#define MTF_UART_BAUDRATE			115200
#define MTF_UART_IRQn				UART0_IRQn
#define MOpmUART_Handler			UART0_Handler
///////////////////////////////////////////////////////////////////////////////
#define MOpmTC0_Handler				TC0_Handler
#define MOpmDMA_Handler				XDMAC_Handler
#define XDMAC_SignalCH				0
#define XDMAC_NoiseCH				1
///////////////////////////////////////////////////////////////////////////////
//#define FLASH_PAGE_SIZE			256			// 256/page for M25P80 flash chip
#define FLASH_PAGE_SIZE			512			// 512/page for AT45DB161D flash chip

#define UART_BUF_SIZE			5018		// = 5012 + overhead bytes-1
#define SCAN_MAX_LENGTH			5000		// C+L: 16000; O: 25000; C:5000
#define MTF_BUF_SIZE			(SCAN_MAX_LENGTH + 2)
//#define ONE_K_BUF				1024

#define MAX_NUM_PEAKS			128
#define CAL_HEAD_SIZE			5
///////////////////////////////////////////////////////////////////////////////
#define SYS_UART_HEAD_55		0x55
#define SYS_UART_HEAD_AA		0xAA
#define SYS_UART_HEAD_03		0x03

#define PORT_RX_TIME_OUT		500				// 500 ms
#define FLASH_ACCESS_TIMEOUT	100				// 1000 ms
///////////////////////////////////////////////////////////////////////////////////////////////
#define USER_SPI_BUF_SIZE		150				// max. "parameter" number = 128 + overhead
#define SPI_16CMD_OVERHEAD		3				// 3 words (Cmd word + Length word + Cks word)
#define CMD_OVERHEAD			6				// 6 for: 1-byte head, 2-byte (Length), 2-bytes (Cmd), 1-bye (Checksum)
#define CMD_OVERHEAD2			9				// 9 for: 1-byte head, 2-byte (Length), 4-bytes (Cmd), 2-bye (Checksum)
#define CMD_OVERHEAD_M178		1				// 1 for: 1-byte head

#define MAX_SET_VOLTAGE			75000UL			// MAX V = 75000mv
#define MAX_WORD				65535

#define DAC_INC					10				// 12.5 mv
#define DAC_DATA_SCALE			(65536.f/MAX_SET_VOLTAGE)	// DAC scale factor
#define DAC_OFFSET				0				// DAC offset = 0V for PCBA Rev 2.0
#define DAC_OFFSET_CH_D			24000UL			// DAC offset = 1.5V for Channel-D

#define TMP125_FACTOR			0.078125f		// Tmp125: degree/bit = 0.25, Remove Last 5-bits dummy = 1/32, Make 0.1degree = 10
#define TEMP_AVERAGE_N			10				// Smooth temperature variation
#define TEMP_INITIAL			220
#define TEMP_UPDATE_TIME		10				// Update temperature every 10 second
#define ADJ_TEMP_RANGE			20				// Adjust channel set voltage if temperature change over 2 degree
#define WAIT_PERIOD				0				// Wait period after startup 60 second
#define MTF_MAX_TEMP			900				// 90 degree

#define ADC_CH_VCC_SCALE		0.2f			// 100*2*2.048/2048: 11-bits resolution (bit-12 is sign)
#define ON_CHIP_ADC_OFFSET0		100
#define ON_CHIP_ADC_OFFSET1		23.2f			// Offset = 100*2*0.116V
//////////////////////////////////////////////////////////////////////////
#define C_SPEED					299792458.f		// Light speed
#define RULER_50G_SPACE			50
#define RULER_100G_SPACE		100
#define RULER_LASER_CONTRAST	200
//////////////////////////////////////////////////////////////////////////
#define Freq_Offset				185000			//GHz
#define Freq_Offset_USER1		180000			//GHz
//////////////////////////////////////////////////////////////////////////
#define Delay_ms(mSecond)	cpu_delay_ms(mSecond, SYSTEM_CLOCK_HZ);
#define Delay_us(uSecond)	cpu_delay_us(uSecond, SYSTEM_CLOCK_HZ);
//////////////////////////////////////////////////////////////////////////
// define error code
#define RX_OK				0
#define RX_UNKNOW_CMD		1
#define DATA_OUT_RANGE		2
#define DATA_NOT_AVAILABLE	3

#define RX_PARA_ERROR		8
#define RX_CKS_ERROR		9
//--------------------------------------------------------------
#define FLASH_CHIP_ERROR		0x01
#define FLASH_ACCESS_ERROR		0x02
#define MTF_TEMP_TOO_HIGH		0x04
#define LOAD_CAL_DATA_FAILED	0x08

#define SPI_READ_TIMEOUT_ER		0x10
#define SPI_WRITE_TIMEOUT_ER	0x20
//////////////////////////////////////////////////////////////
#define READ_ADC_CH0			0x8000
#define READ_ADC_CH1			0xC000
//////////////////////////////////////////////////////////////
#define UART_PORT_CMD		1
#define USB_PORT_CMD		2
//////////////////////////////////////////////////////////////
//#define VENDOR_NAME_LENGTH	32

#define SET_VOLTAGE			1
#define SET_OFFSET			2
#define GET_OFFSET			3

#define GET_RW_SIGNAL		4
#define GET_RW_NOISE		5
#define GET_EQS_SIGNAL		6
#define GET_EQS_NOISE		7

#define GET_ETABLE			8
#define SET_ETABLE			9
#define GET_CAL_TABLE		10
#define SET_CAL_TABLE		11
#define SET_FERC_TABLE		12
#define SET_PERC_TABLE		13
#define GET_PTABLE				17
#define SET_PTABLE				18

#define START_FREQ_POW_CAL	14
#define START_FREQ_CAL		15
#define START_CH_TEST		16

#define MEASURE_SIGNAL_FWF	19
#define SET_SCANPOS			20

#define GET_TEMP			21
#define SET_CH				22
#define SET_WL				23
#define SET_FREQ			24
#define GET_CH				25
#define GET_WL				26
#define GET_FREQ			27

#define RESET_MTF			30
#define STOP_MTF			31
#define FLASH_DL			32
#define FLASH_UP			33

#define GET_FW_VER			40
#define GET_FLASH_VER		41
#define GET_M_DATE			42
#define GET_SN_PN			43
#define GET_SN_FV			44

#define GET_BW_WL			50
#define GET_BW_FR			51
#define GET_BW_CH			52
#define GET_BW_TB			53
#define GET_IL_WL			54
#define GET_IL_FR			55
#define GET_IL_CH			56
#define GET_IL_TB			57
#define GET_VTABLE			58

#define SET_WL_UP			60
#define SET_CH_UP			61
#define SET_WL_DN			62
#define SET_CH_DN			63
#define SCAN_WL				64

#define GET_PK_EQS0			70

#define SET_CAL_MODE		152
#define GET_TTABLE			154
#define GET_M_INFO			155

#define SET_WienerSNR		162

// USER1(Oclaro) ----------------------------------------------------------
#define	No_ERROR			0x00000000

#define SCAN_Cmd1			201
#define SCAN_Cmd8			208
#define SCAN_Cmd9			209
#define SCAN_CmdF			215
#define Oclaro_Cmd10		0x00000010  //210		// FW Download
#define Oclaro_Cmd11		0x00000011  //211		// FW Download
#define Oclaro_Cmd12		0x00000012  //212		// FW Download
#define Oclaro_Cmd30		0x00000030	//230		// Version Request
#define Oclaro_Cmd40		0x00000040	//240		// Device Reset

// DEMO(ADC Raw for Narrow Band demo) -------------------------------------
#define DEMO_Cmd			801

//-------------------------------------------------------------------------
#define LOG_Cmd				901
#define Boot_Cmd			999

#define UNKNOW_CMD			1000

/////////////////////////////////////////////////////////////////////////////////////
/************************************************************************/
/* Module Type															*/
/************************************************************************/
#define ModuleType_M178		100
#define ModuleType_NarrowBW	200
/************************************************************************/
/* Signal Power calculation window										*/
/************************************************************************/
#define BW_3dB				3
#define BW_5dB				5
/************************************************************************/
/* Signature Data		                                                */
/************************************************************************/
#define SECTOR_SIZE 0x20000			// 128KB/sector

#define PROG_A_ADDRESS 0x00420000u	// App#1 address
#define PROG_B_ADDRESS 0x00460000u	// App#2 address
#define TEMP_C_ADDRESS 0x004A0000u	// for firmware upgrade receiving buffer
#define VerA	0xAA
#define VerB	0xBB
#define VerBoot	0xFF

typedef struct {
	char alphabet;
	uint8_t	number;
	uint8_t	YYH;
	uint8_t	YYL;
	uint8_t	MM;
	uint8_t	DD;
	uint8_t	s1;
	uint8_t	s2;
	uint8_t	s3;
	uint8_t	s4;
}versionInfo_t;

typedef struct {
	uint8_t			ackCmdRST;
	uint8_t			Loaded_Ver;
	uint8_t			Requested_Ver;
	versionInfo_t	VerA_Info;
	versionInfo_t	VerB_Info;
}signatureInfo_t;

/////////////////////////////////////////////////////////////////////////////////////
// enum constants
enum {NORMAL_CMD, RW_UL00_CMD, RW_UL80_CMD, EN_CMD, CA_UL00_CMD, CA_UL80_CMD, EQS_UL00_CMD, EQS_UL80_CMD, 
		RW_SIGNAL_CMD, RW_NOISE_CMD, EQS_SIGNAL_CMD, EQS_NOISE_CMD,	CA_SIGNAL_CMD, CA_NOISE_CMD, MTF_SCAN_CMD,
		EN_POS_CMD, FREQ_CAL_CMD, POWER_CAL_CMD, FREQ_POW_CAL_CMD, SIGNAL_FILTER_WF_CMD, NOISE_FILTER_WF_CMD,
		ANALYZE_SIGNAL_CMD,
		USER1_SCAN_Cmd8, USER1_SCAN_Cmd9,
		MOPM_SCAN_CMD,
		LOG_ADC_CMD, LOG_DECONV_CMD, LOG_ResampleWL_CMD, LOG_ResampleP_CMD,
		M178_SCAN_Signal, M178_SCAN_SignalNoise,
		NB_DEMO_rawADC,
		USER1_SCAN_Cmd1, USER1_SCAN_CmdF
		};

enum {MTF_SCAN_MODE_0, MTF_SCAN_MODE_1, MTF_SCAN_MODE_2, MTF_SCAN_MODE_3, MTF_SCAN_MODE_4, MTF_SCAN_MODE_5, MOPM_SCAN};
enum {TEMP_PCB, TEMP_OPT};
enum {UL00_CMD, UL80_CMD};
	
// _OLD means no deconvolution process
enum {RUN_NORMAL, MTF_SET_V_CAL, FREQ_CAL_LASER_ONLY, FREQ_CAL_RULER_REF_INDEX, FREQ_CAL_RULER_REF_LASER, 
	  FREQ_POW_CAL_LASER, FP_CAL_Ch0_LOW_POWER, MTF_CH_TEST, MTF_SCAN, MOPM_RW_SCAN, MTF_RUN_END, 
	  FREQ_POW_CAL_LASER_OLD, MOPM_PSF_GEN, 
	  FP_CAL_Ch0_LOW_POWER_OLD};
		
enum {Wait_Valley, Compare_Valley, Wait_Peak, Compare_Peak};
	
enum {OptoplexCmd=2, User1Cmd=3, M178Cmd=4, EchoCmd=9};
/////////////////////////////////////////////////////////////////////////////////////
#endif // __MTF_HEAD_H__
