///////////////////////////////////////////////////////////////////////////////
//
//	Uart_Proc_M178.h: define constant used in MOpm
//
///////////////////////////////////////////////////////////////////////////////
#ifndef __Uart_Proc_M178_H__
#define __Uart_Proc_M178_H__

///////////////////////////////////////////////////////////////////////////////
#define M178_SUPPLY_ID_1		'O'				//Supply
#define M178_SUPPLY_ID_2		'P'
#define M178_SUPPLY_ID_3		'T'
#define M178_SUPPLY_ID_4		'O'

#define M178_HW_VER_M1			'H'				//HW Ver oo.oo
#define M178_HW_VER_M2			'2'
#define M178_HW_VER_DOT			'.'
#define	M178_HW_VER_D1			'0'
#define M178_HW_VER_D2			'0'

#define M178_FW_VER_M1			'6'				//FW Ver oo.oo		
#define M178_FW_VER_M2			'0'				
#define M178_FW_VER_DOT			'.'				
#define M178_FW_VER_D1			'0'				
#define M178_FW_VER_D2			'A'			
///////////////////////////////////////////////////////////////////////////////
#define M178_SN_Y_1				'1'				// Year
#define M178_SN_Y_2				'8'				//
#define M178_SN_S_3				'O'				// 'OP' FOR Optoplex
#define M178_SN_S_4				'P'				//
#define M178_SN_M_5				'3'				// Month: 1~9, O, N, D
#define M178_SN_C_6				'Z'				// PART ID : A~Z, on PCN#???
#define M178_SN_V_7				'H'				// FW Ver: A~Z, on PCN#???
#define M178_SN_T_8				'P'				// TYPE: 'P' for OCM
#define M178_SN_1_9				'8'				// Device SN
#define M178_SN_2_A				'8'				//
#define M178_SN_3_B				'8'				// 
#define M178_SN_4_C				'8'				// 
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// M178 Command Set format: MsgID_SubCmd
//-----------------------------------------------------------------------------
#define ID_ScanSignal			0x00000003
//-----------------------------------------------------------------------------
#define ID_ScanSignalNoise		0x00000005
//-----------------------------------------------------------------------------
#define ID_FwDownLoadInit		0x00000010
#define ID_FwLoad				0x00000011
#define ID_FwInstall			0x00000012
#define ID_FwVerRequest			0x00000030
#define TxMode					0x00000001
#define DataBlockSize			1024
//-----------------------------------------------------------------------------
#define SubCmd_Reserved			0x00000000
#define SubCmd_01				0x00000001
#define SubCmd_02				0x00000002
//-----------------------------------------------------------------------------
#define ID_DeviceReset			0x00000040
#define ID_DeviceTempRequest	0x00000050
#define ID_DeviceStatusCheck	0x00000060
//-----------------------------------------------------------------------------
#define	MsgID_UNKNOWN			0x77777777
#define SubCmd_UNKNOWN			0x88888888
//-----------------------------------------------------------------------------
// Error Codes
#define M178Cmd_OK				0x00000000
#define M178Cmd_CKS_ERROR		0x000027A3
#define M178Cmd_MsgLength_Error	0x000027A4
#define M178Cmd_SegLength_Error	0x000027A5
#define M178Cmd_Flash_Error		0x0000277D
#define	M178Cmd_Unknown_ERROR	0x00002783

#define M178Cmd_FlashSel_ERROR		0xFFFFFFF3
#define M178Cmd_FlashRead_ERROR		0xFFFFFFF5
#define M178Cmd_RxFile_CRC_Error	0xFFFFFFF6
#define M178Cmd_FlashInstall_ERROR	0xFFFFFFF7
//-----------------------------------------------------------------------------
// Current OCM Status
#define	M178_Status_Normal		0x00000000
#define	M178_Status_Error		0x000000A5
//-----------------------------------------------------------------------------
// Current OCM Status
#define	ReservedID				0x00000000

// M178 Functions
void Reply_to_UART_M178(uint32_t Cmd_ID, uint32_t Payload1, uint32_t Cmd_Sub, uint32_t ErrorCode);
void OnMOPM_UART_TxData_M178(uint32_t Cmd_ID, uint32_t ErrorCode, uint8_t NumOfReportCH);
uint8_t onMOPM_FwUp_Init(void);
uint8_t onMOPM_FwUp_Load(uint32_t HostSeqNo, uint32_t dataSize, uint8_t *frame);
uint32_t onMOPM_FwUP_Install(void);
void OnMOPM_NoiseDataGen_M178(uint16_t* rawADC, uint16_t* pusdoDeconv);
void OnMOPM_SignalPowerCheck_M178(float_t* checkSignal);
//-----------------------------------------------------------------------------

// M178 Variables
#define	M178_FreqOff	180000
uint32_t	seqNO;
//static uint16_t	crc;
static uint8_t *ptr;
// static int32_t ii;

// #define simADC
#define simTmp 354	// 22.2C, temperature unit is 0.1C 
///////////////////////////////////////////////////////////////////////////////
#endif // __Uart_Proc_T154_H__
