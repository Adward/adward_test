///////////////////////////////////////////////////////////////////////
// Flash_Proc.h
//
///////////////////////////////////////////////////////////////////////
#ifndef __FLASH_H__
#define __FLASH_H__

#include "Ch_Proc.h"
///////////////////////////////////////////////////////////////////////
// For flash chip M25P80
#define FLASH_WREN		0x06
#define FLASH_WRDI		0x04
#define FLASH_RDSR		0x05
#define FLASH_RDID		0x9F
#define FLASH_WRSR		0x01
#define FLASH_READ		0x03
#define FLASH_FREAD		0x0B
#define FLASH_PP		0x02
#define FLASH_SE		0xD8
#define FLASH_BE		0xC7

///////////////////////////////////////////////////////////////////////

void OnFlash_WritePage(uint8_t* Data, uint16_t Length, uint16_t PageID);
void OnGet_Flash_Head(void);

void OnGet_Flash_Data(uint8_t* Data, uint16_t Length, uint16_t PageID);
void OnGet_Flash_ID(void);

void OnSet_Flash_WREN(void);
void OnSet_Flash_WRDI(void);
void OnSet_Flash_SE(uint8_t SectorID);
void OnSet_Flash_SE_0(void);

void OnFlash_Initial(void);
void OnSet_Test_Flash_Write(uint16_t PageNum);
char OnGet_Flash_Status(void);
char Is_Flash_Ready(void);
void OnDpram_Write_data(void);
void OnDpram_Read_data(void);
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// For flash chip AT45DB161D
#define FLASH_BUFFER_W			0x84
#define FLASH_PAGE_W			0x83		//0x83: Write with build-in page erase, 0x88: Write NO erase
#define FLASH_BUFFER_R			0xD1
#define FLASH_PAGE_R			0x53
#define FLASH_PAGE_ER			0x81
#define FLASH_STATUS_R			0xD7
#define FLASH_MEMORY_R			0xD2		// Direct read from Main memory

///////////////////////////////////////////////////////////////////////
void OnInitialFlash(void);
void OnSPI_Release_CS(void);
void OnFlash_Wait_done(void);
void OnFlash_Erase_Page_N(uint16_t PageNum);
void OnFlash_Write_Page_N(uint16_t PageNum);
void OnFlash_Read_Page_N(uint16_t PageNum);
void OnFlash_Read_Memory(uint8_t* Data, uint16_t Length, uint16_t PageNum, uint16_t Offset);

void OnFlash_Read_Buffer(uint8_t* Data, uint16_t Length, uint16_t Offset);
void OnFlash_Write_Buffer(uint8_t* Data, uint16_t Length, uint16_t Offset);
void OnSPI_Write_Flash_Block(uint8_t* Data, uint16_t BlockID);
void OnSPI_Read_Flash_Block(uint8_t* Data, uint16_t BlockID);
uint8_t OnFlash_Read_Status(void);
void OnGet_Flash_MID(void);

//-----------------------------------------------------------------------------
uint8_t VendorName[8], Module_SN[24], HW_ver[6], Module_PN[24];
uint8_t Customer_SN[24], Customer_PN[30], Oracle_Num[24], Wid_Num[30];
uint8_t Manufacture_date[12], Flash_ver[2], FlashID[20], MID[5];
uint16_t Ch_ILoss, Ch_BW;
//-----------------------------------------------------------------------------
uint32_t ITU_WL_Table[ITU_WL_TABLE_SIZE];
int32_t ChV_Table[NUM_OF_TEMP][CH_V_TABLE_SIZE];
int16_t IL_Table[NUM_OF_TEMP][IL_TABLE_SIZE], BW_Table[NUM_OF_TEMP][BW_TABLE_SIZE], TTable[NUM_OF_TEMP];
int16_t ChP_Table[NUM_OF_TEMP][CH_V_TABLE_SIZE];
int16_t ChP_PMeter[NUM_OF_TEMP][CH_V_TABLE_SIZE];
/////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif //__FLASH_H__