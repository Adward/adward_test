/**
 * \file
 *
 * \brief User board configuration template
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef CONF_BOARD_H
#define CONF_BOARD_H

#define CONSOLE_UART             UART0

/** Usart Hw ID used by the console (UART0). */
#define CONSOLE_UART_ID          ID_UART0

/* Configure UART pins */
#define CONF_BOARD_UART_CONSOLE

/** Spi Hw ID . */
#define SPI_ID          ID_SPI0

/** SPI base address for SPI master mode*/
#define SPI_MASTER_BASE      SPI0
/** SPI base address for SPI slave mode, (on different board) */
#define SPI_SLAVE_BASE       SPI0

/* Configure ADC example pins */
//#define CONF_BOARD_ADC

/* Configure SPI pins */
#define CONF_BOARD_SPI
#define CONF_BOARD_SPI_NPCS0
//#define CONF_BOARD_SPI_NPCS1
//#define CONF_BOARD_SPI_NPCS2
//#define CONF_BOARD_SPI_NPCS3

/* Configure USART RXD pin */
//#define CONF_BOARD_USART_RXD

/* Configure USART TXD pin */
//#define CONF_BOARD_USART_TXD

/* Configure USART CTS pin */
//#define CONF_BOARD_USART_CTS

/* Configure USART RTS pin */
//#define CONF_BOARD_USART_RTS

/* Configure USART synchronous communication SCK pin */
//#define CONF_BOARD_USART_SCK

/* Configure Backlight control pin */
//#define CONF_BOARD_AAT3155

/* Configure Touchscreen SPI pins */
//#define CONF_BOARD_ADS7843

#endif // CONF_BOARD_H
