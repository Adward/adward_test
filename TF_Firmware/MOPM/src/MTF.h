///////////////////////////////////////////////////////////////////////////////
//
//	MTF.h
///////////////////////////////////////////////////////////////////////////////
#ifndef __MTF_H__
#define __MTF_H__

#ifndef	__GNUC__
#define __GNUC__
#endif

#ifdef __cplusplus
extern "C" {
#endif
///////////////////////////////////////////////////////////////
#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "arm_math.h"
#include "math.h"
#include "fastmath.h"
///////////////////////////////////////////////////////////////
#include "MTF_Head.h"
#include "Uart_Cmd.h"
#include "Flash_Proc.h"
#include "Ch_Proc.h"
#include "Dac_Proc.h"
#include "SPI_Proc.h"
#include "Uart_Proc_M178.h"
///////////////////////////////////////////////////////////////
// Initialization functions
void OnMTF_PCBA_Initial(void);
void OnMTF_Data_Initial(void);
void OnMTF_Uart_Initial(void);
void OnMTF_Set_Parameters(void);
void OnMTF_CheckTemperature(uint32_t CurrentT);

void OnMTF_TC_Intial(void);
void OnMTF_XDMAC_Initial(Afec *const afec, uint16_t DataLength);
void OnMTF_ADC_Initial(void);
void OnMTF_TurnPower_ON_OFF(uint8_t PowerOn);
//////////////////////////////////////////////////////////////
// Mems scan and control functions
void OnMTF_ProcessUserCmd(void);
void OnMTF_SetEqWL_Table(void);
void OnMTF_SetEqS_Table(void);
void OnMTF_SetEqS_Table_Cal(void);
void OnMTF_MEMS_Control(void);
void OnMTF_Scan_Finish(void);
void OnMTF_StartScan(void);
void OnMTF_ScanNext(void);

void OnMTF_SetScanPos(uint16_t StepIndex);
void OnMTF_SetScanPos2(uint16_t StepIndex);
void OnMTF_SetScanPos3(uint16_t StepIndex);
void OnMTF_SetScanPos_EqWL(uint16_t StepIndex);
//////////////////////////////////////////////////////////////
// MOSA Deconvolution functions
int OnMOSA_Deconvolution(uint16_t* pEqs_Data, uint16_t* pEqs_DeconvolutedData, uint16_t pEqs_Data_len, uint16_t pEqs_DeconvolutedData_len);
//////////////////////////////////////////////////////////////
// OPM calibration functions
void OnMTF_Create_PTable(void);
void OnMTF_GetRef_LaserIndex(void);
void OnMTF_FreqCalibration_Ruler(void);
void OnMTF_FreqCalibration_Laser(void);
void OnMTF_Freq_Power_Calibration(void);
void OnMTF_GetRulerPeakFreq_byRefIndex(void);
void OnMTF_Normalize_PTable(int32_t* pLaser_Power, int32_t* pCh_ADC, uint8_t Ch_Num);
void OnMTF_Creat_ETable(void);
void OnMTF_P_Dummy_Data(void);

void OnMTF_GetPeaknValley(uint16_t *pData, uint16_t DataLength);
void OnMTF_GetMultiplePeakCenters(uint16_t* pData);
void OnMTF_GetFilterWaveform(uint8_t* pWF, uint16_t* pEqs_Data);
void OnMTF_GetSingleLaserPeakScanPos(uint16_t* pData, uint16_t DataLength, uint8_t PeakIndex);
float OnMTF_Single_PKC(uint16_t Length, uint16_t StartIndex, uint16_t PeakIndex);

void OnMTF_Sidelobe_Filter(void);
void OnMTF_Analyse_Signal(uint16_t* pEqsADC, uint16_t DataLength);
void OnMOPM_ReportPW(float32_t BW_threshold);
void OnMOPM_GetBW(uint8_t CH_Length, uint16_t* pEqs_Data);
void OnMTF_GetOSNR(uint8_t CH_Length, uint16_t* pEqs_Data);
float32_t OnMTF_GetAvgP(uint8_t CH_Length, uint16_t* pEqs_Data);

// Channel Test functions
void OnMTF_Channel_Test(void);
void OnMTF_GetSinglePeak_Freq(uint16_t* pData, uint8_t Index);
////////////////////////////////////////////////////////////////////////////////////////////
void OnMTF_Convert_RawData2EQS(uint16_t* pData, uint16_t* pEqs);
void OnMTF_Convert_EQS_Adc2dB(uint16_t* pEqs);
void OnMTF_Get_EqsFreqPos(void);
uint8_t OnMTF_GetScanStartFreq_Pos(void);
//////////////////////////////////////////////////////////////
// USB port
void OnOpen_USB_Port(void);
void OnClose_USB_Port(void);
//////////////////////////////////////////////////////////////
void OnMTF_Log_Info(uint8_t InfoID);

//////////////////////////////////////////////////////////////
// USER1 functions
void OnMTF_UART_Decode_USER1(void);
void Reply_to_UART_USER1(uint32_t MsgCmd32, uint32_t ErrorCode32);
void OnMTF_UART_TxData_user1(uint8_t CmdType, uint32_t ErrorCode);
void OnMTF_Convert_RawData2EQS_User1ScanCmd9(uint16_t* pData, uint16_t* pEqs, uint8_t DecimationN);
uint8_t onMOPM_FwUp_Load_oclaro(uint32_t HostSeqNo, uint32_t dataSize, uint8_t *frame);
uint16_t Freq_Begin, Freq_End, rawMaxADC;
uint32_t User1_totalP, decimateN, decimateNplusONE;
uint32_t FwUg_SeqNo;
//////////////////////////////////////////////////////////////
// M178 functions
void OnMTF_UART_Decode_M178(void);
//////////////////////////////////////////////////////////////
// for M7 functions
//void OnMTF_TC0(void);	// for 100-pin evaluation version only
void OnMTF_DMA(void);
//////////////////////////////////////////////////////////////
// for 5T Power Table functions
void OnLoadPower5T_Table(void);
void OnMOPM_RenewPower_Table(void);
//////////////////////////////////////////////////////////////
// for FW upgrade 
void onMTF_SaveSignature(uint8_t saveSignature, signatureInfo_t *signature);
//////////////////////////////////////////////////////////////////////////////
// Global Variables
//////////////////////////////////////////////////////////////////////////////
volatile uint8_t MOpm_UART_Ready, ScanData_Ready, HW_ErrorCode;
volatile uint8_t Rx_byte, Rx_head, Uart_CmdID, UL_CmdID, ScanCount;
volatile uint8_t Rx_Cmd_Type, MOpm_ScanDone;

//////////////////////////////////////////////////////////////////////////////
uint8_t	 T_SensorID, Slot_ID;
uint8_t	 Ch_Num, User_Ch_Num, Total_ChNum, Total_ChNum_Plus, Flash_Initial_Ok;
uint8_t  ScanStartCh, ScanEndCh, ScanMode, ScanSpeed, ScanDir, TxScandata;
uint8_t  Module_Type, forceModuleType;

uint16_t Rx_Checksum16, Tx_Checksum16, Data_Checksum16, HW_Failure_Reg;
uint16_t Rx_Index, FlashBlockID, UartData16, ReadPageNum;
// uint16_t RawDataLength, EqsDataLength, X1, Y1, X2, Y2;
uint16_t RawDataLength, EqsDataLength;
uint32_t Rx_Length, Rx_Checksum32, Data_Checksum32;

uint8_t RxTx_buf[UART_BUF_SIZE];
///////////////////////////////////////////////////////////////////////////////
volatile uint8_t MTF_Mode, Ref_LaserIndex, NumOfFreqCal, NumOfPeaks, NumOfValleys;
volatile uint8_t ETable_Ready, PTable_Ready;

uint16_t *pSinglePk_Pow, *pFilterWaveform, *PeakCenterFreq;
uint16_t MaxPeakLevel, MinPeakLevel, Contrast, Peak3dB_ADC;
uint16_t PeakValue[MAX_NUM_PEAKS], PeakPos[MAX_NUM_PEAKS], ValleyValue[MAX_NUM_PEAKS+1], ValleyPos[MAX_NUM_PEAKS+1];
uint16_t Ch_SignalPower[MAX_NUM_PEAKS], Ch_NiosePower[MAX_NUM_PEAKS];
float    Ch_Freq[MAX_NUM_PEAKS], Ch_FreqERC[MAX_NUM_PEAKS];

//-----------------------------------------------------------------------------
uint32_t Ref_LaserFreq, Ch_Wavelength, Ch0_WL, ChN_WL, Ch0_Freq, ChN_Freq;
uint32_t Set_Freq, Set_WL, ScanStartWL, ScanEndWL;
int32_t  Pre_DAC_Value, DAC_Set_Value, SetCh_DAC;
//-----------------------------------------------------------------------------
uint8_t  Index_TL, Index_TH, SetCh_Dir, Ch_Dir;
uint16_t Target_PosL, Target_PosH, PageNum, Offset, V_STEP0, Offset_DAC;
int32_t  Data32, Data32L, Data32H, WL_L, WL_H;
float    Ch_VL, Ch_VH, Ch_Voltage, SetCh_Voltage;
float	 fDataL, fDataH, fData;
uint16_t thmist_ADC;

//-----------------------------------------------------------------------------
uint16_t WF_Length, WF_PeakIndex;
uint32_t PeakCenterPos[MAX_NUM_PEAKS], PowCal_PeakLevel;	// use 32-bits to hold multiple measurement before average
float32_t PowCal_ratioP, ratioP;

//-----------------------------------------------------------------------------
volatile uint16_t Ch_EN_1, Ch_EN_2, ScanStartFreqPos, EqsF, EqsF_Pos;
volatile uint8_t  Ch_Index, StartPosIndex, EQS_Space, EQWL_deltaWL;
volatile int16_t delta_Freq, Ruler_Space;
volatile uint32_t Scan_Pos_Offset, Scan_Pos_Resolution;
//-----------------------------------------------------------------------------
uint16_t PSF_ADC[PSF_TABLE_SIZE];
float32_t data_psf[PSF_TABLE_SIZE];
uint16_t PTable_head[CAL_HEAD_SIZE], Laser_ChPower[MAX_NUM_PEAKS], Laser_Ch0Power[CAL_HEAD_SIZE];
uint16_t PTable_ADC[MAX_NUM_PEAKS][CAL_HEAD_SIZE];
uint16_t ETable_Freq[MAX_NUM_PEAKS], ETable_Pos[MAX_NUM_PEAKS];
uint16_t ScanStartFreq, ScanStopFreq;
uint8_t  ETableLength, ERC_TableLength, Ch_Space, FERC_Table_Ready;

//-----------------------------------------------------------------------------
uint32_t Sys_ms_Ticks, Sys_Elapsed_time, UartData32;
//-----------------------------------------------------------------------------
uint8_t NoiseFilterInstalled;
uint16_t ScanEndPos, ScanStartPos, ScanPosIndex, ScanDAC, ScanPosLength;
uint16_t SignalCh_buf[MTF_BUF_SIZE], NoiseCh_buf[MTF_BUF_SIZE];
uint16_t SignalCh_Eqs[MTF_BUF_SIZE];//, NoiseCh_Eqs[MTF_BUF_SIZE];

uint16_t EqWL_ScanPos[MTF_BUF_SIZE];
uint16_t MultiPeaks_Eqs[MAX_NUM_PEAKS], PeakOSNR[MAX_NUM_PEAKS];
//uint8_t  MultiPeaks_1BW[MAX_NUM_PEAKS], MultiPeaks_3BW[MAX_NUM_PEAKS], MultiPeaks_5BW[MAX_NUM_PEAKS];
float32_t	MultiPeaks_Norm[MAX_NUM_PEAKS];
uint16_t ReportPW[MAX_NUM_PEAKS];
float32_t fReportSignal[MAX_NUM_PEAKS];
float32_t fReportNoise[80];
uint16_t   ReportWL[80];
/** XDMAC channel configuration. */
xdmac_channel_config_t xdmac_SignalCh_cfg;
//-----------------------------------------------------------------------------
uint16_t	Sidelobe_alpha;
uint16_t	Sidelobe_lmt;
//-----------------------------------------------------------------------------
// static uint16_t WSNR;	//for Wiener Filter real-time control test only
//-----------------------------------------------------------------------------
uint8_t saveSignature;
signatureInfo_t signature[1];
uint8_t refTmp125;
//-----------------------------------------------------------------------------
union IEEE32
{
	uint8_t StrValue[4];	// High-byte first and Low-byte last
	int32_t Int32;
	float Float32;
} Ieee32;
//-----------------------------------------------------------------------------
uint8_t sWindow;			// Signal power calculation window
uint8_t calWindow;			// calibration window for deconvoluted signal

/* Side-lobe Suppression Parameters. */
// int SIDE_LOBE_SUPPRESSION_NARROW_BAND;
float adc_filter_level_db;
int EDGE_AND_ADC_PEAK_RANGE;
int PEAK_DECONVOLUTION_STEP;
int LEFT_EDGE_STEP;
int RIGHT_EDGE_STEP;
int LEFT_EDGE_BEGIN;
int RIGHT_EDGE_BEGIN;

//-----------------------------------------------------------------------------
volatile uint8_t updateT;	// No ADC conversion (Thermistor) when Scan to prevent glitch phenomena
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#endif // __MTF_H__
