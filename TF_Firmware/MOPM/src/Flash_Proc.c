///////////////////////////////////////////////////////////////////////
//	Flash_Proc.c : Read and Write data from/to a SPI flash
//
//	08/24/2011
///////////////////////////////////////////////////////////////////////
#include "MTF.h"
#include "Flash_Proc.h"
#include <math.h>

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
void OnFlash_Initial(void)
{
	uint32_t Pre_Time, CurrentTime;
	uint8_t Flash_Status;
	
	FlashID[0] = 0;
	FlashID[1] = 0;
	Flash_Status = 0;
	HW_Failure_Reg = 0;
	Flash_Initial_Ok = TRUE;
	
	Pre_Time = Sys_ms_Ticks;
	while ((Flash_Status & 0xF0) != 0xA0)				// For AT45DB161D 2-Mbytes flash
	{
		CurrentTime = Sys_ms_Ticks;
		if ((CurrentTime > Pre_Time) && ((CurrentTime - Pre_Time) > FLASH_ACCESS_TIMEOUT))
		{
			// Set Error code here
			Flash_Initial_Ok = FALSE;
			HW_Failure_Reg |= FLASH_CHIP_ERROR;
			break;
		}
		else if (CurrentTime < Pre_Time) Pre_Time = Sys_ms_Ticks;
		Flash_Status = OnFlash_Read_Status();
	}
	
	if (!Flash_Initial_Ok) return;
	//-------------------------------------------------------------------
	Pre_Time = Sys_ms_Ticks;
	while ((FlashID[0] != 'O') && (FlashID[1] != 'P'))
	{
		CurrentTime = Sys_ms_Ticks;
		if ((CurrentTime > Pre_Time) && ((CurrentTime - Pre_Time) > FLASH_ACCESS_TIMEOUT))
		{
			// Set Error code here
			HW_ErrorCode |= LOAD_CAL_DATA_FAILED;
			break;
		}
		else if (CurrentTime < Pre_Time) Pre_Time = Sys_ms_Ticks;	// Sys_ms_Ticks is overflow;
		OnGet_Flash_Head();
	}
}
////////////////////////////////////////////////////////////////////////
uint8_t OnFlash_Read_Status(void)
{
	uint8_t Cmd[1];
	
	Cmd[0] = FLASH_STATUS_R;
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 1);
	OnSPI_Read(MOpm_SPI, Cmd, 1);
	OnSPI_Release_CS();
	return Cmd[0];
}
///////////////////////////////////////////////////////////////////////
void OnGet_Flash_Head(void)
{
	uint8_t Cmd[5], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_READ;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 0;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	OnSPI_Read(MOpm_SPI, FlashID, 20);
	OnSPI_Release_CS();
}
//////////////////////////////////////////////////////////////////////////
void OnFlash_Write_Buffer(uint8_t* Data, uint16_t Length, uint16_t offset)
{
	uint8_t Cmd[4], Index;
	
	Index = 0;
	Cmd[Index ++] = FLASH_BUFFER_W;
	Cmd[Index ++] = 0x0;
	Cmd[Index ++] = (offset & 0xFF00) >> 8;
	Cmd[Index ++] = (offset & 0x00FF);
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	OnSPI_Write(MOpm_SPI, Data, Length);	
	OnSPI_Release_CS();
}
//////////////////////////////////////////////////////////////////////////
void OnFlash_Read_Buffer(uint8_t* Data, uint16_t Length, uint16_t offset)
{
	uint8_t Cmd[4], Index;
	
	Index = 0;
	Cmd[Index ++] = FLASH_BUFFER_R;
	Cmd[Index ++] = 0x0;
	Cmd[Index ++] = (offset & 0xFF00) >> 8;
	Cmd[Index ++] = (offset & 0x00FF);
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	OnSPI_Read(MOpm_SPI, Data, Length);
	OnSPI_Release_CS();
}
////////////////////////////////////////////////////////////////////////
void OnFlash_Write_Page_N(uint16_t Page_Num)
{
	uint8_t Cmd[4], Index;
	uint32_t uData32;
	
	uData32 = (uint32_t)Page_Num << 10;	//(for page size 528 type flash)
	
	Index = 0;
	Cmd[Index ++] = FLASH_PAGE_W;
	Cmd[Index ++] = (uData32 & 0x00FF0000) >> 16;
	Cmd[Index ++] = (uData32 & 0x0000FF00) >> 8;
	Cmd[Index ++] = (uData32 & 0x000000FF);
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);

	OnSPI_Release_CS();
	OnFlash_Wait_done();
}
////////////////////////////////////////////////////////////////////////
void OnFlash_Read_Page_N(uint16_t Page_Num)
{
	uint8_t Cmd[4], Index;
	uint32_t uData32;
	
	uData32 = (uint32_t)Page_Num << 10;	//(for page size 528 type flash)
	
	Index = 0;
	Cmd[Index ++] = FLASH_PAGE_R;
	Cmd[Index ++] = (uData32 & 0x00FF0000) >> 16;
	Cmd[Index ++] = (uData32 & 0x0000FF00) >> 8;
	Cmd[Index ++] = (uData32 & 0x000000FF);
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);

	OnSPI_Release_CS();
	OnFlash_Wait_done();
}
///////////////////////////////////////////////////////////////////////////////////////////
void OnFlash_Read_Memory(uint8_t* Data, uint16_t Length, uint16_t Page_Num, uint16_t offset)
{
	uint8_t Cmd[8], Index;
	uint32_t uData32;
	
	uData32 = (uint32_t)Page_Num << 10;	//(for page size 528 type flash)
	uData32 += offset;
	
	Index = 0;
	Cmd[Index ++] = FLASH_MEMORY_R;
	Cmd[Index ++] = (uData32 & 0x00FF0000) >> 16;
	Cmd[Index ++] = (uData32 & 0x0000FF00) >> 8;
	Cmd[Index ++] = (uData32 & 0x000000FF);
	
	Cmd[Index ++] = 0x0;
	Cmd[Index ++] = 0x0;
	Cmd[Index ++] = 0x0;
	Cmd[Index ++] = 0x0;
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 8);
	OnSPI_Read(MOpm_SPI, Data, Length);
	
	OnSPI_Release_CS();
	OnFlash_Wait_done();
}
///////////////////////////////////////////////////////////////////////
void OnFlash_Wait_done(void)
{
	uint8_t Cmd[4], Flash_Status;
	
	Cmd[0] = FLASH_STATUS_R;
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	Flash_Status = 0x00;
	while (!(Flash_Status & 0x80))
	{
		OnSPI_Write(MOpm_SPI, Cmd, 1);
		OnSPI_Read(MOpm_SPI, Cmd, 1);
		Flash_Status = Cmd[0];
	}
	OnSPI_Release_CS();
}
////////////////////////////////////////////////////////////////////////////
void OnSPI_Release_CS(void)
{
	uint8_t data[1];
	
	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_NONE);	//20170811 VG: for SPI decode by FPGA
	OnSPI_Read(MOpm_SPI, data, 1);
	
	spi_set_lastxfer(MOpm_SPI);				//20170811 VG: for SPI decode by MCU
}
////////////////////////////////////////////////////////////////////////////
void OnSPI_Write_Flash_Block(uint8_t* Data, uint16_t BlockID)
{
	OnFlash_Write_Buffer(&Data[0], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+0);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+1);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE*2], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+2);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE*3], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+3);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE*4], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+4);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE*5], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+5);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE*6], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+6);
	OnFlash_Write_Buffer(&Data[FLASH_PAGE_SIZE*7], FLASH_PAGE_SIZE, 0);
	OnFlash_Write_Page_N(BlockID*8+7);
}
////////////////////////////////////////////////////////////////////////////
void OnSPI_Read_Flash_Block(uint8_t* Data, uint16_t BlockID)
{
	OnFlash_Read_Memory(&Data[0], FLASH_PAGE_SIZE, 8*BlockID, 0);	// Read data direct from flash
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE], FLASH_PAGE_SIZE, (8*BlockID + 1), 0);
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE*2], FLASH_PAGE_SIZE, (8*BlockID + 2), 0);
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE*3], FLASH_PAGE_SIZE, (8*BlockID + 3), 0);
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE*4], FLASH_PAGE_SIZE, (8*BlockID + 4), 0);
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE*5], FLASH_PAGE_SIZE, (8*BlockID + 5), 0);
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE*6], FLASH_PAGE_SIZE, (8*BlockID + 6), 0);
	OnFlash_Read_Memory(&Data[FLASH_PAGE_SIZE*7], FLASH_PAGE_SIZE, (8*BlockID + 7), 0);
}
////////////////////////////////////////////////////////////////////////////
void OnGet_Flash_MID(void)
{
	char Cmd[4];

	Cmd[0] = FLASH_RDID;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);	
	OnSPI_Write(MOpm_SPI, Cmd, 1);
	OnSPI_Read(MOpm_SPI, MID, 5);
	OnSPI_Release_CS();
	//spi_set_lastxfer(MOpm_SPI);
}
////////////////////////////////////////////////////////////////////////////
// Used to for flash chip M25P80
/*
void OnGet_Flash_Data(uint8_t* Data, uint16_t Length, uint16_t PageID)
{
	uint8_t Cmd[4], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_READ;
	Cmd[Index ++] = (PageID & 0xFF00) >> 8;
	Cmd[Index ++] = (PageID & 0x00FF);
	Cmd[Index ++] = 0;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	OnSPI_Read(MOpm_SPI, Data, Length);
}
///////////////////////////////////////////////////////////////////////
void OnGet_Flash_ID(void)
{
	uint8_t Cmd[4], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_RDID;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 1);
	OnSPI_Read(MOpm_SPI, FlashID, 20);
}
///////////////////////////////////////////////////////////////////////
char OnGet_Flash_Status(void)
{
	uint8_t Cmd[4], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_RDSR;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 1);
	OnSPI_Read(MOpm_SPI, Cmd, 1);
	return Cmd[0];
}
///////////////////////////////////////////////////////////////////////
char Is_Flash_Ready(void)
{
	uint32_t Pre_Time, CurrentTime;
	char FlashBusy;

	Pre_Time = Sys_ms_Ticks;

	FlashBusy = 1;
	while (FlashBusy)
	{
		CurrentTime = Sys_ms_Ticks;
		if ((CurrentTime > Pre_Time) && ((CurrentTime - Pre_Time) > FLASH_ACCESS_TIMEOUT))
		{
			// Set Error code here
			HW_ErrorCode |= FLASH_ACCESS_ERROR;
			break;
		}
		else if (CurrentTime < Pre_Time) Pre_Time = Sys_ms_Ticks;
		FlashBusy = OnGet_Flash_Status() & 0x01;
	}
	return FlashBusy;
}
///////////////////////////////////////////////////////////////////////
void OnSet_Flash_WREN(void)
{
	uint8_t Cmd[1], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_WREN;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 1);
}
///////////////////////////////////////////////////////////////////////
void OnSet_Flash_WRDI(void)
{
	uint8_t Cmd[1], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_WRDI;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 1);
}
///////////////////////////////////////////////////////////////////////
void OnSet_Flash_SE_0(void)		// special to erase sector 0
{
	uint8_t Cmd[4], Index;

	Index = 0;
	Cmd[Index ++] = FLASH_SE;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 0;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	Delay_ms(1500);					// delay 1.5s
}
///////////////////////////////////////////////////////////////////////
void OnSet_Flash_SE(uint8_t SectorID)
{
	uint8_t Cmd[4], Index;

	OnSet_Flash_WREN();
	Index = 0;
	Cmd[Index ++] = FLASH_SE;
	Cmd[Index ++] = SectorID;
	Cmd[Index ++] = 0;
	Cmd[Index ++] = 0;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	Delay_ms(1500);					// delay 1.5s
}
/////////////////////////////////////////////////////////////////////////
void OnFlash_WritePage(uint8_t* Data, uint16_t Length, uint16_t PageID)
{
	uint8_t Cmd[4], Index;

	OnSet_Flash_WREN();
	Index = 0;
	Cmd[Index ++] = FLASH_PP;
	Cmd[Index ++] = (PageID & 0xFF00) >> 8;
	Cmd[Index ++] = (PageID & 0x00FF);
	Cmd[Index ++] = 0;

	OnMTF_SPI_CS(MOpm_SPI, SPI_CH_FLASH);
	OnSPI_Write(MOpm_SPI, Cmd, 4);
	OnSPI_Write(MOpm_SPI, Data, Length);
	Delay_ms(10);
}
///////////////////////////////////////////////////////////////////////////////
void OnSet_Test_Flash_Write(uint16_t Page_Num)
{
	uint8_t Data[30], Index;
	
	Index = 0;
	Data[Index ++] = (uint8_t)Page_Num;
	Data[Index ++] = 'O';
	Data[Index ++] = 'P';
	Data[Index ++] = 'T';
	Data[Index ++] = 'O';
	Data[Index ++] = 'P';
	Data[Index ++] = 'L';
	Data[Index ++] = 'E';
	Data[Index ++] = 'X';
	Data[Index ++] = '.';
	Data[Index ++] = 't';
	Data[Index ++] = 'e';
	Data[Index ++] = 's';
	Data[Index ++] = 't';

	Data[Index ++] = '-';
	Data[Index ++] = '1';
	Data[Index ++] = '2';
	Data[Index ++] = '3';
	Data[Index ++] = '4';
	Data[Index ++] = '5';
	Data[Index ++] = '6';

// for Flash M25P80
//	OnFlash_WritePage(Data, Index, PageNum);
//	OnGet_Flash_Status();
	
// for Flash chip AT45DB161D	
	Offset = Index * PageNum;
	OnFlash_Write_Buffer(Data, Index, Offset);
	OnFlash_Write_Page_N(0);
}
*/
////////////////////////////////////////////////////////////////////////////