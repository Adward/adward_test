 /////////////////////////////////////////////////////////////////////////////
 //  Uart_Proc_0.c															//
 //																			//
 /////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
extern  uint8_t	 offSidelobe; 
//////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_Decode(void)
{
	uint8_t Cmd_1, Cmd_2, Cmd_3, Cmd_4, Data8;

	//-------------------------------------------------
	// Get Command Bytes			// Byte-0: Head
	Cmd_1 = RxTx_buf[1];			// Command Byte-1
	Cmd_2 = RxTx_buf[2];			// Command Byte-2
	Cmd_3 = RxTx_buf[3];			// Command Byte-3
	Cmd_4 = RxTx_buf[4];			// Command Byte-4
	//-------------------------------------------------
	Rx_Checksum16 = (RxTx_buf[Rx_Length + CMD_OVERHEAD2-2]<<8) + RxTx_buf[Rx_Length + CMD_OVERHEAD2-1];
	Data8 = 0;
	Uart_CmdID = NORMAL_CMD;			// Reset Uart_CmdID
	//-------------------------------------------------
	Data_Checksum16 = 0;
	for (UartData16 = 1; UartData16 < (Rx_Length + CMD_OVERHEAD2-2); UartData16 ++)
	{
		Data_Checksum16 += RxTx_buf[UartData16];
	}
	//----------------------------------------------------------------------------------
	if ((Cmd_1 == 'S')&&(Cmd_2 == 'E')&&(Cmd_3 == 'T')&&(Cmd_4 == 'V'))
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_VOLTAGE, RX_CKS_ERROR);
		else
		{
			Data8 = 10;
			Slot_ID = RxTx_buf[Data8 ++];
			
			Data32  = (int32_t)(RxTx_buf[Data8 ++]) << 24;
			Data32 += (int32_t)(RxTx_buf[Data8 ++]) << 16;
			Data32 += (int32_t)(RxTx_buf[Data8 ++]) << 8;
			Data32 += (int32_t)(RxTx_buf[Data8 ++]);
			
			if (Data32 >= 0)
			{
				SetCh_Dir = 0;
				UartData32 = Data32;
				if (UartData32 > MAX_SET_VOLTAGE) UartData32 = MAX_SET_VOLTAGE;
				//----------------------------------------------------------------------
				// Convert set voltage value to DAC input number with a Gain factor
				UartData16 = (uint16_t)(UartData32 * DAC_DATA_SCALE + Offset_DAC);
			//	Pre_DAC_Value = UartData16;				// for debug
				OnSet_Voltage(UartData16, SetCh_Dir);
			}
			else
			{
				SetCh_Dir = 1;
				UartData32 = -Data32;				 
				if (UartData32 > MAX_SET_VOLTAGE) UartData32 = MAX_SET_VOLTAGE;

				UartData16 = (uint16_t)(UartData32 * DAC_DATA_SCALE + Offset_DAC);
			//	Pre_DAC_Value = UartData16;				// for debug
				OnSet_Voltage(UartData16, SetCh_Dir);
			}
			MTF_Mode = MTF_SET_V_CAL;
			Reply_to_UART(SET_VOLTAGE, RX_OK);
		}
	} //---------------------------------------------------------------------------
	else if ((Cmd_1 == 'S')&&(Cmd_2 == 'T')&&(Cmd_3 == 'O')&&(Cmd_4 == 'F'))	// Set DAC channel Offset as DAC count
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_OFFSET, RX_CKS_ERROR);
		else
		{
			Data8 = 10;
			Slot_ID = RxTx_buf[Data8 ++];
			
			UartData32  = (int32_t)(RxTx_buf[Data8 ++]) << 24;
			UartData32 += (int32_t)(RxTx_buf[Data8 ++]) << 16;
			UartData32 += (int32_t)(RxTx_buf[Data8 ++]) << 8;
			UartData32 += (int32_t)(RxTx_buf[Data8 ++]);
			
			Offset_DAC = (uint16_t)(UartData32 * DAC_DATA_SCALE);
			Reply_to_UART(SET_OFFSET, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'T') && (Cmd_3 == 'C') && (Cmd_4 == 'H'))			// set channel
	{
		Slot_ID = RxTx_buf[10];
		User_Ch_Num = RxTx_buf[12];						// Channel Number

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_CH, RX_CKS_ERROR);
		else
		{
			if ((User_Ch_Num < 1) || (User_Ch_Num > Total_ChNum)) Reply_to_UART(SET_CH, DATA_OUT_RANGE);
			else
			{
				//OnRead_TmpR();
				OnRead_Temp();
				UartData32 = OnGetChNum_WL(User_Ch_Num);
				OnSetTF_Position(User_Ch_Num, UartData32);
				Reply_to_UART(SET_CH, RX_OK);
			}
		}
	} //----------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'T') && (Cmd_3 == 'W') && (Cmd_4 == 'L'))			// set target WL
	{
		Slot_ID = RxTx_buf[10];		
		Data8 = 11;
		UartData32  = (int32_t)(RxTx_buf[Data8 + 0]) << 24;					// Wavelength
		UartData32 += (int32_t)(RxTx_buf[Data8 + 1]) << 16;
		UartData32 += (int32_t)(RxTx_buf[Data8 + 2]) << 8;
		UartData32 += (int32_t)(RxTx_buf[Data8 + 3]);

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_WL, RX_CKS_ERROR);
		else
		{
			if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(SET_WL, DATA_OUT_RANGE);	// CH0_WL is Short WL end
			else
			{
				OnRead_Temp();
				User_Ch_Num = OnGetWL_ChNum(UartData32);
				OnSetTF_Position(User_Ch_Num, UartData32);
				Reply_to_UART(SET_WL, RX_OK);
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'T') && (Cmd_3 == 'F') && (Cmd_4 == 'R'))			// set target Freq
	{
		Slot_ID = RxTx_buf[10];		
		Data8 = 11;
		Set_Freq  = (int32_t)(RxTx_buf[Data8 + 0]) << 24;					// Freq.
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 1]) << 16;
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 2]) << 8;
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 3]);

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_FREQ, RX_CKS_ERROR);
		else
		{
			if (Set_Freq <= 0) Reply_to_UART(SET_FREQ, DATA_OUT_RANGE);
			else
			{
				UartData32 = (uint32_t)(1000*(C_SPEED/(float)(Set_Freq)) + 0.55f);
				if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(SET_FREQ, DATA_OUT_RANGE);	// CH0_WL is Short WL end
				else
				{
					OnRead_Temp();
					User_Ch_Num = OnGetWL_ChNum(UartData32);
					OnSetTF_Position(User_Ch_Num, UartData32);
					Reply_to_UART(SET_FREQ, RX_OK);
				}
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'W') && (Cmd_2 == 'L') && (Cmd_3 == 'U') && (Cmd_4 == 'P'))			// Increase WL (pm)
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 11;
		UartData16  = (int16_t)(RxTx_buf[Data8 + 0]) << 8;					// WL in pm
		UartData16 += (int16_t)(RxTx_buf[Data8 + 1]);

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_WL_UP, RX_CKS_ERROR);
		else
		{
			UartData32 = Ch_Wavelength + UartData16;
			if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(SET_WL_UP, DATA_OUT_RANGE);	// CH0_WL is Short WL end
			else
			{
				OnRead_Temp();
				User_Ch_Num = OnGetWL_ChNum(UartData32);
				OnSetTF_Position(User_Ch_Num, UartData32);
				Reply_to_UART(SET_WL_UP, RX_OK);
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'W') && (Cmd_2 == 'L') && (Cmd_3 == 'D') && (Cmd_4 == 'N'))			// decrease WL (pm)
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 11;
		UartData16  = (int16_t)(RxTx_buf[Data8 + 0]) << 8;					// WL in pm
		UartData16 += (int16_t)(RxTx_buf[Data8 + 1]);

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_WL_DN, RX_CKS_ERROR);
		else
		{
			UartData32 = Ch_Wavelength - UartData16;
			if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(SET_WL_DN, DATA_OUT_RANGE);	// CH0_WL is Short WL end
			else
			{
				OnRead_Temp();
				User_Ch_Num = OnGetWL_ChNum(UartData32);
				OnSetTF_Position(User_Ch_Num, UartData32);
				Reply_to_UART(SET_WL_DN, RX_OK);
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'C') && (Cmd_2 == 'H') && (Cmd_3 == 'U') && (Cmd_4 == 'P'))			// Increase WL (pm)
	{
		Slot_ID = RxTx_buf[10];
		Data8 = (int16_t)(RxTx_buf[12]);		// CH number change

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_CH_UP, RX_CKS_ERROR);
		else
		{
			if (Ch_Num + Data8 > Total_ChNum) Reply_to_UART(SET_CH_UP, DATA_OUT_RANGE);
			else
			{
				User_Ch_Num = Ch_Num + Data8;
				if ((User_Ch_Num < 1) || (User_Ch_Num > Total_ChNum)) Reply_to_UART(SET_CH_UP, DATA_OUT_RANGE);	// CH0_WL is Short WL end
				else
				{
					OnRead_Temp();
					UartData32 = OnGetChNum_WL(User_Ch_Num);
					OnSetTF_Position(User_Ch_Num, UartData32);
					Reply_to_UART(SET_CH_UP, RX_OK);
				}
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'C') && (Cmd_2 == 'H') && (Cmd_3 == 'D') && (Cmd_4 == 'N'))			// Decrease WL (pm)
	{
		Slot_ID = RxTx_buf[10];
		Data8 = (int16_t)(RxTx_buf[12]);		// CH number change

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_CH_DN, RX_CKS_ERROR);
		else
		{
			if ((Ch_Num + 1) < Data8) Reply_to_UART(SET_CH_DN, DATA_OUT_RANGE);
			else
			{
				User_Ch_Num = Ch_Num - Data8;
				if ((User_Ch_Num < 1) || (User_Ch_Num > Total_ChNum)) Reply_to_UART(SET_CH_DN, DATA_OUT_RANGE);	// CH0_WL is Short WL end
				else
				{
					OnRead_Temp();
					UartData32 = OnGetChNum_WL(User_Ch_Num);
					OnSetTF_Position(User_Ch_Num, UartData32);
					Reply_to_UART(SET_CH_DN, RX_OK);
				}
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'I') && (Cmd_2 == 'L') && (Cmd_3 == 'F') && (Cmd_4 == 'R'))		// get Filter ILoss at Freq
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 11;
		Set_Freq  = (int32_t)(RxTx_buf[Data8 + 0]) << 24;					// Freq.
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 1]) << 16;
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 2]) << 8;
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 3]);

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_IL_FR, RX_CKS_ERROR);
		else
		{
			if (Set_Freq <= 0) Reply_to_UART(GET_IL_FR, DATA_OUT_RANGE);
			else
			{
				UartData32 = (uint32_t)(1000*(C_SPEED/(float)(Set_Freq)) + 0.55f);
				if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(GET_IL_FR, DATA_OUT_RANGE);
				else
				{
					OnRead_Temp();
					User_Ch_Num = OnGetWL_ChNum(UartData32);
					OnGetInsertionLoss(User_Ch_Num, UartData32);
					Reply_to_UART(GET_IL_FR, RX_OK);
				}
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'I') && (Cmd_2 == 'L') && (Cmd_3 == 'W') && (Cmd_4 == 'L'))		// get Filter ILoss at WL
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 11;
		UartData32  = (int32_t)(RxTx_buf[Data8 + 0]) << 24;					// Wavelength
		UartData32 += (int32_t)(RxTx_buf[Data8 + 1]) << 16;
		UartData32 += (int32_t)(RxTx_buf[Data8 + 2]) << 8;
		UartData32 += (int32_t)(RxTx_buf[Data8 + 3]);
		Set_WL = UartData32;
		
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_IL_WL, RX_CKS_ERROR);
		else
		{
			if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(GET_IL_WL, DATA_OUT_RANGE);
			else
			{
				OnRead_Temp();
				User_Ch_Num = OnGetWL_ChNum(UartData32);
				OnGetInsertionLoss(User_Ch_Num, UartData32);
				Reply_to_UART(GET_IL_WL, RX_OK);
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'I') && (Cmd_2 == 'L') && (Cmd_3 == 'C') && (Cmd_4 == 'H'))		// get filter ILoss at Ch Num
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 12;
		User_Ch_Num  = RxTx_buf[Data8 + 0];					// Channel Num

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_IL_CH, RX_CKS_ERROR);
		else
		{
			if ((User_Ch_Num < 1) || (User_Ch_Num > Total_ChNum)) Reply_to_UART(GET_IL_CH, DATA_OUT_RANGE);
			else
			{
				OnRead_Temp();
				UartData32 = OnGetChNum_WL(User_Ch_Num);
				OnGetInsertionLoss(User_Ch_Num, UartData32);
				Reply_to_UART(GET_IL_CH, RX_OK);
			}
		}	
	} //---------------------------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'B') && (Cmd_2 == 'W') && (Cmd_3 == 'F') && (Cmd_4 == 'R'))		// get filter BW at Freq
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 11;
		Set_Freq  = (int32_t)(RxTx_buf[Data8 + 0]) << 24;					// Freq.
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 1]) << 16;
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 2]) << 8;
		Set_Freq += (int32_t)(RxTx_buf[Data8 + 3]);

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_BW_FR, RX_CKS_ERROR);
		else
		{
			if (Set_Freq <= 0) Reply_to_UART(GET_BW_FR, DATA_OUT_RANGE);
			else
			{
				UartData32 = (uint32_t)(1000*(C_SPEED/(float)(Set_Freq)) + 0.55f);
				if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(GET_BW_FR, DATA_OUT_RANGE);
				else
				{
					OnRead_Temp();
					User_Ch_Num = OnGetWL_ChNum(UartData32);
					OnGetFilterBW(User_Ch_Num, UartData32);
					Reply_to_UART(GET_BW_FR, RX_OK);
				}
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'B') && (Cmd_2 == 'W') && (Cmd_3 == 'W') && (Cmd_4 == 'L'))		// get filter BW at WL
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 11;
		UartData32  = (int32_t)(RxTx_buf[Data8 + 0]) << 24;					// Wavelength
		UartData32 += (int32_t)(RxTx_buf[Data8 + 1]) << 16;
		UartData32 += (int32_t)(RxTx_buf[Data8 + 2]) << 8;
		UartData32 += (int32_t)(RxTx_buf[Data8 + 3]);
		
		Set_WL = UartData32;
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_BW_WL, RX_CKS_ERROR);
		else
		{
			if ((UartData32 < Ch0_WL) || (UartData32 > ChN_WL)) Reply_to_UART(GET_BW_WL, DATA_OUT_RANGE);
			else
			{
				OnRead_Temp();
				User_Ch_Num = OnGetWL_ChNum(UartData32);
				OnGetFilterBW(User_Ch_Num, UartData32);
				Reply_to_UART(GET_BW_WL, RX_OK);
			}
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'B') && (Cmd_2 == 'W') && (Cmd_3 == 'C') && (Cmd_4 == 'H'))		// get filter BW at Ch Num
	{
		Slot_ID = RxTx_buf[10];
		Data8 = 12;
		User_Ch_Num  = RxTx_buf[Data8 + 0];					// Channel Num

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_BW_CH, RX_CKS_ERROR);
		else
		{
			if ((User_Ch_Num < 1) || (User_Ch_Num > Total_ChNum)) Reply_to_UART(GET_BW_CH, DATA_OUT_RANGE);
			else
			{
				OnRead_Temp();
				UartData32 = OnGetChNum_WL(User_Ch_Num);
				OnGetFilterBW(User_Ch_Num, UartData32);
				Reply_to_UART(GET_BW_CH, RX_OK);
			}
		}
	} //---------------------------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'C') && (Cmd_3 == 'W') && (Cmd_4 == 'L'))			// get current channel wavelength
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SCAN_WL, RX_CKS_ERROR);
		else 
		{	
			if ((RxTx_buf[7] == 98)&&(RxTx_buf[8] == 76)) TxScandata = 1;	// debug for send Scan data only
			else TxScandata = 0;
			
			Slot_ID = RxTx_buf[10];
			ScanMode = RxTx_buf[12];
			ScanSpeed = RxTx_buf[14];
			ScanStartCh = RxTx_buf[16];
			ScanEndCh = RxTx_buf[18];
			
			if ((ScanStartCh < 1) || (ScanEndCh < 1) || (ScanStartCh > Total_ChNum) || (ScanEndCh > Total_ChNum) || (ScanStartCh >= ScanEndCh)) 
				Reply_to_UART(SCAN_WL, DATA_OUT_RANGE);
			else
			{
				if ((Ch_Wavelength != ITU_WL_Table[ScanStartCh])||(MTF_Mode != MTF_SCAN))
				{
					OnSetTF_Position(ScanStartCh, ITU_WL_Table[ScanStartCh]);
					delay_ms(30);
				}
				if ((MTF_Mode != MTF_SCAN) || (ScanStartWL != ITU_WL_Table[ScanStartCh]) || (ScanEndWL != ITU_WL_Table[ScanEndCh]))
				{
					ScanStartWL = ITU_WL_Table[ScanStartCh];
					ScanEndWL = ITU_WL_Table[ScanEndCh];
					OnMTF_SetEqWL_Table();
				}
				//-------------------------------------------
				ScanDir = 0;
				MTF_Mode = MTF_SCAN;
				Uart_CmdID = MTF_SCAN_CMD;

				//pio_set_pin_high(PIO_PA1_IDX);
				OnMTF_StartScan();
				Reply_to_UART(SCAN_WL, RX_OK);
				delay_ms(10);
				//pio_set_pin_low(PIO_PA1_IDX);
			}
		}
	} //---------------------------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'T') && (Cmd_3 == 'O') && (Cmd_4 == 'P'))			// STOP Scan operation
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(STOP_MTF, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			MTF_Mode = RUN_NORMAL;
			Reply_to_UART(STOP_MTF, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'B') && (Cmd_2 == 'W') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))			// get Bandwidth Table
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_BW_TB, RX_CKS_ERROR);
		else
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_BW_TB, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'I') && (Cmd_2 == 'L') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))			// get Insertion Loss Table
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_IL_TB, RX_CKS_ERROR);
		else
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_IL_TB, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'D') && (Cmd_3 == 'W') && (Cmd_4 == 'L'))			// get current channel wavelength
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_WL, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_WL, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'D') && (Cmd_3 == 'F') && (Cmd_4 == 'R'))			// get current channel Freq
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_FREQ, RX_CKS_ERROR);
		else Reply_to_UART(GET_FREQ, RX_OK);
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'D') && (Cmd_3 == 'C') && (Cmd_4 == 'H'))			// get current Number
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_CH, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_CH, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'F') && (Cmd_2 == 'W') && (Cmd_3 == 'V') && (Cmd_4 == 'R'))			// get FW version
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_FW_VER, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_FW_VER, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'F') && (Cmd_2 == 'H') && (Cmd_3 == 'V') && (Cmd_4 == 'R'))			// get Flash data version
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_FLASH_VER, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_FLASH_VER, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'M') && (Cmd_2 == 'D') && (Cmd_3 == 'A') && (Cmd_4 == 'T'))			// get module manufacture date
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_M_DATE, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_M_DATE, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'N') && (Cmd_3 == 'P') && (Cmd_4 == 'N'))			// get module SN and PN
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_SN_PN, RX_CKS_ERROR);
		else Reply_to_UART(GET_SN_PN, RX_OK);
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S') && (Cmd_2 == 'N') && (Cmd_3 == 'F') && (Cmd_4 == 'V'))			// get module SN and FW version
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_SN_FV, RX_CKS_ERROR);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_SN_FV, RX_OK);
		}
	} //--------------------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'V') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))			// get calibration WL-V table
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_VTABLE, RX_CKS_ERROR);
		else if (ETable_Ready == 0) Reply_to_UART(GET_VTABLE, DATA_NOT_AVAILABLE);
		else 
		{
			Slot_ID = RxTx_buf[10];
			Reply_to_UART(GET_VTABLE, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'E') && (Cmd_3 == 'S') && (Cmd_4 == 'T'))			//Soft Reset: reload initial parameters
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(RESET_MTF, RX_CKS_ERROR);
		else
		{
			Slot_ID = RxTx_buf[10];
			OnMTF_Data_Initial();
			Reply_to_UART(RESET_MTF, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'W') && (Cmd_3 == 'S') && (Cmd_4 == '0'))	//Read Signal RAW data
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_RW_SIGNAL, RX_CKS_ERROR);
		else
		{
			Slot_ID = RxTx_buf[10];
			Uart_CmdID = RW_SIGNAL_CMD;
			
			if ((Ch_Wavelength != ChN_WL)||(MTF_Mode != MOPM_RW_SCAN))
			{
				OnMTF_SetScanPos2(ScanStartPos);
				OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
				delay_ms(30);
			}
			MTF_Mode = MOPM_RW_SCAN;
			OnMTF_StartScan();
			Reply_to_UART(GET_RW_SIGNAL, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if (((Cmd_1 == 'E') && (Cmd_2 == 'Q') && (Cmd_3 == 'S') && (Cmd_4 == '0')))	//Read EQS Signal data
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_EQS_SIGNAL, RX_CKS_ERROR);
		else
		{
			Data8 = 7;
			Sidelobe_lmt  = RxTx_buf[Data8 ++] << 8;	//Sidelobe_lmt=0 disable Sidelobe filter
			Sidelobe_lmt += RxTx_buf[Data8 ++];
			
			Sidelobe_alpha  = RxTx_buf[Data8 ++] << 8;	//Q8 (dB), integer, 15360 is 60dB.
			Sidelobe_alpha += RxTx_buf[Data8 ++];
			
			Data8 = 11;	
			Data32  = RxTx_buf[Data8 ++] << 24;
			Data32 += RxTx_buf[Data8 ++] << 16;
			Data32 += RxTx_buf[Data8 ++] << 8;
			Data32 += RxTx_buf[Data8 ++];
			ScanStartFreq = Data32 - Freq_Offset;
			 
			Data32  = RxTx_buf[Data8 ++] << 24;
			Data32 += RxTx_buf[Data8 ++] << 16;
			Data32 += RxTx_buf[Data8 ++] << 8;
			Data32 += RxTx_buf[Data8 ++];
			ScanStopFreq = Data32 - Freq_Offset;
			
			OnMTF_SetScanPos2(0);
			OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
			delay_ms(30);
			
			Uart_CmdID = EQS_SIGNAL_CMD;
			MTF_Mode = MOPM_RW_SCAN;	//161207 VG: Added to enter data processing
			OnMTF_StartScan();
			Reply_to_UART(GET_EQS_SIGNAL, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'E') && (Cmd_2 == 'Q') && (Cmd_3 == 'S') && (Cmd_4 == '8'))			// Read EQS signal CH compressed raw data continuously.
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_EQS_SIGNAL, RX_CKS_ERROR);
		else
		{
			if ((RxTx_buf[7] == 98)&&(RxTx_buf[8] == 76)) TxScandata = 1;
			else TxScandata = 0;
			
			Slot_ID = RxTx_buf[10];
			ScanMode = RxTx_buf[12];
			
			Data8 = 13;
			Data32  = RxTx_buf[Data8 ++] << 24;
			Data32 += RxTx_buf[Data8 ++] << 16;
			Data32 += RxTx_buf[Data8 ++] << 8;
			Data32 += RxTx_buf[Data8 ++];
			ScanStartFreq = Data32 - Freq_Offset;
			Data32H = (uint32_t)(1000*(C_SPEED/(float)(Data32)) + 0.55f);
			
			Data32  = RxTx_buf[Data8 ++] << 24;
			Data32 += RxTx_buf[Data8 ++] << 16;
			Data32 += RxTx_buf[Data8 ++] << 8;
			Data32 += RxTx_buf[Data8 ++];
			ScanStopFreq = Data32 - Freq_Offset;
			Data32L = (uint32_t)(1000*(C_SPEED/(float)(Data32)) + 0.55f);				
						
			//if ( (ITU_WL_Table[Total_ChNum+1] < Data32H)|| (ITU_WL_Table[0] > Data32L) )
			//	Reply_to_UART(GET_EQS_SIGNAL, DATA_OUT_RANGE);
			//else
			//{
				if (MTF_Mode != MTF_SCAN)
				{
					ScanDir = 0;
					ScanPosIndex = 0;
					OnMTF_SetScanPos2(ScanPosIndex);
					OnMTF_SetEqS_Table();
					delay_ms(30);
				}
				
				//-------------------------------------------
				ScanDir = 0;
				//MTF_Mode = MTF_SCAN;
				MTF_Mode = MOPM_RW_SCAN;
				Uart_CmdID = MOPM_SCAN_CMD;

				//pio_set_pin_high(PIO_PA1_IDX);
				OnMTF_StartScan();
				Reply_to_UART(SCAN_WL, RX_OK);
				delay_ms(10);
				//pio_set_pin_low(PIO_PA1_IDX);
			//}
		}
	} //---------------------------------------------------------------------------------------------------------------------
	else if (((Cmd_1 == 'P') && (Cmd_2 == 'E') && (Cmd_3 == 'Q') && (Cmd_4 == 'S')))	//Read Peaks data of EQS Signal
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_PK_EQS0, RX_CKS_ERROR);
		else
		{
			Uart_CmdID = ANALYZE_SIGNAL_CMD;
			MTF_Mode = RUN_NORMAL;
			Reply_to_UART(GET_PK_EQS0, RX_OK);
			OnMTF_ProcessUserCmd();
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'F') && (Cmd_2 == 'C') && (Cmd_3 == 'A') && (Cmd_4 == 'L'))		// Freq. Calibration
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(START_FREQ_CAL, RX_CKS_ERROR);
		else
		{
			if ((RxTx_buf[7] <= FP_CAL_Ch0_LOW_POWER_OLD) && (RxTx_buf[7] >= FREQ_CAL_LASER_ONLY))
			{
				if ((Ch_Wavelength != ChN_WL)||(MTF_Mode != FREQ_CAL_RULER_REF_INDEX)||(MTF_Mode != FREQ_CAL_RULER_REF_LASER))
				{
					OnMTF_SetScanPos2(ScanStartPos);
					OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
					delay_ms(30);
				}
				//----------------------------------------------------------------------
				Data8 = 7;							// Not build-in other commands
				MTF_Mode = RxTx_buf[Data8 ++];		// MTF_Mode = FREQ_CAL_RULER_REF_LASER, or FREQ_CAL_RULER_REF_INDEX
				NumOfFreqCal = RxTx_buf[Data8 ++];	// Number to do average
				Ref_LaserIndex = RxTx_buf[Data8 ++];
				ETableLength = RxTx_buf[Data8 ++];	// Total channels when cal. with single laser
				
				if ((MTF_Mode == FREQ_CAL_RULER_REF_INDEX) || (MTF_Mode == FREQ_CAL_RULER_REF_LASER))
				{
					MinPeakLevel = RxTx_buf[Data8 ++] << 8;		// MinPeakLevel and Contrast used with RULER only
					MinPeakLevel += RxTx_buf[Data8 ++];
				
					Contrast = RxTx_buf[Data8 ++] << 8;
					Contrast += RxTx_buf[Data8 ++];
				}
				Ref_LaserFreq  = RxTx_buf[Data8 ++] << 24;
				Ref_LaserFreq += RxTx_buf[Data8 ++] << 16;
				Ref_LaserFreq += RxTx_buf[Data8 ++] << 8;
				Ref_LaserFreq += RxTx_buf[Data8 ++];
				
				if ((MTF_Mode == FREQ_CAL_RULER_REF_INDEX) || (MTF_Mode == FREQ_CAL_RULER_REF_LASER))
				{
					Reply_to_UART(START_FREQ_CAL, RX_OK);
					OnMTF_FreqCalibration_Ruler();
				}
				else if ((MTF_Mode == FREQ_POW_CAL_LASER) || (MTF_Mode == FP_CAL_Ch0_LOW_POWER) || (MTF_Mode == FREQ_POW_CAL_LASER_OLD) || (MTF_Mode == FP_CAL_Ch0_LOW_POWER_OLD))
				{
					OnMTF_Freq_Power_Calibration();
					Reply_to_UART(START_FREQ_CAL, RX_OK);
				}
				else // Using Laser only to do Freq calibration
				{
					MTF_Mode = FREQ_CAL_LASER_ONLY;
					OnMTF_FreqCalibration_Laser();
					Reply_to_UART(START_FREQ_CAL, RX_OK);
				}
			}
			else Reply_to_UART(START_FREQ_CAL, DATA_OUT_RANGE);
		}
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'C') && (Cmd_2 == 'H') && (Cmd_3 == 'T') && (Cmd_4 == 'E'))		// Channel Test
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(START_CH_TEST, RX_CKS_ERROR);
		else
		{
			Data8 = 7;								
			NumOfFreqCal = RxTx_buf[Data8 ++];		// Number of testing
			Ch_Num = RxTx_buf[Data8 ++];			// current test channel number
			
			MTF_Mode = RUN_NORMAL;	
			Reply_to_UART(START_CH_TEST, RX_OK);
			OnMTF_Channel_Test();
		}
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'M') && (Cmd_2 == 'W') && (Cmd_3 == 'F') && (Cmd_4 == 'S'))		// Measure filter Waveform: signal filter
	{																					// It get EQS data, ETable nust be ready
		Data8 = 11;
		WF_Length = RxTx_buf[Data8 ++] << 8;
		WF_Length += RxTx_buf[Data8 ++];
	
		MTF_Mode = RxTx_buf[Data8 ++];
		Ch_Num = RxTx_buf[Data8 ++];
	
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(MEASURE_SIGNAL_FWF, RX_CKS_ERROR);
		else
		{
			if (Ch_Num <= MAX_NUM_PEAKS)
			{
				OnMTF_SetScanPos2(0);
				OnMTF_SetEqS_Table_Cal();
				delay_ms(30);
				//-------------------------------------------
				
				//	Reply_to_UART(MEASURE_SIGNAL_FWF, RX_OK);
				Uart_CmdID = SIGNAL_FILTER_WF_CMD;
				OnMTF_StartScan();
			}
			else Reply_to_UART(MEASURE_SIGNAL_FWF, DATA_OUT_RANGE);
		}	
		
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'E') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))		// Read Freq. Cal EncTable
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_ETABLE, RX_CKS_ERROR);
		else if (ETable_Ready == 0) Reply_to_UART(GET_ETABLE, DATA_NOT_AVAILABLE);
		else Reply_to_UART(GET_ETABLE, RX_OK);
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'W') && (Cmd_2 == 'E') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))		// Write Freq. Cal EncTable
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_ETABLE, RX_CKS_ERROR);
		else
		{
			ETableLength = RxTx_buf[11] *256 + RxTx_buf[12];
			if (ETableLength > MAX_NUM_PEAKS) ETableLength = MAX_NUM_PEAKS;
			
			OnMTF_DownloadETable();
			Reply_to_UART(SET_ETABLE, RX_OK);
		}
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R') && (Cmd_2 == 'P') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))		// Read Power-Cal ADCs
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_PTABLE, RX_CKS_ERROR);
		else if (PTable_Ready == 0) Reply_to_UART(GET_PTABLE, DATA_NOT_AVAILABLE);
		else Reply_to_UART(GET_PTABLE, RX_OK);
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'W') && (Cmd_2 == 'P') && (Cmd_3 == 'T') && (Cmd_4 == 'B'))		// Write old Power-Cal ADC
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_PTABLE, RX_CKS_ERROR);
		else
		{
			OnMTF_DownloadPTable();
			Reply_to_UART(SET_PTABLE, RX_OK);
		}
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'R')&&(Cmd_2 == 'D')&&(Cmd_3 == 'T')&&(Cmd_4 == 'P'))	// Read Temperature.
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(GET_TEMP, RX_CKS_ERROR);
		else
		{
			Slot_ID = RxTx_buf[10];
			T_SensorID = RxTx_buf[12];						// Temp Sensor ID: PCB board or Optical housing
			
			if (T_SensorID == TEMP_PCB) OnRead_Tmp125();
			else if (T_SensorID == TEMP_OPT) OnRead_TmpR();
			else Reply_to_UART(GET_TEMP, DATA_OUT_RANGE);
			
			Reply_to_UART(GET_TEMP, RX_OK);
		}
	} //------------------------------------------------------------------------------------
	else if ((Cmd_1 == 'D') && (Cmd_2 == 'L') && (Cmd_3 == 'F') && (Cmd_4 == 'H'))			// download flash data 256-bytes per page
	{
		FlashBlockID = (uint16_t)RxTx_buf[11] << 8;		// page number (512 bytes / page)
		FlashBlockID += RxTx_buf[12];					// download 8 pages per transfer

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(FLASH_DL, RX_CKS_ERROR);
		else
		{
			OnSPI_Write_Flash_Block(&RxTx_buf[13], FlashBlockID);		
			OnMTF_Data_Initial();
			Reply_to_UART(FLASH_DL, RX_OK);
		}
	} //-----------------------------------------------------------------------------------
	else if ((Cmd_1 == 'U') && (Cmd_2 == 'P') && (Cmd_3 == 'F') && (Cmd_4 == 'H'))			// upload flash data
	{
		FlashBlockID = (uint16_t)RxTx_buf[11] << 8;
		FlashBlockID += RxTx_buf[12];

		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(FLASH_UP, RX_CKS_ERROR);
		else
		{
			OnSPI_Read_Flash_Block(&RxTx_buf[13], FlashBlockID);
			Reply_to_UART(FLASH_UP, RX_OK);
		}
	} //----------------------------------------------------------------------------------
	else if ((Cmd_1 == 'S')&&(Cmd_2 == 'P')&&(Cmd_3 == 'O')&&(Cmd_4 == 'S'))
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_SCANPOS, RX_CKS_ERROR);
		else
		{
			Data8 = 11;
			Scan_Pos_Offset		 = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
			Scan_Pos_Offset		+= (uint32_t)(RxTx_buf[Data8 ++]) << 16;	
			Scan_Pos_Offset		+= (uint32_t)(RxTx_buf[Data8 ++]) << 8;
			Scan_Pos_Offset		+= (uint32_t)(RxTx_buf[Data8 ++]);
			Scan_Pos_Resolution  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
			Scan_Pos_Resolution += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
			Scan_Pos_Resolution += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
			Scan_Pos_Resolution += (uint32_t)(RxTx_buf[Data8 ++]);
			Reply_to_UART(SET_SCANPOS, RX_OK);
		}
	} //---------------------------------------------------------------------------
	else if ((Cmd_1 == 'W')&&(Cmd_2 == 'S')&&(Cmd_3 == 'N')&&(Cmd_4 == 'R'))
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(SET_WienerSNR, RX_CKS_ERROR);
		else
		{
			Data8 = 11;
			//WSNR = (uint16_t)(RxTx_buf[Data8 ++]) << 8;
			//WSNR += (uint16_t)(RxTx_buf[Data8 ++]);			
			Reply_to_UART(SET_WienerSNR, RX_OK);
		}
	} //---------------------------------------------------------------------------
	else if ((Cmd_1 == 'L') && (Cmd_2 == 'O') && (Cmd_3 == 'G'))	//Log data
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(LOG_Cmd, RX_CKS_ERROR);
		else
		{
			switch (Cmd_4) {
				case 0x31:
					Uart_CmdID = LOG_ADC_CMD;
					break;
				case 0x32:
					Uart_CmdID = LOG_DECONV_CMD;
					break;
				case 0x33:
					Uart_CmdID = LOG_ResampleWL_CMD;
					break;
				default:
					Uart_CmdID = LOG_ResampleP_CMD;
			}
			MOpm_ScanDone = 1;	//forced to enter onMTF_ProcessUserCmd					
			MTF_Mode = MOPM_RW_SCAN;
			Reply_to_UART(LOG_Cmd, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'D') && (Cmd_2 == 'E') && (Cmd_3 == 'M')  && (Cmd_4 == 'O'))	//Log data
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(DEMO_Cmd, RX_CKS_ERROR);
		else
		{
			OnMTF_SetScanPos2(0);
			OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
			delay_ms(30);
			//-------------------------------------------
			Uart_CmdID = NB_DEMO_rawADC;
			MTF_Mode = MOPM_RW_SCAN;
			OnMTF_StartScan();
			Reply_to_UART(DEMO_Cmd, RX_OK);
		}
	} //--------------------------------------------------------------------------------
	else if ((Cmd_1 == 'B') && (Cmd_2 == 'O') && (Cmd_3 == 'O') && (Cmd_4 == 'T'))	//Log data
	{
		if (Data_Checksum16 != Rx_Checksum16) Reply_to_UART(Boot_Cmd, RX_CKS_ERROR);
		else
		{
			signature->Loaded_Ver = VerBoot;		// reply to Host after soft reset
			saveSignature = TRUE;
			onMTF_SaveSignature(saveSignature, signature);
			Reply_to_UART(Boot_Cmd, RX_OK);
			while (1) NVIC_SystemReset();
		}
	} //--------------------------------------------------------------------------------
	else Reply_to_UART(UNKNOW_CMD, RX_UNKNOW_CMD);
	//--------------------------------------------------------------------------------
}
//////////////////////////////////////////////////////////////////////////////////
void Reply_to_UART(uint16_t CmdID, uint8_t ErrorCode)
{
	uint16_t Index, Length;
	
	Index = 0;
	Tx_Checksum16 = 0;
	UartData32 = 0;
	//------------------------------------------
	if (CmdID == SET_VOLTAGE)
	{
		if (SetCh_Dir == 0) Data32 = (uint32_t)((float)(Pre_DAC_Value - Offset_DAC) / DAC_DATA_SCALE + 0.55f);
		else Data32 = -(int32_t)((float)(Pre_DAC_Value - Offset_DAC) / DAC_DATA_SCALE + 0.55f);
		
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'V';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Data32);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_WL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_WL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"	
	} //--------------------------------------------------------------------
	else if (CmdID == SET_FREQ)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'R';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_FREQ)
	{
		Set_Freq = (uint32_t)(1000*(C_SPEED/(float)(Ch_Wavelength)) + 0.55f);
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'R';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_CH)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = Ch_Num;
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_CH)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = Ch_Num;
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_WL_UP)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'U';
		RxTx_buf[Index ++] = 'P';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_WL_DN)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'N';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Ch_Wavelength);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_CH_UP)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 'U';
		RxTx_buf[Index ++] = 'P';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = Ch_Num;
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_CH_DN)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'N';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = Ch_Num;
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_BW_CH)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = User_Ch_Num;
		
			RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10.f+0.55f) >> 8;
			RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10.f+0.55f);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_BW_FR)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'R';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq);
		
			RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10.f+0.55f) >> 8;
			RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10.f+0.55f);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_BW_WL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Set_WL >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Set_WL >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Set_WL >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Set_WL);
		
			RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10.f+0.55f) >> 8;
			RxTx_buf[Index ++] = (uint16_t)(Ch_BW/10.f+0.55f);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_BW_TB)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = Total_ChNum;
			for (Length = 0; Length < 5; Length ++)
			{
				RxTx_buf[Index ++] = TTable[Length] >> 8;
				RxTx_buf[Index ++] = TTable[Length];
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{	
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[0][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[0][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[1][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[1][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[2][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[2][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[3][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[3][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[4][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(BW_Table[4][Length]/10.f+0.55f);
			}
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_IL_CH)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'I';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = User_Ch_Num;
		
			RxTx_buf[Index ++] = (uint16_t)(Ch_ILoss/10.f+0.55f) >> 8;
			RxTx_buf[Index ++] = (uint16_t)(Ch_ILoss/10.f+0.55f);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_IL_FR)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'I';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'R';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Set_Freq);
		
			RxTx_buf[Index ++] = (uint16_t)(Ch_ILoss/10.f+0.55f) >> 8;
			RxTx_buf[Index ++] = (uint16_t)(Ch_ILoss/10.f+0.55f);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_IL_WL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'I';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Set_WL >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Set_WL >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Set_WL >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Set_WL);
		
			RxTx_buf[Index ++] = (uint16_t)(Ch_ILoss/10.f+0.55f) >> 8;
			RxTx_buf[Index ++] = (uint16_t)(Ch_ILoss/10.f+0.55f);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_IL_TB)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'I';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = Total_ChNum;
			for (Length = 0; Length < 5; Length ++)
			{
				RxTx_buf[Index ++] = TTable[Length] >> 8;
				RxTx_buf[Index ++] = TTable[Length];
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[0][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[0][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[1][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[1][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[2][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[2][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[3][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[3][Length]/10.f+0.55f);
			}
			for (Length = 0; Length < Total_ChNum; Length ++)
			{
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[4][Length]/10.f+0.55f) >> 8;
				RxTx_buf[Index ++] = (uint16_t)(IL_Table[4][Length]/10.f+0.55f);
			}
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_VTABLE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'V';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = ETableLength >> 8;
			RxTx_buf[Index ++] = ETableLength;
			
			for (Length = ETableLength; Length > 0; Length --)
			{
				UartData32 = ETable_Freq[Length-1] + Freq_Offset;
				UartData32 = (uint32_t)(1000.f*C_SPEED/(float)(UartData32) + 0.5f);
				RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 24);
				RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 16);
				RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 8);
				RxTx_buf[Index ++] = (uint8_t)(UartData32);
				
				if (ETable_Pos[Length-1] < 32768)
				{
					UartData32 = sqrtf((32768.f - ETable_Pos[Length-1])/32768.f)*(65535 - Offset_DAC) + Offset_DAC;
					if (UartData32 > 65535) UartData32 = 65535;
					Data32 = -(int32_t)((UartData32 - Offset_DAC)/DAC_DATA_SCALE + 0.5f);
				}
				else
				{
					UartData32 = sqrtf((ETable_Pos[Length-1] - 32768.f)/32768.f)*(65535 - Offset_DAC) + Offset_DAC;
					if (UartData32 > 65535) UartData32 = 65535;
			
					Data32 = (int32_t)((UartData32 - Offset_DAC)/DAC_DATA_SCALE + 0.5f);
				}
				RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
				RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
				RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
				RxTx_buf[Index ++] = (uint8_t)(Data32);
			}
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SCAN_WL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'L';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = ScanMode;
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = ScanSpeed;
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = ScanStartCh;
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = ScanEndCh;
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_FW_VER)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'V';
		RxTx_buf[Index ++] = 'R';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = FW_VER_BYTE_1;
			RxTx_buf[Index ++] = FW_VER_BYTE_2;
			RxTx_buf[Index ++] = '.';
			RxTx_buf[Index ++] = FW_VER_BYTE_3;
			RxTx_buf[Index ++] = FW_VER_BYTE_4;
			RxTx_buf[Index ++] = '.';
			RxTx_buf[Index ++] = FW_VER_BYTE_5;
			RxTx_buf[Index ++] = FW_VER_BYTE_6;
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_FLASH_VER)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 'V';
		RxTx_buf[Index ++] = 'R';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = Flash_ver[0];
			RxTx_buf[Index ++] = Flash_ver[1];
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == GET_M_DATE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'M';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'A';
		RxTx_buf[Index ++] = 'T';

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			for (Length = 0; Length < M_DATE_LENGTH; Length ++)
				RxTx_buf[Index ++] = Manufacture_date[Length];
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	} //--------------------------------------------------------------------
	else if (CmdID == SET_OFFSET)
	{
		UartData32 = (uint32_t)((float)(Offset_DAC)/DAC_DATA_SCALE + 0.55f);
		
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'F';
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 24);
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(UartData32);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //------------------------------------------------------------------
	else if (CmdID == GET_OFFSET)
	{
		UartData32 = (uint32_t)((float)(Offset_DAC)/DAC_DATA_SCALE + 0.55f);
		
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 24);
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 16);
			RxTx_buf[Index ++] = (uint8_t)(UartData32 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(UartData32);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //----------------------------------------------------------------
	else if (CmdID == GET_RW_SIGNAL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = '0';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //--------------------------------------
	else if (CmdID == GET_EQS_SIGNAL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'Q';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = '0';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //--------------------------------------
	else if (CmdID == GET_PK_EQS0)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'Q';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //--------------------------------------
	else if (CmdID == GET_ETABLE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		//----------------------------------------------
		if ((ErrorCode == RX_OK)&&(ETable_Ready))
		{
			RxTx_buf[Index ++] = ETableLength >> 8;
			RxTx_buf[Index ++] = ETableLength;
			
			for (Length = 0; Length < ETableLength; Length ++)
			{
				RxTx_buf[Index ++] = (uint8_t)(ETable_Freq[Length] >> 8);
				RxTx_buf[Index ++] = (uint8_t)(ETable_Freq[Length]);
				
				RxTx_buf[Index ++] = (uint8_t)(ETable_Pos[Length] >> 8);
				RxTx_buf[Index ++] = (uint8_t)(ETable_Pos[Length]);
			}
		}
		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //-------------------------------------------------------------------
	else if (CmdID == SET_ETABLE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //---------------------------------------------------------------------
	else if (CmdID == GET_PTABLE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		PTable_Ready=1;
		//----------------------------------------------
		if ((ErrorCode == RX_OK)&&(PTable_Ready))
		{
			RxTx_buf[Index ++] = ETableLength >> 8;
			RxTx_buf[Index ++] = ETableLength;
			
			for (Length = 0; Length < CAL_HEAD_SIZE; Length ++)
			{
				RxTx_buf[Index ++] = (uint8_t)(PTable_ADC[0][Length] >> 8);
				RxTx_buf[Index ++] = (uint8_t)(PTable_ADC[0][Length]);
			}
			
			for (Length = 0; Length < ETableLength; Length ++)
			{
				RxTx_buf[Index ++] = (uint8_t)(PTable_ADC[Length][0] >> 8);
				RxTx_buf[Index ++] = (uint8_t)(PTable_ADC[Length][0]);
			}
		}
		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //-------------------------------------------------------------------
	else if (CmdID == SET_PTABLE)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = ((Index - 7)/2) >> 8;
		RxTx_buf[6] = (Index - 7)/2;		// Number of words for "Length"
	} //---------------------------------------------------------------------
	else if (CmdID == START_FREQ_CAL)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'A';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		RxTx_buf[Index ++] = MTF_Mode;
		RxTx_buf[Index ++] = Ref_LaserIndex;
		
		RxTx_buf[Index ++] = ETable_Pos[Ref_LaserIndex] >> 8;
		RxTx_buf[Index ++] = ETable_Pos[Ref_LaserIndex];
		
		if ( (MTF_Mode==FREQ_POW_CAL_LASER) || (MTF_Mode==FREQ_POW_CAL_LASER_OLD) )
		{
			RxTx_buf[Index ++] = PTable_ADC[Ref_LaserIndex][0] >> 8;
			RxTx_buf[Index ++] = PTable_ADC[Ref_LaserIndex][0];
		}
		else if ((MTF_Mode == FP_CAL_Ch0_LOW_POWER) || (MTF_Mode==FP_CAL_Ch0_LOW_POWER_OLD))
		{
			RxTx_buf[Index ++] = PTable_ADC[0][Ref_LaserIndex] >> 8;
			RxTx_buf[Index ++] = PTable_ADC[0][Ref_LaserIndex];
		}
				
		RxTx_buf[Index ++] = (uint16_t)ratioP >> 8;
		RxTx_buf[Index ++] = (uint16_t)ratioP;
		
		RxTx_buf[Index ++] = WF_PeakIndex >> 8;
		RxTx_buf[Index ++] = WF_PeakIndex;


		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	} //-----------------------------------------------------------------
	else if (CmdID == START_CH_TEST)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'C';
		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		RxTx_buf[Index ++] = NumOfFreqCal;		// NumOfFreqCal = number test points each channel
		RxTx_buf[Index ++] = Ch_Num;
		
		if (ErrorCode == RX_OK)
		{
			for (Length = 0; Length < NumOfFreqCal; Length ++)
			{
				Ieee32.Float32 = Ch_Freq[Length];
				RxTx_buf[Index ++] = Ieee32.StrValue[0];
				RxTx_buf[Index ++] = Ieee32.StrValue[1];
				RxTx_buf[Index ++] = Ieee32.StrValue[2];
				RxTx_buf[Index ++] = Ieee32.StrValue[3];

				RxTx_buf[Index ++] = Ch_SignalPower[Length] >> 8;
				RxTx_buf[Index ++] = Ch_SignalPower[Length];				
			}
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	} //-----------------------------------------------------------------
	else if (CmdID == MEASURE_SIGNAL_FWF)	// Measure EQS data after E_Table available
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;

		RxTx_buf[Index ++] = 'M';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;
		
		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = WF_Length >> 8;
			RxTx_buf[Index ++] = WF_Length;
		}
		else
		{
			RxTx_buf[Index ++] = 0;
			RxTx_buf[Index ++] = 0;
		}
		RxTx_buf[Index ++] = MTF_Mode;
		RxTx_buf[Index ++] = Ch_Num;
		//----------------------------------------------------------------------------
		RxTx_buf[Index ++] = (ScanStartFreq + EQS_Space * (WF_PeakIndex - WF_Length/2)) >> 8;
		RxTx_buf[Index ++] = (ScanStartFreq + EQS_Space * (WF_PeakIndex - WF_Length/2));
		//----------------------------------------------------------------------------
		if (ErrorCode == RX_OK)
		{
			OnMTF_GetFilterWaveform(&RxTx_buf[Index], SignalCh_Eqs);
			Index += WF_Length*2;
		}
		
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	} //----------------------------------------------------------------
	else if (CmdID == GET_TEMP)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = thmist_ADC >> 8;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[Index ++] = thmist_ADC;
		RxTx_buf[Index ++] = T_SensorID;		// Temp sensor ID

		if (T_SensorID == TEMP_PCB)
		{
			RxTx_buf[Index ++] = Tmp125 >> 8;
			RxTx_buf[Index ++] = Tmp125;
		}
		else
		{
			RxTx_buf[Index ++] = TmpR >> 8;
			RxTx_buf[Index ++] = TmpR;
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	//---------------------------------------------
	else if (CmdID == FLASH_DL)							// download flash page
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[Index ++] = FlashBlockID >> 8;			// echo back "download Block number"
		RxTx_buf[Index ++] = FlashBlockID;

		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	else if (CmdID == FLASH_UP)							// Upload flash page
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'U';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'H';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[Index ++] = FlashBlockID >> 8;			// echo back "download Block number"
		RxTx_buf[Index ++] = FlashBlockID;

		if (ErrorCode == RX_OK) Index += 8*FLASH_PAGE_SIZE;
		else
		{
			RxTx_buf[Index ++] = Data_Checksum16;
			RxTx_buf[Index ++] = Rx_Checksum16;
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	else if (CmdID == GET_SN_PN)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			for (Length = 0; Length < MODULE_SN_LENGTH; Length ++)
				RxTx_buf[Index ++] = Module_SN[Length];

			for (Length = 0; Length < MODULE_PN_LENGTH; Length ++)
				RxTx_buf[Index ++] = Module_PN[Length];
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	else if (CmdID == GET_SN_FV)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'V';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		if (ErrorCode == RX_OK)
		{
			for (Length = 0; Length < MODULE_SN_LENGTH; Length ++)
				RxTx_buf[Index ++] = Module_SN[Length];
			for (Length = 0; Length < MODULE_PN_LENGTH; Length ++)
				RxTx_buf[Index ++] = Module_PN[Length];

			RxTx_buf[Index ++] = FW_VER_BYTE_1;
			RxTx_buf[Index ++] = FW_VER_BYTE_2;
			RxTx_buf[Index ++] = '.';
			RxTx_buf[Index ++] = FW_VER_BYTE_3;
			RxTx_buf[Index ++] = FW_VER_BYTE_4;
			RxTx_buf[Index ++] = '.';
			RxTx_buf[Index ++] = FW_VER_BYTE_5;
			RxTx_buf[Index ++] = FW_VER_BYTE_6;
		
			for (Length = 0; Length < M_DATE_LENGTH; Length ++)
				RxTx_buf[Index ++] = Manufacture_date[Length];
		}
		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	else if (CmdID == RESET_MTF)							// Soft Reset MTF
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	else if (CmdID == STOP_MTF)							// Stop continuous running mode
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	else if (CmdID == SET_SCANPOS)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Scan_Pos_Offset >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Scan_Pos_Offset);
			RxTx_buf[Index ++] = (uint8_t)(Scan_Pos_Resolution >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Scan_Pos_Resolution);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	}
	else if (CmdID == SET_WienerSNR)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		if (ErrorCode == RX_OK)
		{
			//RxTx_buf[Index ++] = (uint8_t)(WSNR >> 8);
			//RxTx_buf[Index ++] = (uint8_t)(WSNR);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	}
	else if (CmdID == LOG_Cmd)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'L';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = '0';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		if (ErrorCode == RX_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(00 >> 8);
			RxTx_buf[Index ++] = (uint8_t)(00);
		}
		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	}
	else if (CmdID == DEMO_Cmd)
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'D';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'M';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = ErrorCode >> 8;
		RxTx_buf[Index ++] = ErrorCode;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[6] = (Index - 7)/2;		// Number of WORDs for "Length"
	}
	else if (CmdID == Boot_Cmd)							// Soft Reset MTF
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'B';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 'T';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	//---------------------------------------------	
	else
	{
		RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'O';
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// Length word

		RxTx_buf[Index ++] = ErrorCode >>8;
		RxTx_buf[Index ++] = ErrorCode;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = Slot_ID;

		RxTx_buf[5] = (Index - 7) >> 9;
		RxTx_buf[6] = (Index - 7) >> 1;
	}
	//---------------------------------------------
	Length = Index;
	//---------------------------------------------------------------------
	for (Index = 1; Index < Length; Index ++) Tx_Checksum16 += RxTx_buf[Index];
	RxTx_buf[Length ++] = Tx_Checksum16 >> 8;
	RxTx_buf[Length ++] = Tx_Checksum16;
	//---------------------------------------------
	OnMTF_UART_Tx(Length);
	//---------------------------------------------
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_TxData_2(uint16_t *pData, uint16_t DataLength, uint8_t CmdType)
{
	uint16_t Index, Length;
	uint8_t Data8;
	
	Index = 0;
	RxTx_buf[Index ++] = SYS_UART_HEAD_AA;
	if (CmdType == RW_SIGNAL_CMD)
	{
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = '0';
		RxTx_buf[Index ++] = 0;		// length word
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 0;		// error code word
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;		// SlotID
		RxTx_buf[Index ++] = 0;
	}
	else if (CmdType == RW_NOISE_CMD)
	{
		RxTx_buf[Index ++] = 'R';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = '0';		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
	}
	else if (CmdType == EQS_SIGNAL_CMD)
	{
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'Q';
		RxTx_buf[Index ++] = 'S';
		RxTx_buf[Index ++] = '0';
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = (uint16_t)((int16_t)(SignalCh_Eqs[WF_PeakIndex]-32768)) >> 8;
		RxTx_buf[Index ++] = (uint16_t)((int16_t)(SignalCh_Eqs[WF_PeakIndex]-32768));
		RxTx_buf[Index ++] = WF_PeakIndex >> 8;
		RxTx_buf[Index ++] = WF_PeakIndex;
	}
	else if (CmdType == EQS_NOISE_CMD)
	{
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'Q';
		RxTx_buf[Index ++] = 'N';
		RxTx_buf[Index ++] = '0';
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
	}
	else if (CmdType == SIGNAL_FILTER_WF_CMD)
	{
		RxTx_buf[Index ++] = 'G';
		RxTx_buf[Index ++] = 'W';
		RxTx_buf[Index ++] = 'F';
		RxTx_buf[Index ++] = 'S';
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
	}
	else if (CmdType == ANALYZE_SIGNAL_CMD)
	{
		RxTx_buf[Index ++] = 'P';
		RxTx_buf[Index ++] = 'E';
		RxTx_buf[Index ++] = 'Q';
		RxTx_buf[Index ++] = 'S';
		
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;

		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
	}
	//-----------------------------------------------
	RxTx_buf[Index ++] = DataLength >> 8;
	RxTx_buf[Index ++] = DataLength;
	//-----------------------------------------------
	RxTx_buf[5] = (Index + (2*DataLength) - 7) >> 9;
	RxTx_buf[6] = (Index + (2*DataLength) - 7) >> 1;
	//-----------------------------------------------
	Length = Index;		Tx_Checksum16 = 0;
	
	if (Rx_Cmd_Type == UART_PORT_CMD)
	{
		for (Index = 0; Index < Length; Index ++)
		{
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = RxTx_buf[Index];
			if (Index > 0) Tx_Checksum16 += RxTx_buf[Index];
		}
		for (Index = 0; Index < DataLength; Index ++)
		{
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = pData[Index] >> 8;
			Tx_Checksum16 += (pData[Index] >> 8) & 0x00FF;

			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = pData[Index] & 0x00FF;
			Tx_Checksum16 += (pData[Index] & 0x00FF);
		}
		while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
		MTF_UART->UART_THR = Tx_Checksum16 >> 8;
	
		while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
		MTF_UART->UART_THR = Tx_Checksum16 & 0x00FF;
	}
	else
	{
		for (Index = 0; Index < Length; Index ++)
		{
			udi_cdc_putc(RxTx_buf[Index]);
			if (Index > 0) Tx_Checksum16 += RxTx_buf[Index];
		}
		for (Index = 0; Index < DataLength; Index ++)
		{
			Data8 = (pData[Index] >> 8) & 0x00FF;
			udi_cdc_putc(Data8);
			Tx_Checksum16 += Data8;

			Data8 = pData[Index] & 0x00FF;
			udi_cdc_putc(Data8);
			Tx_Checksum16 += Data8;
		}
		Data8 = (Tx_Checksum16 >> 8) & 0x00FF;
		udi_cdc_putc(Data8);
		Data8 = (Tx_Checksum16 & 0x00FF);
		udi_cdc_putc(Data8);
	}
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_DownloadETable(void)
{
	uint16_t Index, k;
	
	k = 13;
	for (Index = 0; Index < ETableLength; Index ++)
	{
		ETable_Freq[Index] = RxTx_buf[k ++] << 8;
		ETable_Freq[Index] += RxTx_buf[k ++];
	}
	for (Index = 0; Index < ETableLength; Index ++)
	{
		ETable_Pos[Index] = RxTx_buf[k ++] << 8;
		ETable_Pos[Index] += RxTx_buf[k ++];
	}
	ETable_Ready = TRUE;
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_DownloadPTable(void)
{
	uint16_t Index, k;
	
	// Set default P_Table head, can be changed by flash data
	PTable_head[0] = 256*(128 - 9);	// -10dBm
	PTable_head[1] = 256*(128 - 19);	// -20dBm
	PTable_head[2] = 256*(128 - 29);	// -30dBm
	PTable_head[3] = 256*(128 - 39);	// -40dBm
	PTable_head[4] = 256*(128 - 49);	// -50dBm
	
	k = 11;
	for (Index = 0; Index < CAL_HEAD_SIZE; Index ++) //CH#0 ADC-count
	{
		PTable_ADC[0][Index] = RxTx_buf[k ++] << 8;
		PTable_ADC[0][Index] += RxTx_buf[k ++];
		
		Laser_Ch0Power[Index] = PTable_head[Index];	//256*(128-10)
	}
	for (Index = 0; Index < ETableLength; Index ++)	//profile ADC-count
	{
		PTable_ADC[Index][0] = RxTx_buf[k ++] << 8;
		PTable_ADC[Index][0] += RxTx_buf[k ++];
		
		Laser_ChPower[Index] = PTable_head[0];		//256*(128-10), equal input power for each channel, -10dBm
	}
/*	
	// C-band profile, all PTable_head[0] in dBm
	for (Index=0; Index<ETableLength; Index++)
	{
		Laser_ChPower[Index] = PTable_head[0];		//256*(128-10), equal input power for each channel, -10dBm
	}
	
	// Channel #0 PTable_head in dBm
	Laser_Ch0Power[0] = PTable_head[0];	//256*(128-10)
	Laser_Ch0Power[1] = PTable_head[1]; //256*(128-20)
	Laser_Ch0Power[2] = PTable_head[2]; //256*(128-30)
	Laser_Ch0Power[3] = PTable_head[3]; //256*(128-40)
	Laser_Ch0Power[4] = PTable_head[4];	//256*(128-50)
*/	
	OnMTF_Create_PTable();
	OnMTF_Creat_ETable();
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_Download_CalTable(void)
{
	uint16_t Index, k;
	
	k = 13;
	for (Index = 0; Index < ETableLength; Index ++)
	{
		ETable_Freq[Index] = RxTx_buf[k ++] << 8;
		ETable_Freq[Index] += RxTx_buf[k ++];
	}
	for (Index = 0; Index < ETableLength; Index ++)
	{
		ETable_Pos[Index] = RxTx_buf[k ++] << 8;
		ETable_Pos[Index] += RxTx_buf[k ++];
	}
	ETable_Ready = TRUE;
	//-------------------------------------------------
	for (Index = 0; Index < CAL_HEAD_SIZE; Index ++)
	{
		PTable_head[Index] = RxTx_buf[k ++] << 8;
		PTable_head[Index] += RxTx_buf[k ++];		
	}
	for (Index = 0; Index < CAL_HEAD_SIZE; Index ++)
	{
		for (Ch_Num = 0; Ch_Num < ETableLength; Ch_Num ++)
		{
			PTable_ADC[Ch_Num][Index] = RxTx_buf[k ++] << 8;
			PTable_ADC[Ch_Num][Index] += RxTx_buf[k ++];
		}
	}
	PTable_Ready = TRUE;
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_Download_FERC_Table(void)
{
	uint16_t Index, k;
	
	k = 13;
	for (Index = 0; Index < ERC_TableLength; Index ++)
	{
		Ieee32.StrValue[0] = RxTx_buf[k ++];
		Ieee32.StrValue[1] = RxTx_buf[k ++];
		Ieee32.StrValue[2] = RxTx_buf[k ++];
		Ieee32.StrValue[3] = RxTx_buf[k ++];
		Ch_FreqERC[Index] = Ieee32.Float32;
	}
	FERC_Table_Ready = TRUE;
}
//////////////////////////////////////////////////////////////////////////////////