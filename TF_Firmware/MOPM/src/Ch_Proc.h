///////////////////////////////////////////////////////////////////
// Head file: Ch_Proc.h
#ifndef __CH_PROC_H__
#define	__CH_PROC_H__

///////////////////////////////////////////////////////////////////
#define VENDOR_NAME_ADDR			0
#define VENDOR_NAME_LENGTH			8
#define FLASH_VER_ADDR				8
#define FLASH_VER_LENGTH			2

#define MODULE_SN_ADDR				10
#define MODULE_SN_LENGTH			24

#define MODULE_PN_ADDR				40
#define MODULE_PN_LENGTH			24

#define M_DATE_ADDR					70
#define M_DATE_LENGTH				10		// ASCII format: MM/DD/YYYY (5 words)

#define HW_VER_ADDR					90
#define HW_VER_LENGTH				06

#define CUSTOMER_SN_ADDR			100
#define CUSTOMER_SN_LENGTH			24

#define CUSTOMER_PN_ADDR			130
#define CUSTOMER_PN_LENGTH			30

#define ORACLE_NUM_ADDR				160
#define ORACLE_NUM_LENGTH			24

#define WID_NUM_ADDR				200
#define WID_NUM_LENGTH				30

#define MODULE_TYPE_ADDR			298		// 0=MTF(default); 100=MOPM; 200=MOSA
#define MODULE_TYPE_LENGTH			1
/////////////////////////////////////////////////////////////////////////////////////
// Channel-1 is the first Customer channel,
// Channel-N is the last customer channel
#define CH1_FREQ_ADDR						300
#define CHN_FREQ_ADDR						304
#define CH1_WL_ADDR							308
#define CHN_WL_ADDR							312

#define CH_SPACE_ADDR						316
#define CUSTOMER_CH_NUM_ADDR				318

#define FREQ_OFFSET_ADDR					320
/////////////////////////////////////////////////////////////////////////////////////
// Scan parameters
#define SCAN_POS_OFFSET_ADDR				350
#define SCAN_POS_RESOLUTION_ADDR			354
#define EQWL_deltaWL_ADDR					358
#define EQS_Space_ADDR						360

/////////////////////////////////////////////////////////////////////////////////////
// Peak Finding parameters
#define MinPeakLevel_ADDR					380
#define Contrast_ADDR						382

/////////////////////////////////////////////////////////////////////////////////////
// Sidelobe Filter parameters
#define Sidelobe_lmt_ADDR					390
#define Sidelobe_alpha_ADDR					392

/////////////////////////////////////////////////////////////////////////////////////////////
//#define TOTAL_CH_NUM			88					// Use number defined in flash
//#define TOTAL_CH_NUM_PLUS		TOTAL_CH_NUM + 2

#define NUM_OF_TEMP				5				// Change T table size from 10 to 5
#define TEMP_TABLE_SIZE			NUM_OF_TEMP
#define ITU_WL_TABLE_SIZE		128				// 512
#define IL_WL_TABLE_SIZE		128				// 512


#define IL_TABLE_SIZE			128				// Max
#define BW_TABLE_SIZE			128				// Max
#define CH_V_TABLE_SIZE			128				// 1024
#define RC_TABLE_SIZE			128				// maximum channel 128
#define NUM_OF_RC_TABLE			6				// 3-set of T-V tables

#define POWER_TABLE_SIZE		128				// 120(CH)+5(HEADER)=125; Zeros to 128.
#define PSF_TABLE_SIZE			512
/////////////////////////////////////////////////////////////////////////////////////////////
// Calibration Table address in Flash
#define TEMP_TABLE_ADDR			512
//#define XY_POINT_MAPPING_ADDR	600
//#define IL_POINT_MAPPING_ADDR	800		// not used for 1-point/channel

#define ITU_WL_TABLE_ADDR		1024
#define CH_V_TABLE_ADDR			2048

#define IL_TABLE_ADDR			16384
#define BW_TABLE_ADDR			18432

//#define ChPower_ADDR			19968

#define PowerADC_TABLE_ADDR		20480	// 5T convoluted cal. table (ADC count)
#define ChPower_ADDR			23040	// 5T de-convoluted peak power table (ratio total power in Q8 format, 256*(128+ratio Input_Power_dBm) )
#define PSFADC_TABLE_ADDR		25600
#define PSFADC_TABLE_ADDR2		26112

#define IL_WL_TABLE_ADDR		30720

#define CHV_BYTES				4
#define BT_BYTES				4
#define WL_BYTES				4
#define IL_BYTES				2
#define BW_BYTES				2
#define TT_BYTES				2
#define PW_BYTES				2
#define PSF_BYTES				2
/////////////////////////////////////////////////////////////////////////////////////////////
#define HW_FAIL_DC_DC_STATUS				0x01;
#define HW_FAIL_MONITOR_VOLTAGE				0x02;
#define HW_FAIL_INTERNAL_TEMP				0x04;
// Reserved bits from bit-5 to bit-14
#define HW_FAIL_CALIBRATION_DATA			0x80;		

/////////////////////////////////////////////////////////////////////////////////////
void OnLoad_CalibrationTables(void);

void OnGetTempTableIndex(void);
void OnLoadTemp_Table(void);
void OnGet_Head_Info(void);

void OnLoadChV_Table(int32_t* Data, uint8_t Temp_Index);
void OnLoadIL_dB_Table(int16_t* Data, uint8_t Temp_Index);
void OnLoad_BW_Table(int16_t* Data, uint8_t Temp_Index);

void OnLoadITU_Table(uint32_t* Data);

void OnSetTF_Position(uint8_t SetChNum, int32_t SetWL);
void OnGet_ChControlV_DAC(uint8_t SetChNum, int32_t SetWL);
void OnGetInsertionLoss(uint8_t ChNum, int32_t ILoss_WL);
void OnGetFilterBW(uint8_t ChNum, int32_t ILoss_WL);
void OnGet_V_atWL(uint8_t SetChNum, int32_t SetWL, uint16_t Index_T, float* DataV);
void OnLoadRcT_Table(float* Data, uint8_t PageNum);
void OnLoadRcV_Table(int32_t* Data, uint8_t PageNum);

uint8_t OnGetWL_ChNum(int32_t SetWL);
int32_t OnGetChNum_WL(uint8_t SetChNum);

/////////////////////////////////////////////////////////////////////////////////////
// void OnLoadPower_Table(void);
void OnLoadPSF_Table(void);
///////////////////////////////////////////////////////////////////
#endif // __CH_PROC_H__
