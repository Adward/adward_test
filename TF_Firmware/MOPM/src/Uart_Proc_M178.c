 /////////////////////////////////////////////////////////////////////////////
 //  Uart_Proc_M178.c														//
 //																			//
 /////////////////////////////////////////////////////////////////////////////
#include "MTF.h"

uint8_t M178_CmdBuf[UART_BUF_SIZE];
uint8_t M178_SignalCH[80];;

//////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_Decode_M178(void)
{
	uint16_t	index;
	uint32_t	Cmd_ID;
	uint32_t	Payload1, Payload2_CmdSub;
	uint32_t	Rx_Checksum32, Data_Checksum32;
	uint32_t	statusInstall;
	
	//-------------------------------------------------
	// Get Command Bytes
	index = 1;
	Cmd_ID  = (int32_t)(RxTx_buf[index ++]) << 24;
	Cmd_ID += (int32_t)(RxTx_buf[index ++]) << 16;
	Cmd_ID += (int32_t)(RxTx_buf[index ++]) << 8;
	Cmd_ID += (int32_t)(RxTx_buf[index ++]);
		
	if (Rx_Length > 0x14)										// Header + Footer + 1-Reserved = 20 bytes
	{
		index = 9;
		Payload1  = (int32_t)(RxTx_buf[index ++]) << 24;	// Cmd_Reserved for FW upgrade only.
		Payload1 += (int32_t)(RxTx_buf[index ++]) << 16;
		Payload1 += (int32_t)(RxTx_buf[index ++]) << 8;
		Payload1 += (int32_t)(RxTx_buf[index ++]);
		
		//index = 13;
		Payload2_CmdSub  = (int32_t)(RxTx_buf[index ++]) << 24;
		Payload2_CmdSub += (int32_t)(RxTx_buf[index ++]) << 16;
		Payload2_CmdSub += (int32_t)(RxTx_buf[index ++]) << 8;
		Payload2_CmdSub += (int32_t)(RxTx_buf[index ++]);		
	}
	else
	{
		Payload2_CmdSub = 0;											// No payload
	}
	//-------------------------------------------------
	// Get RxCheckSum double word
	Rx_Checksum32  = (int32_t)(RxTx_buf[Rx_Length + CMD_OVERHEAD_M178 - 4]) << 24;
	Rx_Checksum32 += (int32_t)(RxTx_buf[Rx_Length + CMD_OVERHEAD_M178 - 3]) << 16;
	Rx_Checksum32 += (int32_t)(RxTx_buf[Rx_Length + CMD_OVERHEAD_M178 - 2]) << 8;
	Rx_Checksum32 += (int32_t)(RxTx_buf[Rx_Length + CMD_OVERHEAD_M178 - 1]);
	Uart_CmdID = NORMAL_CMD;			// Reset Uart_CmdID
	//-------------------------------------------------
	Data_Checksum32 = 0;
	for (index = 1; index < ((Rx_Length-4) + CMD_OVERHEAD_M178-1); index+=4)
	{
		Data32 = 0;
		Data32 += (int32_t)(RxTx_buf[index+0]) << 24;
		Data32 += (int32_t)(RxTx_buf[index+1]) << 16;
		Data32 += (int32_t)(RxTx_buf[index+2]) << 8;
		Data32 += (int32_t)(RxTx_buf[index+3]);
		Data_Checksum32 += Data32;	
	}
	Data_Checksum32 = ~Data_Checksum32 + 1;
	//----------------------------------------------------------------------------------
	if ((Cmd_ID == ID_ScanSignal)&&(Payload2_CmdSub == SubCmd_02) || (Cmd_ID == ID_ScanSignalNoise)&&(Payload2_CmdSub == SubCmd_01))
	{
		if (Data_Checksum32 != Rx_Checksum32) Reply_to_UART_M178(Cmd_ID, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
			Reply_to_UART_M178(Cmd_ID, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else
		{
			OnMTF_SetScanPos2(0);
			OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
			delay_ms(30);
			//-------------------------------------------
			if (Cmd_ID == ID_ScanSignal)		Uart_CmdID = M178_SCAN_Signal;
			if (Cmd_ID == ID_ScanSignalNoise)	Uart_CmdID = M178_SCAN_SignalNoise;
			
			MTF_Mode = MOPM_RW_SCAN;		
			OnMTF_StartScan();
		}
	}
	//----------------------------------------------------------------------------------
	else if ((Cmd_ID == ID_FwDownLoadInit)&&(Payload1==TxMode))
	{
		if (Data_Checksum32 != Rx_Checksum32) Reply_to_UART_M178(ID_FwDownLoadInit, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
			Reply_to_UART_M178(ID_FwDownLoadInit, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else if(onMOPM_FwUp_Init())
			Reply_to_UART_M178(ID_FwDownLoadInit, Payload1, Payload2_CmdSub, M178Cmd_Flash_Error);
		else	
			Reply_to_UART_M178(ID_FwDownLoadInit, Payload1, Payload2_CmdSub, M178Cmd_OK);
	}
	//----------------------------------------------------------------------------------
	else if (Cmd_ID == ID_FwLoad)
	{
		index = 17;	// point to FW segment data
		if (Data_Checksum32 != Rx_Checksum32) 
			Reply_to_UART_M178(ID_FwLoad, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
			Reply_to_UART_M178(ID_FwLoad, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else if(Payload2_CmdSub > DataBlockSize)
			Reply_to_UART_M178(ID_FwLoad, Payload1, Payload2_CmdSub, M178Cmd_SegLength_Error);	
		else if(onMOPM_FwUp_Load(Payload1, Payload2_CmdSub, RxTx_buf))
			Reply_to_UART_M178(ID_FwLoad, Payload1, Payload2_CmdSub, M178Cmd_Flash_Error);
		else
			Reply_to_UART_M178(ID_FwLoad, Payload1, Payload2_CmdSub, M178Cmd_OK);
	}
	//----------------------------------------------------------------------------------
	else if (Cmd_ID == ID_FwInstall)
	{
		if (Data_Checksum32 != Rx_Checksum32) 
			Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
			Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else 
		{
			statusInstall = onMOPM_FwUP_Install();
			switch(statusInstall)
			{
				case M178Cmd_OK:
					Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_OK);
					break;
				case M178Cmd_FlashSel_ERROR:	// not expected location for A or B
					Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_FlashSel_ERROR);
					break;
				case M178Cmd_RxFile_CRC_Error:	// CRC or check sum error for the Rx file.  Not ready yet.
					Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_RxFile_CRC_Error);
					break;
				case M178Cmd_FlashRead_ERROR:	// not implement
					Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_FlashRead_ERROR);
					break;
				default:						// write error from TempC 
					Reply_to_UART_M178(ID_FwInstall, Payload1, Payload2_CmdSub, M178Cmd_FlashInstall_ERROR);
					break;				
			}	 
		}
	}
	//----------------------------------------------------------------------------------
	else if (Cmd_ID == ID_DeviceTempRequest)
	{
		if (Data_Checksum32 != Rx_Checksum32) Reply_to_UART_M178(ID_DeviceTempRequest, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
			Reply_to_UART_M178(ID_DeviceTempRequest, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
			else
			Reply_to_UART_M178(ID_DeviceTempRequest, Payload1, Payload2_CmdSub, M178Cmd_OK);
	}
	//----------------------------------------------------------------------------------
	else if (Cmd_ID == ID_DeviceStatusCheck)
	{
		if (Data_Checksum32 != Rx_Checksum32) Reply_to_UART_M178(ID_DeviceStatusCheck, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
		Reply_to_UART_M178(ID_DeviceStatusCheck, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else
		Reply_to_UART_M178(ID_DeviceStatusCheck, Payload1, Payload2_CmdSub, M178Cmd_OK);
	}
	//----------------------------------------------------------------------------------
	else if (Cmd_ID == ID_FwVerRequest)
	{
		if (Data_Checksum32 != Rx_Checksum32) Reply_to_UART_M178(ID_FwVerRequest, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
		Reply_to_UART_M178(ID_FwVerRequest, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else
		Reply_to_UART_M178(ID_FwVerRequest, Payload1, Payload2_CmdSub, M178Cmd_OK);
	}
	//----------------------------------------------------------------------------------
	else if (Cmd_ID == ID_DeviceReset)
	{
		if (Data_Checksum32 != Rx_Checksum32) Reply_to_UART_M178(ID_DeviceReset, Payload1, Payload2_CmdSub, M178Cmd_CKS_ERROR);
		else if(Rx_Length > (Rx_Index-1))
		Reply_to_UART_M178(ID_DeviceReset, Payload1, Payload2_CmdSub, M178Cmd_MsgLength_Error);
		else
		{
			//Reply_to_UART_M178(ID_DeviceReset, Payload1, Payload2_CmdSub, M178Cmd_OK);
			signature->ackCmdRST = TRUE;		// reply to Host after soft reset
			saveSignature = TRUE;
			onMTF_SaveSignature(saveSignature, signature);
			while (1) NVIC_SystemReset();
		}
		
	}
	//----------------------------------------------------------------------------------
	else 
		Reply_to_UART_M178(Cmd_ID, Payload1, Payload2_CmdSub, M178Cmd_Unknown_ERROR);
	//-----------------------------------------------------------------------------------
}

//////////////////////////////////////////////////////////////////////////////////
// Reply any M178 command no data required
void Reply_to_UART_M178(uint32_t Cmd_ID, uint32_t Payload1, uint32_t Cmd_Sub, uint32_t ErrorCode)
{
	uint8_t i;
	uint32_t Index, Length, Data_Checksum32;
	uint32_t Cmd_Reserved=0;
	Index = 0;
	
	//------------------------------------------
	//Header
	//------------------------------------------
	RxTx_buf[Index ++] = SYS_UART_HEAD_55;
	// 1. Command ID
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID);
	
	
	//------------------------------------------
	// 3. Payload: Command dependent
	Index = 9;
	
	if (Cmd_ID==ID_FwDownLoadInit)
	{
		if (ErrorCode == M178Cmd_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
		}
	}//----------------------------------------------------------------
	else if (Cmd_ID==ID_FwInstall)
	{
		if (ErrorCode == M178Cmd_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
		}
	}//----------------------------------------------------------------
	else if (Cmd_ID==ID_FwLoad)
	{
		if (ErrorCode == M178Cmd_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)(seqNO >> 24);
			RxTx_buf[Index ++] = (uint8_t)(seqNO >> 16);
			RxTx_buf[Index ++] = (uint8_t)(seqNO >> 8);
			RxTx_buf[Index ++] = (uint8_t)(seqNO);
		
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
		}
	}//----------------------------------------------------------------
	else if (Cmd_ID==ID_DeviceStatusCheck)
	{			
		if (ErrorCode == M178Cmd_OK)
		{
			RxTx_buf[Index ++] = (uint8_t)((int32_t)M178_Status_Normal >> 24);
			RxTx_buf[Index ++] = (uint8_t)((int32_t)M178_Status_Normal >> 16);
			RxTx_buf[Index ++] = (uint8_t)((int32_t)M178_Status_Normal >> 8);
			RxTx_buf[Index ++] = (uint8_t)((int32_t)M178_Status_Normal);
		}
	}//----------------------------------------------------------------
	else if (Cmd_ID==ID_DeviceTempRequest)
	{	
		if (ErrorCode == M178Cmd_OK)
		{	
			int16_t	tmpX10;
			
			if (refTmp125)	tmpX10 = Tmp125X10;
			else			tmpX10 = TmpRX10;
			
			RxTx_buf[Index ++] = (uint8_t)((int32_t)tmpX10 >> 24);
			RxTx_buf[Index ++] = (uint8_t)((int32_t)tmpX10 >> 16);
			RxTx_buf[Index ++] = (uint8_t)((int32_t)tmpX10 >> 8);
			RxTx_buf[Index ++] = (uint8_t)((int32_t)tmpX10);
		}		
	}//----------------------------------------------------------------
	else if (Cmd_ID==ID_FwVerRequest || Cmd_ID==ID_DeviceReset)
	{
		if (ErrorCode == M178Cmd_OK)
		{
			// Reserved Word
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
			RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
			
			// 36 byte Reserved
			for (i=0; i<36; i++)	RxTx_buf[Index ++] = 0;
			
			// 37 (14 'FW Ver' + 23 '0') bytes for FW version
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_1;
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_2;
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_3;
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_4;
			
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_M1;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_M2;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_DOT;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_D1;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_D2;
			
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_M1;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_M2;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_DOT;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_D1;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_D2;		
			// 23 byte 0
			for (i=0; i<23; i++)	RxTx_buf[Index ++] = 0;
			
			// 20 (12'FW Ver' + 8 '0') bytes for SN
			RxTx_buf[Index ++] = Manufacture_date[8];	//(uint8_t)M178_SN_Y_1;
			RxTx_buf[Index ++] = Manufacture_date[9];	//(uint8_t)M178_SN_Y_2;			
			RxTx_buf[Index ++] = (uint8_t)M178_SN_S_3;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_S_4;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_M_5;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_C_6;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_V_7;			
			RxTx_buf[Index ++] = (uint8_t)M178_SN_T_8;
			RxTx_buf[Index ++] = Module_SN[6];	//(uint8_t)M178_SN_1_9;
			RxTx_buf[Index ++] = Module_SN[7];	//(uint8_t)M178_SN_2_A;
			RxTx_buf[Index ++] = Module_SN[8];	//(uint8_t)M178_SN_3_B;
			RxTx_buf[Index ++] = Module_SN[9];	//(uint8_t)M178_SN_4_C;			
				// 8 byte 0
			for (i=0; i<8; i++)		RxTx_buf[Index ++] = 0;
			
			// 23 byte 0 for Filter SN
			for (i=0; i<23; i++)	RxTx_buf[Index ++] = 0;
		}
	}//----------------------------------------------------------------
	else
	{	// M178Cmd_Unknown_ERROR
		// Reserved Word
		RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);			
	}
	//---------------------------------------------------------------------
	// 5. Footer: Error Code
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode >> 24);
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode >> 16);
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode >> 8);
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode);
	//---------------------------------------------------------------------
	// 2. Message Length
	Length = Index + sizeof(int32_t) - CMD_OVERHEAD_M178;		// Number of bytes for "Length"
	Index = 5;
	RxTx_buf[Index ++] = (uint8_t)(Length >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Length >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Length >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Length);
	//---------------------------------------------------------------------
	// 6. Footer: Checksum
	Data_Checksum32 = 0;
	for (Index = 1; Index < (Length-4); Index+=4)
	{
		Data32 = 0;
		Data32 += (int32_t)(RxTx_buf[Index+0]) << 24;
		Data32 += (int32_t)(RxTx_buf[Index+1]) << 16;
		Data32 += (int32_t)(RxTx_buf[Index+2]) << 8;
		Data32 += (int32_t)(RxTx_buf[Index+3]);
		Data_Checksum32 += Data32;
	}
	Data_Checksum32 = ~Data_Checksum32 + 1;
	
	Index = Length - sizeof(int32_t) + CMD_OVERHEAD_M178;		//restored Index
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32);
	//---------------------------------------------
	OnMTF_UART_Tx(Length + CMD_OVERHEAD_M178);
	//---------------------------------------------		
}
//////////////////////////////////////////////////////////////////////////////////
// This function is called by main() and used for replying (Cmd_ID==ID_ScanSignal, 0x03) and (Cmd_ID==ID_ScanSignalNoise, 0x05)
void OnMOPM_UART_TxData_M178(uint32_t Cmd_ID, uint32_t ErrorCode, uint8_t NumOfReportCH)
{
	union
	{
		uint8_t StrValue[4];	// High-byte first and Low-byte last
		int32_t Int32;
		float_t Float32;
	} Ieee32;
	
	uint8_t		indexPeak;
	uint8_t		indexCH=0;
	uint32_t	Index, Length, Data_Checksum32;
	
	Index = 0;
	//------------------------------------------
	//Header
	//------------------------------------------
	RxTx_buf[Index ++] = SYS_UART_HEAD_55;
	//---------------------------------------------------------------------
	// 1. Command ID
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Cmd_ID);
	//---------------------------------------------------------------------
	// 3. Payload: Reserved Word, 0
	Index = 9;
	RxTx_buf[Index ++] = (uint8_t)0;
	RxTx_buf[Index ++] = (uint8_t)0;
	RxTx_buf[Index ++] = (uint8_t)0;
	RxTx_buf[Index ++] = (uint8_t)0;
	//---------------------------------------------------------------------
	// 4. Total number of Channels Found
	Data_Checksum32 = (uint32_t)NumOfReportCH;
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32);
	//---------------------------------------------------------------------
	// 5. Found Peak Power/Frequency
	for (indexPeak = 0; indexPeak < NumOfReportCH; indexPeak ++)
	{
		// Peak Power
		// 0. raw peak
		//Ieee32.Float32 = (float32_t)(SignalCh_Eqs[MultiPeaks_Eqs[indexPeak]]-32768)/256;		//256*(128+power), power in dBm Q8 format
		
		// 2. reported channel power (with area method)
		if (Cmd_ID==ID_ScanSignal)
			Ieee32.Float32 = fReportSignal[indexPeak];
		else
		{
			//Ieee32.Float32 = (M178_SignalCH[indexPeak]) ? fReportSignal[indexCH++] : fReportNoise[indexPeak];
			
			if (M178_SignalCH[indexPeak]>0)
				if (fReportSignal[indexCH]>-45)
					Ieee32.Float32 = fReportSignal[indexCH++];
				else
				{
					Ieee32.Float32 = fReportNoise[indexPeak];
					indexCH++;
				}
			else
					Ieee32.Float32 = fReportNoise[indexPeak];
		}
		
		RxTx_buf[Index ++] = Ieee32.StrValue[3];				//MSB first
		RxTx_buf[Index ++] = Ieee32.StrValue[2];
		RxTx_buf[Index ++] = Ieee32.StrValue[1];
		RxTx_buf[Index ++] = Ieee32.StrValue[0];
		
		// Peak Frequency
		if (Cmd_ID==ID_ScanSignal)
			Data_Checksum32 = ScanStartFreq + EQS_Space*MultiPeaks_Eqs[indexPeak] + Freq_Offset - M178_FreqOff;	// frequency offset 180000
		else
			Data_Checksum32 = ScanStartFreq + ReportWL[indexPeak] + Freq_Offset - M178_FreqOff;
			
		RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 24);	//MSB first
		RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32);
	}
	//---------------------------------------------------------------------
	// 6.Error Code
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode >> 24);
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode >> 16);
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode >> 8);
	RxTx_buf[Index ++] = (uint8_t)(ErrorCode);
	//---------------------------------------------------------------------
	// 2. Message Length
	Length = Index + sizeof(int32_t) - CMD_OVERHEAD_M178;		// Number of bytes for "Length"
	Index = 5;
	
	RxTx_buf[Index ++] = (uint8_t)(Length >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Length >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Length >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Length);
	//---------------------------------------------------------------------
	// 7. Footer: Checksum
	Data_Checksum32 = 0;
	for (Index = 1; Index < (Length-4); Index+=4)
	{
		Data32 = 0;
		Data32 += (int32_t)(RxTx_buf[Index+0]) << 24;
		Data32 += (int32_t)(RxTx_buf[Index+1]) << 16;
		Data32 += (int32_t)(RxTx_buf[Index+2]) << 8;
		Data32 += (int32_t)(RxTx_buf[Index+3]);
		Data_Checksum32 += Data32;
	}
	Data_Checksum32 = ~Data_Checksum32 + 1;
	
	Index = Length - sizeof(int32_t) + CMD_OVERHEAD_M178;		//restored Index
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 24);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 16);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32 >> 8);
	RxTx_buf[Index ++] = (uint8_t)(Data_Checksum32);
	//---------------------------------------------------------------------
	OnMTF_UART_Tx(Length + CMD_OVERHEAD_M178);
	//---------------------------------------------------------------------
}
//////////////////////////////////////////////////////////////////////////////////
void OnMOPM_SignalCH_M178(void)
{
	uint8_t pkIndex = 0;
	uint8_t	freqError = 10;	 // 10GHz
	
	for (uint8_t i=0; i<80; i++)
		{
			ReportWL[i] = Ch_Space*(i+1);
			if(abs(Ch_Space*(i+1) - EQS_Space*MultiPeaks_Eqs[pkIndex]) < freqError)
			{
				M178_SignalCH[i] = 1;
				ReportWL[i] = EQS_Space*MultiPeaks_Eqs[pkIndex++];
			}
			else if((Ch_Space*(i+1) - EQS_Space*MultiPeaks_Eqs[pkIndex]) >= freqError)
			{
				M178_SignalCH[i] = 0;
				pkIndex++;
			}
			else
				M178_SignalCH[i] = 0;
		}
}
//////////////////////////////////////////////////////////////////////////////////
void OnMOPM_NoiseDataGen_M178(uint16_t* rawADC, uint16_t* pusdoDeconv)
{
	uint8_t chIndex, windowSize;
	uint16_t dataIndex, diffDeconv, dataOffset;
	float32_t	tmpNoisePower, factor;
	
	OnMOPM_SignalCH_M178();
	
	for (chIndex=0; chIndex<80; chIndex++)
	{
		diffDeconv = pusdoDeconv[chIndex];
		fReportNoise[chIndex] = 0;
		factor = 1;
		
		if ( (chIndex==0 && M178_SignalCH[chIndex+1]) || (chIndex==79 && M178_SignalCH[chIndex-1]) )
		{
			windowSize = Ch_Space/4;
			factor = 1;
		}
		else if( M178_SignalCH[chIndex-1] || M178_SignalCH[chIndex+1] )
		{
			windowSize = Ch_Space/4;
			factor = 1;
		}
		else
			windowSize = Ch_Space/4;
		
		dataOffset = ( Ch_Space*(chIndex+1)-windowSize/2 )/EQS_Space;
		for (dataIndex=0; dataIndex<windowSize; dataIndex++)
		{
			tmpNoisePower = (rawADC[dataOffset+dataIndex]-diffDeconv-500)*0.0056-61.808;		// offset 500 to obtain reasonable results comparing to traditional M178 
			fReportNoise[chIndex] += exp10f(tmpNoisePower/10);
		}
		fReportNoise[chIndex] = 10*log10f(factor*fReportNoise[chIndex]);
		
	}
	
}
//////////////////////////////////////////////////////////////////////////////////
void OnMOPM_SignalPowerCheck_M178(float_t* checkSignal)
{
	int8_t  maxP_Diff = 20;		//max power divergence is 20 dB
	float32_t tmpMaxP = -200;
	uint8_t chIndex, validPeakNum;
	
	validPeakNum = 0;
	// Finding max. Signal power
	for (chIndex = 0; chIndex < NumOfPeaks; chIndex++)
	if (fReportSignal[chIndex] > tmpMaxP) tmpMaxP =fReportSignal[chIndex];
	
	// Pick Up Signal
	for (chIndex = 0; chIndex < NumOfPeaks; chIndex++)
	{
		if ( (tmpMaxP-checkSignal[chIndex]) < maxP_Diff )	//valid signal
		{
			fReportSignal[validPeakNum] = checkSignal[chIndex];
			MultiPeaks_Eqs[validPeakNum] = MultiPeaks_Eqs[chIndex];
			validPeakNum++;
		}
	}
	
	NumOfPeaks = validPeakNum;
	
	// clear old data in MultiPeaks_Eqs[]
	for (chIndex =  NumOfPeaks; chIndex < MAX_NUM_PEAKS; chIndex ++)
	MultiPeaks_Eqs[chIndex] = 0;
}
//////////////////////////////////////////////////////////////////////////////////
