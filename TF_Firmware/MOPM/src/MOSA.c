///////////////////////////////////////////////////////////////////////////////
//
//	MOpm.c:	Main function for MEMS MOPM
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
///////////////////////////////////////////////////////////////////////////////
int main (void)
{
	uint16_t Count = 0;
	uint32_t CurrentTime, PreUpdateTime;
	
	//----------------------------------------------------
	//fpu_enable();
	OnMTF_PCBA_Initial();
	OnMTF_Data_Initial();
	//----------------------------------------------------
	CurrentTime = Sys_ms_Ticks/100;
	PreUpdateTime = CurrentTime;
	
	//----------------------------------------------------
	
	while(1)
	{
		CurrentTime = Sys_ms_Ticks/100;
		if (MOpm_UART_Ready)				//UART Rx done
		{
			if (Rx_head == OptoplexCmd)		OnMTF_UART_Decode();
			else if (Rx_head == User1Cmd)	OnMTF_UART_Decode_USER1();
			else if (Rx_head == M178Cmd )	OnMTF_UART_Decode_M178();
			else if (Rx_head == EchoCmd )	OnMTF_UART_Tx(Rx_Index);
			Rx_head = 0;
			Rx_Index = 0;
			Rx_Length = 0;
			Count = 0;
			MOpm_UART_Ready = 0;
		}
		//----------------------------------------------------
		if (Count > PORT_RX_TIME_OUT)
		{
			Rx_Index = 0;
			Rx_head = 0;
			Rx_Length = 0;
			Count = 0;
			MOpm_UART_Ready = 0;
		}
		//-----------------------------------------------------
		if ((updateT == 1) && ( (MTF_Mode == RUN_NORMAL)||(MTF_Mode == MTF_SCAN)||(MTF_Mode == MOPM_RW_SCAN)) )
		{
			if (PreUpdateTime > CurrentTime) PreUpdateTime = CurrentTime;
			else
			{
				if ((CurrentTime - PreUpdateTime) > TEMP_UPDATE_TIME)
				{
					OnMTF_CheckTemperature(CurrentTime);
					PreUpdateTime = CurrentTime;
				}
			}
		}
		//----------------------------------------------------
		if (MOpm_ScanDone)
		{
			if (((MTF_Mode == MTF_SCAN) && (TxScandata == 1)) || (MTF_Mode == MOPM_RW_SCAN) || (MTF_Mode == MOPM_PSF_GEN))
				OnMTF_ProcessUserCmd();
			
			MOpm_ScanDone = 0;
			if ((MTF_Mode == MTF_SCAN)||(MTF_Mode == MOPM_RW_SCAN)) OnMTF_ScanNext();
			
		}
		
		//----------------------------------------------------
		/* Restart watchdog */
		wdt_restart(WDT);
		
		//----------------------------------------------------
		Delay_us(1000);		// delay 1ms
		//if (Rx_head > 0) Count ++;	// 160115 VG: Fixed UART/USB time out bug.
		if (Rx_Index > 0) Count ++;
	}
	return 1;
}
///////////////////////////////////////////////////////////////////////////////

