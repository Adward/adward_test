///////////////////////////////////////////////////////////////////////////////
//
//	Calibration function for Mems MTF
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void OnMTF_FreqCalibration_Ruler(void)
{
	uint8_t Index, Count;
	volatile uint8_t *IsDataReady;
	
	ScanCount = 0;
	ETable_Ready = FALSE;
	IsDataReady = &ScanData_Ready;
	PeakCenterFreq = ETable_Freq;

	for (Index =  0; Index < MAX_NUM_PEAKS; Index ++) PeakCenterPos[Index] = 0;
	//-------------------------------------------------------------------------
	if (NumOfFreqCal == 0) NumOfFreqCal = 1;
	for (Index = 0; Index < NumOfFreqCal; Index ++)
	{
		*IsDataReady = FALSE;		
		OnMTF_StartScan();
		Sys_Elapsed_time = 0;
		Count = 0;

		while (*IsDataReady == FALSE)
		{
			// check if time-out here	
			delay_ms(100);
			Count += 1;
			// if (Count > 10) break;	// Time-out
		}
		//-------------------------------------------------------------------
		OnMTF_GetPeaknValley(SignalCh_buf, EqsDataLength);
		OnMTF_GetMultiplePeakCenters(SignalCh_buf);
	}
	//-----------------------------------------------------------------------
	for (Index =  0; Index < NumOfPeaks; Index ++) 
		ETable_Pos[Index] = PeakCenterPos[Index] / NumOfFreqCal;

	if (MTF_Mode == FREQ_CAL_RULER_REF_LASER) OnMTF_GetRef_LaserIndex();
	OnMTF_GetRulerPeakFreq_byRefIndex();
	//-----------------------------------------------------------------------
	ETableLength = NumOfPeaks;	// update ETable length based on find number of peaks
	ETable_Ready = TRUE;		// when cal. with ruler
	MTF_Mode = RUN_NORMAL;
	Reply_to_UART(GET_ETABLE, RX_OK);
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_FreqCalibration_Laser(void)
{
	uint8_t Index, Count;
	volatile uint8_t *IsDataReady;
	
	ScanCount = 0;
	IsDataReady = &ScanData_Ready;
	PeakCenterFreq = ETable_Freq;

	//----------------------------------------------------------
	// in cal with single Laser, Ref_LaserIndex = channel Index
	// each channel cab be calibrated individually, not necessary in order
	//
	PeakCenterPos[Ref_LaserIndex] = 0;		// reset before scan
	if (NumOfFreqCal == 0) NumOfFreqCal = 1;
	for (Index = 0; Index < NumOfFreqCal; Index ++)
	{
		*IsDataReady = FALSE;
		OnMTF_StartScan();
		Sys_Elapsed_time = 0;
		Count = 0;

		while (*IsDataReady == FALSE)
		{
			// check if time-out here
			delay_ms(100);
			Count += 1;
			// if (Count > 10) break;	// Time-out
		}
		//-------------------------------------------------------------------
		OnMTF_GetSingleLaserPeakScanPos(SignalCh_buf, RawDataLength, Ref_LaserIndex);
	}
	//-----------------------------------------------------------------------	
	ETable_Pos[Ref_LaserIndex] = PeakCenterPos[Ref_LaserIndex] / NumOfFreqCal;
	//-----------------------------------------------------------------------
	PeakCenterFreq[Ref_LaserIndex] = Ref_LaserFreq - Freq_Offset;	// Ref_LaserFreq set by PC
	ETable_Ready = TRUE;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Freq_Power_Calibration(void)
{
	uint8_t Index, Count, x, tmp_MTF_Mode;
	volatile uint8_t *IsDataReady;
	uint16_t *SignalCh_buf_tmp, index;
	
	ScanCount = 0;
	ETable_Ready = FALSE;
	PTable_Ready = FALSE;
	IsDataReady = &ScanData_Ready;
	PeakCenterFreq = ETable_Freq;
	Uart_CmdID = NORMAL_CMD;
	//----------------------------------------------------------
	// in cal with single Laser, Ref_LaserIndex = channel Index
	// each channel can be calibrated individually, not necessary in order
	//
	PowCal_ratioP = 0;
	PowCal_PeakLevel = 0;
	PeakCenterPos[Ref_LaserIndex] = 0;		// reset before scan
	if (NumOfFreqCal == 0) NumOfFreqCal = 1;
	
	tmp_MTF_Mode = MTF_Mode;				// backup MTF_Mode before to start 2-way scan
	ScanPosIndex = 0;
	OnMTF_SetScanPos2(ScanPosIndex);
	delay_ms(30);
	ScanDir = 0;
	OnMTF_SetEqS_Table_Cal();					// Prepare to scan in 2-way direction. MTF_Mode = MOPM_RW_SCAN.
	
	for (Index = 0; Index < NumOfFreqCal; Index ++)
	{
		*IsDataReady = FALSE;
		OnMTF_StartScan();
		Sys_Elapsed_time = 0;
		Count = 0;

		while (*IsDataReady == FALSE)
		{
			// check if time-out here
			delay_ms(100);
			Count += 1;
			// if (Count > 10) break;	// Time-out
		}
		//-------------------------------------------------------------------
		// Cal. using Raw data

		//* 1-way Scan in calibration	
		SignalCh_buf_tmp = SignalCh_buf;
	
		//* 2-way Scan in calibration
/*		if (ScanDir)
		{
			ScanDir = 0;
			for (index=0; index<ScanPosLength; index++)
				NoiseCh_buf[index] = SignalCh_buf[ScanPosLength-index-1];
			SignalCh_buf_tmp = NoiseCh_buf;
		}
		else
		{
			ScanDir = 1;
			SignalCh_buf_tmp = SignalCh_buf;
		}
*/

		
		
		if ((tmp_MTF_Mode==FREQ_POW_CAL_LASER_OLD || tmp_MTF_Mode==FP_CAL_Ch0_LOW_POWER_OLD)) {
			//0. No Deconvolution (to get PSF)
			OnMTF_GetSingleLaserPeakScanPos(SignalCh_buf_tmp, RawDataLength, Ref_LaserIndex);
		}		
		else {
			//1.
			OnMTF_GetSingleLaserPeakScanPos(SignalCh_buf_tmp, RawDataLength, Ref_LaserIndex);
			
			//-----------------------------------------------------------------------
			ratioP = 0;
			OnMTF_Convert_RawData2EQS(SignalCh_buf_tmp, SignalCh_Eqs);
			if (WF_PeakIndex> (calWindow/2) )
			{
				for (uint8_t Index2 = 0; Index2 < calWindow; Index2 ++)
				{
					ratioP += exp10f(SignalCh_Eqs[WF_PeakIndex - calWindow/2 + Index2]/1801.27);
				}
				if (ratioP != 0)
				{
					ratioP = 100000*exp10f(SignalCh_Eqs[WF_PeakIndex]/1801.27)/ratioP;
				}
			}
		}
		
		PowCal_PeakLevel += MaxPeakLevel;
		PowCal_ratioP += ratioP;
		
		/* Restart watchdog */
		wdt_restart(WDT);
	}
	MTF_Mode = tmp_MTF_Mode;		//Restore MTF_Mode after 5 iteration (NumOfFreqCal=10, 2-way scan). 
	//-----------------------------------------------------------------------
	// Take average for multiple scans
	if ( (MTF_Mode==FREQ_POW_CAL_LASER) || (MTF_Mode==FREQ_POW_CAL_LASER_OLD) )
	{
		ETable_Pos[Ref_LaserIndex] = PeakCenterPos[Ref_LaserIndex] / NumOfFreqCal;
		PTable_ADC[Ref_LaserIndex][0] = PowCal_PeakLevel / NumOfFreqCal;
		PeakCenterFreq[Ref_LaserIndex] = Ref_LaserFreq - Freq_Offset;	// Ref_LaserFreq
	}
	else if ((MTF_Mode == FP_CAL_Ch0_LOW_POWER) || (MTF_Mode==FP_CAL_Ch0_LOW_POWER_OLD))
	{
		PTable_ADC[0][Ref_LaserIndex] = PowCal_PeakLevel / NumOfFreqCal;
	}
	
	//-----------------------------------------------------------------------
	ratioP = PowCal_ratioP / NumOfFreqCal ;
/*	
	if ((tmp_MTF_Mode==FREQ_POW_CAL_LASER || tmp_MTF_Mode==FP_CAL_Ch0_LOW_POWER))
	{
		ratioP = 0;
		OnMTF_Convert_RawData2EQS(SignalCh_EqsDeconv, SignalCh_Eqs);
		if (WF_PeakIndex> (calWindow/2) )
		{
			for (uint8_t Index2 = 0; Index2 < calWindow; Index2 ++)
			{
				ratioP += exp10f(SignalCh_Eqs[WF_PeakIndex - calWindow/2 + Index2]/1801.27);
			}
			if (ratioP != 0)
			{
				ratioP = 100000*exp10f(SignalCh_Eqs[WF_PeakIndex]/1801.27)/ratioP;
			}		
		}
		
	}
*/	
	
	//-----------------------------------------------------------------------
	// Normalize power cal PTable 
	if ((MTF_Mode == FP_CAL_Ch0_LOW_POWER)&&(Ref_LaserIndex == CAL_HEAD_SIZE-1) || (MTF_Mode == FP_CAL_Ch0_LOW_POWER_OLD)&&(Ref_LaserIndex == CAL_HEAD_SIZE-1))
	{
/*  // 20170827 VG: No need to generate PTable when calibration.  Only record raw ADC. 		
		// C-band profile, all PTable_head[0], in dBm and ADC-count
		for (x=0; x<ETableLength; x++)
		{
			Laser_ChPower[x] = PTable_head[0];	//256*(128-10), equal input power for each channel, -10dBm
		}		
		
		// Channel #0 PTable_head in dBm and ADC count
		Laser_Ch0Power[0] = PTable_head[0];	//256*(128-10)
		Laser_Ch0Power[1] = PTable_head[1]; //256*(128-20)
		Laser_Ch0Power[2] = PTable_head[2]; //256*(128-30)
		Laser_Ch0Power[3] = PTable_head[3]; //256*(128-40)
		Laser_Ch0Power[4] = PTable_head[4];	//256*(128-50)
		
		OnMTF_Create_PTable();
*/		
		PTable_Ready = TRUE;	
		ETable_Ready = TRUE;
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Channel_Test(void)
{
	uint8_t Index, Count;
	uint16_t Index2, IndexP;
	volatile uint8_t *IsDataReady;
	
	ScanCount = 0;
	IsDataReady = &ScanData_Ready;
	Uart_CmdID = NORMAL_CMD;
	//----------------------------------------------------------
	// in cal with single Laser, Ref_LaserIndex = channel Index
	// each channel cab be calibrated individually, not necessary in order
	//
	PowCal_PeakLevel = 0;
	if (NumOfFreqCal == 0) NumOfFreqCal = 1;
	for (Index = 0; Index < NumOfFreqCal; Index ++)
	{
		*IsDataReady = FALSE;
		OnMTF_StartScan();
		Sys_Elapsed_time = 0;
		Count = 0;

		while (*IsDataReady == FALSE)
		{
			// check if time-out here
			delay_ms(100);
			Count += 1;
			// if (Count > 10) break;	// Time-out
		}
		//-------------------------------------------------------------------
		OnMTF_Convert_RawData2EQS(SignalCh_buf, SignalCh_Eqs);
		OnMTF_Convert_EQS_Adc2dB(SignalCh_Eqs);
		//-------------------------------------------------------------------
		IndexP = 0;		MaxPeakLevel = 0;
		for (Index2 = 0; Index2 < EqsDataLength; Index2 ++)
		{
			if (MaxPeakLevel < SignalCh_Eqs[Index2])
			{
				MaxPeakLevel = SignalCh_Eqs[Index2];
				IndexP = Index2;
			}
		}
		//------------------------------------------
		Peak3dB_ADC = 600;
		Peak3dB_ADC = MaxPeakLevel - Peak3dB_ADC;
		pSinglePk_Pow = SignalCh_Eqs;
		//------------------------------------------------------------------------	
		// Use Ch_Freq and Ch_SignalPower array for channel test multiple-run data
		// Ch_Freq is measured Freq with offset "ScanStartFreq"
		Ch_Freq[Index] = OnMTF_Single_PKC(EqsDataLength, 0, IndexP)*EQS_Space;
		Ch_SignalPower[Index] = MaxPeakLevel;
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_GetPeaknValley(uint16_t *pData, uint16_t DataLength)
{
	uint16_t Local_Min, Local_Max;
	uint16_t Local_MinP_Pos, Local_MaxP_Pos;
	uint16_t Index, Index_V, Index_P, State;

	//Local_Min = 16383;		// max. of 14-bit number
	Local_Min = 35328;		// max. of 10dBm number
	Local_Max = 0;
	//Length = SCAN_MAX_LENGTH;
	
	Local_MinP_Pos = 0;
	Local_MaxP_Pos = 0;
	MaxPeakLevel = 0;
	
	State = Wait_Valley;
	Index_V = 0; Index_P = 0;
	Index = 0;					// Start from begin for scan data
	//--------------------------------------------------------------------------------
	while ((Index < (DataLength-1)) && (Index_V <= MAX_NUM_PEAKS) && (Index_P < MAX_NUM_PEAKS))
	{
		if (State == Wait_Valley)
		{
			if (pData[Index] <= Local_Min)
			{
				Local_Min = pData[Index];
				Local_Max = pData[Index];
				Local_MinP_Pos = Index;
				Local_MaxP_Pos = Index;
			}
			else if (pData[Index] >= Local_Max)
			{
				Local_Max = pData[Index];
				Local_MaxP_Pos = Index;
				State = Compare_Valley;
			}
		}
		//-----------------------------------------------------
		if (State == Compare_Valley)
		{
			if ((Local_Max - Local_Min) > Contrast)
			{
				ValleyValue[Index_V] = Local_Min;
				ValleyPos[Index_V] = Local_MinP_Pos;
				Index_V ++;
				
				Local_MinP_Pos = Index;
				Local_MaxP_Pos = Index;
				Local_Min = pData[Index];
				Local_Max = pData[Index];
				State =  Wait_Peak;				
			}
			else State = Wait_Valley;
		}
		//-----------------------------------------------------
		if (State == Wait_Peak)
		{
			if (pData[Index] >= Local_Max)
			{
				Local_Max = pData[Index];
				Local_Min = pData[Index];
				Local_MaxP_Pos = Index;
				Local_MinP_Pos = Index;
			}
			else if (pData[Index] <= Local_Min)
			{
				Local_Min = pData[Index];
				Local_MinP_Pos = Index;
				State = Compare_Peak;
			}
		}
		//-----------------------------------------------------
		if (State == Compare_Peak)
		{
			if (((Local_Max - Local_Min) > Contrast) && (Local_Max > MinPeakLevel))
			{
				PeakValue[Index_P] = Local_Max;
				PeakPos[Index_P] = Local_MaxP_Pos;
				if (MaxPeakLevel < Local_Max) MaxPeakLevel = Local_Max;
				Index_P ++;
									
				Local_MinP_Pos = Index;
				Local_MaxP_Pos = Index;
				Local_Min = pData[Index];
				Local_Max = pData[Index];
				
				ValleyValue[Index_P] = Local_Min;		// 20170303 VG: added a fake valley point for Multi-center calculation
				ValleyPos[Index_P] = Local_MinP_Pos;	// 20170303 VG: added a fake valley point for Multi-center calculation
				
				State = Wait_Valley;
			}
			else State = Wait_Peak;
		}
		Index = Index + 1;
	}
	//---------------------------------------------------------
	if ((State == Wait_Valley)&&(Index_P > 0))
	{
		if ((Local_Max == Local_Min)&&((PeakValue[Index_P-1]-Local_Max)>Contrast))
		{
			ValleyValue[Index_V] = Local_Min;
			ValleyPos[Index_V] = Local_MinP_Pos;
			Index_V ++;
		}
	}
	//---------------------------------------------------------
	NumOfPeaks = Index_P;
	if (Index_P == Index_V)
		NumOfValleys = Index_V + 1;		// 20170303 VG: added a fake valley point for Multi-center calculation
	else
		NumOfValleys = Index_V;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_GetRef_LaserIndex(void)
{
	uint8_t Index;
	
	Ref_LaserIndex = 0;
	for (Index = 1; Index < NumOfPeaks-1; Index ++)
	{
		if ((PeakValue[Index] > PeakValue[Index-1])&&(PeakValue[Index] > PeakValue[Index+1]))
		{
			if (((PeakValue[Index]-PeakValue[Index-1]) >= RULER_LASER_CONTRAST)
				&&((PeakValue[Index]-PeakValue[Index+1]) >= RULER_LASER_CONTRAST))
			{
				Ref_LaserIndex = Index;
				return;
			}
		}
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_GetMultiplePeakCenters(uint16_t* pData)
{
	uint16_t Index, Index0, Length;
	uint16_t ValleyLevel;
	
	//-------------------------------------------------
	if (PeakPos[0] < ValleyPos[0]) Index0 = 1;
	else Index0 = 0;

	for (Index = Index0; Index < NumOfPeaks; Index ++)
	{
		Peak3dB_ADC = 600;
		if ((PeakPos[Index] < ValleyPos[NumOfValleys-1]) && (Index < NumOfValleys-1))
		{
			pSinglePk_Pow = &pData[ValleyPos[Index-Index0]];
			Length = ValleyPos[Index-Index0+1] - ValleyPos[Index-Index0] + 1;
			
			if ((ValleyValue[Index-Index0]) > ValleyValue[Index-Index0+1]) ValleyLevel = ValleyValue[Index-Index0] + 100;
			else ValleyLevel = ValleyValue[Index-Index0+1] + 100;
			
			if ((PeakValue[Index] - ValleyLevel) > Peak3dB_ADC) Peak3dB_ADC = PeakValue[Index] - Peak3dB_ADC;
			else Peak3dB_ADC = (int)((PeakValue[Index] + ValleyLevel)/2 + 0.5f);
			//PeakCenterPos[Index-Index0] += (uint16_t)(OnMTF_Single_PKC(Length, ValleyPos[Index-Index0], PeakPos[Index]) * ScanEndPos/SCAN_MAX_LENGTH + 0.5f);
			PeakCenterPos[Index-Index0] += (uint16_t)(OnMTF_Single_PKC(Length, ValleyPos[Index-Index0], PeakPos[Index]) + 0.5f);		// 2017/01/11 VG: Index already has unit, GHz/point.
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
float OnMTF_Single_PKC(uint16_t Length, uint16_t StartIndex, uint16_t PeakIndex)
{
	float xL, xR;
	uint16_t Index;
	uint16_t X1, Y1, X2, Y2;

	xL = -1.f;	xR = -1.f;
	for (Index = PeakIndex - StartIndex; Index > 0; Index --)
	{
		if ((pSinglePk_Pow[Index] >= Peak3dB_ADC) && (pSinglePk_Pow[Index-1] <= Peak3dB_ADC))
		{
			X1 = Index;						X2 = Index - 1;
			Y1 = pSinglePk_Pow[Index];		Y2 = pSinglePk_Pow[Index - 1];
			xL = (float)(StartIndex + X1 + (float)(Peak3dB_ADC - Y1)*(X2-X1)/(float)(Y2-Y1));
			break;
		}
	}
	//----------------------------------------------------------------------------------------
	for (Index = PeakIndex - StartIndex; Index < Length-1; Index ++)
	{
		if ((pSinglePk_Pow[Index] >= Peak3dB_ADC) && (pSinglePk_Pow[Index + 1] <= Peak3dB_ADC))
		{
			X1 = Index;						X2 = Index + 1;
			Y1 = pSinglePk_Pow[Index];		Y2 = pSinglePk_Pow[Index + 1];
			xR = (float)(StartIndex + X1 + (float)(Peak3dB_ADC - Y1)*(X2-X1)/(float)(Y2-Y1));
			break;
		}
	}
	//----------------------------------------------------------------------------------------
	if ((xL > 0) && (xR > 0) && (xR > xL))
	{
		return (xR + xL)/2.f;
	}
	else return 0.f;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_GetSingleLaserPeakScanPos(uint16_t* pData, uint16_t DataLength, uint8_t PeakIndex)
{
	uint16_t Index, IndexP;
	
	IndexP = 0;		MaxPeakLevel = 0;
	for (Index = 0; Index < DataLength; Index ++)
	{
		if (MaxPeakLevel < pData[Index]) 
		{
			MaxPeakLevel = pData[Index];
			IndexP = Index;
		}
	}
	//------------------------------------------
	Peak3dB_ADC = 1600;
	Peak3dB_ADC = MaxPeakLevel - Peak3dB_ADC;
	pSinglePk_Pow = pData;
	if (MTF_Mode != FP_CAL_Ch0_LOW_POWER)		// Convert Peak Center to Scan Position for ETable;
	{
		//PeakCenterPos[PeakIndex] += (uint16_t)(OnMTF_Single_PKC(DataLength, 0, IndexP) * ScanEndPos/SCAN_MAX_LENGTH + 0.5f);
PeakCenterPos[PeakIndex] += (uint16_t)((OnMTF_Single_PKC(DataLength, 0, IndexP)+Scan_Pos_Offset )* ScanEndPos/Scan_Pos_Resolution + 0.5f);	//20170314 VG: replaced with partial scan.
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_GetSinglePeak_Freq(uint16_t* pData, uint8_t ChIndex) // used for debug
{
	uint16_t Index, IndexP;
	
	IndexP = 0;		MaxPeakLevel = 0;
	for (Index = 0; Index < EqsDataLength; Index ++)
	{
		if (MaxPeakLevel < pData[Index])
		{
			MaxPeakLevel = pData[Index];
			IndexP = Index;
		}
	}
	//------------------------------------------
	Peak3dB_ADC = 600;
	Peak3dB_ADC = MaxPeakLevel - Peak3dB_ADC;
	pSinglePk_Pow = pData;
	PeakCenterPos[ChIndex] = (uint16_t)(OnMTF_Single_PKC(EqsDataLength, 0, IndexP) * EQS_Space + 0.5f) + ScanStartFreq;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_GetRulerPeakFreq_byRefIndex(void)
{
	uint8_t Index;
	
	Ruler_Space = RULER_50G_SPACE;
	for (Index = 0; Index < NumOfPeaks; Index ++)
	{
		PeakCenterFreq[Index] = Ref_LaserFreq + (Index - Ref_LaserIndex)*Ruler_Space - Freq_Offset;
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Convert_EQS_Adc2dB(uint16_t* pEqs)
{
	uint16_t Index, Eqs_Freq, Ch_Space2, n;
	uint16_t x, x1, x2, y1, y2;
	uint8_t Ch;
	float y; 
	
	if (!ETable_Ready | !PTable_Ready) return;
	Ch_Space2 = Ch_Space/2;
	Ch = 0;	
	//-------------------------------------------------------------------------	
	for (Index = 0; Index < EqsDataLength; Index ++)
	{
		Eqs_Freq = ScanStartFreq + Index * EQS_Space;
		if (Eqs_Freq <= (ETable_Freq[Ch] + Ch_Space2) || (Ch == ETableLength - 1))
		{
			x = pEqs[Index];
			for (n = 0; n < CAL_HEAD_SIZE-1; n ++)
			{				
				x2 = PTable_ADC[Ch][n+1];
				if ((x > x2) || (n == CAL_HEAD_SIZE-2))
				{
					//y1 = PTable_head[n];
					//y2 = PTable_head[n+1];
					y1 = Laser_ChPower[Ch]-(PTable_head[0]-PTable_head[n]);
					y2 = Laser_ChPower[Ch]-(PTable_head[0]-PTable_head[n+1]);
					x1 = PTable_ADC[Ch][n];
					y = (float)y1 + (float)(x-x1)*(float)(y1-y2)/(float)(x1-x2);
					break;
				}
			}
			pEqs[Index] = (uint16_t)(y + 0.5f);
		}
		else
		{
			Ch ++;
			Index --;
		}
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Convert_RawData2EQS(uint16_t* pData, uint16_t* pEqsADC)
{
	uint16_t Index, Index0, EqsIndex, ScanPos, ScanPos_1, Max;

	Index0 = 1;	Max = 0;
	StartPosIndex = OnMTF_GetScanStartFreq_Pos();
	EqsF = ScanStartFreq;
	//---------------------------------------------------------
	//for (EqsIndex = 0; EqsIndex < RawDataLength; EqsIndex ++)
	for (EqsIndex = 0; EqsIndex < SCAN_MAX_LENGTH; EqsIndex ++)	//161205 VG: to get entire C-band *SignalCh_Eqs
	{
		if (EqsF > ScanStopFreq)	break;
			
		OnMTF_Get_EqsFreqPos();
	
		for (Index = Index0; Index < RawDataLength; Index ++)
		{
			//ScanPos = Index*ScanEndPos/SCAN_MAX_LENGTH;
			//ScanPos_1 = (Index-1)*ScanEndPos/SCAN_MAX_LENGTH;
			ScanPos = (Index+Scan_Pos_Offset)*ScanEndPos/Scan_Pos_Resolution;			//20170314 VG: partial scan
			ScanPos_1 = (Index+Scan_Pos_Offset-1)*ScanEndPos/Scan_Pos_Resolution;		//20170314 VG: partial scan
			if (ScanPos >= EqsF_Pos)
			{
				Index0 = Index;
				pEqsADC[EqsIndex] = pData[Index-1] + (pData[Index] - pData[Index-1])*(EqsF_Pos - ScanPos_1)/(ScanPos - ScanPos_1);
				//--------------------------
				if (Max < pEqsADC[EqsIndex])
				{
					Max = pEqsADC[EqsIndex];
					rawMaxADC = pEqsADC[EqsIndex];	// Save to report Max ADC
					WF_PeakIndex = EqsIndex;		// Save index of peak used for Waveform data
				}
				break;
			}
		}
		EqsF = EqsF + EQS_Space;
	}
	EqsDataLength = EqsIndex;	
//	OnMTF_Convert_EQS_Adc2dB(pEqs);
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Convert_RawData2EQS_User1ScanCmd9(uint16_t* pData, uint16_t* pEqsADC, uint8_t DecimationN)
{
	uint16_t Index, Index0, EqsIndex, ScanPos, ScanPos_1, Max;

	Index0 = 1;	Max = 0;
	StartPosIndex = OnMTF_GetScanStartFreq_Pos();
	EqsF = ScanStartFreq;
	//---------------------------------------------------------
	//for (EqsIndex = 0; EqsIndex < RawDataLength; EqsIndex ++)
	for (EqsIndex = 0; EqsIndex < SCAN_MAX_LENGTH; EqsIndex ++)	//161205 VG: to get entire C-band *SignalCh_Eqs
	{
		if (EqsF > ScanStopFreq)	break;
		
		OnMTF_Get_EqsFreqPos();
		
		for (Index = Index0; Index < RawDataLength; Index ++)
		{
			ScanPos = (Index+Scan_Pos_Offset)*ScanEndPos/Scan_Pos_Resolution;			//20170314 VG: partial scan
			ScanPos_1 = (Index+Scan_Pos_Offset-1)*ScanEndPos/Scan_Pos_Resolution;		//20170314 VG: partial scan
			if (ScanPos >= EqsF_Pos)
			{
				Index0 = Index;
				pEqsADC[EqsIndex] = pData[Index-1] + (pData[Index] - pData[Index-1])*(EqsF_Pos - ScanPos_1)/(ScanPos - ScanPos_1);
				break;
			}
		}
		EqsF = EqsF + DecimationN;
	}
	EqsDataLength = EqsIndex;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Get_EqsFreqPos(void)
{
	if (EqsF <= ETable_Freq[StartPosIndex])
	{
		Ch_EN_1 = ScanStartFreqPos;	
		Ch_EN_2 = ETable_Pos[StartPosIndex];
		Ruler_Space = ETable_Freq[StartPosIndex] - ScanStartFreq;
		delta_Freq = EqsF - ScanStartFreq;
	
		if (Ruler_Space == 0) EqsF_Pos = Ch_EN_1;
		else EqsF_Pos = Ch_EN_1 + (Ch_EN_2 - Ch_EN_1)*delta_Freq/Ruler_Space;
	}
	else
	{
		delta_Freq = EqsF - ETable_Freq[StartPosIndex];
		Ruler_Space = ETable_Freq[StartPosIndex+1] - ETable_Freq[StartPosIndex];
		Ch_Index = (uint8_t)(delta_Freq / Ruler_Space);
		delta_Freq = delta_Freq - Ch_Index*Ruler_Space;
		Ch_Index += StartPosIndex;
			
		if (Ch_Index < ETableLength - 1)
		{
			Ch_EN_1 = ETable_Pos[Ch_Index];
			Ch_EN_2 = ETable_Pos[Ch_Index + 1];
		}
		else
		{
			Ch_EN_1 = ETable_Pos[ETableLength - 1] + (Ch_Index - ETableLength + 1)*(ETable_Pos[ETableLength - 1] - ETable_Pos[ETableLength - 2]);
			Ch_EN_2 = Ch_EN_1 + (ETable_Pos[ETableLength - 1] - ETable_Pos[ETableLength - 2]);
		}
		EqsF_Pos = Ch_EN_1 + (Ch_EN_2 - Ch_EN_1)*delta_Freq/Ruler_Space;
	}
}
///////////////////////////////////////////////////////////////////////////////
uint8_t OnMTF_GetScanStartFreq_Pos(void)
{
	uint8_t Index;
	int32_t StartPos;
	uint16_t X1, Y1, X2, Y2;

	for (Index = 0; Index < ETableLength; Index ++)
	{
		if (ScanStartFreq <= ETable_Freq[Index]) break;
	}
	if (Index == 0) Index = 1;
	//----------------------------------------------------------
	X1 = ETable_Freq[Index - 1];		X2 = ETable_Freq[Index];
	Y1 = ETable_Pos[Index - 1];		Y2 = ETable_Pos[Index];
	StartPos = Y1 + (ScanStartFreq - X1)*(Y2-Y1)/(X2-X1);
	
	if (StartPos < 0)
	{
		ScanStartFreq = X1 - Y1*(X2-X1)/(Y2-Y1);	// set ScanStartFreq if it is out-of-range
		ScanStartFreqPos = 0;
	}
	else ScanStartFreqPos = StartPos;
	//----------------------------------------------------------
	for (Index = 0; Index < ETableLength; Index ++)
	{
		if (ScanStartFreqPos <= ETable_Pos[Index]) break;
	}
	return Index;
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_GetFilterWaveform(uint8_t* pWF, uint16_t* pEqs_Data)
{
	uint16_t Index, Index2;
	uint16_t indexP;
	float32_t maxP, minP, rangeP;
	
	maxP=-1; minP=1;

	Index = 0;
	for (Index2 = 0; Index2 < WF_Length; Index2 ++)
	{
		if (WF_PeakIndex + Index2 <= WF_Length/2)
		{
			pWF[Index ++] = pEqs_Data[0] >> 8;
			pWF[Index ++] = pEqs_Data[0];
			PSF_ADC[Index2] = pEqs_Data[0];
		}
		else if (WF_PeakIndex + Index2 >= WF_Length/2 + EqsDataLength-1)
		{
			pWF[Index ++] = pEqs_Data[EqsDataLength-1] >> 8;
			pWF[Index ++] = pEqs_Data[EqsDataLength-1];
			PSF_ADC[Index2] = pEqs_Data[EqsDataLength-1];
		}
		else
		{
			pWF[Index ++] = pEqs_Data[WF_PeakIndex - WF_Length/2 + Index2] >> 8;
			pWF[Index ++] = pEqs_Data[WF_PeakIndex - WF_Length/2 + Index2];
			PSF_ADC[Index2] = pEqs_Data[WF_PeakIndex - WF_Length/2 + Index2];
		}
	}
	
	//Normalized PSF
	for (indexP=0; indexP<PSF_TABLE_SIZE; indexP++) {
		data_psf[indexP] = powf(10.0, PSF_ADC[indexP] / 1801.27 - 9.49);
		if(data_psf[indexP]>maxP)	maxP = data_psf[indexP];
		if(data_psf[indexP]<minP)	minP = data_psf[indexP];
	}
	
	rangeP = maxP-minP;
	for (indexP=0; indexP<PSF_TABLE_SIZE; indexP++) {
		data_psf[indexP] = (data_psf[indexP]-minP)/(rangeP);
	}
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_Create_PTable(void)
{
	uint8_t Index;
	int32_t Laser_Power[CAL_HEAD_SIZE], Ch_ADC[CAL_HEAD_SIZE];

	for (Ch_Num = ETableLength; Ch_Num > 0; Ch_Num --)	// Start last ch number first
	{
		if (Ch_Num > 1)
		{
			Laser_Power[0] = Laser_ChPower[Ch_Num-1];
			Ch_ADC[0] = PTable_ADC[Ch_Num-1][0];			
			for (Index = 1; Index < CAL_HEAD_SIZE; Index ++)
			{
				if (Index == 1) Laser_Power[Index] = Laser_ChPower[Ch_Num-1] - (Laser_Ch0Power[0] - Laser_Ch0Power[1]);
				//else Laser_Power[Index] = Laser_ChPower[Ch_Num-1] - (Laser_Ch0Power[Index-1] - Laser_Ch0Power[Index]);
				else Laser_Power[Index] = Laser_ChPower[Ch_Num-1] - (Laser_Ch0Power[0] - Laser_Ch0Power[Index]);
		
				//Ch_ADC[Index] = PTable_ADC[Ch_Num-1][0] - (PTable_ADC[0][Index-1] - PTable_ADC[0][Index]);
				Ch_ADC[Index] = PTable_ADC[Ch_Num-1][0] - (PTable_ADC[0][0] - PTable_ADC[0][Index]);
			}
		}
		else
		{
			Laser_Power[0] = Laser_ChPower[0];
			Ch_ADC[0] = PTable_ADC[0][0];
			for (Index = 1; Index < CAL_HEAD_SIZE; Index ++)
			{
				Laser_Power[Index] = Laser_Ch0Power[Index];
				Ch_ADC[Index] = PTable_ADC[0][Index];
			}
		}
		//-----------------------------------------------------
		OnMTF_Normalize_PTable(Laser_Power, Ch_ADC, Ch_Num-1);
	}
	
	PTable_Ready = TRUE;
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_Normalize_PTable(int32_t* pLaser_Power, int32_t* pCh_ADC, uint8_t Ch)
{
	uint8_t k, n;
	int32_t x, x1, x2, y1, y2;
	float y;
	
	for (k = 0; k < CAL_HEAD_SIZE; k ++)
	{
		x = (int32_t)PTable_head[k];
		for (n = 0; n < CAL_HEAD_SIZE-1; n ++)
		{
			x1 = pLaser_Power[n];
			x2 = pLaser_Power[n+1];
			y1 = pCh_ADC[n];
			y2 = pCh_ADC[n+1];

			if ((x > x2) || (n == CAL_HEAD_SIZE-2))
			{
				y = (float)y1 + (float)(x-x1)*(float)(y1-y2)/(float)(x1-x2);
				break;
			}
		}
		PTable_ADC[Ch][k] = (uint16_t)(y + 0.5f);
	}
}
//////////////////////////////////////////////////////////////////////////////////
// Create ETable from VTable if it is available.
void OnMTF_Creat_ETable(void)	
{	
	uint8_t		ch;
	float32_t	tmpV;
	int16_t		X1, X2;
	
	OnGetTempTableIndex();
	
	X1 = TTable[Index_TL];		X2 = TTable[Index_TH];
	
	for (ch=0; ch<Total_ChNum_Plus; ch++)
	{
		Ch_VL= ChV_Table[Index_TL][ch];   Ch_VH= ChV_Table[Index_TH][ch];
		if(X2 != X1)
			tmpV = (float32_t)Ch_VL + (float32_t)((CurrentTmp-X1)*(Ch_VH-Ch_VL)/(X2-X1));
		else
			tmpV = (float32_t)Ch_VL;
			
		if (tmpV>0)
			ETable_Pos[Total_ChNum_Plus-ch-1] = (uint16_t)(32768.f*(1+(tmpV/MAX_SET_VOLTAGE)*(tmpV/MAX_SET_VOLTAGE)));
		else
			ETable_Pos[Total_ChNum_Plus-ch-1] = (uint16_t)(32768.f*(1-(tmpV/MAX_SET_VOLTAGE)*(tmpV/MAX_SET_VOLTAGE)));
			
		ETable_Freq[Total_ChNum_Plus-ch-1] = (uint16_t)(Ch0_Freq-ch*Ch_Space - Freq_Offset);
	}
	
	ETable_Ready = TRUE;
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_P_Dummy_Data(void)
{
	uint8_t x;
	
	// C-band profile, all PTable_head[0], in dBm and ADC-count
	for (x=0; x<MAX_NUM_PEAKS; x++)
	{
		Laser_ChPower[x] = PTable_head[0];	//256*(128-10), equal input power for each channel, -10dBm
		PTable_ADC[x][0] = 9746;			//-10dBm associated ADC count for each channel
	}
	
	// Channel #0 PTable_head in dBm and ADC count
	Laser_Ch0Power[0] = PTable_head[0];	//256*(128-10)
	Laser_Ch0Power[1] = PTable_head[1]; //256*(128-20)
	Laser_Ch0Power[2] = PTable_head[2]; //256*(128-30)
	Laser_Ch0Power[3] = PTable_head[3]; //256*(128-40)
	Laser_Ch0Power[4] = PTable_head[4];	//256*(128-50)
	
	PTable_ADC[0][0] = 9746;
	PTable_ADC[0][1] = 7923;
	PTable_ADC[0][2] = 6100;
	PTable_ADC[0][3] = 4280;
	PTable_ADC[0][4] = 2500;
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_Sidelobe_Filter(void)
{
	int16_t i, k;
	uint16_t max_band_power;
	int16_t diff;
	int32_t accum;
	uint8_t	g_b_report_done = TRUE;
		
	if (g_b_report_done && (Sidelobe_lmt !=0)){
		max_band_power = SignalCh_Eqs[0];
		for (i=1; i<EqsDataLength; i++){
			if (SignalCh_Eqs[i] > max_band_power) max_band_power = SignalCh_Eqs[i];
		}
			
		//over write SignalCh_Eqs to store filtered results
		for(i=0; i<EqsDataLength; i++){
			accum = 0;
			diff = (uint16_t)(Sidelobe_alpha*((float32_t)(max_band_power - SignalCh_Eqs[i])/256-Sidelobe_lmt));	//diff Unit is dB; /256 means not in Q8
			if (diff > 0){
				//for (k=-diff; k<=diff; k++){
				for (k=-diff; k<diff; k++){
					if ((i+k)<0){
						accum = accum + SignalCh_Eqs[0];
					}
					else if ((i+k) >= EqsDataLength-1){
						accum = accum + SignalCh_Eqs[EqsDataLength-1];
					}
					else{
						accum = accum + SignalCh_Eqs[i+k];
					}
				}//k loop
				//SignalCh_Eqs[i] = (int16_t)(accum/(2*diff + 1));
				SignalCh_Eqs[i] = (int16_t)((accum/diff)>>1);
			}//diff>0
		}//i loop
	}//if
}
///////////////////////////////////////////////////////////////////////////////////
void OnMTF_Analyse_Signal(uint16_t* pEqsADC, uint16_t DataLength)
{
	uint8_t Index, Count, tmpIndex;
	volatile uint8_t *IsDataReady;
	
	ScanCount = 0;	
	IsDataReady = &ScanData_Ready;

	for (Index =  0; Index < MAX_NUM_PEAKS; Index ++) PeakCenterPos[Index] = 0;
	//-------------------------------------------------------------------------
	if (NumOfFreqCal == 0) NumOfFreqCal = 1;
	for (Index = 0; Index < NumOfFreqCal; Index ++)
	{
		*IsDataReady = TRUE;
//		*IsDataReady = FALSE;
//		OnMTF_StartScan();
		Sys_Elapsed_time = 0;
		Count = 0;

		while (*IsDataReady == FALSE)
		{
			// check if time-out here
			delay_ms(100);
			Count += 1;
			// if (Count > 10) break;	// Time-out
		}
		//-------------------------------------------------------------------
		OnMTF_GetPeaknValley(pEqsADC, DataLength);
		OnMTF_GetMultiplePeakCenters(pEqsADC);
	}
	//-----------------------------------------------------------------------
	for (Index =  0; Index < NumOfPeaks; Index ++)
	MultiPeaks_Eqs[Index] = PeakCenterPos[Index] / NumOfFreqCal;
	
	
	tmpIndex = Index;
	// clear old data in MultiPeaks_Eqs[]
	for (Index =  tmpIndex; Index < MAX_NUM_PEAKS; Index ++)
	MultiPeaks_Eqs[Index] = 0;

	//-----------------------------------------------------------------------
	//MTF_Mode = RUN_NORMAL;
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_GetOSNR(uint8_t CH_Length, uint16_t* pEqs_Data)
{
	uint16_t Index;
	uint16_t Noise_L, Noise_R;

	
	for (Index = 0; Index < NumOfPeaks; Index ++)
	{
		if (MultiPeaks_Eqs[Index] <= CH_Length/2)
		{
			Noise_L = pEqs_Data[0];
		}
		else if (MultiPeaks_Eqs[Index] + CH_Length/2 >=  EqsDataLength-1)
		{
			Noise_R = pEqs_Data[EqsDataLength-1];
		}
		else
		{
			Noise_L = pEqs_Data[MultiPeaks_Eqs[Index] - CH_Length/2];
			Noise_R = pEqs_Data[MultiPeaks_Eqs[Index] + CH_Length/2];
		}
		PeakOSNR[Index]=pEqs_Data[MultiPeaks_Eqs[Index]] - (Noise_L+Noise_R)/2;
	}
}
///////////////////////////////////////////////////////////////////////////////
float32_t OnMTF_GetAvgP(uint8_t CH_Length, uint16_t* pEqs_Data)
{
	uint16_t	Index, Index2, Data16;
	float32_t	tmp, totalP;
	
	totalP = 0;
	for (Index2=0; Index2<NumOfPeaks; Index2++)
	{
		tmp = 0;
		for (Index = 0; Index < CH_Length; Index ++)
		{
			if (MultiPeaks_Eqs[Index2] <= CH_Length/2)
			{
				Data16 = pEqs_Data[0];
			}
			else if (MultiPeaks_Eqs[Index2] + CH_Length/2 >=  EqsDataLength-1)
			{
				Data16 = pEqs_Data[EqsDataLength-1];
			}
			else 
			{
				Data16 = pEqs_Data[MultiPeaks_Eqs[Index2] - CH_Length/2 + Index];
			}
				
			tmp += exp10f((float_t)(Data16/256-128)/10);
		}
		totalP += tmp/CH_Length;
	}
	
	totalP = totalP/NumOfPeaks;
	return (256*(128 + 10 * log10f(totalP)));
}
///////////////////////////////////////////////////////////////////////////////
/**
 * \brief To identify 10G Signal
 */
/*
void OnMOPM_GetBW(uint8_t CH_Length, uint16_t* pEqs_Data)
{
	uint16_t Index, Index2;
	uint16_t tPeak, tPeak_1dB, tPeak_3dB, tPeak_5dB;
	uint8_t  BW_1dB_count, BW_3dB_count, BW_5dB_count;

	
	for (Index = 0; Index < NumOfPeaks; Index ++)
	{
		 tPeak = pEqs_Data[MultiPeaks_Eqs[Index]];
		 tPeak_1dB = tPeak-256; tPeak_3dB = tPeak-(3*256); tPeak_5dB = tPeak-(5*256);
		 BW_1dB_count=0; BW_3dB_count=0; BW_5dB_count=0;
		 
		 for(Index2=0; Index2<CH_Length; Index2++)
		 {
			 if (MultiPeaks_Eqs[Index] <= CH_Length/2)
			 {
				tPeak = pEqs_Data[0];
			 }
			 else if (MultiPeaks_Eqs[Index] + CH_Length/2 >=  EqsDataLength-1)
			 {
				tPeak = pEqs_Data[EqsDataLength-1];
			 }
			 else
			 {
				 tPeak = pEqs_Data[MultiPeaks_Eqs[Index] - CH_Length/2 + Index2];
				 if (tPeak>tPeak_1dB)	BW_1dB_count++;
				 if (tPeak>tPeak_3dB)	BW_3dB_count++;
				 if (tPeak>tPeak_5dB)	BW_5dB_count++;
			 }
		 }
		 MultiPeaks_1BW[Index] = BW_1dB_count;
		 MultiPeaks_3BW[Index] = BW_3dB_count;
		 MultiPeaks_5BW[Index] = BW_5dB_count;	 
		 //MultiPeaks_Norm[Index] = (float32_t)(BW_3dB_count-BW_1dB_count)/(BW_5dB_count-BW_1dB_count);
		 MultiPeaks_Norm[Index] = (float32_t)(BW_5dB_count+BW_3dB_count+BW_1dB_count);	//20170707 VG: 10G Signal if MultiPeaks_Norm[Index] < 57.
	}
}
*/
///////////////////////////////////////////////////////////////////////////////
/**
 * \brief Total channel power calculation, 3dB BW area.
	BW_threshold in dB
 */
void OnMOPM_ReportPW(float32_t BW_threshold)
{
	uint8_t Index, off_L, off_R, index_sum;
	uint16_t thresholdQ8, threshold, index_buf;
	float32_t x1, x2, xL, xR, totalP, xx, oo;
	uint16_t y1, y2;
	
	thresholdQ8 = (uint16_t)(256*BW_threshold);
	
	for (Index = 0; Index < NumOfPeaks; Index++)
	{
		threshold = SignalCh_Eqs[PeakCenterPos[Index]] - thresholdQ8;
		// left 3dB point
		for(off_L = 0; off_L<25; off_L++) {
			if (SignalCh_Eqs[PeakCenterPos[Index]-off_L] < threshold) break;
		}
		xx = SignalCh_Eqs[PeakCenterPos[Index]-off_L];
		
		// right 3dB point
		for(off_R = 0; off_R<25; off_R++) {
			if (SignalCh_Eqs[PeakCenterPos[Index]+off_R] < threshold) break;
		}
		oo = SignalCh_Eqs[PeakCenterPos[Index]+off_R];

		// total power calculation within BW
		totalP = 0;
//		for(index_sum=0; index_sum<(off_L+off_R); index_sum++)
//		{
//			index_buf = PeakCenterPos[Index]-off_L+1;
//			y1 = SignalCh_Eqs[index_buf + index_sum];
			index_buf = PeakCenterPos[Index];
			y1 = SignalCh_Eqs[index_buf];
			x1 = (float32_t)y1/256 -128;		//dBm
			totalP += exp10f(x1/10);
//		}
		//totalP = totalP + xL + xR;	//mW
		//totalP = totalP + xx;			//mW
		
		totalP = 10 * log10f(totalP);
		fReportSignal[Index] = totalP;
		
		totalP = 10 * totalP;
		ReportPW[Index] = (uint16_t)((int16_t)totalP);
	}
	
}