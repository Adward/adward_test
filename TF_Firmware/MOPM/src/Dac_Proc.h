///////////////////////////////////////////////////////////////////////
// Dac_Proc.h
//
///////////////////////////////////////////////////////////////////////
#ifndef __DAC_H__
#define __DAC_H__

/////////////////////////////////////////////////////////////////////////
// DAC control constant
#define DAC_WRITE_REGISTER_N				0x00
#define DAC_UPDATE_REGISTER_N				0x10
#define DAC_WRITE_REGISTER_N_UPDATE_ALL		0x20
#define DAC_WRITE_UPDATE_REGISTER_N			0x30

#define ADDRESS_DAC_A						0x00
#define ADDRESS_DAC_B						0x01
#define ADDRESS_DAC_C						0x02
#define ADDRESS_DAC_D						0x03
#define ADDRESS_DAC_ALL						0x0F

#define DAC_WRITE_REGISTER_A				0x00
#define DAC_WRITE_REGISTER_B				0x01
#define DAC_WRITE_REGISTER_C				0x02
#define DAC_WRITE_REGISTER_D				0x03

#define DAC_WRITE_A_UPDATE_ALL				0x20
#define DAC_WRITE_B_UPDATE_ALL				0x21
#define DAC_WRITE_C_UPDATE_ALL				0x22
#define DAC_WRITE_D_UPDATE_ALL				0x23

#define MEMS_CH_A					0x30
#define MEMS_CH_C					0x31
//////////////////////////////////////////////////////////////
// define Thermistor coefficient
/*
#define	THE_A			(2.695f/1000)
#define THE_B			(2.92155f/10000)
#define THE_C			(5.10885f/10000000)
#define THE_R			(0.5f)
#define THE_ADJ			(2711.5f)
*/
#define	THE_A			(2.696141317f/1000)
#define THE_B			(2.772947827f/10000)
#define THE_C			(9.768598582f/10000000)
#define THE_R			(0.0f)
#define THE_ADJ			(2731.5f)
//////////////////////////////////////////////////////////////
// DAC control and read temperature functions
void OnRead_Tmp125(void);
void OnRead_TmpR(void);
void OnRead_Temp(void);
void DAC_Output(uint16_t DAC_Value, uint8_t Dir);
void DAC1_Output(uint16_t DAC_Value, uint8_t MemsCH);
void DAC2_Output(uint16_t DAC_Value, uint8_t Dir);
void OnSet_Voltage(int32_t DAC_Value, uint8_t MemsCH);

//----------------------------------------------------------------------
int16_t Tmp125X10, TmpRX10, TmpR, Tmp125, CurrentTmp, Pre_Tmp;
int16_t Tmp125_buf[TEMP_AVERAGE_N], TmpR_buf[TEMP_AVERAGE_N];
uint8_t  Tmp125_Index, Tmp125_ReadNum, TmpR_Index, TmpR_ReadNum;
///////////////////////////////////////////////////////////////////////////////
#endif // __Dac_H__