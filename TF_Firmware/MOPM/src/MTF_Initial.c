///////////////////////////////////////////////////////////////////////////////
//
//	Initial functions for MEMS MTF
//
///////////////////////////////////////////////////////////////////////////////
#include "MTF.h"

///////////////////////////////////////////////////////////////////////////////
void OnMTF_PCBA_Initial(void)
{
	/** Watchdog period 5000ms (max is 15 sec)*/
	#define WDT_PERIOD                        5000
	uint32_t wdt_mode, timeout_value;
	
	// Disable the watchdog
	// WDT->WDT_MR = WDT_MR_WDDIS;			// for debug only
	
	/* Get timeout value. */
	timeout_value = wdt_get_timeout_value(WDT_PERIOD * 1000, BOARD_FREQ_SLCK_XTAL);
	if (timeout_value == WDT_INVALID_ARGUMENT) {
		/* Invalid timeout value, error. Default is 5 second. */
		timeout_value = wdt_get_timeout_value(5000 * 1000, BOARD_FREQ_SLCK_XTAL);
	}
	wdt_mode =	WDT_MR_WDRSTEN    |
				WDT_MR_WDDBGHLT   |		/* WDT stops in debug state. */
				WDT_MR_WDIDLEHLT;		/* WDT stops in idle state. */
	wdt_init(WDT, wdt_mode, timeout_value, timeout_value);
	
	//------------------------------------------------------------------------
	// Set system clock
	sysclk_init();
	ioport_init();
	//------------------------------------------------------------------------
	// Set SPI pins (Peripheral_A is default at power on)
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD20B_SPI0_MISO);
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD21B_SPI0_MOSI);
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD22B_SPI0_SPCK);

	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB2D_SPI0_NPCS0);
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA31A_SPI0_NPCS1);
	pio_set_peripheral(PIOD, PIO_PERIPH_C, PIO_PD12C_SPI0_NPCS2);
	pio_set_peripheral(PIOD, PIO_PERIPH_B, PIO_PD27B_SPI0_NPCS3);

	ioport_disable_port(IOPORT_PIOD, PIO_PD20B_SPI0_MISO);
	ioport_disable_port(IOPORT_PIOD, PIO_PD21B_SPI0_MOSI);
	ioport_disable_port(IOPORT_PIOD, PIO_PD22B_SPI0_SPCK);

	ioport_disable_port(IOPORT_PIOB, PIO_PB2D_SPI0_NPCS0);
	ioport_disable_port(IOPORT_PIOA, PIO_PA31A_SPI0_NPCS1);
	ioport_disable_port(IOPORT_PIOD, PIO_PD12C_SPI0_NPCS2);
	ioport_disable_port(IOPORT_PIOD, PIO_PD27B_SPI0_NPCS3);
	
//--------------------------------------------------------------------------
	//ioport_set_pin_dir(PIO_PD13_IDX, IOPORT_DIR_OUTPUT);
	//ioport_set_pin_dir(PIO_PA1_IDX, IOPORT_DIR_OUTPUT);
//--------------------------------------------------------------------------
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA9A_URXD0);
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA10A_UTXD0);
	ioport_disable_port(IOPORT_PIOA, PIO_PA9A_URXD0 | PIO_PA10A_UTXD0);
//-----------------------------------------------------------------------
	OnMTF_Uart_Initial();
	OnMTF_SPI_Initial();
	OnMTF_TC_Intial();
	SysTick_Config(sysclk_get_cpu_hz()/100);		// Tick at 10 ms (1/100 s)

	OnMTF_ADC_Initial();
	OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
	//--------------------------------------------------------------------
	//OnOpen_USB_Port();
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_TC_Intial(void)
{
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	// set PCK6=MCK/2
	pmc_pck_set_source(6, 4);						//6=>PCK6; 4=>clock source=MCK
	pmc_pck_set_prescaler(6, PMC_MCKR_PRES_CLK_2);	//6=>PCK6; 1=>prescaler=2
	
	if (pmc_enable_periph_clk(ID_PIOA) != MTF_OK) OnMTF_Log_Info(0);

	//set TIOA if available
	//ioport_set_port_mode(IOPORT_PIOA, PIO_PA0B_TIOA0, PIO_PA0B_TIOA0); //TC0 controlled
	//ioport_disable_port(IOPORT_PIOA, PIO_PA0B_TIOA0); //peripheral controlled

	/* Configure PMC */
	if (pmc_enable_periph_clk(ID_TC0) != MTF_OK) OnMTF_Log_Info(1);

	/* TIOA configuration */
	//	gpio_configure_pin(PIN_TC0_TIOA0, PIN_TC0_TIOA0_FLAGS);

	/** Configure TC for a 24KHz frequency and trigger on RC compare. */
	if (tc_find_mck_divisor(SAMPLE_FREQ, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk) != 1) OnMTF_Log_Info(2);

	tc_init(TC0, 0, ul_tcclks | TC_CMR_CPCTRG| TC_CMR_WAVE | TC_CMR_ACPA_CLEAR | TC_CMR_ACPC_SET);
	
	// Enable PCK6 for M7 only. (data sheet p.303/1871) 
	if(ul_tcclks == 0)
		pmc_enable_pck(6);
	else
		pmc_disable_pck(6);

	TC0->TC_CHANNEL[0].TC_RA = (ul_sysclk / ul_div) / (SAMPLE_FREQ * 2);
	TC0->TC_CHANNEL[0].TC_RC = (ul_sysclk / ul_div) / SAMPLE_FREQ;
	//	tc_write_rc(TC0, 0, (ul_sysclk / ul_div) / 1);

	/* Start the Timer. */
	//	tc_start(TC0, 0);

	/* Configure and enable interrupt on RC compare when AFEC0_CH0, AFEC1_CH0 is NOT used*/
	//NVIC_EnableIRQ((IRQn_Type) ID_TC0);
	//tc_enable_interrupt(TC0, 0, TC_IER_CPCS);
}
///////////////////////////////////////////////////////////////////////////////
/**
 * \brief configure xdmac for afec Signal Channel Only.
 *
 * \param afec:  Base address of the AFEC
 * \param DataLength: ADC sample length
 */
void OnMTF_XDMAC_Initial(Afec *const afec, uint16_t DataLength)
{
	uint32_t xdmaint;
	/* Initialize and enable DMA controller */
	pmc_enable_periph_clk(ID_XDMAC);

	/*Enable XDMA interrupt */
	NVIC_ClearPendingIRQ(XDMAC_IRQn);
	NVIC_SetPriority( XDMAC_IRQn ,1);
	NVIC_EnableIRQ(XDMAC_IRQn);

	/* Initialize channel config */
	xdmac_SignalCh_cfg.mbr_ubc = XDMAC_UBC_NVIEW_NDV0 |
		XDMAC_UBC_NDE_FETCH_DIS |
		XDMAC_UBC_NDEN_UPDATED |
		DataLength;

	// xdmac_channel_cfg.mbr_sa = (uint32_t)&(afec->AFEC_CDR);
	xdmac_SignalCh_cfg.mbr_sa = (uint32_t)&(afec->AFEC_LCDR);
	xdmac_SignalCh_cfg.mbr_da = (uint32_t)SignalCh_buf;
	xdmac_SignalCh_cfg.mbr_cfg = XDMAC_CC_TYPE_PER_TRAN |
		XDMAC_CC_MBSIZE_SINGLE |
		XDMAC_CC_DSYNC_PER2MEM |
		XDMAC_CC_CSIZE_CHK_1 |
		XDMAC_CC_DWIDTH_HALFWORD|
		XDMAC_CC_SIF_AHB_IF1 |
		XDMAC_CC_DIF_AHB_IF0 |
		XDMAC_CC_SAM_FIXED_AM |
		XDMAC_CC_DAM_INCREMENTED_AM |
		XDMAC_CC_PERID(XDAMC_CHANNEL_HWID_AFEC0);

	xdmac_SignalCh_cfg.mbr_bc =  0;
	xdmac_SignalCh_cfg.mbr_ds =  0;
	xdmac_SignalCh_cfg.mbr_sus = 0;
	xdmac_SignalCh_cfg.mbr_dus = 0;

	xdmac_configure_transfer(XDMAC, XDMAC_SignalCH, &xdmac_SignalCh_cfg);

	xdmac_channel_set_descriptor_control(XDMAC, XDMAC_SignalCH, 0);

	xdmac_enable_interrupt(XDMAC, XDMAC_SignalCH);
	xdmaint =  (XDMAC_CIE_BIE   |
		XDMAC_CIE_DIE   |
		XDMAC_CIE_FIE   |
		XDMAC_CIE_RBIE  |
		XDMAC_CIE_WBIE  |
		XDMAC_CIE_ROIE);

	xdmac_channel_enable_interrupt(XDMAC, XDMAC_SignalCH, xdmaint);
	xdmac_channel_enable(XDMAC, XDMAC_SignalCH);
	
	// Set ADC interrupt callback
	// will not prematurely trigger interrupt
	//afec_set_callback(AFEC0, AFEC_INTERRUPT_RXBUF_FULL, OnMTF_Scan_Finish, 1);
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_0, OnMTF_MEMS_Control, 1);
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_ADC_Initial(void)
{
	struct afec_config AFEC_Config;
	struct afec_ch_config AFEC_Ch_Config;

	afec_get_config_defaults(&AFEC_Config);
	AFEC_Config.resolution		= AFEC_14_BITS;
	AFEC_Config.mck				= sysclk_get_cpu_hz();
	AFEC_Config.afec_clock		= 28000000UL;
	AFEC_Config.startup_time	= AFEC_STARTUP_TIME_8;
	//AFEC_Config.settling_time	= AFEC_SETTLING_TIME_0;
	AFEC_Config.tracktim		= 2;
	AFEC_Config.transfer		= 1;
	AFEC_Config.anach			= true;
	AFEC_Config.useq			= false;
	AFEC_Config.tag				= false;
	AFEC_Config.stm				= true;
	AFEC_Config.ibctl			= 1;

	AFEC_Ch_Config.diff = false;
	//AFEC_Ch_Config.offset = true;
	AFEC_Ch_Config.gain = AFEC_GAINVALUE_0;

	//-----------------------------------------------------------------
	ioport_set_port_mode(IOPORT_PIOA, PIO_PD30X1_AFE0_AD0, 0);		//IOPORT_MODE_PULLUP);
	ioport_set_port_dir(IOPORT_PIOA, PIO_PD30X1_AFE0_AD0, IOPORT_DIR_INPUT);
	ioport_disable_port(IOPORT_PIOA, PIO_PD30X1_AFE0_AD0);			//peripheral controlled
	
	ioport_set_port_mode(IOPORT_PIOA, PIO_PA21X1_AFE0_AD1, 0);		//IOPORT_MODE_PULLUP);
	ioport_set_port_dir(IOPORT_PIOA, PIO_PA21X1_AFE0_AD1, IOPORT_DIR_INPUT);
	ioport_disable_port(IOPORT_PIOA, PIO_PA21X1_AFE0_AD1);			//peripheral controlled
	
	ioport_set_port_mode(IOPORT_PIOB, PIO_PB3X1_AFE0_AD2, 0);
	ioport_set_port_dir(IOPORT_PIOB, PIO_PB3X1_AFE0_AD2, IOPORT_DIR_INPUT);
	ioport_disable_port(IOPORT_PIOB, PIO_PB3X1_AFE0_AD2);			//peripheral controlled
	//-----------------------------------------------------------------
	afec_enable(AFEC0);
	//afec_enable(AFEC1);
	afec_init(AFEC0, &AFEC_Config);
	//afec_init(AFEC1, &AFEC_Config);

	afec_ch_set_config(AFEC0, AFEC_CHANNEL_0, &AFEC_Ch_Config);		//for signal channel PD
	//afec_ch_set_config(AFEC1, AFEC_CHANNEL_0, &AFEC_Ch_Config);		//for noise channel PD
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_1, &AFEC_Ch_Config);		//for temperature sensor channel

	//afec_channel_set_analog_offset(AFEC1, AFEC_CHANNEL_0, 0x200);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_0, 0x200);
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_1, 0x200);

	afec_channel_disable(AFEC0, AFEC_CHANNEL_1);

	afec_set_trigger(AFEC0, AFEC_TRIG_TIO_CH_0);
	//afec_set_trigger(AFEC1, AFEC_TRIG_TIO_CH_0);

	afec_channel_enable(AFEC0, AFEC_CHANNEL_0);
	//afec_channel_enable(AFEC1, AFEC_CHANNEL_0);
	
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void SysTick_Handler(void)
{
//	if (Sys_ms_Ticks & 0x1) pio_set_pin_low(PIO_PD13_IDX);
//	else pio_set_pin_high(PIO_PD13_IDX);

	Sys_ms_Ticks ++;
	Sys_Elapsed_time ++;
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_TurnPower_ON_OFF(uint8_t PowerOn)	// for Hand-held OSA battery control
{
	//pio_set_pin_low(PIO_PD13_IDX);
	
	if (PowerOn == 1) delay_ms(500);
	else delay_s(2);
	//pio_set_pin_high(PIO_PD13_IDX);
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Uart_Initial(void)
{
	const usart_serial_options_t uart_serial_options =
	{
		.baudrate = MTF_UART_BAUDRATE,
		.paritytype = MTF_UART_PARITY
	};
	//--------------------------------------------------------------------------
	//uart_reset((Usart*)MTF_UART);
	usart_serial_init((Usart*)MTF_UART, (usart_serial_options_t*)&uart_serial_options);
	//uart_enable_rx((Usart*)MTF_UART);
	//uart_enable_tx((Usart*)MTF_UART);

	// Enable UART interrupt at RX ready
	uart_enable_interrupt(MTF_UART, UART_IER_RXRDY);
	// Enable UART interrupt
	NVIC_EnableIRQ(MTF_UART_IRQn);
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_SPI_Initial(void)
{
	spi_enable_clock(MOpm_SPI);
	spi_disable(MOpm_SPI);
	spi_reset(MOpm_SPI);
	spi_set_lastxfer(MOpm_SPI);
	spi_set_master_mode(MOpm_SPI);
	spi_disable_mode_fault_detect(MOpm_SPI);
	spi_set_peripheral_chip_select_value(MOpm_SPI, NO_CH_SELECTED);
	spi_set_fixed_peripheral_select(MOpm_SPI);
	
	spi_disable_peripheral_select_decode(MOpm_SPI);		//CS lines are connected directly to devices.	
	//spi_enable_peripheral_select_decode(MOpm_SPI);	//CS lines are connected to a decoder.

	spi_set_delay_between_chip_select(MOpm_SPI, SPI_DLYBCS);

	// SPI for DAC1, DAC2 and FLASH-M25P80
	spi_set_clock_polarity(MOpm_SPI, SPI_CH0_3, SPI_CH0_CLK_POL);
	spi_set_clock_phase(MOpm_SPI, SPI_CH0_3, SPI_CH0_CLK_PHASE);
	spi_set_bits_per_transfer(MOpm_SPI, SPI_CH0_3, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(MOpm_SPI, SPI_CH0_3, (sysclk_get_cpu_hz()/SPI_CLK_CH0_3));
	spi_set_transfer_delay(MOpm_SPI, SPI_CH0_3, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(MOpm_SPI, SPI_CH0_3, SPI_CS_RISE_NO_TX);

	// SPI for TMP125
	spi_set_clock_polarity(MOpm_SPI, SPI_CH4_7, SPI_CH4_CLK_POL);
	spi_set_clock_phase(MOpm_SPI, SPI_CH4_7, SPI_CH4_CLK_PHASE);
	spi_set_bits_per_transfer(MOpm_SPI, SPI_CH4_7, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(MOpm_SPI, SPI_CH4_7, (sysclk_get_cpu_hz()/SPI_CLK_CH4_7));
	spi_set_transfer_delay(MOpm_SPI, SPI_CH4_7, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(MOpm_SPI, SPI_CH4_7, SPI_CS_RISE_NO_TX);

	// SPI for Flash-AT45DB161E
	spi_set_clock_polarity(MOpm_SPI, SPI_CH8_11, SPI_CH8_CLK_POL);
	spi_set_clock_phase(MOpm_SPI, SPI_CH8_11, SPI_CH8_CLK_PHASE);
	spi_set_bits_per_transfer(MOpm_SPI, SPI_CH8_11, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(MOpm_SPI, SPI_CH8_11, (sysclk_get_cpu_hz()/SPI_CLK_CH8_11));
	spi_set_transfer_delay(MOpm_SPI, SPI_CH8_11, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(MOpm_SPI, SPI_CH8_11, SPI_CS_KEEP_LOW);
	
	// SPI for FPGA DPRAM
	spi_set_clock_polarity(MOpm_SPI, SPI_CH12_14, SPI_CH12_CLK_POL);
	spi_set_clock_phase(MOpm_SPI, SPI_CH12_14, SPI_CH12_CLK_PHASE);
	spi_set_bits_per_transfer(MOpm_SPI, SPI_CH12_14, SPI_CSR_BITS_8_BIT);
	spi_set_baudrate_div(MOpm_SPI, SPI_CH12_14, (sysclk_get_cpu_hz()/SPI_CLK_CH12_14));
	spi_set_transfer_delay(MOpm_SPI, SPI_CH12_14, SPI_DLYBSP, SPI_DLYBCT);
	spi_configure_cs_behavior(MOpm_SPI, SPI_CH12_14, SPI_CS_KEEP_LOW);

	spi_enable(MOpm_SPI);
}
////////////////////////////////////////////////////////////////////////////////
void OnOpen_USB_Port(void)
{
	udc_start();
}
////////////////////////////////////////////////////////////////////////////////
void OnClose_USB_Port(void)
{
	udc_stop();
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Log_Info(uint8_t InfoID)
{
}
////////////////////////////////////////////////////////////////////////////////
void OnMTF_CheckTemperature(uint32_t CheckTime)
{
	OnRead_Temp();
	
	if (CurrentTmp > MTF_MAX_TEMP) HW_Failure_Reg |= MTF_TEMP_TOO_HIGH;

	//-------------------------------------------------------------------------
	if (CheckTime > WAIT_PERIOD)
	{
		if (((CurrentTmp > Pre_Tmp) && ((CurrentTmp - Pre_Tmp) > ADJ_TEMP_RANGE))
			|| ((CurrentTmp < Pre_Tmp) && ((Pre_Tmp - CurrentTmp) > ADJ_TEMP_RANGE)))
		{
//			if (MTF_Mode == RUN_NORMAL)	OnSetTF_Position(Ch_Num, Ch_Wavelength);
//			else if (MTF_Mode == MTF_SCAN) OnMTF_SetEqWL_Table();
			
			OnMTF_Creat_ETable();
			OnMOPM_RenewPower_Table();
		}
	}
	else 
	{
		Pre_Tmp = CurrentTmp;			// No adjust during waiting period
	}
}
////////////////////////////////////////////////////////////////////////////////
void OnMTF_Data_Initial(void)
{
	OnMTF_Set_Parameters();
	
	OnRead_Temp();
	Pre_Tmp = CurrentTmp;
	
	OnFlash_Initial();
	if (Flash_Initial_Ok) OnLoad_CalibrationTables();
	//--------------------------------------------------------------
	// Use initial T = 22 degree if TMP125 not ready
	//Tmp125_Index = 0;	TmpR_Index = 0;
	//OnRead_TmpR();
	
	if (Tmp125X10 == 0) Tmp125X10 = TEMP_INITIAL;
	if (TmpRX10 == 0) TmpRX10 = TEMP_INITIAL;
	
	
	
	//--------------------------------------------------------------
	Ch_Num = 1;							// Set initial position at CH-1
	Ch_Wavelength = Ch0_WL;			// Set initial WL = 1529163pm

	if (Flash_Initial_Ok)	// Set to CH-1 only with valid Calibration data
		OnSetTF_Position(Ch_Num, Ch_Wavelength);
		ScanStartFreq = ChN_Freq - Freq_Offset;
		ScanStopFreq  = Ch0_Freq - Freq_Offset;
	
	//--------------------------------------------------------------	
	// for M178 Device Reset (0x40) only
	Rx_Cmd_Type = UART_PORT_CMD;
	flash_read_user_signature(signature, (uint32_t)sizeof(signatureInfo_t));
/*	if (signature->Loaded_Ver!=VerA || signature->Loaded_Ver!=VerB)
	{
		signature->Loaded_Ver = VerA;
		signature->VerA_Info.s1 = 0x55;
		signature->VerA_Info.s2 = 0xAA;
		signature->VerA_Info.s3 = 0x11;
		signature->VerA_Info.s4 = 0xEE;
		signature->Requested_Ver = 0xFF;
		saveSignature = TRUE;
		onMTF_SaveSignature(saveSignature, signature);
	}
*/	
	saveSignature = signature->ackCmdRST;
	if (signature->ackCmdRST==TRUE)
	{
		signature->ackCmdRST = FALSE;
		onMTF_SaveSignature(saveSignature, signature);
		if (Module_Type==ModuleType_M178)
			Reply_to_UART_M178(ID_DeviceReset, ReservedID, ReservedID, M178Cmd_OK);
		else
			Reply_to_UART_USER1(Oclaro_Cmd40, No_ERROR);
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Set_Parameters(void)
{
	HW_ErrorCode = 0;
	Sys_ms_Ticks = 0;		MOpm_ScanDone = 0;
	
	Rx_head = 0;			Rx_Index = 0;				MOpm_UART_Ready = 0;	
	ScanCount = 0;			Slot_ID = 0;				Rx_Length = 0;
	ERC_TableLength = 0;	FERC_Table_Ready = 0;

	Total_ChNum = 91;		Total_ChNum_Plus = Total_ChNum + 2;
	ScanStartCh = 0;		ScanEndCh =  Total_ChNum;
	
	//MinPeakLevel = 17408;	Contrast = 768;		//for DEMO: -40dBm,256*(128-40)=22528; 5dB,256*5=1280
												// -20dBm for Huwai and -35/-28 dBm for ZTE
	//MinPeakLevel = 24000;	Contrast = 1800;
	//MinPeakLevel = 3000;	Contrast = 500;		// for ADC
	//MinPeakLevel = 27136;	Contrast = 1800; //for power in Q8, -22dBm=>256*(128-22)=27136; 	7.2dB=>256*7.2=1843
    //MinPeakLevel = 27136;	Contrast = 256; //for power in Q8, -22dBm=>256*(128-22)=27136; 	1dB=>256*1=256 (The larger 3dB BW, the smaller contrast) (3dB BW = 40GHz)
	EQS_Space = 1;			Ch_Space = 50;
	EQWL_deltaWL = 10;		// 10pm/point for EQ WL Scan
		
	Offset_DAC = 0;
	ScanEndPos = 65535;
	ScanStartPos = 0;	
	ScanPosIndex = 0;
	ScanStartFreq = 191700 - Freq_Offset;
	ScanStopFreq = 196300 - Freq_Offset;
	
	EqsF = ScanStartFreq;
	
	Ch0_WL = 1529163;		// Ch0 is Short WL end
	ChN_WL = 1565899;		// ChN is Long WL end
	
	Ch_Voltage = 10.123f;
	Ch_Wavelength = 1560123.f;
	
	RawDataLength = MTF_BUF_SIZE;
	Ruler_Space = RULER_50G_SPACE;

	Ch_Dir = 0;
	V_STEP0 = (uint16_t)(100 * DAC_DATA_SCALE);		// V-STEP = 0.1v
	
	MTF_Mode = RUN_NORMAL;
	//---------------------------------------------
	// Set default P_Table head, can be changed by flash data
/*	
	PTable_head[0] = 256*(128 - 10);	// -10dBm
	PTable_head[1] = 256*(128 - 20);	// -20dBm
	PTable_head[2] = 256*(128 - 30);	// -30dBm
	PTable_head[3] = 256*(128 - 40);	// -40dBm
	PTable_head[4] = 256*(128 - 50);	// -50dBm
*/	
	//---------------------------------------------
	Uart_CmdID = NORMAL_CMD;
	UL_CmdID = UL00_CMD;
	
	Flash_Initial_Ok = FALSE;
	NoiseFilterInstalled = FALSE;
	ETable_Ready = FALSE;
	PTable_Ready = FALSE;
	
	//---------------------------------------------
	if (Scan_Pos_Resolution == 0)
	{
		Scan_Pos_Offset = 0;
		Scan_Pos_Resolution = SCAN_MAX_LENGTH;
	}
	
	//---------------------------------------------
	saveSignature = FALSE;
	
	//---------------------------------------------
#ifndef simADC
	refTmp125 = FALSE;
#else
	refTmp125 = TRUE;
#endif
	
	//---------------------------------------------
	forceModuleType = FALSE;
	
	//----------------------------------------------------
	updateT = TRUE;
}
/////////////////////////////////////////////////////////////////////////////////