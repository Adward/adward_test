 /////////////////////////////////////////////////////////////////////////////
 //  Uart_Cmd.c															    //
 //																			//
 /////////////////////////////////////////////////////////////////////////////
#include "MTF.h"
//////////////////////////////////////////////////////////////////////////////
// Interrupt Service Routing for MOpm_UART
void MOpmUART_Handler(void){OnMTF_UART_Rx();}
//////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_Rx()
{
	// Read RX byte
	Rx_byte = (uint8_t)((MTF_UART)->UART_RHR);
	if (Rx_Index < UART_BUF_SIZE) 
	{
		RxTx_buf[Rx_Index] = Rx_byte;
		Rx_Index ++;
	}
	//--------------------------------------------------------
	if (Rx_Index < 4) return;
	else if (MOpm_UART_Ready == 0)
	{
		if ((Rx_Index == 4)&&(Rx_head == 0)) OnMTF_Rx_head();
		
		Rx_Cmd_Type = UART_PORT_CMD;
		OnMTF_Check_RxCmd();
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Rx_head(void)
{
	if ((RxTx_buf[0] == 0xAA)&&(RxTx_buf[1] >= 'A')&&(RxTx_buf[1] <= 'Z'))
		Rx_head = OptoplexCmd;
	
	else if ((RxTx_buf[0] == 0x55)&&(RxTx_buf[1] == 0x00)&&(RxTx_buf[2] == 0x00)&&(RxTx_buf[3] == 0x00))
		Rx_head = M178Cmd;	//20170629 VG: for M178
		
	//else if ((RxTx_buf[0] == 0x00)&&(RxTx_buf[1] == 0x00)&&(RxTx_buf[2] == 0x00)&&(RxTx_buf[3] == 0x03))
	else if ((RxTx_buf[0] == 0x00)&&(RxTx_buf[1] == 0x00)&&(RxTx_buf[2] == 0x00)&&(RxTx_buf[3]==0x03 || RxTx_buf[3]==0x10 || RxTx_buf[3]==0x11 || RxTx_buf[3]==0x12 || RxTx_buf[3]==0x30 || RxTx_buf[3]==0x40))
		Rx_head = User1Cmd;	//20170116 VG: for Oclaro Co. 
		
	// "3.1415" and "TPTP" for UART auto detection 
	else if ((RxTx_buf[0] == '3')&&(RxTx_buf[1] == '.')&&(RxTx_buf[2] == '1')&&(RxTx_buf[3] == '4'))
		Rx_head = EchoCmd;
	else if ((RxTx_buf[0] == 'T')&&(RxTx_buf[1] == 'P')&&(RxTx_buf[2] == 'T')&&(RxTx_buf[3] == 'P'))
		Rx_head = EchoCmd;	
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_Check_RxCmd(void)
{
	if (Rx_head == OptoplexCmd)
	{
		if (Rx_Index >= 7) 
			Rx_Length = (((uint16_t)RxTx_buf[5])*256 + RxTx_buf[6])*2;	// Length in WORDs
		if ((Rx_Length > 0) && (Rx_Index >= Rx_Length + CMD_OVERHEAD2)) 
			MOpm_UART_Ready = 1;	
	}
	else if (Rx_head == M178Cmd)
	{
		if (Rx_Index >= 9)
			Rx_Length = ( ((uint32_t)RxTx_buf[5]<<24) + ((uint32_t)RxTx_buf[6]<<16) + ((uint32_t)RxTx_buf[7]<<8) + (uint32_t)RxTx_buf[8] );	// Length in Bytes				
		if ((Rx_Length >= 0x14) && (Rx_Index >= Rx_Length + CMD_OVERHEAD_M178))
			MOpm_UART_Ready = 1;
	}
	else if (Rx_head == User1Cmd)
	{
		if (Rx_Index >= 7) 
			//Rx_Length = 0x2C;											// Fixed Length in Byte
			Rx_Length = ( ((uint32_t)RxTx_buf[4]<<24) + ((uint32_t)RxTx_buf[5]<<16) + ((uint32_t)RxTx_buf[6]<<8) + (uint32_t)RxTx_buf[7] );	// Length in Bytes
		if ((Rx_Length > 0) && (Rx_Index >= Rx_Length))					MOpm_UART_Ready = 1;
	}
	else if (Rx_head == EchoCmd)
	{
		if (Rx_Index >= 3) Rx_Length = 0x03;											// Fixed Length in Byte
		if ((Rx_Length > 0) && (Rx_Index >= Rx_Length))					MOpm_UART_Ready = 1;
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnSys_USB_RX(uint8_t port)
{
	int16_t DataLength;
	
	DataLength = udi_cdc_get_nb_received_data();
	
	if (MOpm_UART_Ready == 0)
	{
		if (Rx_head == 0)
		{
			udi_cdc_read_buf((void*)RxTx_buf, DataLength);
			OnMTF_Rx_head();
			//--------------------------------------------------------
			Rx_Length = ((uint16_t)RxTx_buf[3])*256 + RxTx_buf[4];	//20170116 VG: no used? TBD
			Rx_Index = DataLength;
		}
		else if (Rx_Index + DataLength <= UART_BUF_SIZE)
		{
			udi_cdc_read_buf((void*)&RxTx_buf[Rx_Index], DataLength);
			Rx_Index += DataLength;
		}
		//-------------------------------------------------------------
		Rx_Cmd_Type = USB_PORT_CMD;
		OnMTF_Check_RxCmd();
	}
}
///////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_Tx(uint16_t Length)
{
	uint16_t Index;

	if (Rx_Cmd_Type == UART_PORT_CMD)
	{
		for (Index = 0; Index < Length; Index ++)
		{
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = RxTx_buf[Index];
		}
	}
	else
	{
		for (Index = 0; Index < Length; Index ++)
		{
			udi_cdc_putc(RxTx_buf[Index]);
		}
	}
}
///////////////////////////////////////////////////////////////////////////////
