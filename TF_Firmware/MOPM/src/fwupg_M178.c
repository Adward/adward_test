/*
 * fwupg_M178.c
 *
 * Created: 2017/9/29 09:51:40
 *  Author: Administrator
 */ 

#include "MTF.h"
enum{FwUp_Normal, FwUp_Init, FwUp_Load, FwUp_Install};

static uint8_t	M178_FwUp_state = FwUp_Normal;
static uint32_t	UpgPROG_ADDRESS, start_address, crc_address;



uint16_t updcrc(uint8_t c, uint16_t crc)
{
	uint8_t flag;
	for (uint8_t i = 0; i < 8; ++i)
	{
		flag = !!(crc & 0x8000);
		crc <<= 1;
		if (c & 0x80)
		crc |= 1;
		if (flag)
		crc ^= 0x1021;
		c <<= 1;
	}
	return crc;
}

// Calc the file CRC and validate against the last 2 bytes
uint16_t ccitt_check_file_crc(uint8_t *frame, int32_t codeLength) {
	uint16_t crc = 0xFFFF;
	//uint8_t *ptr = frame;
	ptr = frame;
	// int32_t ii;
	// uint8_t ggyy;

/*	
	while (--codeLength  >= 0) {
		crc = crc ^ (int) *ptr++ << 8;
		int32_t i = 8;
		do {
			if (crc & 0x8000) {
				crc = crc << 1 ^ (0x1021);
				}else{
				crc = crc << 1;
			}
		}while(--i);
	}
*/

	for (float_t iii=0; iii < codeLength; iii++) {
		//crc = updcrc(*ptr++, crc);
		
		//ggyy = *ptr++;
		//crc = updcrc(ggyy, crc);
		crc = updcrc(*ptr++, crc);
		
		//----------------------------------------------------
		/* Restart watchdog */
		wdt_restart(WDT);
	}
	// augment
	crc = updcrc(0, updcrc(0, crc));

	// Compare the crc with the last 2 bytes of frame
//	fileCRC_high =  crc >> 8;		// the HI CRC byte
//	fileCRE_low =  crc & 0X00FF;	// The LOW CRC byte

	return(crc);
}

// Reset the content before write
uint8_t xmodem_clean_buffer_before_write(uint32_t address)
{
	uint32_t	status1, status2;
	
	status1 = flash_erase_sector((uint32_t)address);						//prepare program memory area for buffer copy
	status2 = flash_erase_sector((uint32_t)(address + SECTOR_SIZE));		//allocated 2 sectors for program
	
	if(status1>0 || status2>0)
		return TRUE;
	else
		return FALSE;
}

// Clean progA,  copy progC -> progA or ProgB
uint8_t xmodem_region_copy(uint32_t destAddress)
{
	uint32_t	status1, status2;
	
	// clean progA
	status1 = xmodem_clean_buffer_before_write(destAddress);
	
	// Copy
	status2 = flash_write(destAddress, (uint8_t *)TEMP_C_ADDRESS, SECTOR_SIZE * 2 ,0);	//transfer firmware to firmware download buffer
	
	if(status1>0 || status2>0)
		return TRUE;
	else
		return FALSE;
}

uint8_t onMOPM_FwUp_Init()
{
	uint32_t	status0, status1, status2;
	
	//if (M178_FwUp_state == FwUp_Normal)
	//{
		status0 = flash_init(FLASH_ACCESS_MODE_128, 6);
		// Read Signature Flash information
		status1 = flash_read_user_signature(signature, (uint32_t)sizeof(signatureInfo_t));
		
		if (signature->Loaded_Ver == VerB)
			UpgPROG_ADDRESS = PROG_A_ADDRESS;
		else 
			UpgPROG_ADDRESS = PROG_B_ADDRESS;
		
		// Clean temporary flash buffer	
		status2 = xmodem_clean_buffer_before_write(TEMP_C_ADDRESS);
		
		if(status0>0 || status1>0 || status2>0)
		{
			M178_FwUp_state = FwUp_Normal;
			return TRUE;
		}
		else
		{
			M178_FwUp_state = FwUp_Init;
			start_address = TEMP_C_ADDRESS;
			return FALSE;
		}
	//}
}

uint8_t onMOPM_FwUp_Load(uint32_t HostSeqNo, uint32_t dataSize, uint8_t *frame)
{
	uint8_t *ptr = frame;
	uint32_t	fwriteStatus;
	ptr +=  17;  // Skip frame header
	
	if (M178_FwUp_state == FwUp_Init)
	{
		M178_FwUp_state = FwUp_Load;
		seqNO = 1;
	}
	
	if (M178_FwUp_state == FwUp_Load)
	{
		if (HostSeqNo == seqNO)
		{
			fwriteStatus = flash_write(start_address, (uint8_t *)ptr, dataSize ,0); //transfer firmware to firmware download buffer
			start_address = start_address + dataSize;
			
			if (fwriteStatus)
				return TRUE;	// flash write error
			else
			{
				seqNO++;
				return FALSE;
			}
		}
		else
			return TRUE;		// seqNO Error		
	}
}

uint8_t onMOPM_FwUp_Load_oclaro(uint32_t HostSeqNo, uint32_t dataSize, uint8_t *frame)
{
	uint8_t *ptr = frame;
	uint32_t	fwriteStatus;
	ptr +=  24;  // Skip frame header
	
	if (M178_FwUp_state == FwUp_Init)
	{
		M178_FwUp_state = FwUp_Load;
		FwUg_SeqNo = 1;
	}
	
	if (M178_FwUp_state == FwUp_Load)
	{
		if (HostSeqNo == FwUg_SeqNo)
		{
			fwriteStatus = flash_write(start_address, (uint8_t *)ptr, dataSize ,0); //transfer firmware to firmware download buffer
			start_address = start_address + dataSize;
			
			if (fwriteStatus)
			return TRUE;	// flash write error
			else
			{
				FwUg_SeqNo++;
				return FALSE;
			}
		}
		else
		return TRUE;		// seqNO Error
	}
}

uint32_t onMOPM_FwUP_Install()
{
	bool testLocation;
	int32_t	codeLength;
	uint8_t buff[16], crcBuf[2];
	uint16_t crcRx, crcCal;
	
	
	if (M178_FwUp_state == FwUp_Load)
	{
		M178_FwUp_state = FwUp_Install;
		
		// Receive done. Copy from progC -> progA and execute it.
		crc_address = start_address - 4;
		memcpy(crcBuf, crc_address, sizeof(crcBuf));
		
		codeLength = crc_address - TEMP_C_ADDRESS;
		crcRx = crcBuf[0] + crcBuf[1] * 256;
	}

	// check CRC for entire code only
//	for (uint8_t xxx=1; xxx<10; xxx++)
//	{
//		crc = 0xFFFF;
//		crcCal = ccitt_check_file_crc(TEMP_C_ADDRESS+0x400, (int32_t)0x10*xxx);
//	}	
	// crcCal = ccitt_check_file_crc(TEMP_C_ADDRESS);
//	crc = 0xFFFF;
	crcCal = ccitt_check_file_crc(TEMP_C_ADDRESS, (int32_t)codeLength);
	
	if(M178_FwUp_state == FwUp_Install)
	{
		M178_FwUp_state = FwUp_Normal;
		
		memcpy(buff, TEMP_C_ADDRESS, sizeof(buff));
		testLocation = (UpgPROG_ADDRESS == PROG_A_ADDRESS) ? (buff[10]>=(PROG_B_ADDRESS>>16)) : (buff[10]<(PROG_B_ADDRESS>>16));	// PASS = FALSE
		if (testLocation)
			return(M178Cmd_FlashSel_ERROR);
		else if(crcRx != crcCal)
			return(M178Cmd_RxFile_CRC_Error);			
		else if(xmodem_region_copy(UpgPROG_ADDRESS))
			return(M178Cmd_FlashInstall_ERROR);
		else
		{
			if(UpgPROG_ADDRESS == PROG_A_ADDRESS)
			{
				signature->Loaded_Ver = VerA;
				signature->VerA_Info.s1 = M178_FW_VER_M1;
				signature->VerA_Info.s2 = M178_FW_VER_M2;
				signature->VerA_Info.s3 = M178_FW_VER_D1;
				signature->VerA_Info.s4 = M178_FW_VER_D2;
			}
			else
			{
				signature->Loaded_Ver = VerB;
				signature->VerB_Info.s1 = M178_FW_VER_M1;
				signature->VerB_Info.s2 = M178_FW_VER_M2;
				signature->VerB_Info.s3 = M178_FW_VER_D1;
				signature->VerB_Info.s4 = M178_FW_VER_D2;
			}
			signature->Requested_Ver = 0xFF;	
			saveSignature = TRUE;
			return(M178Cmd_OK);
		}
	}			
}

void onMTF_SaveSignature(uint8_t saveSignature, signatureInfo_t *signature)
{
	if (saveSignature) {
		flash_erase_user_signature();
		flash_write_user_signature(signature,(uint32_t)sizeof(signatureInfo_t));
		
		// Read Signature to return to Code Area
		flash_read_user_signature(signature, (uint32_t)sizeof(signatureInfo_t));
		saveSignature = FALSE;
	}
}