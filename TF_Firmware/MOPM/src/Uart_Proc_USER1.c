 /////////////////////////////////////////////////////////////////////////////
 //  Uart_Proc_User1.c														//
 //																			//
 /////////////////////////////////////////////////////////////////////////////
#include "MTF.h"

#define	Data_CKS_ERROR		0x000027A2
#define	Message_CKS_ERROR	0x000027A3
#define Length_ERROR		0x000027A4
#define RX_UNKNOW_CMD_ERROR	0x00002783
//////////////////////////////////////////////////////////////////////////////
uint32_t convert_float32_to_IEEE32(float32_t number)
{
	uint32_t Data32;
	union
	{
		uint8_t StrValue[4];	// High-byte first and Low-byte last
		int32_t Int32;
		float Float32;
	} Ieee32;
	
	Ieee32.Float32 = number;
	Data32 = 0;
	Data32  = (uint32_t)Ieee32.StrValue[3]<<24;
	Data32 += (uint32_t)Ieee32.StrValue[2]<<16;
	Data32 += (uint32_t)Ieee32.StrValue[1]<<8;
	Data32 += (uint32_t)Ieee32.StrValue[0];
	
	return Data32;
}

//////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_Decode_USER1(void)
{
	uint16_t Data8;
	// uint16_t Freq_Begin, Freq_End;
	uint32_t MsgCmd, MsgLendth, SubCmd, DecimateN, RX_DataChecksum32, RX_MsgChecksum32;
	uint32_t DataChecksum32, MsgChecksum32;	
	uint32_t FwUg_Length, statusInstall;

	//-------------------------------------------------
	// HEADER_ID
	Data8 = 0;			
	MsgCmd  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
	MsgCmd += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
	MsgCmd += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
	MsgCmd += (uint32_t)(RxTx_buf[Data8 ++]);
	//-------------------------------------------------
	// HEADER_Length
	MsgLendth  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
	MsgLendth += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
	MsgLendth += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
	MsgLendth += (uint32_t)(RxTx_buf[Data8 ++]);
	//-------------------------------------------------
	Data8 = 16;
	// PAYLOAD_Sub-Command
	if (MsgCmd == 0x11)			// FW_Load Command in firmware upgrade
	{
		FwUg_SeqNo  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
		FwUg_SeqNo += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
		FwUg_SeqNo += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
		FwUg_SeqNo += (uint32_t)(RxTx_buf[Data8 ++]);
		
		FwUg_Length  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
		FwUg_Length += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
		FwUg_Length += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
		FwUg_Length += (uint32_t)(RxTx_buf[Data8 ++]);
	}
	else if (MsgCmd == 0x03)	// Scan command
	{
	// PAYLOAD_Sub-Command
		SubCmd  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
		SubCmd += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
		SubCmd += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
		SubCmd += (uint32_t)(RxTx_buf[Data8 ++]);
	//-------------------------------------------------
	// PAYLOAD_Scan Range
		Freq_Begin  = (uint16_t)(RxTx_buf[Data8 ++]) << 8;
		Freq_Begin += (uint16_t)(RxTx_buf[Data8 ++]);
		Freq_End    = (uint16_t)(RxTx_buf[Data8 ++]) << 8;
		Freq_End   += (uint16_t)(RxTx_buf[Data8 ++]);	
	//-------------------------------------------------
	// PAYLOAD_Decimation Factor
	DecimateN  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
	DecimateN += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
	DecimateN += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
	DecimateN += (uint32_t)(RxTx_buf[Data8 ++]);
	decimateN = DecimateN;
	//-------------------------------------------------
	}
	// FOOTER_Checksum
	Data8 = MsgLendth-12;
	RX_DataChecksum32  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
	RX_DataChecksum32 += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
	RX_DataChecksum32 += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
	RX_DataChecksum32 += (uint32_t)(RxTx_buf[Data8 ++]);
	
	Data8 = MsgLendth-4;
	RX_MsgChecksum32  = (uint32_t)(RxTx_buf[Data8 ++]) << 24;
	RX_MsgChecksum32 += (uint32_t)(RxTx_buf[Data8 ++]) << 16;
	RX_MsgChecksum32 += (uint32_t)(RxTx_buf[Data8 ++]) << 8;
	RX_MsgChecksum32 += (uint32_t)(RxTx_buf[Data8 ++]);
	
	
	Data8 = 0;
	Uart_CmdID = NORMAL_CMD;						// Reset Uart_CmdID
	//-------------------------------------------------
	if ((MsgCmd==0x03) && (MsgLendth != 0x2C))		// Scan commands
	{
		Reply_to_UART_USER1(SCAN_Cmd8, Length_ERROR);
		return;
	}
	
	if ((MsgCmd==0x30) && (MsgLendth != 0x20))		// FW version inquiry
	{
		Reply_to_UART_USER1(Oclaro_Cmd30, Length_ERROR);
		return;
	}
	
	if ((MsgCmd==0x40) && (MsgLendth != 0x20))		// Device Reset
	{
		Reply_to_UART_USER1(Oclaro_Cmd40, Length_ERROR);
		return;
	}
	//-------------------------------------------------
	DataChecksum32 = 0;
	for (UartData16 = 16; UartData16 < (MsgLendth-12); UartData16 ++)
	{
		DataChecksum32 += RxTx_buf[UartData16];
	}
	
	MsgChecksum32 = 0;
	for (UartData16 = 0; UartData16 < (MsgLendth-4); UartData16 ++)
	{
		MsgChecksum32 += RxTx_buf[UartData16];
	}
	//----------------------------------------------------------------------------------
	if ((MsgCmd == 0x03) && (SubCmd == 0x08 || SubCmd == 0x09 || SubCmd == 0x0F || SubCmd == 0x01))
	{
		//if (MsgLendth != 0x2C) 							Reply_to_UART_USER1(SCAN_Cmd8, Length_ERROR);
		if (~DataChecksum32 != RX_DataChecksum32)		Reply_to_UART_USER1(SCAN_Cmd8, Data_CKS_ERROR);
		else if (~MsgChecksum32 != RX_MsgChecksum32)	Reply_to_UART_USER1(SCAN_Cmd8, Message_CKS_ERROR);
		
		else
		{
			OnMTF_SetScanPos2(0);
			OnMTF_XDMAC_Initial(AFEC0, SCAN_MAX_LENGTH);
			delay_ms(30);
			
			if (SubCmd == 0x08) Uart_CmdID = USER1_SCAN_Cmd8;
			else if (SubCmd == 0x09) Uart_CmdID = USER1_SCAN_Cmd9;
			else if (SubCmd == 0x01) Uart_CmdID = USER1_SCAN_Cmd1;
			else if (SubCmd == 0x0F) Uart_CmdID = USER1_SCAN_CmdF;
			
			MTF_Mode = MOPM_RW_SCAN;
			OnMTF_StartScan();
		}
	} //---------------------------------------------------------------------------
	else if (MsgCmd == 0x30)	// Version Request
	{
		if (~DataChecksum32 != RX_DataChecksum32)		Reply_to_UART_USER1(Oclaro_Cmd30, Data_CKS_ERROR);
		else if (~MsgChecksum32 != RX_MsgChecksum32)	Reply_to_UART_USER1(Oclaro_Cmd30, Message_CKS_ERROR);
		else											Reply_to_UART_USER1(Oclaro_Cmd30, No_ERROR);
	} //---------------------------------------------------------------------------
	else if (MsgCmd == 0x40)	// Device Reset
	{
		if (~DataChecksum32 != RX_DataChecksum32)		Reply_to_UART_USER1(Oclaro_Cmd40, Data_CKS_ERROR);
		else if (~MsgChecksum32 != RX_MsgChecksum32)	Reply_to_UART_USER1(Oclaro_Cmd40, Message_CKS_ERROR);	
		else
		{
			signature->ackCmdRST = TRUE;		// reply to Host after soft reset
			saveSignature = TRUE;
			onMTF_SaveSignature(saveSignature, signature);
			while (1) NVIC_SystemReset();
		}
	} //---------------------------------------------------------------------------
	else if (MsgCmd == 0x10)	// FW Download Initialization
	{
		if (~DataChecksum32 != RX_DataChecksum32)		Reply_to_UART_USER1(Oclaro_Cmd10, Data_CKS_ERROR);
		else if (~MsgChecksum32 != RX_MsgChecksum32)	Reply_to_UART_USER1(Oclaro_Cmd10, Message_CKS_ERROR);
		else if(onMOPM_FwUp_Init())						Reply_to_UART_USER1(Oclaro_Cmd10, M178Cmd_Flash_Error);
		else											Reply_to_UART_USER1(Oclaro_Cmd10, No_ERROR);
	}
	//---------------------------------------------------------------------------
	else if (MsgCmd == 0x11)	// FW Load
	{
		if (~DataChecksum32 != RX_DataChecksum32)							Reply_to_UART_USER1(Oclaro_Cmd11, Data_CKS_ERROR);
		else if (~MsgChecksum32 != RX_MsgChecksum32)						Reply_to_UART_USER1(Oclaro_Cmd11, Message_CKS_ERROR);
		else if(onMOPM_FwUp_Load_oclaro(FwUg_SeqNo, FwUg_Length, RxTx_buf))	
			Reply_to_UART_USER1(Oclaro_Cmd11, M178Cmd_Flash_Error);
		else																
			Reply_to_UART_USER1(Oclaro_Cmd11, No_ERROR);
	}
	//---------------------------------------------------------------------------
	else if (MsgCmd == 0x12)	// FW  Install
	{
		if (~DataChecksum32 != RX_DataChecksum32)						Reply_to_UART_USER1(Oclaro_Cmd10, Data_CKS_ERROR);
		else if (~MsgChecksum32 != RX_MsgChecksum32)					Reply_to_UART_USER1(Oclaro_Cmd10, Message_CKS_ERROR);
		else
		{
			statusInstall = onMOPM_FwUP_Install();
			switch(statusInstall)
			{
				case M178Cmd_OK:
				Reply_to_UART_USER1(ID_FwInstall, M178Cmd_OK);
				break;
				case M178Cmd_FlashSel_ERROR:	// not expected location for A or B
				Reply_to_UART_USER1(ID_FwInstall, M178Cmd_FlashSel_ERROR);
				break;
				case M178Cmd_RxFile_CRC_Error:	// CRC or check sum error for the Rx file.  Not ready yet.
				Reply_to_UART_USER1(ID_FwInstall, M178Cmd_RxFile_CRC_Error);
				break;
				case M178Cmd_FlashRead_ERROR:	// not implement
				Reply_to_UART_USER1(ID_FwInstall, M178Cmd_FlashRead_ERROR);
				break;
				default:						// write error from TempC
				Reply_to_UART_USER1(ID_FwInstall, M178Cmd_FlashInstall_ERROR);
				break;
			}
		};
	}
	else Reply_to_UART_USER1(MsgCmd, RX_UNKNOW_CMD_ERROR);
	//-----------------------------------------------------------------------------
}

//////////////////////////////////////////////////////////////////////////////////
// Reply any USER1 command no optical or power data required
void Reply_to_UART_USER1(uint32_t MsgCmd32, uint32_t ErrorCode32)
{
	uint8_t  i;
	uint16_t Index, Length;
	uint32_t Data32, DataChecksum32, MsgChecksum32;
	uint32_t Cmd_Reserved=0;
	
	Index = 0;	
	// HEADER ------------------------------------------
		// 1. Reply Command ID
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32);
		
		// 2.Length
		RxTx_buf[Index ++] = (uint8_t) 0;
		RxTx_buf[Index ++] = (uint8_t) 0;
		RxTx_buf[Index ++] = (uint8_t) 0;
		RxTx_buf[Index ++] = (uint8_t) 0;
		
		//3. Device Status, Reserved (always zero)
		RxTx_buf[Index ++] = (uint8_t) 0;
		RxTx_buf[Index ++] = (uint8_t) 0;
		RxTx_buf[Index ++] = (uint8_t) 0;
		RxTx_buf[Index ++] = (uint8_t) 0;
		
		// 4.temperature
		Data32 = (uint32_t)((int32_t)CurrentTmp);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data32);
	
	// PAYLOAD ------------------------------------------	
		// 5~8. Version Request and Device Reset 
		if (MsgCmd32 == Oclaro_Cmd30 || MsgCmd32 == Oclaro_Cmd40)
		{
			// 5.Payload_reserved 36-byte zero 
			for (Length=0; Length<36; Length++)	RxTx_buf[Index ++] = 0;
		
			// 6.Payload_37-byte FW Version (14 'FW Ver' + 23 '0')
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_1;
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_2;
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_3;
			RxTx_buf[Index ++] = (uint8_t)M178_SUPPLY_ID_4;
			
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_M1;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_M2;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_DOT;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_D1;
			RxTx_buf[Index ++] = (uint8_t)M178_HW_VER_D2;
			
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_M1;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_M2;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_DOT;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_D1;
			RxTx_buf[Index ++] = (uint8_t)M178_FW_VER_D2;
			
			for (i=0; i<23; i++)	RxTx_buf[Index ++] = 0;	// 23-byte 0
		
			// 7.Payload_20-byte Assembly SN (12'FW Ver' + 8 '0')
			RxTx_buf[Index ++] = Manufacture_date[8];	//(uint8_t)M178_SN_Y_1;
			RxTx_buf[Index ++] = Manufacture_date[9];	//(uint8_t)M178_SN_Y_2;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_S_3;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_S_4;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_M_5;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_C_6;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_V_7;
			RxTx_buf[Index ++] = (uint8_t)M178_SN_T_8;
			RxTx_buf[Index ++] = Module_SN[6];			//(uint8_t)M178_SN_1_9;
			RxTx_buf[Index ++] = Module_SN[7];			//(uint8_t)M178_SN_2_A;
			RxTx_buf[Index ++] = Module_SN[8];			//(uint8_t)M178_SN_3_B;
			RxTx_buf[Index ++] = Module_SN[9];			//(uint8_t)M178_SN_4_C;
			
			for (i=0; i<8; i++)		RxTx_buf[Index ++] = 0;	// 8-byte 0
		
			// 8.Payload_23-byte Filter SN 
			for (i=0; i<23; i++)	RxTx_buf[Index ++] = 0;	// 23-byte 0
		}//----------------------------------------------------------------
		else if (MsgCmd32==Oclaro_Cmd10)
		{
			if (ErrorCode32 == No_ERROR)
			{
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
			}
		}//----------------------------------------------------------------
		else if (MsgCmd32==Oclaro_Cmd12)
		{
			if (ErrorCode32 == No_ERROR)
			{
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
			}
		}//----------------------------------------------------------------
		else if (MsgCmd32==Oclaro_Cmd11)
		{
			if (ErrorCode32 == M178Cmd_OK)
			{
				RxTx_buf[Index ++] = (uint8_t)(FwUg_SeqNo >> 24);
				RxTx_buf[Index ++] = (uint8_t)(FwUg_SeqNo >> 16);
				RxTx_buf[Index ++] = (uint8_t)(FwUg_SeqNo >> 8);
				RxTx_buf[Index ++] = (uint8_t)(FwUg_SeqNo);
				
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 24);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 16);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved >> 8);
				RxTx_buf[Index ++] = (uint8_t)(Cmd_Reserved);
			}
		}//----------------------------------------------------------------
		
	
	// FOOTER ------------------------------------------		
		// 18. Data Checksum
		DataChecksum32 = 0;
		DataChecksum32 = ~DataChecksum32;
		RxTx_buf[Index ++] = (uint8_t)(DataChecksum32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(DataChecksum32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(DataChecksum32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(DataChecksum32);
	
		// 19.Error Code
		RxTx_buf[Index ++] = (uint8_t)(ErrorCode32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(ErrorCode32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(ErrorCode32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(ErrorCode32);
		
	//---------------------------------------------
	// 2.Fill Length
	Length = Index + 4;	//4 bytes = 1 32-bit double-words.  It's Data Message Checksum.
	
	RxTx_buf[4] = (uint8_t)(Length >> 24);
	RxTx_buf[5] = (uint8_t)(Length >> 16);
	RxTx_buf[6] = (uint8_t)(Length >> 8);
	RxTx_buf[7] = (uint8_t)(Length);
	Length = Index;
	//---------------------------------------------------------------------
	//20.MsgChecksum
	MsgChecksum32 = 0;
	for (Index = 0; Index < Length; Index ++) MsgChecksum32 += RxTx_buf[Index];
	MsgChecksum32 = ~MsgChecksum32;
	RxTx_buf[Length ++] = (uint8_t)(MsgChecksum32 >> 24);
	RxTx_buf[Length ++] = (uint8_t)(MsgChecksum32 >> 16);
	RxTx_buf[Length ++] = (uint8_t)(MsgChecksum32 >> 8);
	RxTx_buf[Length ++] = (uint8_t)(MsgChecksum32);
	//---------------------------------------------
	OnMTF_UART_Tx(Length);
	//---------------------------------------------
}
//////////////////////////////////////////////////////////////////////////////////
void OnMTF_UART_TxData_user1(uint8_t CmdType, uint32_t ErrorCode)
{
	uint8_t  Data8,  idx;
	uint16_t Data16, Index, Length;
	uint32_t Data32, MsgCmd32, DataChecksum32, MsgChecksum32;
	float32_t chP0, chP1, chP2, chP3, dBw3;
		
	Index = 0;
		
		// HEADER ------------------------------------------
		// 1. Command ID, Scan Spectrum (0x00000003) only
		MsgCmd32 = 0x00000003;
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(MsgCmd32);
		
		// 2.Length in byte (Header(H), Footer(F), Samples(M), Total Power(T), Peak Power(P), Spectrum Data(S)
		if (CmdType == USER1_SCAN_Cmd8)	Data32 = (uint32_t)(4*12 + 2*2*NumOfPeaks + 4*2*EqsDataLength);	//(H=8, F=3, M=1	 ); P=1; S=2 
		if (CmdType == USER1_SCAN_Cmd9)	Data32 = (uint32_t)(4*13 + 4*4*NumOfPeaks + 4*2*EqsDataLength);	//(H=8, F=3, M=1, T=1); P=4; S=2 
		if (CmdType == USER1_SCAN_Cmd1)	Data32 = (uint32_t)(4*11 + 2*2*NumOfPeaks + 4*2*0            );	//(H=8, F=3, M=0	 ); P=1; S=0 
		if (CmdType == USER1_SCAN_CmdF)	Data32 = (uint32_t)(4* 9 + 4*4*0          + 4*2*EqsDataLength);	//(H=5, F=3, M=1	 ); P=0; S=2 
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data32);
		
		// 3.Reserved (always zero)
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		// 4.temperature
		Data32 = (uint32_t)((int32_t)CurrentTmp);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data32);
		
		// PAYLOAD -----------------------------------------
		// 5.Reserved (always zero)
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		RxTx_buf[Index ++] = 0;
		
		if(CmdType != USER1_SCAN_CmdF)	// It's for all scan command except CmdF
		{
		// 6. Max raw ADC power
		//Data32 = (uint32_t)((int32_t)(SignalCh_Eqs[WF_PeakIndex]-32768));	//256*(128+power), power in dBm Q8 format
		Data32 = (uint32_t)((uint32_t)rawMaxADC);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data32);
		
		// 7. Frequency@Max power in GHz
		Data32 = (uint32_t)(ScanStartFreq + WF_PeakIndex + Freq_Offset - Freq_Offset_USER1);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data32);
		
		// 8. Total number of Channels Found
		Data32 = (uint32_t)NumOfPeaks;
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 24);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 16);
		RxTx_buf[Index ++] = (uint8_t)(Data32 >> 8);
		RxTx_buf[Index ++] = (uint8_t)(Data32);
		}
		
		
	// Start to TX Data
	Length = Index;
	MsgChecksum32  = 0;
	DataChecksum32 = 0;
	
	if (Rx_Cmd_Type == UART_PORT_CMD)
	{
		// 1~8.
		for (Index = 0; Index < Length; Index ++)
		{
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = RxTx_buf[Index];
			MsgChecksum32 += RxTx_buf[Index];
			if (Index>15) DataChecksum32 += RxTx_buf[Index];		//Header are first 16 bytes.
		}
		
		// 9~10. Cmd1 and Cmd8: Found Peak Power/Frequency
		if (CmdType == USER1_SCAN_Cmd8 || CmdType == USER1_SCAN_Cmd1) 
		{
			for (Index = 0; Index < NumOfPeaks; Index ++)
			{	
				// Peak Power
				// 0. raw peak
				// Data16 = (uint16_t)((int16_t)(SignalCh_Eqs[MultiPeaks_Eqs[Index]]-32768));	//256*(128+power), power in dBm Q8 format
				// ---------------------------------------------------------------------------------------------------------------------------
				// 1. reported total power (with peak)
				   /*
				   dBw3 = (float32_t)MultiPeaks_3BW[Index];
				   chP0 = (float32_t)(SignalCh_Eqs[MultiPeaks_Eqs[Index]]);
				   chP0 = chP0/256 -128;
				   chP1 = exp10f(chP0/10);
				   chP2 = (float32_t)(0.0006*dBw3*dBw3 + 0.0495*dBw3 - 0.0105);
				   chP3 = 256* 10 * log10f(chP1 * chP2);
				   Data16 = (uint16_t)((int16_t)chP3);
				   */   
				// ---------------------------------------------------------------------------------------------------------------------------
				// 2. reported total power (with area method)
				Data16 = ReportPW[Index];
				for (idx = 0; idx < 2; idx ++)
				{
					Data8 = (uint8_t)((Data16 >> 8*(1-idx)) & 0x00FF);			//MSB first
					while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
					MTF_UART->UART_THR = Data8;
					MsgChecksum32  += Data8;
					DataChecksum32 += Data8;
				}
				
				// Peak Frequency in GHz
				Data16 = ScanStartFreq + EQS_Space*MultiPeaks_Eqs[Index] +Freq_Offset -Freq_Offset_USER1;	// frequency offset 180000
				for (idx = 0; idx < 2; idx ++)
				{
					Data8 = (uint8_t)((Data16 >> 8*(1-idx)) & 0x00FF);			//MSB first
					while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
					MTF_UART->UART_THR = Data8;
					MsgChecksum32  += Data8;
					DataChecksum32 += Data8;
				}
			}
		}
		// 9~12. Cmd9: Found Peak Power/Frequency, Peak OSNR and Power
		if (CmdType == USER1_SCAN_Cmd9) 
		{
			for (Index = 0; Index < NumOfPeaks; Index ++)
			{
				// Peak Power
				Data32 = convert_float32_to_IEEE32((float32_t)(SignalCh_Eqs[MultiPeaks_Eqs[Index]]-32768)/256);	//256*(128+power), power in dBm
				for (idx = 0; idx < 4; idx ++)
				{
					Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);			//MSB first
					while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
					MTF_UART->UART_THR = Data8;
					MsgChecksum32  += Data8;
					DataChecksum32 += Data8;
				}
				
				// Peak Frequency in THz
				Data32 = convert_float32_to_IEEE32((float32_t)(ScanStartFreq + EQS_Space*MultiPeaks_Eqs[Index] +Freq_Offset)/1000);
				for (idx = 0; idx < 4; idx ++)
				{
					Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);			//MSB first
					while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
					MTF_UART->UART_THR = Data8;
					MsgChecksum32  += Data8;
					DataChecksum32 += Data8;
				}
				
				// Peak OSNR
				Data32 = convert_float32_to_IEEE32((float32_t)PeakOSNR[Index]/256);	// dB
				for (idx = 0; idx < 4; idx ++)
				{
					Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);			//MSB first
					while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
					MTF_UART->UART_THR = Data8;
					MsgChecksum32  += Data8;
					DataChecksum32 += Data8;
				}
				
				// Peak Power
				Data32 = convert_float32_to_IEEE32((float32_t)(SignalCh_Eqs[MultiPeaks_Eqs[Index]]-32768)/256);	//256*(128+power), power in dBm
				for (idx = 0; idx < 4; idx ++)
				{
					Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);			//MSB first
					while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
					MTF_UART->UART_THR = Data8;
					MsgChecksum32  += Data8;
					DataChecksum32 += Data8;
				}
			}
		} // 9~12 for Cmd9
		
		if(CmdType != USER1_SCAN_Cmd1)	// It's for all scan command except Cmd1
		{
		// 14.Total Number of Data Spectrum Points
		for (Index = 0; Index < 4; Index ++)
		{
			Data8 = ((uint8_t)(EqsDataLength >> 8*(3-Index)) & 0x000000FF);		//MSB first
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = Data8;
			MsgChecksum32  += Data8;
			DataChecksum32 += Data8;
		}
		
		// 15. All Data Spectrum Powers
		for (Index = 0; Index < EqsDataLength; Index ++)
		{
			// EQS Power
			Data32 = convert_float32_to_IEEE32((float32_t)(SignalCh_Eqs[Index]-32768)/256);	//256*(128+power), power in dBm
			for (idx = 0; idx < 4; idx ++)
			{
				Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);			//MSB first
				while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
				MTF_UART->UART_THR = Data8;
				MsgChecksum32  += Data8;
				DataChecksum32 += Data8;
			}
		}
		
		// 16. All Data Spectrum Frequency
		for (Index = 0; Index < EqsDataLength; Index ++)
		{
			// EQS Frequency in THz
			if (CmdType == USER1_SCAN_CmdF) 
				//Data32 = convert_float32_to_IEEE32((float32_t)(ScanStartFreq + decimateN*Index +Freq_Offset)/1000);
				Data32 = convert_float32_to_IEEE32((float32_t)(ScanStartFreq + decimateNplusONE*Index +Freq_Offset)/1000);
			else
				Data32 = convert_float32_to_IEEE32((float32_t)(ScanStartFreq + EQS_Space*Index +Freq_Offset)/1000);
				
			for (idx = 0; idx < 4; idx ++)
			{
				Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);				//MSB first
				while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
				MTF_UART->UART_THR = Data8;
				MsgChecksum32  += Data8;
				DataChecksum32 += Data8;
			}
		}
		}	//14~16 for Cmd_8,Cmd_9 and Cmd_F
		
		// 17.Total Spectrum Power
		if (CmdType == USER1_SCAN_Cmd9)
		{
			Data32 = convert_float32_to_IEEE32((float32_t)(User1_totalP-32768)/256);	//256*(128+power), power in dBm
			for (idx = 0; idx < 4; idx ++)
			{
				Data8 = (uint8_t)((Data32 >> 8*(3-idx)) & 0x000000FF);			//MSB first
				while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
				MTF_UART->UART_THR = Data8;
				MsgChecksum32  += Data8;
				DataChecksum32 += Data8;
			}
		}
			
		// FOOTER -----------------------------------------------
		// 18. Data Checksum
		Data32 = ~DataChecksum32;
		for (Index = 0; Index < 4; Index ++)
		{
			Data8 = (uint8_t)((Data32 >> 8*(3-Index)) & 0x000000FF);					//MSB first
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = Data8;
			MsgChecksum32  += Data8;
		}
		// 19. Error Code
		Data32 = ErrorCode;
		for (Index = 0; Index < 4; Index ++)
		{
			Data8 = (uint8_t)((Data32 >> 8*(3-Index)) & 0x000000FF);					//MSB first
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = Data8;
			MsgChecksum32  += Data8;
		}
		// 20. Message Checksum
		Data32 = ~MsgChecksum32;
		for (Index = 0; Index < 4; Index ++)
		{
			Data8 = (uint8_t)((Data32 >> 8*(3-Index)) & 0x000000FF);					//MSB first
			while (!(MTF_UART->UART_SR & UART_SR_TXRDY));
			MTF_UART->UART_THR = Data8;
		}
	}
	else
	{
		for (Index = 0; Index < Length; Index ++)
		{
			udi_cdc_putc(RxTx_buf[Index]);
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////