/////////////////////////////////////////////////////////////////////////
//	Uart_Cmd.h
#ifndef __UART_CMD_H__
#define __UART_CMD_H__
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
char Is_Rx_done_10(uint8_t RxLength, uint8_t* Rx_data);
char Is_Rx_done_11(uint8_t RxLength, uint8_t* Rx_data);

void OnMTF_Rx_head(void);
void OnMTF_Check_RxCmd(void);
void OnMTF_UART_Cmd(void);

void OnMTF_UART_Rx(void);
void OnMTF_UART_Tx(uint16_t Length);

void Reply_to_UART(uint16_t CmdID, uint8_t ErrorCode);

void OnMTF_UART_TxData_1(uint16_t *pData, uint16_t Length, uint8_t CmdType);
void OnMTF_UART_TxData_2(uint16_t *pData, uint16_t Length, uint8_t CmdType);
void OnMTF_UART_TxData_10(uint16_t *pData, uint16_t DataLength, uint8_t CmdType);

void OnMTF_UART_Decode(void);
void OnMTF_DownloadETable(void);
void OnMTF_DownloadPTable(void);
void OnMTF_Download_CalTable(void);
void OnMTF_Download_FERC_Table(void);
/*
void OnSend_RingData(uint8_t* TxData, uint16_t DataLength);
void OnSend_ScanData(uint8_t* TxData, uint16_t DataLength);
void OnSend_RcTData(uint8_t* TxData, uint16_t DataLength);
//-----------------------------------------------------------------------
void OnDAC_Initial(void);
void DAC_Output(int32_t DAC_Value, uint8_t Dir);
void DAC_Output0(int32_t DAC_Value, uint8_t Dir);
void DAC_Output1(int32_t DAC_Value, uint8_t Dir);
void DAC_Output2(int32_t DAC_Value, int32_t DAC_Value0, uint8_t Dir);
void OnSetBetaCycle(int32_t DAC_Value);
void OnTOFScanRange(void);
void OnTOFScanRange_eqWL(void);

void OnGetRingData0(void);
void OnGetRingData1(uint8_t AveNum);
void OnGetRingData2(uint8_t AveNum, int16_t DeltaV);
void OnGetRingData3(int16_t DeltaV);
void OnGetRingData4(float DeltaT);
void OnGetRingData5(int16_t DeltaV);

void OnRcDataRipple(void);
void OnRcVerification(uint8_t AveNum);
void OnRcSwitCh(uint32_t WL1, uint32_t WL2, uint8_t Ch1, uint8_t Ch2, float Ch1_V);
//---------------------------------------------------------------------------------------
void OnSet_Voltage9(int32_t DAC_Value1, int32_t DAC_Value2, float CycleT, uint8_t Dir);
void OnSet_Voltage8(int32_t DAC_Value1, int32_t DAC_Value2, uint8_t Dir);
void OnSet_Voltage7(int32_t DAC_Value, uint8_t Dir);
void OnSet_Voltage6(int32_t DAC_Value, uint8_t Dir);
void OnSet_Voltage5(int32_t DAC_Value, uint8_t Dir);
void OnSet_Voltage4(int32_t DAC_Value, uint8_t Dir);
void OnSet_Voltage3(int32_t DAC_Value, uint8_t Dir);
void OnSet_Voltage2(int32_t DAC_Value, uint8_t Dir);
void OnSet_Voltage(int32_t DAC_Value, uint8_t Dir);
void OnGetDAC_GainFactor(int32_t SetVoltage);
//--------------------------------------------------------------
void OnInital_OnChip_ADC(uint8_t ADC_ChId);
void OnRead_OnChipADC(uint8_t ADC_ChId);
void OnSetADC_Channel(uint8_t ADC_ChId);

void OnRead_ADC_Ring0(uint8_t AdcMode);
void OnRead_ADC_Ring1(uint8_t AdcMode);

void OnInitializeTMP125(void);
void OnRead_TMP125(void);

void OnInitializeADC_LTC1864(void);
void OnReadADC_LTC1865(uint8_t Ch_ID);
void OnADC_LTC1865(volatile avr32_spi_t *pSPI, uint8_t *Tx_Data, uint8_t *Rx_Data, uint8_t Length);

void OnCheckMemsSwitchTime(char MethodID);
*/
/////////////////////////////////////////////////////////////////
#endif